# WWSU version 9 ALPHA
The WWSU Radio Sails.js API application enables external / remote control of core WWSU functionality. Applications can be developed utilizing this API. 

**This repository was designed specifically for WWSU Radio and is not intended to function out of the box for others**

## Primary developer no longer regularly maintains this repos beyond 9.0.x
The primary developer, Patrick Schmalstig, is departing WWSU Radio at the end of April 2022 as they graduate from Wright State University. Maintenance of this codebase will be up to the responsibility of future engineers; Patrick will only be providing maintenance / development on a contractual basis from WWSU Radio starting May 2022.
