/**
 * calendar.js
 *
 * @description :: Calendar events.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const CalendarDb = require("../../assets/plugins/wwsu-calendar/js/wwsu-calendar");

module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    type: {
      type: "string",
      isIn: [
        "show",
        "sports",
        "remote",
        "prerecord",
        "genre",
        "playlist",
        "event",
        "onair-booking",
        "prod-booking",
        "office-hours",
      ],
      defaultsTo: "event",
    },

    active: {
      type: "boolean",
      defaultsTo: true,
    },

    priority: {
      type: "number",
      allowNull: true,
    },

    hostDJ: {
      type: "number",
      allowNull: true,
    },

    cohostDJ1: {
      type: "number",
      allowNull: true,
    },

    cohostDJ2: {
      type: "number",
      allowNull: true,
    },

    cohostDJ3: {
      type: "number",
      allowNull: true,
    },

    eventID: {
      type: "number",
      allowNull: true,
    },

    playlistID: {
      type: "number",
      allowNull: true,
    },

    director: {
      type: "number",
      allowNull: true,
    },

    hosts: {
      type: "string",
      allowNull: true,
    },

    name: {
      type: "string",
      allowNull: true,
    },

    description: {
      type: "string",
      allowNull: true,
      columnType: "text",
    },

    logo: {
      type: "number",
      allowNull: true,
      description: "ID reference to uploads model",
    },

    banner: {
      type: "number",
      allowNull: true,
      description: "ID reference to uploads model",
    },

    lastAired: {
      type: "ref",
      columnType: "datetime",
    },

    scoreTrack: {
      type: "number",
      defaultsTo: 0,
      description:
        "Score tracker to determine when an event should be the featured event of the week. A -1 means it is currently featured.",
    },

    discordChannel: {
      type: "string",
      allowNull: true,
    },

    discordCalendarMessage: {
      type: "string",
      allowNull: true,
    },

    discordScheduleMessage: {
      type: "string",
      allowNull: true,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["lastAired", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    // When deactivating a calendar event, scoreTrack should reset to 0.
    if (typeof criteria.active !== "undefined" && !criteria.active) {
      criteria.scoreTrack = 0;
    }

    ["lastAired", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  calendardb: new CalendarDb(),

  initialize: async function () {
    // Load up calendar events into the calendar system
    sails.models.calendar.calendardb.query(
      "calendar",
      await sails.models.calendar.find(),
      true
    );
  },

  beforeDestroy: function (criteria, proceed) {
    if (criteria.ID === 1)
      // Do not allow destroying the master member.
      throw new Error("Not allowed to destroy calendar record 1.");
    return proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.models.calendar.calendardb.query("calendar", data);
    sails.log.silly(`calendar socket: ${data}`);
    sails.sockets.broadcast("calendar", "calendar", data);

    var temp;
    temp = (async () => {
      var event = sails.models.calendar.calendardb.processRecord(
        newlyCreatedRecord,
        {},
        moment().toISOString(true)
      );
      await sails.helpers.discord.calendar.postEvent(event);
    })();
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var temp;

    var data = { update: updatedRecord };

    sails.models.calendar.calendardb.query("calendar", data);
    sails.log.silly(`calendar socket: ${data}`);
    sails.sockets.broadcast("calendar", "calendar", data);

    // If setting active to false, treat as deletion in web sockets and delete all schedules and notify subscribers of a discontinued show
    temp = (async () => {
      var event = sails.models.calendar.calendardb.processRecord(
        updatedRecord,
        {},
        moment().toISOString(true)
      );
      if (!updatedRecord.active) {
        let scheduleRecords = await sails.models.schedule
          .destroy({ calendarID: updatedRecord.ID })
          .fetch();

        // Only post discontinued notifications if we deleted a schedule
        if (scheduleRecords && scheduleRecords.length > 0) {
          await sails.helpers.onesignal.sendEvent(event, false, false);
          await sails.helpers.discord.sendSchedule(
            event,
            updatedRecord,
            false,
            false
          );
        }
      } else {
        await sails.helpers.discord.calendar.postEvent(event);
        await sails.helpers.onesignal.sendEvent(event, false, false);
        await sails.helpers.discord.sendSchedule(
          event,
          updatedRecord,
          false,
          false
        );
      }
    })();

    // Deactivate any active main calendar events that now have no host DJ
    temp2 = (async () => {
      if (
        updatedRecord.active &&
        !updatedRecord.hostDJ &&
        ["show", "remote", "prerecord"].indexOf(updatedRecord.type) !== -1 &&
        updatedRecord.ID !== 1
      ) {
        await sails.models.calendar.updateOne(
          {
            ID: updatedRecord.ID,
          },
          { active: false }
        );
      }
    })();

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.models.calendar.calendardb.query("calendar", data);
    sails.log.silly(`calendar socket: ${data}`);
    sails.sockets.broadcast("calendar", "calendar", data);
    var temp;

    temp = (async () => {
      // Remove logo and banner file
      if (destroyedRecord.logo)
        await sails.models.uploads.destroyOne({ ID: destroyedRecord.logo });
      if (destroyedRecord.banner)
        await sails.models.uploads.destroyOne({ ID: destroyedRecord.banner });

      // Remove all calendar schedules
      let scheduleRecords = await sails.models.schedule
        .destroy({ calendarID: destroyedRecord.ID })
        .fetch();
      var event = sails.models.calendar.calendardb.processRecord(
        destroyedRecord,
        {},
        moment().toISOString(true)
      );

      // Only post discontinued notifications if we deleted a schedule or the event was active
      if (
        destroyedRecord.active ||
        (scheduleRecords && scheduleRecords.length > 0)
      ) {
        await sails.helpers.onesignal.sendEvent(event, false, false);
        await sails.helpers.discord.sendSchedule(
          event,
          destroyedRecord,
          false,
          false
        );
      }
    })();

    return proceed();
  },
};
