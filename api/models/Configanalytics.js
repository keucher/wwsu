/**
 * Configanalytics.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  attributes: {
    ID: {
      type: "number",
      unique: true,
      required: true,
    },

    /*
      START OF SEMESTER
    */

    startOfSemester: {
      description:
        "moment.js compatible date/time indicating the start of the current semester / schedule rotation.",
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      defaultsTo: "2002-01-01T00:00:00Z",
    },

    /*
      REPUTATION
    */

    warningPointsPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 20,
      description:
        "Number of reputation points to subtract for each warning point earned.",
    },

    cancellationPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract for each broadcast cancellation.",
    },

    silenceAlarmPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract each time a broadcast triggers the silence alarm.",
    },

    absencePenalty: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to subtract for each broadcast absence / no-show.",
    },

    unauthorizedBroadcastPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to subtract for each broadcast airing that was not scheduled.",
    },

    missedIDPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to subtract for each time a broadcast fails to take the top of hour ID break.",
    },

    signOnEarlyPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 3,
      description:
        "Number of reputation points to subtract for each time a broadcast goes on the air 5 or more minutes before scheduled start time.",
    },

    signOnLatePenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract for each time a broadcast goes on the air 5 or more minutes after scheduled start time.",
    },

    signOffEarlyPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract for each time a broadcast signs off 5 or more minutes before scheduled end time.",
    },

    signOffLatePenalty: {
      type: "number",
      min: 0,
      defaultsTo: 3,
      description:
        "Number of reputation points to subtract for each time a broadcast signs off 5 or more minutes after scheduled end time.",
    },

    breakReputation: {
      type: "number",
      min: 0,
      defaultsTo: 2,
      description:
        "Number of reputation points to add if the host took the configured ideal number of breaks per hour.",
    },

    showReputation: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to add for each live broadcast.",
    },

    prerecordReputation: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description: "Number of reputation points to add for each prerecord.",
    },

    remoteReputation: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to add for each remote broadcast.",
    },

    sportsReputation: {
      type: "number",
      min: 0,
      defaultsTo: 8,
      description:
        "Number of reputation points to add for each sports broadcast.",
    },

    genreReputation: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to add for each genre broadcast.",
    },

    playlistReputation: {
      type: "number",
      min: 0,
      defaultsTo: 2,
      description:
        "Number of reputation points to add for each playlist broadcast.",
    },

    /*
      BROADCAST PERFORMANCE SCORES
    */

    minShowTime: {
      type: "number",
      min: 0,
      defaultsTo: 15,
      description:
        "Minimum number of minutes a broadcast must have been on the air to receive a score; broadcasts shorter than this will receive a score of 0.",
    },

    listenerRatioPoints: {
      type: "number",
      min: 0,
      defaultsTo: 60,
      description:
        "Number of points to award to a broadcast for every 1 online listener to showTime ratio (eg. every 1 average online listener).",
    },

    messagePoints: {
      type: "number",
      min: 0,
      defaultsTo: 6,
      description:
        "Award this many points for every 1 message exchanged per hour on average during the broadcast.",
    },

    idealBreaks: {
      type: "number",
      min: 1, // Top of hour break is required by the FCC; do not allow values below 1.
      defaultsTo: 3,
      description:
        "How many breaks per hour is the ideal number for a broadcast?",
    },

    breakPoints: {
      type: "number",
      min: 0,
      defaultsTo: 60,
      description:
        "Award this many points if a broadcast took the ideal number of breaks (or more) per hour on average; less points will be awarded for less breaks depending on the number of breaks taken. Broadcasts will never be awarded more than this many points for breaks.",
    },

    viewerRatioPoints: {
      type: "number",
      min: 0,
      defaultsTo: 120,
      description:
        "Award this many points for every 1 video viewer to showTime ratio (eg. every 1 average video stream viewer).",
    },

    videoStreamPoints: {
      type: "number",
      min: 0,
      defaultsTo: 30,
      description:
        "Award this many points if a broadcast streamed video their entire broadcast; less points will be awarded for less video stream time.",
    },
  },

  initialize: async function () {
    sails.config.custom.analytics =
      await sails.models.configanalytics.findOrCreate({ ID: 1 }, { ID: 1 });
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-analytics socket: ${data}`);
    sails.sockets.broadcast("config-analytics", "config-analytics", data);

    (async () => {
      // Update config
      sails.config.custom.analytics =
        await sails.models.configanalytics.findOrCreate({ ID: 1 }, { ID: 1 });

      proceed();

      // Update weekly analytics
      await sails.helpers.analytics.calculateStats();
    })();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-analytics socket: ${data}`);
    sails.sockets.broadcast("config-analytics", "config-analytics", data);

    (async () => {
      // Update config
      sails.config.custom.analytics =
        await sails.models.configanalytics.findOrCreate({ ID: 1 }, { ID: 1 });

      proceed();

      // Update weekly analytics
      await sails.helpers.analytics.calculateStats();
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-analytics socket: ${data}`);
    sails.sockets.broadcast("config-analytics", "config-analytics", data);

    (async () => {
      // Update config
      sails.config.custom.analytics =
        await sails.models.configanalytics.findOrCreate({ ID: 1 }, { ID: 1 });

      proceed();

      // Update weekly analytics
      await sails.helpers.analytics.calculateStats();
    })();
  },
};
