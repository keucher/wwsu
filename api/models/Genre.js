/**
 * Genre.js
 *
 * @description :: Container of genres in RadioDJ.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "radiodj",
  // migrate: "safe", // NOT SUPPORTED by Sails :(
  attributes: {
    ID: {
      columnName: "id", // No idea why RadioDJ did this
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      maxLength: 50,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },
};
