/**
 * Subscribers.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",

  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    host: {
      type: "string",
      required: true,
    },

    device: {
      // API note: You should never change this value after you create the record; delete and create a new one.
      type: "string",
      required: true,
    },

    type: {
      type: "string",
      required: true,
    },

    subtype: {
      type: "string",
      required: true,
    },

    description: {
      type: "string",
      required: true,
      columnType: "text",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`subscribers socket: ${data}`);
    sails.sockets.broadcast("subscribers", "subscribers", data);
    sails.sockets.broadcast(
      `subscribers-${newlyCreatedRecord.device}`,
      "subscribers",
      data
    );
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`subscribers socket: ${data}`);
    sails.sockets.broadcast("subscribers", "subscribers", data);
    sails.sockets.broadcast(
      `subscribers-${updatedRecord.device}`,
      "subscribers",
      data
    );
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`subscribers socket: ${data}`);
    sails.sockets.broadcast("subscribers", "subscribers", data);
    sails.sockets.broadcast(
      `subscribers-${destroyedRecord.device}`,
      "subscribers",
      data
    );
    return proceed();
  },
};
