/**
 * Events.js
 *
 * @description :: Container of RadioDJ events.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "radiodj",
  // migrate: "safe", // NOT SUPPORTED by Sails :(
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },
    type: {
      type: "number",
    },
    time: {
      type: "string",
    },
    name: {
      type: "string",
    },
    date: {
      type: "ref",
      columnType: "date",
    },
    day: {
      type: "string",
    },
    hours: {
      type: "string",
    },
    data: {
      type: "string",
    },
    enabled: {
      type: "string",
      isIn: ["True", "False"],
    },
    catID: {
      type: "number",
    },
    smart: {
      type: "number",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["date", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["date", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },
};
