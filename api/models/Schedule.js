/**
 * Schedule.js
 *
 * @description :: Schedule for calendar events.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    calendarID: {
      type: "number",
      required: true,
    },

    scheduleID: {
      type: "number",
      allowNull: true,
    },

    overriddenID: {
      type: "number",
      allowNull: true,
    },

    scheduleType: {
      type: "string",
      isIn: [
        "unscheduled",
        "updated",
        "canceled",
        "updated-system",
        "canceled-system",
      ],
      allowNull: true,
    },

    scheduleReason: {
      type: "string",
      allowNull: true,
    },

    originalTime: {
      type: "ref",
      columnType: "datetime",
    },

    type: {
      type: "string",
      isIn: [
        "show",
        "sports",
        "remote",
        "prerecord",
        "genre",
        "playlist",
        "event",
        "onair-booking",
        "prod-booking",
        "office-hours",
        "task",
      ],
      allowNull: true,
    },

    priority: {
      type: "number",
      allowNull: true,
    },

    hostDJ: {
      type: "number",
      allowNull: true,
    },

    cohostDJ1: {
      type: "number",
      allowNull: true,
    },

    cohostDJ2: {
      type: "number",
      allowNull: true,
    },

    cohostDJ3: {
      type: "number",
      allowNull: true,
    },

    eventID: {
      type: "number",
      allowNull: true,
    },

    playlistID: {
      type: "number",
      allowNull: true,
    },

    director: {
      type: "number",
      allowNull: true,
    },

    hosts: {
      type: "string",
      allowNull: true,
    },

    name: {
      type: "string",
      allowNull: true,
    },

    description: {
      type: "string",
      allowNull: true,
      columnType: "text",
    },

    logo: {
      type: "number",
      allowNull: true,
    },

    banner: {
      type: "number",
      allowNull: true,
    },

    newTime: {
      type: "ref",
      columnType: "datetime",
    },

    oneTime: {
      type: "json",
    },

    startDate: {
      type: "ref",
      columnType: "date",
    },

    endDate: {
      type: "ref",
      columnType: "date",
    },

    startTime: {
      type: "string",
      allowNull: true,
    },

    recurrenceRules: {
      type: "json",
    },

    recurrenceInterval: {
      type: "json",
    },

    duration: {
      type: "number",
      min: 0,
      max: 60 * 24,
      allowNull: true,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["originalTime", "newTime", "startDate", "endDate", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["originalTime", "newTime", "startDate", "endDate", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  initialize: async function () {
    sails.models.calendar.calendardb.query(
      "schedule",
      await sails.models.schedule.find(),
      true
    );
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.models.calendar.calendardb.query("schedule", data);
    sails.log.silly(`schedule socket: ${data}`);
    sails.sockets.broadcast("schedule", "schedule", data);
    var temp;

    temp = (async (event) => {
      var _event = sails.models.calendar.calendardb.scheduleToEvent(event);

      // Process notifications and Discord
      await sails.helpers.discord.calendar.postSchedule(_event);

      if (_event.active) {
        if (
          !event.scheduleType &&
          ["show", "remote", "prerecord"].indexOf(_event.type) !== -1
        ) {
          await sails.helpers.onesignal.sendEvent(_event, false, true);
          await sails.helpers.emails.queueEvent(_event, false, true);
        }

        if (
          event.scheduleType === "updated" ||
          event.scheduleType === "updated-system"
        ) {
          if (
            (event.newTime !== null || event.duration !== null) &&
            moment().isBefore(_event.end)
          ) {
            await sails.helpers.onesignal.sendEvent(_event, false, false);
            await sails.helpers.emails.queueEvent(_event, false, false);
            await sails.helpers.discord.sendSchedule(
              _event,
              newlyCreatedRecord,
              false,
              false
            );
          }
          await sails.models.attendance
            .update(
              { unique: _event.unique },
              {
                calendarID: _event.calendarID,
                unique: _event.unique,
                dj: _event.hostDJ,
                cohostDJ1: _event.cohostDJ1,
                cohostDJ2: _event.cohostDJ2,
                cohostDJ3: _event.cohostDJ3,
                event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                scheduledStart: moment(_event.start).format(
                  "YYYY-MM-DD HH:mm:ss"
                ),
                scheduledEnd: moment(_event.end).format("YYYY-MM-DD HH:mm:ss"),
              }
            )
            .fetch();
        }

        if (
          event.scheduleType === "canceled" ||
          event.scheduleType === "canceled-system"
        ) {
          if (moment().isBefore(_event.end)) {
            await sails.helpers.onesignal.sendEvent(_event, false, false);
            await sails.helpers.emails.queueEvent(_event, false, false);
            await sails.helpers.discord.sendSchedule(
              _event,
              newlyCreatedRecord,
              false,
              false
            );
          }

          // Create / update logs
          if (
            [
              "show",
              "remote",
              "sports",
              "prerecord",
              "genre",
              "playlist",
            ].indexOf(_event.type) !== -1
          ) {
            sails.models.attendance
              .findOrCreate(
                { unique: _event.unique },
                {
                  calendarID: _event.calendarID,
                  unique: _event.unique,
                  dj: _event.hostDJ,
                  cohostDJ1: _event.cohostDJ1,
                  cohostDJ2: _event.cohostDJ2,
                  cohostDJ3: _event.cohostDJ3,
                  happened: -1,
                  cancellation: event.scheduleType !== "canceled-system",
                  event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                  scheduledStart: moment(_event.start).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  scheduledEnd: moment(_event.end).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  actualStart: null,
                  actualEnd: null,
                }
              )
              .exec(async (err, created, wasCreated) => {
                if (err) {
                  throw err;
                }
                if (!created || created.happened === 1) return;

                if (!wasCreated)
                  await sails.models.attendance.updateOne(
                    { ID: created.ID },
                    {
                      happened: -1,
                      cancellation: event.scheduleType !== "canceled-system",

                      calendarID: _event.calendarID,
                      unique: _event.unique,
                      dj: _event.hostDJ,
                      cohostDJ1: _event.cohostDJ1,
                      cohostDJ2: _event.cohostDJ2,
                      cohostDJ3: _event.cohostDJ3,
                      event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                      scheduledStart: moment(_event.start).format(
                        "YYYY-MM-DD HH:mm:ss"
                      ),
                      scheduledEnd: moment(_event.end).format(
                        "YYYY-MM-DD HH:mm:ss"
                      ),
                    }
                  );

                await sails.models.logs
                  .create({
                    attendanceID: created.ID,
                    logtype: "cancellation",
                    loglevel: "warning",
                    logsubtype: `${_event.hosts} - ${_event.name}`,
                    logIcon:
                      sails.models.calendar.calendardb.getIconClass(_event),
                    title: `The event was marked canceled.`,
                    event: `${_event.type}: ${_event.hosts} - ${
                      _event.name
                    } was canceled for ${moment(_event.start).format(
                      "LLLL"
                    )}.<br />Reason: ${event.scheduleReason}.${
                      event.scheduleType === "canceled-system"
                        ? `<br />This event was canceled by the system due to a higher priority event. This cancellation was marked excused by default.`
                        : ``
                    }`,
                    createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                    excused: event.scheduleType === "canceled-system",
                  })
                  .fetch()
                  .tolerate((err) => {
                    sails.log.error(err);
                  });
              });
          } else if (_event.type === "office-hours") {
            sails.models.timesheet
              .findOrCreate(
                { unique: _event.unique },
                {
                  calendarID: _event.calendarID,
                  unique: _event.unique,
                  name: _event.hosts,
                  approved: -1,
                  scheduledIn: moment(_event.start).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  scheduledOut: moment(_event.end).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                }
              )
              .exec(async (err, created, wasCreated) => {
                if (err) {
                  throw err;
                }

                if (!created || created.approved === 1) {
                  return false;
                }

                if (!wasCreated)
                  await sails.models.timesheet.updateOne(
                    { ID: created.ID },
                    { approved: -1 }
                  );

                await sails.models.logs
                  .create({
                    attendanceID: null,
                    logtype: "director-cancellation",
                    loglevel: "info",
                    logsubtype: _event.hosts,
                    logIcon: `fas fa-user-times`,
                    title: `A director cancelled their office hours!`,
                    event: `Director: ${
                      _event.hosts
                    }<br />Scheduled time: ${moment(_event.start).format(
                      "llll"
                    )} - ${moment(_event.end).format("llll")}`,
                    createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                  })
                  .fetch()
                  .tolerate((err) => {
                    sails.log.error(err);
                  });
              });
          }
        }

        if (_event.scheduleType === null) {
          await sails.helpers.discord.sendSchedule(
            _event,
            newlyCreatedRecord,
            false,
            true
          );
        }
      }
    })(newlyCreatedRecord);

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.models.calendar.calendardb.query("schedule", data);
    sails.log.silly(`schedule socket: ${data}`);
    sails.sockets.broadcast("schedule", "schedule", data);
    var temp;

    temp = (async (event) => {
      var _event = sails.models.calendar.calendardb.scheduleToEvent(event);

      // Process notifications and discord
      await sails.helpers.discord.calendar.postSchedule(_event);

      if (_event.active) {
        if (
          !event.scheduleType &&
          ["show", "remote", "prerecord"].indexOf(_event.type) !== -1
        ) {
          await sails.helpers.onesignal.sendEvent(_event, false, true);
          await sails.helpers.emails.queueEvent(_event, false, true);
        }

        if (
          event.scheduleType === "updated" ||
          event.scheduleType === "updated-system"
        ) {
          if (
            (event.newTime !== null || event.duration !== null) &&
            moment().isBefore(_event.end)
          ) {
            await sails.helpers.onesignal.sendEvent(_event, false, false);
            await sails.helpers.emails.queueEvent(_event, false, false);
            await sails.helpers.discord.sendSchedule(
              _event,
              updatedRecord,
              false,
              false
            );
          }
          await sails.models.attendance
            .update(
              { unique: _event.unique },
              {
                calendarID: _event.calendarID,
                unique: _event.unique,
                dj: _event.hostDJ,
                cohostDJ1: _event.cohostDJ1,
                cohostDJ2: _event.cohostDJ2,
                cohostDJ3: _event.cohostDJ3,
                event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                scheduledStart: moment(_event.start).format(
                  "YYYY-MM-DD HH:mm:ss"
                ),
                scheduledEnd: moment(_event.end).format("YYYY-MM-DD HH:mm:ss"),
              }
            )
            .fetch();
        }

        if (
          event.scheduleType === "canceled" ||
          event.scheduleType === "canceled-system"
        ) {
          if (moment().isBefore(_event.end)) {
            await sails.helpers.onesignal.sendEvent(_event, false, false);
            await sails.helpers.emails.queueEvent(_event, false, false);
            await sails.helpers.discord.sendSchedule(
              _event,
              updatedRecord,
              false,
              false
            );
          }

          // Create / update logs
          if (
            [
              "show",
              "remote",
              "sports",
              "prerecord",
              "genre",
              "playlist",
            ].indexOf(_event.type) !== -1
          ) {
            sails.models.attendance
              .findOrCreate(
                { unique: _event.unique },
                {
                  calendarID: _event.calendarID,
                  unique: _event.unique,
                  dj: _event.hostDJ,
                  cohostDJ1: _event.cohostDJ1,
                  cohostDJ2: _event.cohostDJ2,
                  cohostDJ3: _event.cohostDJ3,
                  happened: -1,
                  cancellation: event.scheduleType !== "canceled-system",
                  event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                  scheduledStart: moment(_event.start).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  scheduledEnd: moment(_event.end).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  actualStart: null,
                  actualEnd: null,
                }
              )
              .exec(async (err, created, wasCreated) => {
                if (err) {
                  throw err;
                }

                if (!created || created.happened === 1) return;

                if (!wasCreated)
                  await sails.models.attendance.updateOne(
                    { ID: created.ID },
                    {
                      happened: -1,
                      cancellation: event.scheduleType !== "canceled-system",

                      calendarID: _event.calendarID,
                      unique: _event.unique,
                      dj: _event.hostDJ,
                      cohostDJ1: _event.cohostDJ1,
                      cohostDJ2: _event.cohostDJ2,
                      cohostDJ3: _event.cohostDJ3,
                      event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                      scheduledStart: moment(_event.start).format(
                        "YYYY-MM-DD HH:mm:ss"
                      ),
                      scheduledEnd: moment(_event.end).format(
                        "YYYY-MM-DD HH:mm:ss"
                      ),
                    }
                  );

                await sails.models.logs
                  .create({
                    attendanceID: created.ID,
                    logtype: "cancellation",
                    loglevel: "warning",
                    logsubtype: `${_event.hosts} - ${_event.name}`,
                    logIcon:
                      sails.models.calendar.calendardb.getIconClass(_event),
                    title: `The event was marked canceled.`,
                    event: `${_event.type}: ${_event.hosts} - ${
                      _event.name
                    } was canceled for ${moment(_event.start).format(
                      "LLLL"
                    )}.<br />Reason: ${event.scheduleReason}.${
                      event.scheduleType === "canceled-system"
                        ? `<br />This event was canceled by the system due to a higher priority event. This cancellation was marked excused by default.`
                        : ``
                    }`,
                    createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                    excused: event.scheduleType === "canceled-system",
                  })
                  .fetch()
                  .tolerate((err) => {
                    sails.log.error(err);
                  });
              });
          } else if (_event.type === "office-hours") {
            sails.models.timesheet
              .findOrCreate(
                { unique: _event.unique },
                {
                  calendarID: _event.calendarID,
                  unique: _event.unique,
                  name: _event.hosts,
                  approved: -1,
                  scheduledIn: moment(_event.start).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  scheduledOut: moment(_event.end).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                }
              )
              .exec(async (err, created, wasCreated) => {
                if (err) {
                  throw err;
                }

                if (!created || created.approved === 1) {
                  return false;
                }

                if (!wasCreated)
                  await sails.models.timesheet.updateOne(
                    { ID: created.ID },
                    { approved: -1 }
                  );

                await sails.models.logs
                  .create({
                    attendanceID: null,
                    logtype: "director-cancellation",
                    loglevel: "info",
                    logsubtype: _event.hosts,
                    logIcon: `fas fa-user-times`,
                    title: `A director cancelled their office hours!`,
                    event: `Director: ${
                      _event.hosts
                    }<br />Scheduled time: ${moment(_event.start).format(
                      "llll"
                    )} - ${moment(_event.end).format("llll")}`,
                    createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                  })
                  .fetch()
                  .tolerate((err) => {
                    sails.log.error(err);
                  });
              });
          }
        }
      }
    })(updatedRecord);

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.models.calendar.calendardb.query("schedule", data);
    sails.log.silly(`schedule socket: ${data}`);
    sails.sockets.broadcast("schedule", "schedule", data);
    var temp;

    temp = (async (event) => {
      var _event = sails.models.calendar.calendardb.scheduleToEvent(event);

      // Process notifications and Discord
      await sails.helpers.discord.calendar.postSchedule(_event);

      if (_event.active) {
        if (
          !event.scheduleType &&
          ["show", "remote", "prerecord"].indexOf(_event.type) !== -1
        ) {
          await sails.helpers.onesignal.sendEvent(_event, false, true);
          await sails.helpers.emails.queueEvent(_event, false, true);
        }

        if (
          event.scheduleType === "updated" ||
          event.scheduleType === "updated-system"
        ) {
          if (
            (event.newTime !== null || event.duration !== null) &&
            moment().isBefore(_event.end)
          ) {
            await sails.helpers.onesignal.sendEvent(_event, false, false, true);
            await sails.helpers.emails.queueEvent(_event, false, false, true);
            await sails.helpers.discord.sendSchedule(
              _event,
              destroyedRecord,
              false,
              false,
              true
            );
          }
          await sails.models.attendance
            .update(
              { unique: _event.unique },
              {
                calendarID: _event.calendarID,
                unique: _event.unique,
                dj: _event.hostDJ,
                cohostDJ1: _event.cohostDJ1,
                cohostDJ2: _event.cohostDJ2,
                cohostDJ3: _event.cohostDJ3,
                event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                scheduledStart: moment(_event.start).format(
                  "YYYY-MM-DD HH:mm:ss"
                ),
                scheduledEnd: moment(_event.end).format("YYYY-MM-DD HH:mm:ss"),
              }
            )
            .fetch();
        }

        if (
          event.scheduleType === "canceled" ||
          event.scheduleType === "canceled-system"
        ) {
          if (moment().isBefore(_event.end)) {
            await sails.helpers.onesignal.sendEvent(_event, false, false, true);
            await sails.helpers.emails.queueEvent(_event, false, false, true);
            await sails.helpers.discord.sendSchedule(
              _event,
              destroyedRecord,
              false,
              false,
              true
            );

            // Destroy records (but only if the event did not already happen)
            if (
              [
                "show",
                "remote",
                "sports",
                "prerecord",
                "genre",
                "playlist",
              ].indexOf(_event.type) !== -1
            ) {
              var destroyed = await sails.models.attendance
                .destroy({ unique: _event.unique, happened: -1 })
                .fetch();
              if (destroyed && destroyed.length > 0) {
                var IDs = destroyed.map((record) => record.ID);
                await sails.models.logs.destroy({ attendanceID: IDs }).fetch();
              }

              await sails.models.attendance
                .update(
                  { unique: _event.unique },
                  {
                    calendarID: _event.calendarID,
                    unique: _event.unique,
                    dj: _event.hostDJ,
                    cohostDJ1: _event.cohostDJ1,
                    cohostDJ2: _event.cohostDJ2,
                    cohostDJ3: _event.cohostDJ3,
                    event: `${_event.type}: ${_event.hosts} - ${_event.name}`,
                    scheduledStart: moment(_event.start).format(
                      "YYYY-MM-DD HH:mm:ss"
                    ),
                    scheduledEnd: moment(_event.end).format(
                      "YYYY-MM-DD HH:mm:ss"
                    ),
                  }
                )
                .fetch();
            } else if (_event.type === "office-hours") {
              await sails.models.timesheet
                .destroy({ unique: _event.unique, approved: -1 })
                .fetch();
              await sails.models.logs
                .destroy({
                  logtype: `director-cancellation`,
                  event: `Director: ${
                    _event.hosts
                  }<br />Scheduled time: ${moment(_event.start).format(
                    "llll"
                  )} - ${moment(_event.end).format("llll")}`,
                })
                .fetch();
            }
          }
        }

        if (!event.scheduleType) {
          await sails.helpers.discord.sendSchedule(
            _event,
            destroyedRecord,
            false,
            false,
            false,
            true
          );
        }
      }

      // Remove any schedules that were created as an override for the deleted schedule.
      // DISABLED: interferes with the event conflict detection
      //await sails.models.schedule.destroy({ overriddenID: event.ID }).fetch();

      // Remove any schedules that were created to override this schedule
      // DISABLED: interferes with the event conflict detection
      //await sails.models.schedule.destroy({ scheduleID: event.ID }).fetch();

      // Remove any clockwheels created for this schedule
      await sails.models.clockwheels.destroy({ scheduleID: event.ID }).fetch();

      // Remove logo and banner file
      if (destroyedRecord.logo)
        await sails.models.uploads.destroyOne({ ID: destroyedRecord.logo });
      if (destroyedRecord.banner)
        await sails.models.uploads.destroyOne({ ID: destroyedRecord.banner });
    })(destroyedRecord);

    return proceed();
  },
};
