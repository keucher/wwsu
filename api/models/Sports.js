/**
 * Sports.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",

  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
    },

    value: {
      type: "string",
      allowNull: true,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`sports socket: ${data}`);
    sails.sockets.broadcast("sports", "sports", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`sports socket: ${data}`);
    sails.sockets.broadcast("sports", "sports", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`sports socket: ${data}`);
    sails.sockets.broadcast("sports", "sports", data);
    return proceed();
  },
};
