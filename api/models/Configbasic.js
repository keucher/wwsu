const randomstring = require("randomstring");

/**
 * Configbasic.js
 *
 * @description :: Basic configuration for the WWSU sails app. Make sure to check for ID 1 and create if not exists.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  attributes: {
    ID: {
      type: "number",
      unique: true,
      required: true,
    },

    /*
      MAINTENANCE MODE
    */

    maintenance: {
      description:
        "When true, many regular checks will be disabled. Enable when performing maintenance.",
      type: "boolean",
      defaultsTo: false,
    },

    /*
      SECRETS
    */

    hostSecret: {
      description:
        "A random string to encode hosts and IP addresses. WARNING! Changing this will invalidate all active discipline!",
      type: "string",
      allowNull: true,
      encrypt: true,
    },

    metaSecret: {
      description:
        "The secret token parameter that should be provided for all calls to PUT /api/meta or GET /api/meta/update.",
      type: "string",
      allowNull: true,
      encrypt: true,
    },

    /*
      WEBSITES
    */

    website: {
      description: "URL to the WWSU website",
      type: "string",
      allowNull: true,
      isURL: true,
    },

    recordings: {
      description:
        "URL where broadcast recordings can be downloaded (redirected from /recordings)",
      type: "string",
      allowNull: true,
      isURL: true,
    },

    facebook: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU Facebook page.",
    },

    twitter: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU Twitter page.",
    },

    youtube: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU YouTube page.",
    },

    instagram: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU Instagram page.",
    },

    discord: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "Permanent invite link to the WWSU Discord server.",
    },

    /*
      SHOUTCAST v2
    */

    shoutcastStream: {
      description:
        "The URL to the Shoutcast v2 internet stream server for WWSU",
      type: "string",
      isURL: true,
      allowNull: true,
    },

    shoutcastStreamID: {
      description:
        "The id number of the Shoutcast stream for WWSU on the Shoutcast server.",
      type: "number",
      defaultsTo: 1,
    },

    /*
      OWNCAST
    */

    owncastStream: {
      description: "The URL to the Owncast video stream server for WWSU",
      type: "string",
      isURL: true,
      allowNull: true,
    },

    /*
      CAMPUS ALERTS
      */

    campusAlerts: {
      type: "string",
      isURL: true,
      allowNull: true,
      description:
        "The URL to the Wright State Campus Alerts JSON feed. Must NOT contain query parameters.",
    },

    /*
      TOMORROW.IO
    */

    tomorrowioAPI: {
      description:
        "API key for tomorrow.io weather platform to get current weather and forecasts.",
      type: "string",
      allowNull: true,
      encrypt: true,
    },

    tomorrowioLocation: {
      description:
        "Location parameter for tomorrow.io weather (such as latitude,longitude)",
      type: "string",
      allowNull: true,
    },

    /*
      SKYWAYJS
    */

    skywayAPI: {
      description:
        "API key for skyway.js (service used for DJ Controls to establish audio calls for remote broadcasts).",
      type: "string",
      allowNull: true,
    },

    skywaySecret: {
      description: "API secret for skyway.js service",
      type: "string",
      allowNull: true,
      encrypt: true,
    },

    /*
      ONESIGNAL
    */

    oneSignalAppID: {
      type: "string",
      allowNull: true,
      description: "The ID of the OneSignal app.",
    },

    oneSignalRest: {
      type: "string",
      allowNull: true,
      description: "The REST API key of the OneSignal app.",
      encrypt: true,
    },

    oneSignalCategoryMessage: {
      type: "string",
      allowNull: true,
      description: "The UUID for the message Android category.",
    },

    oneSignalCategoryEvent: {
      type: "string",
      allowNull: true,
      description: "The UUID for the event Android category.",
    },

    oneSignalCategoryAnnouncement: {
      type: "string",
      allowNull: true,
      description: "The UUID for the announcement Android category.",
    },

    oneSignalCategoryRequest: {
      type: "string",
      allowNull: true,
      description: "The UUID for the request Android category.",
    },

    /*
      BASIC BREAK-TIMES CONFIGURATION
    */

    breakCheck: {
      type: "number",
      defaultsTo: 10,
      min: 3,
      max: 50,
      description:
        "Error deviation for breaks in minutes; prevents airing breaks (except top of hour ID) less than this many minutes apart from each other. Ideally, this number should not be greater than the smallest duration between two clockwheel breaks. Also, any music tracks longer than this many minutes will be considered too long by the system.",
    },

    linerTime: {
      type: "number",
      defaultsTo: 10,
      min: 1,
      max: 59,
      description:
        "Do not play a track from the liners category in automation any more often than once every defined number of minutes.",
    },

    /*
      MAX ALLOWED QUEUE TIMES
    */

    maxQueueLive: {
      description:
        "When starting a live show, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up going live.",
      type: "number",
      min: 0,
      defaultsTo: 60 * 3,
    },

    maxQueuePrerecord: {
      description:
        "When starting a prerecord, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting the prerecord.",
      type: "number",
      min: 0,
      defaultsTo: 60 * 3,
    },

    maxQueueSports: {
      description:
        "When starting a sports broadcast, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting.",
      type: "number",
      min: 0,
      defaultsTo: 60,
    },

    maxQueueSportsReturn: {
      description:
        "When returning from break during a sports broadcast, if total queue is greater than this many seconds, all non-commercials and IDs will be skipped/removed immediately to speed up going back on air.",
      type: "number",
      min: 0,
      defaultsTo: 30,
    },

    maxQueueRemote: {
      description:
        "When starting a remote broadcast, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting.",
      type: "number",
      min: 0,
      defaultsTo: 60 * 3,
    },

    /*
      LIKED TRACKS
    */

    songsLikedCooldown: {
      description:
        "When someone likes a track on the website, the same track cannot be liked again by the same IP address for this many days. 0 means a track can only ever be liked one time per IP address.",
      type: "number",
      min: 0,
      defaultsTo: 7,
    },

    songsLikedPriorityChange: {
      description:
        "When someone likes a track, its priority in RadioDJ will be adjusted by this amount (positive = increase, negative = decrease)",
      type: "number",
      min: -100,
      max: 100,
      defaultsTo: 2,
    },

    /*
      REQUEST SYSTEM
    */

    requestsDailyLimit: {
      description:
        "Each IP address is allowed to make this many track requests per day (reset at midnight). 0 = disable the request system.",
      type: "number",
      min: 0,
      defaultsTo: 10,
    },

    requestsPriorityChange: {
      description:
        "When someone requests a track, its priority in RadioDJ will be adjusted by this amount (positive = increase, negative = decrease). Keep in mind if priority adjustment is set in RadioDJ for a played track, its priority will also be reduced/adjusted by that amount when played.",
      type: "number",
      min: -100,
      max: 100,
      defaultsTo: 1,
    },

    /*
      BOOKINGS
    */

    maxDurationPerBooking: {
      type: "number",
      min: 1,
      defaultsTo: 60 * 2,
      description:
        "Maximum duration, in minutes, an org member is allowed to book a studio at one time.",
    },

    maxDurationPerDay: {
      type: "number",
      min: 1,
      defaultsTo: 60 * 3,
      description:
        "Maximum duration, in minutes, an org member is allowed to book any studio on a given day.",
    },

    /*
      PROFANITY FILTER
    */

    filterProfanity: {
      description:
        "The specified words will be censored (including within other words) in metadata and messages.",
      type: "json",
      custom: function (value) {
        // Must be an array
        if (value.constructor !== Array) return false;

        // All entries in array must be a string. But allow empty array.
        if (value.length > 0 && value.find((val) => typeof val !== "string"))
          return false;

        return true;
      },
      defaultsTo: [
        "5h1t",
        "5hit",
        "assfukka",
        "asshole",
        "asswhole",
        "b!tch",
        "b17ch",
        "b1tch",
        "ballbag",
        "ballsack",
        "beastiality",
        "bestiality",
        "bi+ch",
        "biatch",
        "bitch",
        "blow job",
        "blowjob",
        "boiolas",
        "boner",
        "bunny fucker",
        "butthole",
        "buttmuch",
        "buttplug",
        "carpet muncher",
        "chink",
        "cl1t",
        "clit",
        "cnut",
        "cock-sucker",
        "cockface",
        "cockhead",
        "cockmunch",
        "cocksuck",
        "cocksuka",
        "cocksukka",
        "cokmuncher",
        "coksucka",
        "cummer",
        "cumming",
        "cumshot",
        "cunilingus",
        "cunillingus",
        "cunnilingus",
        "cunt",
        "cyberfuc",
        "dickhead",
        "dildo",
        "dog-fucker",
        "doggin",
        "dogging",
        "donkeyribber",
        "doosh",
        "duche",
        "dyke",
        "ejaculate",
        "ejaculating",
        "ejaculation",
        "ejakulate",
        "f u c k",
        "fag",
        "fannyflaps",
        "fannyfucker",
        "fatass",
        "fcuk",
        "felching",
        "fellate",
        "fellatio",
        "flange",
        "fuck",
        "fudge packer",
        "fudgepacker",
        "fuk",
        "fux",
        "fux0r",
        "f_u_c_k",
        "gangbang",
        "gaylord",
        "gaysex",
        "goatse",
        "god-dam",
        "goddamn",
        "hardcoresex",
        "heshe",
        "homosex",
        "horniest",
        "horny",
        "hotsex",
        "jack-off",
        "jackoff",
        "jerk-off",
        "jism",
        "jiz",
        "jizm",
        "kawk",
        "kunilingus",
        "l3i+ch",
        "l3itch",
        "labia",
        "m0f0",
        "m0fo",
        "m45terbate",
        "ma5terb8",
        "ma5terbate",
        "master-bate",
        "masterb8",
        "masterbat*",
        "masterbat3",
        "masterbate",
        "masterbation",
        "masturbate",
        "mo-fo",
        "mof0",
        "mofo",
        "muthafecker",
        "muthafuckker",
        "mutherfucker",
        "n1gga",
        "n1gger",
        "nigg3r",
        "nigg4h",
        "nigga",
        "nigger",
        "nob jokey",
        "nobhead",
        "nobjocky",
        "nobjokey",
        "numbnuts",
        "nutsack",
        "pecker",
        "penis",
        "phonesex",
        "phuck",
        "phuk",
        "phuq",
        "pimpis",
        "piss",
        "prick",
        "pusse",
        "pussi",
        "pussies",
        "pussy",
        "rectum",
        "retard",
        "rimjaw",
        "rimming",
        "s hit",
        "schlong",
        "scroat",
        "scrote",
        "scrotum",
        "semen",
        "sh!+",
        "sh!t",
        "sh1t",
        "shag",
        "shagger",
        "shaggin",
        "shagging",
        "shemale",
        "shi+",
        "shit",
        "skank",
        "slut",
        "sluts",
        "smegma",
        "spunk",
        "s_h_i_t",
        "t1tt1e5",
        "t1tties",
        "teets",
        "testical",
        "testicle",
        "titfuck",
        "tits",
        "tittie5",
        "tittiefucker",
        "titties",
        "tw4t",
        "twat",
        "twunt",
        "vagina",
        "vulva",
        "w00se",
        "wang",
        "wanker",
        "wanky",
        "whore",
      ],
    },

    /*
      HTML SANITIZE
    */

    sanitize: {
      description:
        "Options for sanitize-html to prevent annoying or unsafe HTML from being used in messages and public locations.",
      type: "json",
      defaultsTo: {
        allowedTags: [
          "h2",
          "h3",
          "h4",
          "h5",
          "h6",
          "blockquote",
          "p",
          "a",
          "ul",
          "ol",
          "nl",
          "li",
          "b",
          "i",
          "strong",
          "em",
          "strike",
          "code",
          "hr",
          "br",
          "u",
          "s",
          "span",
        ],
        allowedAttributes: {
          a: ["href", "name", "target"],
          span: ["style"],
        },
        allowedStyles: {
          span: {
            // Match HEX and RGB
            color: [
              /^#(0x)?[0-9a-f]+$/i,
              /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/,
            ],
          },
        },
        // Lots of these won't come up by default because we don't allow them
        selfClosing: [
          "br",
          "hr",
          "area",
          "base",
          "basefont",
          "input",
          "link",
          "meta",
        ],
        // URL schemes we permit
        allowedSchemes: ["http", "https"],
        allowedSchemesByTag: {},
        allowedSchemesAppliedToAttributes: ["href", "src", "cite"],
        allowProtocolRelative: true,
      },
    },

    /*
      NATIONAL WEATHER SERVICE ALERTS TO ACCEPT
      Should contain alert name (case sensitive) as the key, and its string color hexadecimal as value.
    */

    nwsAlerts: {
      type: "json",
      custom: (value) => {
        // Must be an object
        if (typeof value !== "object") return false;

        // Every key must be a string, and value must be a string that evaluates to a valid color hex code.
        var pattern = new RegExp("^#([a-fA-F0-9]){3}$|[a-fA-F0-9]{6}$");

        for (let key in value) {
          if (!Object.prototype.hasOwnProperty.call(value, key)) continue;
          if (typeof key !== "string") return false;
          if (!pattern.test(value[key])) return false;
        }

        return true;
      },
      defaultsTo: {
        "911 Telephone Outage": "#C0C0C0",
        "Administrative Message": "#FFFFFF",
        "Air Quality Alert": "#808080",
        "Air Stagnation Advisory": "#808080",
        "Arroyo and Small Stream Flood Advisory": "#00FF7F",
        "Ashfall Advisory": "#696969",
        "Ashfall Warning": "#A9A9A9",
        "Avalanche Advisory": "#CD853F",
        "Avalanche Warning": "#1E90FF",
        "Avalanche Watch": "#F4A460",
        "Blizzard Warning": "#FF4500",
        "Blizzard Watch": "#ADFF2F",
        "Blowing Dust Advisory": "#BDB76B",
        "Brisk Wind Advisory": "#D8BFD8",
        "Child Abduction Emergency": "#FFD700",
        "Civil Danger Warning": "#FFB6C1",
        "Civil Emergency Message": "#FFB6C1",
        "Coastal Flood Advisory": "#7CFC00",
        "Coastal Flood Warning": "#228B22",
        "Coastal Flood Watch": "#66CDAA",
        "Dense Fog Advisory": "#708090",
        "Dense Smoke Advisory": "#F0E68C",
        "Dust Storm Warning": "#FFE4C4",
        "Earthquake Warning": "#8B4513",
        "Evacuation - Immediate": "#7FFF00",
        "Excessive Heat Warning": "#C71585",
        "Excessive Heat Watch": "#800000",
        "Extreme Cold Warning": "#0000FF",
        "Extreme Cold Watch": "#0000FF",
        "Extreme Fire Danger": "#E9967A",
        "Extreme Wind Warning": "#FF8C00",
        "Fire Warning": "#A0522D",
        "Fire Weather Watch": "#FFDEAD",
        "Flash Flood Warning": "#8B0000",
        "Flash Flood Watch": "#2E8B57",
        "Flood Advisory": "#00FF7F",
        "Flood Warning": "#00FF00",
        "Flood Watch": "#2E8B57",
        "Freeze Warning": "#483D8B",
        "Freeze Watch": "#00FFFF",
        "Freezing Fog Advisory": "#008080",
        "Freezing Rain Advisory": "#DA70D6",
        "Freezing Spray Advisory": "#00BFFF",
        "Frost Advisory": "#6495ED",
        "Gale Warning": "#DDA0DD",
        "Gale Watch": "#FFC0CB",
        "Hard Freeze Warning": "#9400D3",
        "Hard Freeze Watch": "#4169E1",
        "Hazardous Materials Warning": "#4B0082",
        "Hazardous Seas Warning": "#D8BFD8",
        "Hazardous Seas Watch": "#483D8B",
        "Heat Advisory": "#FF7F50",
        "Heavy Freezing Spray Warning": "#00BFFF",
        "Heavy Freezing Spray Watch": "#BC8F8F",
        "High Surf Advisory": "#BA55D3",
        "High Surf Warning": "#228B22",
        "High Wind Warning": "#DAA520",
        "High Wind Watch": "#B8860B",
        "Hurricane Force Wind Warning": "#CD5C5C",
        "Hurricane Force Wind Watch": "#9932CC",
        "Hurricane Warning": "#DC143C",
        "Hurricane Watch": "#FF00FF",
        "Hydrologic Advisory": "#00FF7F",
        "Ice Storm Warning": "#8B008B",
        "Lake Effect Snow Advisory": "#48D1CC",
        "Lake Effect Snow Warning": "#008B8B",
        "Lake Effect Snow Watch": "#87CEFA",
        "Lake Wind Advisory": "#D2B48C",
        "Lakeshore Flood Advisory": "#7CFC00",
        "Lakeshore Flood Warning": "#228B22",
        "Lakeshore Flood Watch": "#66CDAA",
        "Law Enforcement Warning": "#C0C0C0",
        "Local Area Emergency": "#C0C0C0",
        "Low Water Advisory": "#A52A2A",
        "Nuclear Power Plant Warning": "#4B0082",
        "Radiological Hazard Warning": "#4B0082",
        "Red Flag Warning": "#FF1493",
        "Severe Thunderstorm Warning": "#FFA500",
        "Severe Thunderstorm Watch": "#DB7093",
        "Shelter In Place Warning": "#FA8072",
        "Small Craft Advisory": "#D8BFD8",
        "Small Craft Advisory For Hazardous Seas": "#D8BFD8",
        "Small Craft Advisory For Rough Bar": "#D8BFD8",
        "Small Craft Advisory For Winds": "#D8BFD8",
        "Small Stream Flood Advisory": "#00FF7F",
        "Special Marine Warning": "#FFA500",
        "Storm Warning": "#9400D3",
        "Storm Watch": "#FFE4B5",
        Test: "#F0FFFF",
        "Tornado Warning": "#FF0000",
        "Tornado Watch": "#FFFF00",
        "Tropical Storm Warning": "#B22222",
        "Tropical Storm Watch": "#F08080",
        "Tsunami Advisory": "#D2691E",
        "Tsunami Warning": "#FD6347",
        "Tsunami Watch": "#FF00FF",
        "Typhoon Warning": "#DC143C",
        "Typhoon Watch": "#FF00FF",
        "Urban and Small Stream Flood Advisory": "#00FF7F",
        "Volcano Warning": "#2F4F4F",
        "Wind Advisory": "#D2B48C",
        "Wind Chill Advisory": "#AFEEEE",
        "Wind Chill Warning": "#B0C4DE",
        "Wind Chill Watch": "#5F9EA0",
        "Winter Storm Warning": "#FF69B4",
        "Winter Storm Watch": "#4682B4",
        "Winter Weather Advisory": "#7B68EE",
      },
    },
  },

  initialize: async function () {
    sails.config.custom.basic = await sails.models.configbasic
      .findOrCreate(
        { ID: 1 },
        {
          ID: 1,
          hostSecret: randomstring.generate(),
          metaSecret: randomstring.generate(),
        }
      )
      .decrypt();
  },

  // Websockets standards
  // TODO: Do any necessary maintenance of specific setting changes
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };

    // Delete sensitive records so they do not go out over sockets
    delete data.insert.hostSecret;
    delete data.insert.metaSecret;
    delete data.insert.tomorrowioAPI;
    delete data.insert.skywaySecret;
    delete data.insert.oneSignalRest;

    sails.log.silly(`config-basic socket: ${data}`);
    sails.sockets.broadcast("config-basic", "config-basic", data);

    (async () => {
      // Update config
      sails.config.custom.basic = await sails.models.configbasic
        .findOrCreate(
          { ID: 1 },
          {
            ID: 1,
            hostSecret: randomstring.generate(),
            metaSecret: randomstring.generate(),
          }
        )
        .decrypt();

        proceed();
    })();

  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };

    // Delete sensitive records so they do not go out over sockets
    delete data.update.hostSecret;
    delete data.update.metaSecret;
    delete data.update.tomorrowioAPI;
    delete data.update.skywaySecret;
    delete data.update.oneSignalRest;

    sails.log.silly(`config-basic socket: ${data}`);
    sails.sockets.broadcast("config-basic", "config-basic", data);

    (async () => {
      // Update config
      sails.config.custom.basic = await sails.models.configbasic
        .findOrCreate(
          { ID: 1 },
          {
            ID: 1,
            hostSecret: randomstring.generate(),
            metaSecret: randomstring.generate(),
          }
        )
        .decrypt();

        proceed();
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-basic socket: ${data}`);
    sails.sockets.broadcast("config-basic", "config-basic", data);

    (async () => {
      // Update config
      sails.config.custom.basic = await sails.models.configbasic
        .findOrCreate(
          { ID: 1 },
          {
            ID: 1,
            hostSecret: randomstring.generate(),
            metaSecret: randomstring.generate(),
          }
        )
        .decrypt();

        proceed();
    })();
  },
};
