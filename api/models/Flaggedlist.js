/**
 * Flaggedlist.js
 *
 * @description :: Track and broadcast reports.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    host: {
      type: "string",
      required: true,
      description: "The host IP that reported.",
    },

    attendanceID: {
      type: "number",
      allowNull: true,
      description: "The ID of the broadcast if reporting a broadcast.",
    },

    trackID: {
      type: "number",
      allowNull: true,
      description:
        "The ID of the song if reporting a song that played in automation.",
    },

    meta: {
      type: "string",
      required: true,
      description: "The metadata of the track or broadcast reported.",
    },

    reason: {
      type: "string",
      required: true,
      description: "The reason provided for the report.",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`flaggedlist socket: ${data}`);
    sails.sockets.broadcast(`flaggedlist`, "flaggedlist", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`flaggedlist socket: ${data}`);
    sails.sockets.broadcast(`flaggedlist`, "flaggedlist", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`calendar socket: ${data}`);
    sails.sockets.broadcast(`flaggedlist`, "flaggedlist", data);
    return proceed();
  },
};
