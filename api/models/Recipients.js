/**
 * Recipients.js
 *
 * @description :: This model contains a collection of recipients that can receive messages.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "ram",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    hostID: {
      type: "number",
      allowNull: true,
    },

    host: {
      type: "string",
      unique: true,
      required: true,
    },

    belongsTo: {
      type: "number",
      allowNull: true,
      defaultsTo: 0,
    },

    device: {
      type: "string",
      allowNull: true,
    },

    group: {
      type: "string",
      isIn: ["system", "website", "display", "computers", "discord"],
    },

    label: {
      type: "string",
    },

    status: {
      type: "number",
      min: 0,
      max: 5,
    },

    peer: {
      type: "string",
      allowNull: true,
    },

    startRemotes: {
      type: "boolean",
      defaultsTo: false,
    },

    answerCalls: {
      type: "boolean",
      defaultsTo: false,
    },

    time: {
      type: "ref",
      columnType: "datetime",
      defaultsTo: new Date(),
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["time", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["time", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Template are initial records created in the Recipients model upon execution of bootstrap.
  template: [
    {
      host: "website",
      group: "website",
      label: "Public",
      status: 0,
    },
  ],

  sockets: {}, // For recipients connecting via sockets, we will pair the sockets with the Recipients.ID in the format: sockets[Recipients.ID] = [array, of, socket, IDs]

  initialize: async function () {
    // Load internal sails.models.recipients into memory
    await sails.models.recipients.createEach(sails.models.recipients.template);

    // Generate sails.models.recipients based off of messages from the last hour... website only.
    let records = await sails.models.messages
      .find({
        status: "active",
        from: { startsWith: "website-" },
        createdAt: {
          ">": moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss"),
        },
      })
      .sort("createdAt DESC")
      .tolerate(() => {});
    if (records && records.length > 0) {
      let insertRecords = [];
      let hosts = [];
      records.forEach((record) => {
        if (hosts.indexOf(record.from) === -1) {
          hosts.push(record.from);
          insertRecords.push({
            host: record.from,
            group: "website",
            label: record.fromFriendly,
            status: 0,
            time: record.createdAt,
          });
        }
      });

      await sails.models.recipients.createEach(insertRecords);
    }
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`recipients socket: ${data}`);
    sails.sockets.broadcast("recipients", "recipients", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`recipients socket: ${data}`);
    sails.sockets.broadcast("recipients", "recipients", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`recipients socket: ${data}`);
    sails.sockets.broadcast("recipients", "recipients", data);
    return proceed();
  },
};
