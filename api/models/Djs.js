/**
 * Djs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    active: {
      type: "boolean",
      defaultsTo: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
    },

    realName: {
      type: "string",
      allowNull: true,
    },

    email: {
      type: "string",
      allowNull: true,
    },

    login: {
      type: "string",
      allowNull: true,
    },

    profile: {
      type: "string",
      defaultsTo: "",
      columnType: "text",
    },

    avatar: {
      type: "number",
      allowNull: true,
      description: "ID reference to uploads model",
    },

    permissions: {
      type: "json",
      defaultsTo: [],
    },

    doorCode: {
      type: "string",
      encrypt: true, // Instead of bcrypting, we use sails encryption because we want to be able to decrypt if we need to retrieve the door code
      allowNull: true,
    },

    doorCodeAccess: {
      type: "json",
      defaultsTo: [],
    },

    lastSeen: {
      type: "ref",
      columnType: "datetime",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["lastSeen", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["lastSeen", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  beforeDestroy: function (criteria, proceed) {
    if (criteria.ID === 1)
      // Do not allow destroying the master member.
      throw new Error("Not allowed to destroy DJ record 1.");
    return proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    newlyCreatedRecord.login = newlyCreatedRecord.login === null ? false : true;
    let data = { insert: newlyCreatedRecord };
    sails.log.silly(`djs socket: ${data}`);
    sails.sockets.broadcast("djs", "djs", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    updatedRecord.login = updatedRecord.login === null ? false : true;
    let data = { update: updatedRecord };
    sails.log.silly(`djs socket: ${data}`);
    sails.sockets.broadcast("djs", "djs", data);
    let records;
    let hosts = "";
    let temp;
    let temp2;
    let temp3;
    let temp4;

    // Update host data in calendar events
    temp = (async () => {
      records = await sails.models.calendar.find({
        or: [
          { hostDJ: updatedRecord.ID },
          { cohostDJ1: updatedRecord.ID },
          { cohostDJ2: updatedRecord.ID },
          { cohostDJ3: updatedRecord.ID },
        ],
      });
      if (records.length > 0) {
        records.map(async (record) => {
          try {
            let toUpdate = {};
            if (!updatedRecord.active) {
              if (record.hostDJ === updatedRecord.ID) {
                toUpdate.hostDJ = null;
                record.hostDJ = null;
              }
              if (record.cohostDJ1 === updatedRecord.ID) {
                toUpdate.cohostDJ1 = null;
                record.cohostDJ1 = null;
              }
              if (record.cohostDJ2 === updatedRecord.ID) {
                toUpdate.cohostDJ2 = null;
                record.cohostDJ2 = null;
              }
              if (record.cohostDJ3 === updatedRecord.ID) {
                toUpdate.cohostDJ3 = null;
                record.cohostDJ3 = null;
              }
            }
            hosts = await sails.helpers.calendar.generateHosts(record);
            await sails.models.calendar
              .update(
                { ID: record.ID },
                Object.assign(toUpdate, { hosts: hosts || "Unknown Hosts" })
              )
              .fetch();
          } catch (e) {}
        });
      }
    })();
    temp2 = (async () => {
      records = await sails.models.schedule.find({
        or: [
          { hostDJ: updatedRecord.ID },
          { cohostDJ1: updatedRecord.ID },
          { cohostDJ2: updatedRecord.ID },
          { cohostDJ3: updatedRecord.ID },
        ],
      });
      if (records.length > 0) {
        records.map(async (record) => {
          try {
            let toUpdate = {};
            if (!updatedRecord.active) {
              if (record.hostDJ === updatedRecord.ID) {
                toUpdate.hostDJ = null;
                record.hostDJ = null;
              }
              if (record.cohostDJ1 === updatedRecord.ID) {
                toUpdate.cohostDJ1 = null;
                record.cohostDJ1 = null;
              }
              if (record.cohostDJ2 === updatedRecord.ID) {
                toUpdate.cohostDJ2 = null;
                record.cohostDJ2 = null;
              }
              if (record.cohostDJ3 === updatedRecord.ID) {
                toUpdate.cohostDJ3 = null;
                record.cohostDJ3 = null;
              }
            }
            hosts = await sails.helpers.calendar.generateHosts(record);
            await sails.models.schedule
              .update(
                { ID: record.ID },
                Object.assign(toUpdate, { hosts: hosts })
              )
              .fetch();
          } catch (e) {}
        });
      }
    })();
    temp3 = (async () => {
      if (!updatedRecord.active) {
        // Destroy directors with the same name if marking this DJ as inactive
        let destroyed = await sails.models.directors
          .destroy({ name: updatedRecord.realName, ID: { "!=": 1 } })
          .fetch();
      }
    })();
    temp4 = (async () => {
      if (!updatedRecord.active) {
        // When marking DJ as inactive, de-authorize any hosts whose belongsTo was set to this DJ.
        await sails.models.hosts
          .update({ belongsTo: updatedRecord.ID }, { authorized: false })
          .fetch();
      }
    })();

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    let data = { remove: destroyedRecord.ID };
    sails.log.silly(`djs socket: ${data}`);
    sails.sockets.broadcast("djs", "djs", data);
    let records;
    let hosts;
    let maps;
    let toUpdate;
    let temp;

    temp = (async () => {
      // Update DJ data in calendar events
      records = await sails.models.calendar.find({
        or: [
          { hostDJ: destroyedRecord.ID },
          { cohostDJ1: destroyedRecord.ID },
          { cohostDJ2: destroyedRecord.ID },
          { cohostDJ3: destroyedRecord.ID },
        ],
      });
      if (records.length > 0) {
        maps = records.map(async (record) => {
          try {
            toUpdate = {};
            if (record.hostDJ === destroyedRecord.ID) {
              toUpdate.hostDJ = null;
              record.hostDJ = null;
            }
            if (record.cohostDJ1 === destroyedRecord.ID) {
              toUpdate.cohostDJ1 = null;
              record.cohostDJ1 = null;
            }
            if (record.cohostDJ2 === destroyedRecord.ID) {
              toUpdate.cohostDJ2 = null;
              record.cohostDJ2 = null;
            }
            if (record.cohostDJ3 === destroyedRecord.ID) {
              toUpdate.cohostDJ3 = null;
              record.cohostDJ3 = null;
            }

            toUpdate.hosts = await sails.helpers.calendar.generateHosts(record);
            await sails.models.calendar
              .update(
                { ID: record.ID },
                Object.assign(toUpdate, { hosts: hosts || "Unknown Hosts" })
              )
              .fetch();
          } catch (e) {}
        });
        await Promise.all(maps);
      }

      records = await sails.models.schedule.find({
        or: [
          { hostDJ: destroyedRecord.ID },
          { cohostDJ1: destroyedRecord.ID },
          { cohostDJ2: destroyedRecord.ID },
          { cohostDJ3: destroyedRecord.ID },
        ],
      });
      if (records.length > 0) {
        maps = records.map(async (record) => {
          try {
            toUpdate = {};
            if (record.hostDJ === destroyedRecord.ID) {
              toUpdate.hostDJ = null;
              record.hostDJ = null;
            }
            if (record.cohostDJ1 === destroyedRecord.ID) {
              toUpdate.cohostDJ1 = null;
              record.cohostDJ1 = null;
            }
            if (record.cohostDJ2 === destroyedRecord.ID) {
              toUpdate.cohostDJ2 = null;
              record.cohostDJ2 = null;
            }
            if (record.cohostDJ3 === destroyedRecord.ID) {
              toUpdate.cohostDJ3 = null;
              record.cohostDJ3 = null;
            }

            toUpdate.hosts = await sails.helpers.calendar.generateHosts(record);
            await sails.models.schedule
              .update(
                { ID: record.ID },
                Object.assign(toUpdate, { hosts: hosts })
              )
              .fetch();
          } catch (e) {}
        });
        await Promise.all(maps);
      }

      // Update attendance records
      await sails.models.attendance
        .update({ dj: destroyedRecord.ID }, { dj: null })
        .fetch();
      await sails.models.attendance
        .update({ cohostDJ1: destroyedRecord.ID }, { cohostDJ1: null })
        .fetch();
      await sails.models.attendance
        .update({ cohostDJ2: destroyedRecord.ID }, { cohostDJ2: null })
        .fetch();
      await sails.models.attendance
        .update({ cohostDJ3: destroyedRecord.ID }, { cohostDJ3: null })
        .fetch();

      // De-authorize any hosts whose belongsTo was set to this DJ. Also, set belongsTo to 0.
      await sails.models.hosts
        .update(
          { belongsTo: destroyedRecord.ID },
          { authorized: false, belongsTo: 0 }
        )
        .fetch();

      // Destroy notes records
      await sails.models.djnotes.destroy({ dj: destroyedRecord.ID }).fetch();

      // Edit meta if necessary
      if (sails.models.meta.memory.hostDJ === destroyedRecord.ID) {
        await sails.helpers.meta.change.with({ hostDJ: null });
      }
      if (sails.models.meta.memory.cohostDJ1 === destroyedRecord.ID) {
        await sails.helpers.meta.change.with({ cohostDJ1: null });
      }
      if (sails.models.meta.memory.cohostDJ2 === destroyedRecord.ID) {
        await sails.helpers.meta.change.with({ cohostDJ2: null });
      }
      if (sails.models.meta.memory.cohostDJ3 === destroyedRecord.ID) {
        await sails.helpers.meta.change.with({ cohostDJ3: null });
      }

      // Destroy directors with the same name
      let destroyed = await sails.models.directors
        .destroy({ name: destroyedRecord.realName, ID: { "!=": 1 } })
        .fetch();
      // Set blog posts authored by this DJ to null.
      await sails.models.blogs
        .update({ author: destroyedRecord.ID }, { author: null })
        .fetch();

      // Remove avatar file
      if (destroyedRecord.avatar)
        await sails.models.uploads.destroyOne({ ID: destroyedRecord.avatar });
    })();
    return proceed();
  },
};
