/**
 * sails.models.status.js
 *
 * @description :: Model contains information about the status of certain systems.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

// API NOTE: Do not use sails.models.status.update() to update statuses; use sails.helpers.status.modify instead. This helper has additional functionality.

module.exports = {
  datastore: "ram",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
    },

    label: {
      type: "string",
    },

    status: {
      type: "number",
      min: 1,
      max: 5,
      defaultsTo: 4,
    },

    summary: {
      type: "string",
      defaultsTo: "See DJ Controls.",
      columnType: "text",
    },

    data: {
      type: "string",
      defaultsTo: "No additional information available.",
      columnType: "text",
    },

    // websocket API note: Do not use time in client code; changes to time will not be pushed in sockets to avoid unnecessary data transfer.
    time: {
      type: "ref",
      columnType: "datetime",
    },
  },

  // The template are subsystems that should be added into memory upon lifting of the Sails app via bootstrap().
  // RadioDJs added automatically.
  // Datastores added automatically.
  // Hosts added automatically.
  template: [
    {
      name: "app",
      label: "SailsJS Server App",
      summary: "Not yet checked since app reboot; is checked every 15 seconds.",
      data: "SailsJS status has not been checked since initialization. It is checked every 15 seconds.",
      status: 4,
      time: null,
    },
    {
      name: "website",
      label: "Website",
      summary: "Not yet checked since app reboot; is checked every minute.",
      data: "No connection to wwsu1069.org could be made yet since initialization. This test is run every minute.",
      status: 4,
      time: null,
    },
    {
      name: "stream-public",
      label: "Radio Stream",
      summary: "Not yet checked since app reboot; is checked every minute.",
      data: "Public radio stream has not reported online since initialization. This test is run every minute.",
      status: 4,
      time: null,
    },
    {
      name: "stream-video",
      label: "Video Stream",
      summary: "Not yet checked since app reboot; is checked every minute.",
      data: "Video stream has not reported online since initialization. This test is run every minute.",
      status: 4,
      time: null,
    },
    {
      name: "silence",
      label: "Silence",
      summary: "Operational.",
      data: "The silence detection system has not reported silence since initialization.",
      status: 5,
      time: null,
    },
    {
      name: "EAS-NWS",
      label: "EAS NWS CAPS",
      summary: "Not yet checked since app reboot; is checked every minute.",
      data: "No successful connection to all EAS NWS CAPS has been made yet since initialization. EAS CAPS is queried every minute.",
      status: 4,
      time: null,
    },
    {
      name: "EAS-WSU",
      label: "EAS Wright State Alert",
      summary: "Not yet checked since app reboot; is checked every minute.",
      data: "No successful connection to the Wright State Alert feed has been made yet since initialization. The feed is queried every minute.",
      status: 4,
      time: null,
    },
    {
      name: "server",
      label: "Server",
      summary: "Not yet checked since app reboot; is checked every minute.",
      data: "No server data has been returned yet since initialization. Server data is checked every minute.",
      status: 4,
      time: null,
    },
    {
      name: "discord",
      label: "Discord Bot",
      status: 4,
      summary: "Not yet checked since app reboot.",
      data: "The Discord Bot has not yet been initialized.",
    },
    {
      name: "music-library",
      label: "Music Library",
      summary: "Not yet checked since app reboot; is checked every 5 minutes.",
      data: "Music library tests have not yet executed since initialization. They are executed every 5 minutes.",
      status: 4,
      time: null,
    },
    {
      name: "calendar",
      label: "Calendar",
      summary: "Not yet checked since app reboot; is checked every 5 minutes.",
      data: "Calendar has not been loaded since initialization. Calendar checks are run every 5 minutes.",
      status: 4,
      time: null,
    },
    {
      name: "calendar-triggerer",
      label: "Calendar Triggerer",
      summary: "No automated programming started yet since app reboot.",
      data: "No programming has been triggered by the calendar system since initialization.",
      status: 4,
      time: null,
    },
    {
      name: "underwritings",
      label: "Underwritings",
      summary:
        "Not yet checked since app reboot; is checked every clockwheel break.",
      data: "Underwritings were not yet checked since initialization. Underwritings are checked at every clockwheel break.",
      status: 4,
      time: null,
    },
    {
      name: "delay-system",
      label: "Delay System",
      summary: "Not yet checked since app reboot; is checked every 15 seconds.",
      data: "Status of Delay System has not been checked since initialization. Delay system is checked by the responsible DJ Controls every 15 seconds.",
      status: 4,
      time: moment().format("YYYY-MM-DD HH:mm:ss"),
    },
    {
      name: "reported",
      label: "Reported Problems",
      status: 4,
      summary: "Not yet checked since app reboot.",
      data: "System has not yet checked reported problems since initialization.",
      time: null,
    },
    {
      name: "inventory",
      label: "Inventory",
      status: 4,
      summary: "Not yet checked since app reboot; is checked every 5 minutes.",
      data: "Inventory checks have not been run since initialization. Checks are run every 5 minutes.",
      time: null,
    },
    {
      name: "recorder",
      label: "Recorder",
      status: 4,
      summary: "A recording was not yet saved since app reboot.",
      data: "A recording has not yet been saved since initialization.",
      time: null,
    },
    {
      name: "rss-wsuguardian",
      label: "WSUGuardian RSS",
      status: 4,
      summary: "Not yet checked since app reboot; is checked every 5 minutes.",
      data: "The wsuguardian RSS feed has not been pulled yet since initialization. It is checked every 5 minutes.",
    },
    {
      name: "tomorrowio",
      label: "Tomorrow.io",
      status: 4,
      summary: "Not yet checked since app reboot; is checked every 5 minutes.",
      data: "Tomorrow.io data has not been pulled since initialization. It is checked every 5 minutes.",
    },
  ],

  // Used for checking when the dump button was last pressed so we can prevent "critical" status when it reads 0 initially.
  lastDump: null,

  // Used for determining the last time the daily cleanup was run.
  lastCleanup: null,

  // Used for checking if important tasks are running that should block shut down (via sails.config.beforeShutdown).
  // Number indicates number of tasks in that category running. You can also use a Map to track specific operations.
  tasks: {
    createRecord: 0,
    calendar: new Map(), // Map of operations to prevent conflicts in active operations.
    analyticsEmail: 0,
    cleanup: 0,
  },

  /* Object used internally to check for errors.
   * Items in the object which are to be used by sails.helpers.error.count and sails.helpers.error.reset are formatted as the following:
   * key: {
   *     count: 0, // Used by the helper to count the number of times that error occurred.
   *     trigger: 15, // If count reaches this number, then fn() is executed
   *     active: false, // Set to false by default; used to make sure error checks do not run on top of each other.
   *     condition: function() {}, // If this function returns true, then count is automatically reset to 0 in the count helper. Can be async.
   *     fn: function() {}, // This function is executed when count reaches trigger. The function should return a number to which the count should be changed to (such as 0). Can be async.
   * }
   */
  errorCheck: {
    // This contains a moment timestamp of when the previous triggered error happened.
    prevError: null,

    // This is used for determining the last time the silence detection was activated.
    prevSilence: null,

    // Contains a moment timestamp of when the most recent station ID was queued.
    prevID: "2002-01-01T00:00:00Z",

    // moment stamp of when the most recent PSA break was queued.
    prevBreak: "2002-01-01T00:00:00Z",

    // moment stamp of when the most recent standard liner was queued.
    prevLiner: "2002-01-01T00:00:00Z",

    // Used for determining if we have a sudden jump in queue time in RadioDJ
    trueZero: 0,

    // Used for queue length error detecting and trueZero
    prevQueueLength: 0,
    prevTrackLength: 0,
    prevCountdown: 0,

    // Used for determining when to re-fetch RadioDJ queue
    queueWait: 0,

    // Used for determining if RadioDJ froze.
    prevDuration: 0,
    prevElapsed: 0,
    prevFetchedDuration: 0,
    prevFetchedElapsed: 0,

    // Use for altRadioDJ
    prevAltDuration: 0,
    prevAltElapsed: 0,

    // Used for telling the system we are waiting for a good RadioDJ to switch to
    waitForGoodRadioDJ: false,

    // Determine if we should throttle (put into a queue) API requests.
    // 0 = disable, 1 = throttle, 2 = outright reject / 503.
    throttleAPI: 0,

    // Absolute fallback if we cannot load the default rotation; this tells bootstrap.js to queue random tracks when the queue runs low.
    playRandomTracks: false,

    // Determine if error post is running
    errorPostRunning: false,

    // Triggered when CRON checks fails to getQueue.
    queueFail: {
      count: 0,
      trigger: 30,
      active: false,
      fn: function () {
        // LINT: async required because of sails.js await.
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (resolve, reject) => {
          try {
            // Do not continue if we are in the process of waiting for a status 5 RadioDJ
            if (sails.models.status.errorCheck.waitForGoodRadioDJ) {
              return resolve(0);
            }

            await sails.helpers.meta.change.with({
              changingState: `Switching radioDJ instances due to queueFail`,
            });
            sails.sockets.broadcast("system-error", "system-error", true);
            await sails.models.logs
              .create({
                attendanceID: sails.models.meta.memory.attendanceID,
                logtype: "system-queuefail",
                loglevel: "danger",
                logsubtype: "",
                logIcon: `fas fa-exclamation-triangle`,
                title: `Switched RadioDJs: Failed repeatedly to return queue data.`,
                event: `Please check to make sure all RadioDJs are functional and did not freeze. Sometimes, this error may also be thrown when a track in the queue contains special characters that cannot be processed.`,
              })
              .fetch()
              .tolerate((err) => {
                sails.log.error(err);
              });
            await sails.helpers.onesignal.sendMass(
              "emergencies",
              "RadioDJ Queue Failure",
              `On ${moment().format(
                "LLLL"
              )}, the system switched RadioDJs because the active one was not returning queue data. It might have crashed or there is a problem connecting to it on the network. Please see DJ Controls.`
            );
            var maps = sails.config.custom.radiodjs
              .filter(
                (instance) => instance.name === sails.models.meta.memory.radiodj
              )
              .map(async (instance) => {
                var status = await sails.models.status.findOne({
                  name: `radiodj-${instance.name}`,
                });
                if (status && status.status !== 1) {
                  await sails.helpers.status.modify.with({
                    name: `radiodj-${instance.name}`,
                    label: `RadioDJ ${instance.label}`,
                    status: instance.errorLevel,
                    summary: `queueFail: RadioDJ is not reporting what is in the queue; it may have crashed or a bad track is in the queue.`,
                    data: `RadioDJ triggered queueFail for failing to report queue data. We tried switching active RadioDJs. <br />
                <strong>TO FIX / CHECK: </strong>
                <ul>
                <li>Make sure RadioDJ is running on all computers that it should be running on.</li>
                <li>Make sure the MariaDB / MySQL database server on each computer running RadioDJ is functional.</li>
                <li>Make sure the REST server on each RadioDJ is online, configured properly, and accessible (RadioDJ Options -> Plugins -> REST server). You may have to play a track in RadioDJ before REST begins working.</li>
                <li>Make sure the network connection on the computers running RadioDJ is good, and that the server can connect.</li>
                </ul>`,
                  });
                }
                return true;
              });
            await Promise.all(maps);
            await sails.helpers.rest.cmd("EnableAssisted", 1, 0);
            await sails.helpers.rest.cmd("EnableAutoDJ", 1, 0);
            await sails.helpers.rest.cmd("StopPlayer", 1, 0);
            var queue = sails.models.meta.automation;
            await sails.helpers.rest.changeRadioDj();
            await sails.helpers.rest.cmd("ClearPlaylist", 1, 5000);
            await sails.helpers.error.post(queue);
            await sails.helpers.meta.change.with({ changingState: null });
            return resolve(0);
          } catch (e) {
            await sails.helpers.meta.change.with({ changingState: null });
            sails.log.error(e);
            return reject(e);
          }
        });
      },
    },

    // Triggered when RadioDJ appears to be frozen
    frozen: {
      count: 0,
      trigger: 30,
      active: false,
      fn: function () {
        // LINT: async required because of sails.js await
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (resolve, reject) => {
          try {
            // If the previous error was over a minute ago, attempt standard recovery. Otherwise, switch RadioDJs.
            if (
              !moment().isBefore(
                moment(sails.models.status.errorCheck.prevError).add(
                  1,
                  "minutes"
                )
              )
            ) {
              sails.log.verbose(
                `No recent error; attempting standard recovery.`
              );
              await sails.models.logs
                .create({
                  attendanceID: sails.models.meta.memory.attendanceID,
                  logtype: "system-frozen",
                  loglevel: "warning",
                  logsubtype: "",
                  logIcon: `fas fa-exclamation-triangle`,
                  title: `Attempted to re-load RadioDJ queue: RadioDJ was frozen.`,
                  event: `Please make sure all RadioDJs are functioning correctly and not freezing up. This could be caused by a corrupted track.`,
                })
                .fetch()
                .tolerate((err) => {
                  sails.log.error(err);
                });
              await sails.helpers.error.post();
            } else {
              // Do not continue if we are in the process of waiting for a status 5 RadioDJ
              if (sails.models.status.errorCheck.waitForGoodRadioDJ) {
                return resolve(0);
              }

              await sails.helpers.meta.change.with({
                changingState: `Switching automation instances due to frozen`,
              });
              sails.log.verbose(`Recent error; switching RadioDJs.`);
              await sails.models.logs
                .create({
                  attendanceID: sails.models.meta.memory.attendanceID,
                  logtype: "system-frozen",
                  loglevel: "danger",
                  logsubtype: "",
                  logIcon: `fas fa-exclamation-triangle`,
                  title: `Switched active RadioDJs: RadioDJ froze multiple times.`,
                  event: `The queue froze multiple times in a short period. Please check to ensure all RadioDJs are functional and not frozen. This could be caused by an audio device issue (check the RadioDJ sound card settings), corrupt track (make sure most recently played audio tracks are okay), or a problem loading the track audio (make sure the storage device is healthy and, for networked drives, that they are connected and accessible by RadioDJ). If all of these are okay, try restarting all RadioDJ instances (and play, then stop, a track upon loading to initialize the REST server).`,
                })
                .fetch()
                .tolerate((err) => {
                  sails.log.error(err);
                });
              await sails.helpers.onesignal.sendMass(
                "emergencies",
                "RadioDJ stopped playing / froze",
                `On ${moment().format(
                  "LLLL"
                )}, the system switched RadioDJs because the active one froze or stopped playing. There might be an audio device issue, corrupt track, or problem reading audio files. Please see DJ Controls.`
              );
              var maps = sails.config.custom.radiodjs
                .filter(
                  (instance) =>
                    instance.name === sails.models.meta.memory.radiodj
                )
                .map(async (instance) => {
                  var status = await sails.models.status.findOne({
                    name: `radiodj-${instance.name}`,
                  });
                  if (status && status.status !== 1) {
                    await sails.helpers.status.modify.with({
                      name: `radiodj-${instance.name}`,
                      label: `RadioDJ ${instance.label}`,
                      status: instance.errorLevel,
                      summary: `queueFrozen: RadioDJ is not responding.`,
                      data: `RadioDJ triggered queueFrozen multiple times; it has probably crashed or froze up. We tried changing active RadioDJs.<br />
                      <strong>TO FIX / CHECK: </strong>
                <ul>
                <li>Make sure RadioDJ is running on all computers that it should be running on. Restart any RadioDJs not working properly.</li>
                <li>Make sure the MariaDB / MySQL database server on each computer running RadioDJ is functional.</li>
                <li>Make sure the REST server on each RadioDJ is online, configured properly, and accessible (RadioDJ Options -> Plugins -> REST server). You may have to play a track in RadioDJ before REST begins working.</li>
                <li>Make sure the network connection on the computers running RadioDJ is good, and that the server can connect.</li>
                </ul>`,
                    });
                  }
                  return true;
                });
              await Promise.all(maps);
              sails.sockets.broadcast("system-error", "system-error", true);
              await sails.helpers.rest.cmd("EnableAutoDJ", 0, 0);
              await sails.helpers.rest.cmd("EnableAssisted", 1, 0);
              await sails.helpers.rest.cmd("StopPlayer", 0, 0);
              var queue = sails.models.meta.automation;
              await sails.helpers.rest.changeRadioDj();
              await sails.helpers.rest.cmd("ClearPlaylist", 1);
              await sails.helpers.error.post(queue);
              await sails.helpers.meta.change.with({ changingState: null });
            }
            return resolve(0);
          } catch (e) {
            await sails.helpers.meta.change.with({ changingState: null });
            sails.log.error(e);
            return reject(e);
          }
        });
      },
    },

    // Check to see if we successfully queued what we needed to in order to return to the sports broadcast
    sportsReturn: {
      count: 0,
      trigger: 6,
      active: false,
      condition: function () {
        sails.models.meta.automation.map(() => {
          // WORK ON THIS
        });
      },
      fn: function () {
        // LINT: async required because of sails.js lint
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (resolve, reject) => {
          try {
            // WORK ON THIS
            return resolve(0);
          } catch (e) {
            return reject(e);
          }
        });
      },
    },

    // Check to see if we successfully queued a station ID. If not, try again.
    stationID: {
      count: 0,
      trigger: 6,
      active: false,
      condition: function () {
        var inQueue = false;
        sails.models.meta.automation
          .filter(
            (track) =>
              sails.config.custom.subcats.IDs.indexOf(
                parseInt(track.IDSubcat)
              ) > -1
          )
          .map(() => {
            inQueue = true;
          });
        return inQueue;
      },
      fn: function () {
        // LINT: async required because of sails.js lint
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (resolve, reject) => {
          try {
            await sails.helpers.songs.queue(
              sails.config.custom.subcats.IDs,
              "Top",
              1
            );
          } catch (e) {
            return reject(e);
          }
          return resolve(1);
        });
      },
    },

    // Check to see if we are in genre rotation and the queue is empty (usually mean no more tracks can play)
    genreEmpty: {
      count: 0,
      trigger: 10,
      active: false,
      fn: function () {
        // LINT: async required because of sails.js lint
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (resolve, reject) => {
          try {
            await sails.helpers.meta.change.with({
              changingState: `Switching to automation via genreEmpty`,
            });
            await sails.helpers.genre.start(null, true);
            await sails.helpers.meta.change.with({ changingState: null });
            return resolve(0);
          } catch (e) {
            await sails.helpers.meta.change.with({ changingState: null });
            sails.models.status.errorCheck.playRandomTracks = true;
            await sails.helpers.status.modify.with({
              name: "calendar-triggerer",
              status: 2,
              summary: `Failed to run scheduled programming and default rotation.`,
              data: `Unable to trigger any automated programming! All scheduled genres/playlists/prerecords failed, and the default rotation also failed. The system is currently falling back to queuing random tracks.<br />
              <strong>TO FIX: </strong>Please make sure all scheduled rotations/playlists/prerecords are set up correctly on the calendar, have the correct RadioDJ event or playlist set, and have tracks that can be queued. Also make sure a manual event named "Default" exists in RadioDJ and triggers a rotation that queues music when nothing else is scheduled.`,
            });
            if (sails.models.meta.memory.state !== "automation_on") {
              await sails.helpers.meta.change.with({
                state: "automation_on",
                genre: "Default",
                playlistPlayed: moment().format("YYYY-MM-DD HH:mm:ss"),
              });
              await sails.helpers.meta.newShow();
              await sails.models.logs
                .create({
                  attendanceID: sails.models.meta.memory.attendanceID,
                  logtype: "sign-on-emergency",
                  loglevel: "orange",
                  logsubtype: "",
                  logIcon: `fas fa-music`,
                  title: `Emergency random music started.`,
                  event:
                    "There were no scheduled automation programs correctly configured. Furthermore, there was no correctly configured Default manual event in RadioDJ to trigger a default rotation. System fell back to queuing random music.",
                })
                .fetch()
                .tolerate((err) => {
                  sails.log.error(err);
                });
            }
            return reject(e);
          }
        });
      },
    },

    changingStateTookTooLong: {
      count: 0,
      trigger: 60,
      active: false,
      fn: function () {
        // LINT: async required because of sails.js lint
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (resolve, reject) => {
          try {
            await sails.models.logs
              .create({
                attendanceID: sails.models.meta.memory.attendanceID,
                logtype: "system-changingstate",
                loglevel: "danger",
                logsubtype: "",
                logIcon: `fas fa-exclamation-triangle`,
                title: `changingState took too long to finish; Node server was terminated.`,
                event: `An error might have occurred in the node server that resulted in the state changing to not complete. Node was terminated as a precaution (if using a process manager such as pm2, it will reboot Node automatically).`,
              })
              .fetch()
              .tolerate((err) => {
                sails.log.error(err);
              });
            await sails.helpers.onesignal.sendMass(
              "emergencies",
              "Node terminated due to ChangingState timeout",
              `On ${moment().format(
                "LLLL"
              )}, the system terminated Node (and hopefully was rebooted by process manager) because ChangingState took more than 60 seconds (application likely froze/hung). Please see DJ Controls.`
            );
            await sails.helpers.emails.queueEmergencies(
              "WWSU Node server terminated - changingState took too long",
              `On ${moment().format(
                "LLLL"
              )}, the system terminated Node (and hopefully was rebooted by process manager) because ChangingState took more than 60 seconds (Node likely froze/hung). Please see DJ Controls / investigate ASAP to make sure the system is in good working order.`,
              true
            );
            await sails.helpers.meta.change.with({ changingState: null });

            // Wait 10 seconds to allow the email to go through. Meanwhile, enable lofi.
            sails.config.custom.basic.maintenance = true;
            setTimeout(() => {
              process.exit(1);
            }, 10000);
          } catch (e) {
            process.exit(1);
          }
        });
      },
    },

    altDone: {
      count: 0,
      trigger: 5,
      active: false,
      condition: function () {},
      fn: function () {
        return new Promise(async (resolve, reject) => {
          try {
            await sails.helpers.rest.cmd(
              "EnableAssisted",
              1,
              undefined,
              sails.config.custom.radiodjs.find(
                (instance) =>
                  instance.name === sails.models.meta.memory.altRadioDJ
              )
            );
            await sails.helpers.meta.change.with({ altRadioDJ: null });
            await sails.helpers.rest.cmd("PausePlayer", 0);
            return resolve(0);
          } catch (e) {
            return reject(e);
          }
        });
      },
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["time", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["time", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`status socket: ${data}`);
    sails.sockets.broadcast("status", "status", data);
    sails.sockets.broadcast(
      `status-${newlyCreatedRecord.name}`,
      "status",
      data
    );
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`status socket: ${data}`);
    sails.sockets.broadcast("status", "status", data);
    sails.sockets.broadcast(`status-${updatedRecord.name}`, "status", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`status socket: ${data}`);
    sails.sockets.broadcast("status", "status", data);
    sails.sockets.broadcast(`status-${destroyedRecord.name}`, "status", data);
    return proceed();
  },
};
