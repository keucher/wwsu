/**
 * Uploads.js
 *
 * @description :: Record of file uploads.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const fs = require("fs");

module.exports = {
  datastore: "wwsusails",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    hostID: {
      type: "number",
      required: true,
    },

    path: {
      type: "string",
      required: true,
      columnType: "text",
    },

    type: {
      type: "string",
      required: true,
      isIn: ["calendar/logo", "calendar/banner", "directors", "djs", "blogs"],
    },
  },

    // MariaDB does not allow ISO strings
    beforeCreate: function (criteria, proceed) {
      ["createdAt", "updatedAt"].map((key) => {
        if (criteria[key])
          criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
      });
  
      proceed();
    },
  
    // MariaDB does not allow ISO strings
    beforeUpdate: function (criteria, proceed) {
      // Do not allow editing the path
      delete criteria.path;

      ["createdAt", "updatedAt"].map((key) => {
        if (criteria[key])
          criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
      });
  
      proceed();
    },

  afterDestroy: function (destroyedRecord, proceed) {
    // Remove the file from the filesystem
    fs.unlinkSync(destroyedRecord.path);

    // Null out all the applicable uploads
    (async () => {
      await sails.models.calendar
        .update({ logo: destroyedRecord.ID }, { logo: null })
        .fetch();
      await sails.models.calendar
        .update({ banner: destroyedRecord.ID }, { banner: null })
        .fetch();
      await sails.models.schedule
        .update({ logo: destroyedRecord.ID }, { logo: null })
        .fetch();
      await sails.models.schedule
        .update({ banner: destroyedRecord.ID }, { banner: null })
        .fetch();
      await sails.models.directors
        .update({ avatar: destroyedRecord.ID }, { avatar: null })
        .fetch();
      await sails.models.djs
        .update({ avatar: destroyedRecord.ID }, { avatar: null })
        .fetch();
      await sails.models.blogs
        .update({ featuredImage: destroyedRecord.ID }, { featuredImage: null })
        .fetch();
    })();

    return proceed();
  },
};
