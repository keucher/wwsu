/**
 * Configstatus.js
 *
 * @description :: Configuration for status alarm triggers.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    /*
      MUSIC LIBRARY - BAD TRACKS
    */

    musicLibraryVerify1: {
      type: "number",
      min: 1,
      defaultsTo: 250,
      description:
        "Music library should go into critical status when this many tracks in RadioDJ are flagged as invalid / corrupt.",
    },

    musicLibraryVerify2: {
      type: "number",
      min: 1,
      defaultsTo: 50,
      description:
        "Music library should go into major status when this many tracks in RadioDJ are flagged as invalid / corrupt.",
    },

    musicLibraryVerify3: {
      type: "number",
      min: 1,
      defaultsTo: 10,
      description:
        "Music library should go into minor status when this many tracks in RadioDJ are flagged as invalid / corrupt.",
    },

    /*
      MUSIC LIBRARY - TRACKS THAT ARE TOO LONG
    */

    musicLibraryLong1: {
      type: "number",
      min: 1,
      defaultsTo: 250,
      description:
        "Music library should go into critical status when this many tracks in the music system category are longer than breakCheck minutes long.",
    },

    musicLibraryLong2: {
      type: "number",
      min: 1,
      defaultsTo: 50,
      description:
        "Music library should go into major status when this many tracks in the music system category are longer than breakCheck minutes long.",
    },

    musicLibraryLong3: {
      type: "number",
      min: 1,
      defaultsTo: 10,
      description:
        "Music library should go into minor status when this many tracks in the music system category are longer than breakCheck minutes long.",
    },

    /*
      SERVER - 1 MINUTE CPU LOAD
      Note: On critical (1) status, web and API requests will be rejected with a 503 service unavailable.
      Note: On major (2) status, web and API requests will be throttled / put into a queue.
    */

    server1MinLoad1: {
      type: "number",
      min: 0,
      defaultsTo: 8,
      description:
        "Server should go into critical status when the 1-minute CPU load exceeds this value (generally, set this to number of CPU cores * 8).",
    },

    server1MinLoad2: {
      type: "number",
      min: 0,
      defaultsTo: 4,
      description:
        "Server should go into major status when the 1-minute CPU load exceeds this value (generally, set this to number of CPU cores * 4).",
    },

    server1MinLoad3: {
      type: "number",
      min: 0,
      defaultsTo: 2,
      description:
        "Server should go into minor status when the 1-minute CPU load exceeds this value (generally, set this to number of CPU cores * 2).",
    },

    /*
      SERVER - 5 MINUTE CPU LOAD
      Note: On critical (1) status, web and API requests will be rejected with a 503 service unavailable.
      Note: On major (2) status, web and API requests will be throttled / put into a queue.
    */

    server5MinLoad1: {
      type: "number",
      min: 0,
      defaultsTo: 6,
      description:
        "Server should go into critical status when the 5-minute CPU load exceeds this value (generally, set this to number of CPU cores * 6).",
    },

    server5MinLoad2: {
      type: "number",
      min: 0,
      defaultsTo: 3,
      description:
        "Server should go into major status when the 5-minute CPU load exceeds this value (generally, set this to number of CPU cores * 3).",
    },

    server5MinLoad3: {
      type: "number",
      min: 0,
      defaultsTo: 1.5,
      description:
        "Server should go into minor status when the 5-minute CPU load exceeds this value (generally, set this to number of CPU cores * 1.5).",
    },

    /*
      SERVER - 15 MINUTE CPU LOAD
      Note: On critical (1) status, web and API requests will be rejected with a 503 service unavailable.
      Note: On major (2) status, web and API requests will be throttled / put into a queue.
    */

    server15MinLoad1: {
      type: "number",
      min: 0,
      defaultsTo: 4,
      description:
        "Server should go into critical status when the 15-minute CPU load exceeds this value (generally, set this to number of CPU cores * 4).",
    },

    server15MinLoad2: {
      type: "number",
      min: 0,
      defaultsTo: 2,
      description:
        "Server should go into major status when the 15-minute CPU load exceeds this value (generally, set this to number of CPU cores * 2).",
    },

    server15MinLoad3: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Server should go into critical status when the 15-minute CPU load exceeds this value (generally, set this to number of CPU cores).",
    },

    /*
      SERVER - FREE MEMORY AVAILABLE
      Note: On critical (1) status, web and API requests will be rejected with a 503 service unavailable.
      Note: On major (2) status, web and API requests will be throttled / put into a queue.
    */

    serverMemory1: {
      type: "number",
      min: 0,
      defaultsTo: 1000 * 1000 * 64,
      description:
        "Server should go into critical status when the free / available memory (RAM) (in bytes) drops below this value (generally, set this to 5% total RAM capacity).",
    },

    serverMemory2: {
      type: "number",
      min: 0,
      defaultsTo: 1000 * 1000 * 128,
      description:
        "Server should go into major status when the free / available memory (RAM) (in bytes) drops below this value (generally, set this to 10% total RAM capacity).",
    },

    serverMemory3: {
      type: "number",
      min: 0,
      defaultsTo: 1000 * 1000 * 512,
      description:
        "Server should go into minor status when the free / available memory (RAM) (in bytes) drops below this value (generally, set this to 25% total RAM capacity).",
    },
  },

  initialize: async function () {
    // Initialize config
    sails.config.custom.status = await sails.models.configstatus.findOrCreate(
      { ID: 1 },
      { ID: 1 }
    );
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-status socket: ${data}`);
    sails.sockets.broadcast("config-status", "config-status", data);

    // Update config
    sails.config.custom.status = newlyCreatedRecord;

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-status socket: ${data}`);
    sails.sockets.broadcast("config-status", "config-status", data);

    // Update config
    sails.config.custom.status = updatedRecord;

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-status socket: ${data}`);
    sails.sockets.broadcast("config-status", "config-status", data);

    // If the master record was destroyed, treat as a "reset to default" and re-create it
    if (destroyedRecord.ID === 1) {
      (async () => {
        sails.config.custom.status =
          await sails.models.configstatus.findOrCreate({ ID: 1 }, { ID: 1 });

        proceed();
      })();
    }
  },
};
