/**
 * Blogs.js
 *
 * @description :: Blogs table.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

// Properties to return in websockets; we're ignoring some to save on data.
const keysToReturn = [
  "ID",
  "createdAt",
  "updatedAt",
  "active",
  "needsApproved",
  "categories",
  "title",
  "starts",
  "expires",
];

module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    active: {
      type: "boolean",
      defaultsTo: true,
    },

    needsApproved: {
      type: "boolean",
      defaultsTo: true,
    },

    notified: {
      type: "boolean",
      defaultsTo: false,
      description:
        "This is set to true when the blog post was published and people have been notified.",
    },

    categories: {
      type: "json",
      required: true,
    },

    tags: {
      type: "json",
    },

    title: {
      type: "string",
      required: true,
      maxLength: 64,
    },

    author: {
      type: "number",
      allowNull: true,
      description: "Reference to DJs",
    },

    featuredImage: {
      type: "number",
      allowNull: true,
      description: "Reference to Uploads",
    },

    summary: {
      type: "string",
      required: true,
      maxLength: 255,
    },

    contents: {
      type: "string",
      required: true,
      columnType: "text",
    },

    starts: {
      type: "ref",
      columnType: "datetime",
    },

    expires: {
      type: "ref",
      columnType: "datetime",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["starts", "expires", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["starts", "expires", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    // Transmit that we have a new blog post to web visitors if it's active.
    if (
      newlyCreatedRecord.active &&
      !newlyCreatedRecord.needsApproved &&
      (!newlyCreatedRecord.starts ||
        moment().isSameOrAfter(moment(newlyCreatedRecord.starts))) &&
      (!newlyCreatedRecord.expires ||
        moment().isBefore(moment(newlyCreatedRecord.expires)))
    )
      sails.sockets.broadcast("blogs-web", "blogs-web", {
        insert: {
          ID: newlyCreatedRecord.ID,
          categories: newlyCreatedRecord.categories,
        },
      });

    // Save on data by omitting some details for regular blogs fetching; fetch these via blogs/get-one instead.
    for (let key in newlyCreatedRecord) {
      if (!Object.prototype.hasOwnProperty.call(newlyCreatedRecord, key))
        continue;
      if (keysToReturn.indexOf(key) === -1) delete newlyCreatedRecord[key];
    }

    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`blogs socket: ${data}`);
    sails.sockets.broadcast("blogs", "blogs", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    // Transmit that we have an updated blog post to web visitors if it's active. Clients should fetch new blog post or display the blog has been updated.
    if (
      updatedRecord.active &&
      !updatedRecord.needsApproved &&
      (!updatedRecord.starts ||
        moment().isSameOrAfter(moment(updatedRecord.starts))) &&
      (!updatedRecord.expires ||
        moment().isBefore(moment(updatedRecord.expires)))
    )
      sails.sockets.broadcast("blogs-web", "blogs-web", {
        update: {
          ID: updatedRecord.ID,
          categories: updatedRecord.categories,
        },
      });

    // Save on data by omitting some details; fetch these via blogs/get-one instead.
    for (let key in updatedRecord) {
      if (!Object.prototype.hasOwnProperty.call(updatedRecord, key)) continue;
      if (keysToReturn.indexOf(key) === -1) delete updatedRecord[key];
    }

    var data = { update: updatedRecord };
    sails.log.silly(`blogs socket: ${data}`);
    sails.sockets.broadcast("blogs", "blogs", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`blogs socket: ${data}`);
    sails.sockets.broadcast("blogs", "blogs", data);

    (async () => {
      // If blog had a featured image, delete it.
      if (destroyedRecord.featuredImage)
        await sails.models.uploads.destroyOne({
          ID: destroyedRecord.featuredImage,
        });
    })();

    return proceed();
  },
};
