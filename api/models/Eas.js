/**
 * Eas.js
 *
 * @description :: Internal Emergency Alert System.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    source: {
      type: "string",
    },

    reference: {
      type: "string",
    },

    alert: {
      type: "string",
    },

    information: {
      type: "string",
      columnType: "text",
    },

    severity: {
      type: "string",
    },

    color: {
      type: "string",
      defaultsTo: "#FFFF00",
    },

    counties: {
      type: "string",
      defaultsTo: "Wright State University",
      columnType: "text",
    },

    starts: {
      type: "ref",
      columnType: "datetime",
    },

    expires: {
      type: "ref",
      columnType: "datetime",
    },

    needsAired: {
      type: "boolean",
      defaultsTo: false,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["starts", "expires", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["starts", "expires", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  activeCAPS: [], // Array of active NWS alerts, cleared at each check, to help determine maintenance / cleaning up of NWS alerts.

  pendingAlerts: {}, // An object of alerts pending to be processed, key is unique ID, value is an object of alert information (includes property _new, which is true if the record is to be inserted rather than updated)

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`eas socket: ${data}`);
    sails.sockets.broadcast("eas", "eas", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`eas socket: ${data}`);
    sails.sockets.broadcast("eas", "eas", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`eas socket: ${data}`);
    sails.sockets.broadcast("eas", "eas", data);
    return proceed();
  },
};
