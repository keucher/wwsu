/**
 * Configsports.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
      description:
        "Name of the sport. MUST exist as a subcategory in each of the RadioDJ categories of Sports Openers, Sports Liners, Sports Returns, and Sports Closers for it to work properly.",
    },
  },

  initialize: async function () {
    // Load full configuration
    sails.config.custom.sports = await sails.models.configsports.find();

    await sails.helpers.config.basic.sports.reload();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-sports socket: ${data}`);
    sails.sockets.broadcast("config-sports", "config-sports", data);

    // Reload configuration
    (async () => {
      sails.config.custom.sports = await sails.models.configsports.find();

      await sails.helpers.config.basic.sports.reload();

      proceed();
    })();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-sports socket: ${data}`);
    sails.sockets.broadcast("config-sports", "config-sports", data);

    (async () => {
      sails.config.custom.sports = await sails.models.configsports.find();

      await sails.helpers.config.basic.sports.reload();

      proceed();
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-sports socket: ${data}`);
    sails.sockets.broadcast("config-sports", "config-sports", data);

    (async () => {
      sails.config.custom.sports = await sails.models.configsports.find();

      await sails.helpers.config.basic.sports.reload();

      proceed();
    })();
  },
};
