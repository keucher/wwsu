/**
 * Category.js
 *
 * @description :: Categories in radioDJ.
 */

module.exports = {
  datastore: "radiodj",
  // migrate: "safe", // NOT SUPPORTED by Sails :(
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      maxLength: 100,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  initialize: async function () {
    // Populate missing hard-coded categories
    let maps = [
      "Show Openers",
      "Show Returns",
      "Show Closers",
      "Sports Openers",
      "Sports Liners",
      "Sports Closers",
      "Sports Returns",
      "PSAs",
      "Underwritings",
      "Liners",
      "EAS",
    ].map(async (cat) => {
      await sails.models.category.findOrCreate(
        {
          name: cat,
        },
        { name: cat }
      );
    });
    await Promise.all(maps);
  },
};
