/**
 * Directors.js
 *
 * @description :: A model containing all of the station directors.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
    },

    login: {
      type: "string",
      required: true,
    },

    email: {
      type: "string",
      allowNull: true,
    },

    admin: {
      type: "boolean",
      defaultsTo: false,
    },

    assistant: {
      type: "boolean",
      defaultsTo: false,
    },

    avatar: {
      type: "number",
      allowNull: true,
      description: "ID reference to uploads model",
    },

    position: {
      type: "string",
      defaultsTo: "Unknown",
    },

    present: {
      type: "number",
      defaultsTo: 0,
      min: 0,
      max: 2, // 1 = in office, 2 = in remote
    },

    since: {
      type: "ref",
      columnType: "datetime",
    },

    profile: {
      type: "string",
      defaultsTo: "",
      columnType: "text",
    },

    canSendEmails: {
      type: "boolean",
      defaultsTo: false,
    },

    emailEmergencies: {
      type: "boolean",
      defaultsTo: false,
    },

    emailCalendar: {
      type: "boolean",
      defaultsTo: false,
    },

    emailWeeklyAnalytics: {
      type: "boolean",
      defaultsTo: false,
    },

    emailFlags: {
      type: "boolean",
      defaultsTo: false,
    },

    emailBlogApprovals: {
      type: "boolean",
      defaultsTo: false,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["since", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["since", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  beforeDestroy: function (criteria, proceed) {
    if (criteria.ID === 1)
      // Do not allow destroying the master member.
      throw new Error("Not allowed to destroy director record 1.");
    return proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    delete newlyCreatedRecord.login;
    let data = { insert: newlyCreatedRecord };
    sails.log.silly(`directors socket: ${data}`);
    sails.sockets.broadcast("directors", "directors", data);

    // Create or activate office-hours calendar event for this director
    (async () => {
      sails.models.calendar
        .findOrCreate(
          { type: "office-hours", director: newlyCreatedRecord.ID },
          {
            type: "office-hours",
            active: true,
            priority: sails.models.calendar.calendardb.getDefaultPriority({
              type: "office-hours",
            }),
            hosts: newlyCreatedRecord.name,
            name: newlyCreatedRecord.name,
            director: newlyCreatedRecord.ID,
            lastAired: moment().toISOString(true),
          }
        )
        .exec(async (err, record, wasCreated) => {
          if (err) {
            throw err;
          }
          if (!wasCreated && record)
            await sails.models.calendar
              .update(
                { ID: record.ID },
                {
                  type: "office-hours",
                  active: true,
                  priority: sails.models.calendar.calendardb.getDefaultPriority(
                    { type: "office-hours" }
                  ),
                  hosts: newlyCreatedRecord.name,
                  name: newlyCreatedRecord.name,
                  director: newlyCreatedRecord.ID,
                }
              )
              .fetch();
        });
    })();

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    delete updatedRecord.login;
    let data = { update: updatedRecord };
    sails.log.silly(`directors socket: ${data}`);
    sails.sockets.broadcast("directors", "directors", data);
    let records;
    let temp;
    let temp2;

    // Update host data in calendar and schedule
    temp = (async () => {
      records = await sails.models.calendar.find({
        director: updatedRecord.ID,
        active: true,
      });
      if (records.length > 0) {
        records.map(async (record) => {
          try {
            let hosts = await sails.helpers.calendar.generateHosts(record);
            await sails.models.calendar
              .update({ ID: record.ID }, { hosts: hosts || "Unknown Hosts" })
              .fetch();
          } catch (e) {
            sails.log.error(e);
          }
        });
      }

      records = await sails.models.schedule.find({
        director: updatedRecord.ID,
      });
      if (records.length > 0) {
        records.map(async (record) => {
          try {
            let hosts = await sails.helpers.calendar.generateHosts(record);
            await sails.models.schedule
              .update({ ID: record.ID }, { hosts: hosts })
              .fetch();
          } catch (e) {
            sails.log.error(e);
          }
        });
      }
    })();

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    let data = { remove: destroyedRecord.ID };
    sails.log.silly(`directors socket: ${data}`);
    sails.sockets.broadcast("directors", "directors", data);
    let records;
    let maps;

    (async () => {
      // Deactivate office hours events for this director. Update all other events using this director ID to null director.
      records = await sails.models.calendar.find({
        director: destroyedRecord.ID,
        active: true,
      });
      if (records.length > 0) {
        records.map(async (record) => {
          try {
            if (record.type === "office-hours") {
              await sails.models.calendar
                .update({ ID: record.ID }, { active: false })
                .fetch();
            } else {
              record.director = null;
              let hosts = await sails.helpers.calendar.generateHosts(record);
              await sails.models.calendar
                .update(
                  { ID: record.ID },
                  { hosts: hosts || "Unknown Hosts", director: null }
                )
                .fetch();
            }
          } catch (e) {}
        });
      }

      records = await sails.models.schedule.find({
        director: destroyedRecord.ID,
      });
      if (records.length > 0) {
        records.map(async (record) => {
          try {
            if (record.type === "office-hours") {
              await sails.models.schedule.destroy({ ID: record.ID }).fetch();
            } else {
              record.director = null;
              let hosts = await sails.helpers.calendar.generateHosts(record);
              await sails.models.schedule
                .update({ ID: record.ID }, { hosts: hosts, director: null })
                .fetch();
            }
          } catch (e) {}
        });
      }

      // Find an org member with the same name. If found, turn admin to false for all hosts belonging to that org member.
      let dj = await sails.models.djs
        .find({ realName: destroyedRecord.name })
        .limit(1);
      if (dj[0]) {
        await sails.models.hosts
          .update({ belongsTo: dj[0].ID }, { admin: false })
          .fetch();
      }

      // Remove avatar file
      if (destroyedRecord.avatar)
        await sails.models.uploads.destroyOne({ ID: destroyedRecord.avatar });
    })();

    return proceed();
  },
};
