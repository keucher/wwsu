module.exports = {
  friendlyName: "nowplaying/",

  description: "Nowplaying page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/nowplaying",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `See what is currently on the air on WWSU and what played recently.`,
        author: "Now Playing on WWSU - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/nowplaying`,
        "og:type": "website",
        "og:description": `See what is currently on the air on WWSU and what played recently.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};