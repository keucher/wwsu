module.exports = {
  friendlyName: "bookings/",

  description: "Bookings page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "bookings/bookings",
    },
  },

  fn: async function (inputs) {
    // Bookings interface should refresh itself as well as display signs.
    sails.sockets.join(this.req, "display-refresh");

    // All done.
    return {
      layout: "bookings/layout",
    };
  },
};
