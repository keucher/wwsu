module.exports = {
  friendlyName: "members/",

  description: "Members page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/members",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `View the profiles of all our current org members and the radio shows they host or co-host.`,
        author: "Members / DJs - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/members`,
        "og:type": "website",
        "og:description": `View the profiles of all our current org members and the radio shows they host or co-host.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};