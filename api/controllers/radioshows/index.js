module.exports = {
  friendlyName: "radioshows/",

  description: "Radio shows page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/radioshows",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `View the radio shows currently airing on WWSU along with their regular broadcast schedule.`,
        author: "Radio Shows - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/radioshows`,
        "og:type": "website",
        "og:description": `View the radio shows currently airing on WWSU along with their regular broadcast schedule.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};