module.exports = {
  friendlyName: "blog/",

  description: "Get a blog post page",

  inputs: {
    id: {
      required: true,
      type: "number",
      description: "The ID of the blog post",
    },

    monikor: {
      type: "string",
      description: "Optional monikor",
    },
  },

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/blog",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("blog/index called.");
    sails.log.debug(inputs);

    let post = await sails.models.blogs.findOne({ ID: inputs.id });
    if (!post) throw "notFound";
    sails.log.debug("post located.");

    let author;
    if (post.author) {
      author = await sails.models.djs.findOne({ ID: post.author });
      sails.log.debug("author located.");
    }

    sails.log.debug("Sending layout / meta");

    return {
      layout: "website/layout",
      postID: post.ID,
      meta: {
        description: post.summary,
        keywords: post.tags ? post.tags.join() : undefined,
        author: author ? author.realName : undefined,
        "og:title": `${post.title} - WWSU 106.9 FM`,
        "og:image": post.featuredImage
          ? `${sails.config.custom.baseUrl}/api/uploads/${post.featuredImage}`
          : undefined,
        "og:url": `${sails.config.custom.baseUrl}/blog/${post.ID}${
          inputs.monikor ? `/${inputs.monikor}` : ``
        }`,
        "og:type": "article",
        "og:description": post.summary,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
