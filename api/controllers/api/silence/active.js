module.exports = {
  friendlyName: "PUT api/silence/active",

  description:
    "DJ Controls should call this endpoint every minute whenever silence is detected.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    /* NOTE! status.data for silence detected MUST contain "Silence / very low audio detected" so DJ Controls alerts hosts. */

    sails.log.debug("Controller silence/active called.");

    // Bail if in maintenance mode
    if (sails.config.custom.basic.maintenance)
      throw { forbidden: "Request rejected; system is in maintenance mode." };

    // Log it
    await sails.models.logs
      .create({
        attendanceID: sails.models.meta.memory.attendanceID,
        logtype: "silence",
        loglevel: "orange",
        logsubtype: sails.models.meta.memory.show,
        logIcon: `fas fa-volume-off`,
        title: `Silence detection alarm triggered.`,
        event: `Silence / very low audio detected for several seconds. Please investigate the cause and ensure this does not happen again.`,
      })
      .fetch()
      .tolerate(() => {});

    // If in altRadioDJ, assume it is a problem with that. Stop and resume current RadioDJ. Add another log.
    if (sails.models.meta.memory.altRadioDJ) {
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "silence-altradiodj",
          loglevel: "orange",
          logsubtype: sails.models.meta.memory.show,
          logIcon: `fas fa-volume-off`,
          title: `Silence occurred during an alert broadcast.`,
          event: `Silence / very low audio detected when trying to broadcast an alert on RadioDJ ${sails.models.meta.memory.altRadioDJ}. Make sure this RadioDJ can go on the air at any time necessary and the audio channels are on for this RadioDJ.`,
        })
        .fetch()
        .tolerate(() => {});

      await sails.helpers.meta.change({ altRadioDJ: null });
      await sails.helpers.rest.cmd("PausePlayer", 0);
      await sails.helpers.error.reset("altDone");
    }

    // If we are in automation, and prevSilence is less than 3 minutes ago, assume an audio issue and switch RadioDJs
    if (
      sails.models.status.errorCheck.prevSilence &&
      moment().isBefore(
        moment(sails.models.status.errorCheck.prevSilence).add(3, "minutes")
      ) &&
      sails.models.meta.memory.state.startsWith("automation_")
    ) {
      await sails.helpers.meta.change.with({
        changingState: `Multiple silence alarms; switching automation instances`,
      });

      // Log the problem
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "silence-switch",
          loglevel: "danger",
          logsubtype: "",
          logIcon: `fas fa-volume-mute`,
          title: `System changed active RadioDJ due to multiple silence alarms within 3 minutes.`,
          event: `Please check to make sure all RadioDJs are functioning correctly and audio is not muted or very quiet from all RadioDJ sources.`,
        })
        .fetch()
        .tolerate((err) => {
          sails.log.error(err);
        });
      await sails.helpers.onesignal.sendMass(
        "emergencies",
        "Silence Detection Triggered Multiple Times",
        `System had switched automation instances on ${moment().format(
          "LLLL"
        )} because the silence detection system triggered multiple times. Please check DJ Controls.`
      );

      await sails.helpers.status.modify.with({
        name: `silence`,
        status: 1,
        label: `Silence`,
        summary: `Silence detected multiple times! There may be a problem with RadioDJ. Switched RadioDJ instances.`,
        data: `Silence / very low audio detected multiple times! Switched RadioDJ instances. Silence detection was also triggered less than 3 minutes ago.
          <br /><strong>TO FIX:</strong>
          <ul>
            <li>Check to ensure all RadioDJs are working and audio levels are at expected levels. System switched RadioDJs as a precaution.</li>
            <li>If audio is going over the air, check the Audio Settings on the DJ Controls responsible for silence detection. Make sure the correct devices are set for silence detection and the devices are receiving on-air audio. For silence detection settings, make sure the threshold is not too high or the delay too short.</li>
          </ul>`,
      });

      // Find a RadioDJ to switch to
      if (sails.config.custom.radiodjs.length) {
        let maps = sails.config.custom.radiodjs
          .filter(
            (instance) => instance.name === sails.models.meta.memory.radiodj
          )
          .map(async (instance) => {
            let status = await sails.models.status.findOne({
              name: `radiodj-${instance.name}`,
            });
            if (status && status.status !== 1) {
              await sails.helpers.status.modify.with({
                name: `radiodj-${instance.name}`,
                label: `RadioDJ ${instance.label}`,
                status: 2,
                summary: `Silence detection was triggered on this RadioDJ.`,
                data: `Silence detection triggered multiple times. This RadioDJ might not be outputting audio.
                <br /><strong>TO FIX: </strong>
                <ul>
                  <li>Check the RadioDJ Options -> Sound devices to make sure they are set correctly.</li>
                  <li>Make sure audio levels (such as the mixer board) are set to optimum levels.</li>
                  <li>Make sure this computer has a good network connection and has access to the music library.</li>
                  <li>Try restarting RadioDJ, or the computer, if it is still not working.</li>
                </ul>`,
              });
            }
            return true;
          });
        await Promise.all(maps);

        sails.sockets.broadcast("system-error", "system-error", true);

        // Only wait 1 second for these commands in case radioDJ is frozen; we don't want too much additional silence.
        await sails.helpers.rest.cmd("EnableAutoDJ", 0, 1000);
        await sails.helpers.rest.cmd("EnableAssisted", 1, 1000);
        await sails.helpers.rest.cmd("StopPlayer", 0, 1000);

        // Switch RadioDJs and re-queue the queue
        let queue = sails.models.meta.automation;
        await sails.helpers.rest.changeRadioDj();
        await sails.helpers.rest.cmd("ClearPlaylist", 1);
        await sails.helpers.error.post(queue);
        await sails.helpers.meta.change.with({ changingState: null });
      }
    }

    // else If a track is playing in RadioDJ, skip it and log it (provided we are not in a prerecord)
    else if (
      typeof sails.models.meta.automation[0] !== "undefined" &&
      parseInt(sails.models.meta.automation[0].ID) !== 0 &&
      sails.models.meta.memory.state !== "prerecord_on"
    ) {
      // Add a log about the track
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "silence-track",
          loglevel: "warning",
          logsubtype: sails.models.meta.memory.show,
          logIcon: `fas fa-forward`,
          title: `Track was skipped due to silence detection.`,
          event: `Track: ${sails.models.meta.automation[0].ID} (${sails.models.meta.automation[0].Artist} - ${sails.models.meta.automation[0].Title})<br />Please check this track to ensure it does not have more consecutive silence / very low audio than what silence detection is set at.`,
        })
        .fetch()
        .tolerate(() => {});

      // Skip the track if there's a track playing in automation and there's another track queued
      if (typeof sails.models.meta.automation[1] !== "undefined") {
        await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
      }

      // Activate status issue
      await sails.helpers.status.modify.with({
        name: `silence`,
        status: 2,
        label: `Silence`,
        summary: `Silence detected! Track ${sails.models.meta.automation[0].Artist} - ${sails.models.meta.automation[0].Title} was skipped.`,
        data: `Silence / very low audio detected! Track skipped due to potential silence.
          <br /><strong>TO FIX:</strong>
          <ul>
            <li>Check track ${sails.models.meta.automation[0].ID} (${sails.models.meta.automation[0].Artist} - ${sails.models.meta.automation[0].Title}) in RadioDJ by looking at its waveform (you can do this in Tracks Manager -> search for the track -> right-click and click cue editor); ensure it does not have excessive silence / low audio that would exceed the silence detection settings in DJ Controls.</li>
            <li>If audio is going over the air, check the Audio Settings on the DJ Controls responsible for silence detection. Make sure the correct audio devices are selected for silence detection and are receiving on-air audio. Check the silence detection settings to ensure the threshold is not too high and the delay not too short.</li>
          </ul>`,
      });
    }

    // else If we are not in automation, and prevSilence is less than 2 minutes ago, assume irresponsible DJ and automatically end the show (but go into automation_break).
    else if (
      sails.models.status.errorCheck.prevSilence &&
      moment().isBefore(
        moment(sails.models.status.errorCheck.prevSilence).add(2, "minutes")
      ) &&
      !sails.models.meta.memory.state.startsWith("automation_")
    ) {
      await sails.helpers.meta.change.with({
        changingState: `Ending current broadcast due to no audio`,
      });

      // Log the problem
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "silence-terminated",
          loglevel: "danger",
          logsubtype: sails.models.meta.memory.show,
          logIcon: `fas fa-microphone-slash`,
          title: `Broadcast terminated: Silence detection triggered twice within 2 minutes.`,
          event: `This is likely the responsibility of the DJ / host. System terminated the broadcast and went to automation to stop the silence. Please investigate and, if necessary, speak with the host(s) about ensuring proper volume levels and avoiding on-air silence.<br />Broadcast: ${sails.models.meta.memory.show}`,
        })
        .fetch()
        .tolerate((err) => {
          sails.log.error(err);
        });
      await sails.helpers.onesignal.sendMass(
        "emergencies",
        "Multiple Silences During Broadcast",
        `${
          sails.models.meta.memory.show
        } was terminated automatically on ${moment().format(
          "LLLL"
        )} because of a long period of silence or multiple silences in a short time. Please talk with the DJ about avoiding silence on the air.`
      );

      await sails.helpers.status.modify.with({
        name: `silence`,
        status: 1,
        label: `Silence`,
        summary: `Silence detected multiple times! Current broadcast was terminated.`,
        data: `Silence / very low audio detected! Silence detection was triggered more than once in the last 2 minutes. The broadcast was terminated as a result.
          <br /><strong>TO FIX:</strong>
          <ul>
            <li>Review the recordings for the show to see if the show host(s) might have made an error and had excessive silence. Reprimend them as necessary if so.</li>
            <li>If audio is going over the air, check the Audio Settings on the DJ Controls responsible for silence detection. Make sure the correct audio devices are selected for silence detection and are receiving on-air audio. Check the silence detection settings to ensure the threshold is not too high and the delay not too short.</li>
          </ul>`,
      });

      await sails.helpers.state.automation(true);

      // Otherwise, do not do anything except update status.
    } else {
      await sails.helpers.status.modify.with({
        name: `silence`,
        status: 2,
        label: `Silence`,
        summary: `Silence detected!`,
        data: `Silence / very low audio detected!
          <br /><strong>TO FIX:</strong>
          <ul>
          <li>Review the recordings for the show to see if the show host(s) might have made an error and had excessive silence. Reprimend them as necessary if so.</li>
          <li>If audio is going over the air, check the Audio Settings on the DJ Controls responsible for silence detection. Make sure the correct audio devices are selected for silence detection and are receiving on-air audio. Check the silence detection settings to ensure the threshold is not too high and the delay not too short.</li>
        </ul>`,
      });
    }

    sails.models.status.errorCheck.prevSilence = moment();
    return;
  },
};
