module.exports = {
  friendlyName: "POST api / announcements",

  description: "Add an announcement.",

  inputs: {
    type: {
      type: "string",
      required: true,
      description:
        "The type of announcement; determines which subsystems receive the announcement.",
    },

    level: {
      type: "string",
      required: true,
      isIn: ["danger", "warning", "info", "success", "secondary"],
      description:
        "Announcement warning level. Must be danger, warning, info, or trivial.",
    },

    title: {
      type: "string",
      required: true,
      description: "The announcement title.",
    },

    announcement: {
      type: "string",
      required: true,
      description: "The announcement text.",
    },

    displayTime: {
      type: "number",
      defaultsTo: 15,
      min: 5,
      max: 60,
    },

    starts: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the announcement starts. Defaults to now. Recommended ISO string.`,
    },

    expires: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the announcement expires. Defaults to the year 3000. Recommended ISO string.`,
    },
  },

  fn: async function (inputs) {
    // Add the announcement to the database
    return await sails.models.announcements
      .create({
        type: inputs.type,
        level: inputs.level,
        title: inputs.title,
        announcement: inputs.announcement,
        displayTime: inputs.displayTime,
        starts:
          inputs.starts !== null && typeof inputs.starts !== "undefined"
            ? moment(inputs.starts).format("YYYY-MM-DD HH:mm:ss")
            : moment().subtract(1, "minutes").format("YYYY-MM-DD HH:mm:ss"),
        expires:
          inputs.expires !== null && typeof inputs.expires !== "undefined"
            ? moment(inputs.expires).format("YYYY-MM-DD HH:mm:ss")
            : moment({ year: 3000 }).format("YYYY-MM-DD HH:mm:ss"),
      })
      .fetch();
  },
};
