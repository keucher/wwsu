module.exports = {
  friendlyName: "POST api/state/sports",

  description: "Request to begin a sports broadcast.",

  inputs: {
    topic: {
      type: "string",
      defaultsTo: "",
      description:
        "A string containing a short blurb about this sports broadcast.",
    },

    sport: {
      type: "string",
      required: true,
      custom: (value) => {
        return sails.config.custom.basic.sports.indexOf(value) !== -1;
      },
      description: "Name of the sport that is being broadcast.",
    },

    webchat: {
      type: "boolean",
      defaultsTo: true,
      description:
        "Should the web chat be enabled during this broadcast? Defaults to true.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // If the specified host is not locked down to onair, deny the request
    if (this.req.payload.lockDown !== "onair") {
      throw {
        forbidden:
          "This host does not have permission to start sports broadcasts produced in-studio; please set the lockdown setting for this host to 'onair' to allow live broadcasts.",
      };
    }

    // Make sure member has permission to start a live broadcast
    if (
      !(await sails.helpers.checkPermissionByLockdown(
        this.req.payload.ID,
        "live"
      ))
    ) {
      throw {
        forbidden:
          "Member/director logged in to this host does not have permission to start live broadcasts. Add this permission to the org member starting the broadcast.",
      };
    }

    // Do not continue if not in automation mode; client should request automation before requesting sports
    if (
      !sails.models.meta.memory.state.startsWith("automation_") &&
      !sails.models.meta.memory.state.startsWith("prerecord_")
    ) {
      throw {
        forbidden:
          "Cannot start a live sports broadcast outside of automation or prerecord. Please go to automation first.",
      };
    }

    // Block the request if we are changing states right now
    if (sails.models.meta.memory.changingState !== null) {
      throw {
        conflict:
          "Request rejected; system is currently changing states. Please try again in several seconds.",
      };
    }

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict:
          "Request rejected; currently airing an alert. Please try again in a minute.",
      };
    }

    try {
      // Lock so that any other state changing requests are blocked until we are done
      await sails.helpers.meta.change.with({
        changingState: `Switching to sports`,
      });

      // Filter profanity
      if (inputs.topic !== "") {
        inputs.topic = await sails.helpers.filterProfane(inputs.topic);
        inputs.topic = await sails.helpers.sanitize(inputs.topic);
      }

      // Skip RadioDJ stuff and go immediately to live if not using RadioDJ.
      if (!sails.config.custom.radiodjs.length) {
        await sails.helpers.meta.change.with({
          hostDJ: null,
          cohostDJ1: null,
          cohostDJ2: null,
          cohostDJ3: null,
          state: "sports_on",
          host: this.req.payload.ID,
          show: inputs.sport,
          topic: inputs.topic,
          trackStamp: null,
          webchat: inputs.webchat,
        });
        await sails.helpers.meta.newShow();
        return;
      }

      // Set meta to prevent accidental messages in DJ Controls
      await sails.helpers.meta.change.with({
        state: "automation_sports",
        host: this.req.payload.ID,
        show: inputs.sport,
        topic: inputs.topic,
        trackStamp: null,
      });

      // await sails.helpers.error.count('goLive');

      // Operation: Remove all music tracks, queue a clockwheel break, queue an opener if one exists for this sport, and start the next track if current track is music.
      await sails.helpers.rest.cmd("EnableAutoDJ", 0);
      await sails.helpers.songs.remove(
        true,
        sails.config.custom.subcats.noClearShow,
        false,
        true
      );
      await sails.helpers.rest.cmd("EnableAssisted", 1);
      await sails.helpers.break.checkClockwheel(false);
      await sails.helpers.break.executeArray(
        sails.config.custom.breaks.filter(
          (record) => record.type === "sports" && record.subtype === "start"
        ),
        "Sports Start"
      );

      // Queue a Sports opener if there is one
      if (typeof sails.config.custom.sportscats[inputs.sport] !== "undefined") {
        await sails.helpers.songs.queue(
          [sails.config.custom.sportscats[inputs.sport]["Sports Openers"]],
          "Bottom",
          1
        );
      }

      await sails.helpers.rest.cmd("EnableAssisted", 0);

      let queueLength = await sails.helpers.songs.calculateQueueLength();

      // If the radioDJ queue is unacceptably long, try to reduce it.
      if (queueLength >= sails.config.custom.basic.maxQueueSports) {
        await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
        await sails.helpers.songs.remove(
          true,
          sails.config.custom.subcats.noClearShow,
          false,
          true
        );
        if (
          sails.config.custom.subcats.noClearShow &&
          sails.config.custom.subcats.noClearShow.indexOf(
            sails.models.meta.memory.trackIDSubcat
          ) === -1
        ) {
          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
        } // Skip currently playing track if it is not a noClearShow track
      }

      // Add a station ID
      await sails.helpers.songs.queue(
        sails.config.custom.subcats.IDs,
        "Top",
        1
      );

      queueLength = await sails.helpers.songs.calculateQueueLength();

      // Queue check
      // await sails.helpers.error.count('sportsQueue');

      // Change meta
      await sails.helpers.meta.change.with({
        hostDJ: null,
        cohostDJ1: null,
        cohostDJ2: null,
        cohostDJ3: null,
        queueFinish: moment().add(queueLength, "seconds").toISOString(true),
        show: inputs.sport,
        topic: inputs.topic,
        trackStamp: null,
        webchat: inputs.webchat,
      });

      await sails.helpers.meta.change.with({ changingState: null });
    } catch (e) {
      await sails.helpers.meta.change.with({
        state: "automation_on",
        host: null,
        show: "",
        topic: "",
        trackStamp: null,
        changingState: null,
      });
      throw e;
    }
  },
};
