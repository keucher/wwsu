module.exports = {
  friendlyName: "POST api/state/live",

  description: "Request to start a live in-studio broadcast.",

  inputs: {
    topic: {
      type: "string",
      defaultsTo: "",
      description:
        "A string containing a short blurb about this live broadcast.",
    },

    showname: {
      type: "string",
      required: true,
      custom: function (value) {
        let temp2 = value.split(" - ");
        if (temp2.length !== 2) {
          return false;
        }
        let temp3 = temp2[0].split("; ");
        if (temp3.length > 4) {
          return false;
        }
        return true;
      },
      description:
        'Name of the show beginning. It must follow the format "DJ names/handles (each separated with "; ", maximum 4) - show name".',
    },

    webchat: {
      type: "boolean",
      defaultsTo: true,
      description:
        "Should the web chat be enabled during this show? Defaults to true.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // If the specified host is not locked down to onair, deny the request
    if (this.req.payload.lockDown !== "onair") {
      throw {
        forbidden:
          "This host does not have permission to start live in-studio broadcasts; please set the lockdown setting for this host to 'onair' to allow live broadcasts.",
      };
    }

    // Make sure member has permission to start a live broadcast
    if (
      !(await sails.helpers.checkPermissionByLockdown(
        this.req.payload.ID,
        "live"
      ))
    ) {
      throw {
        forbidden:
          "Member/director logged in to this host does not have permission to start live broadcasts. Add this permission to the org member starting the broadcast.",
      };
    }

    // Do not continue if not in prerecord or automation mode; client should request automation before requesting live
    if (
      !sails.models.meta.memory.state.startsWith("automation_") &&
      !sails.models.meta.memory.state.startsWith("prerecord_")
    ) {
      throw {
        forbidden:
          "Cannot start a live broadcast outside of automation or prerecord. Please go to automation first.",
      };
    }

    // Block the request if we are changing states right now
    if (sails.models.meta.memory.changingState !== null) {
      throw {
        conflict:
          "Request rejected; system is currently changing states. Please try again in several seconds.",
      };
    }

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict:
          "Request rejected; currently airing an alert. Please try again in a minute.",
      };
    }

    try {
      // Lock so other state changing requests get blocked until we are done
      await sails.helpers.meta.change.with({
        changingState: `Switching to live`,
      });

      // Filter profanity and sanitize
      if (inputs.topic !== "") {
        inputs.topic = await sails.helpers.filterProfane(inputs.topic);
        inputs.topic = await sails.helpers.sanitize(inputs.topic);
      }
      if (inputs.showname !== "") {
        inputs.showname = await sails.helpers.filterProfane(inputs.showname);
        inputs.showname = await sails.helpers.sanitize(inputs.showname);
      }

      // Skip RadioDJ stuff and go immediately to live if not using RadioDJ.
      if (!sails.config.custom.radiodjs.length) {
        await sails.helpers.meta.changeDjs(inputs.showname);
        await sails.helpers.meta.change.with({
          state: "live_on",
          host: this.req.payload.ID,
          show: inputs.showname,
          topic: inputs.topic,
          trackStamp: null,
          webchat: inputs.webchat,
        });
        await sails.helpers.meta.newShow();
        return;
      }

      // Send meta early so that DJ Controls does not think this person is interfering with another show
      await sails.helpers.meta.change.with({
        state: "automation_live",
        host: this.req.payload.ID,
        show: inputs.showname,
        topic: inputs.topic,
        trackStamp: null,
      });

      // await sails.helpers.error.count('goLive');

      // Disable auto-DJ
      await sails.helpers.rest.cmd("EnableAutoDJ", 0);

      // Remove all tracks not in noClearShow
      await sails.helpers.songs.remove(
        true,
        sails.config.custom.subcats.noClearShow,
        false,
        true
      );

      // Queue clockwheel break if necessary
      await sails.helpers.rest.cmd("EnableAssisted", 1);
      await sails.helpers.break.checkClockwheel(false);

      // Queue live show start break
      await sails.helpers.break.executeArray(
        sails.config.custom.breaks.filter(
          (record) => record.type === "live" && record.subtype === "start"
        ),
        "Live Start"
      );

      // Queue a show opener if applicable
      if (
        typeof sails.config.custom.showcats[inputs.showname.split(" - ")[1]] !==
        "undefined"
      ) {
        await sails.helpers.songs.queue(
          [
            sails.config.custom.showcats[inputs.showname.split(" - ")[1]][
              "Show Openers"
            ],
          ],
          "Bottom",
          1
        );
      } else if (
        typeof sails.config.custom.showcats["Default"] !==
        "undefined"
      ) {
        await sails.helpers.songs.queue(
          [sails.config.custom.showcats["Default"]["Show Openers"]],
          "Bottom",
          1
        );
      }

      let queueLength = await sails.helpers.songs.calculateQueueLength();

      // If our queue length in RadioDJ is unacceptably long, try to reduce it further.
      if (queueLength >= sails.config.custom.basic.maxQueueLive) {
        await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active

        // Again, remove tracks not in noClearShow in case we missed any
        await sails.helpers.songs.remove(
          true,
          sails.config.custom.subcats.noClearShow,
          false,
          true
        );

        // Add a PSA to give the DJ a little more time
        await sails.helpers.songs.queue(
          sails.config.custom.subcats.PSAs,
          "Top",
          1
        );

        if (
          sails.config.custom.subcats.noClearShow &&
          sails.config.custom.subcats.noClearShow.indexOf(
            sails.models.meta.memory.trackIDSubcat
          ) === -1
        ) {
          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
        } // Skip currently playing track if it is not a noClearShow track
      }

      // Add a station ID
      await sails.helpers.songs.queue(
        sails.config.custom.subcats.IDs,
        "Top",
        1
      );

      queueLength = await sails.helpers.songs.calculateQueueLength();

      // Prep show
      await sails.helpers.rest.cmd("EnableAssisted", 0);
      await sails.helpers.meta.changeDjs(inputs.showname);
      await sails.helpers.meta.change.with({
        queueFinish: moment().add(queueLength, "seconds").toISOString(true),
        show: inputs.showname,
        topic: inputs.topic,
        trackStamp: null,
        webchat: inputs.webchat,
      });

      await sails.helpers.meta.change.with({ changingState: null });
    } catch (e) {
      await sails.helpers.meta.change.with({
        state: "automation_on",
        host: null,
        show: "",
        topic: "",
        trackStamp: null,
        changingState: null,
      });
      throw e;
    }
  },
};
