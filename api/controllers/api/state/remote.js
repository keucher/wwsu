module.exports = {
  friendlyName: "POST api/state/remote",

  description: "Request to begin a remote broadcast.",

  inputs: {
    topic: {
      type: "string",
      defaultsTo: "",
      description:
        "A string containing a short blurb about this remote broadcast.",
    },

    showname: {
      type: "string",
      required: true,
      custom: function (value) {
        var temp2 = value.split(" - ");
        if (temp2.length !== 2) {
          return false;
        }
        var temp3 = temp2[0].split("; ");
        if (temp3.length > 4) {
          return false;
        }
        return true;
      },
      description:
        'Name of the broadcast beginning. It must follow the format "DJ names/handles (each separated with "; ", maximum 4) - show name".',
    },

    webchat: {
      type: "boolean",
      defaultsTo: true,
      description:
        "Should the web chat be enabled during this show? Defaults to true.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Do not allow starting a remote broadcast if delay system status is 1 (critical).
    let delayStatus = await sails.models.status.find({
      name: "delay-system",
    });
    if (delayStatus && delayStatus[0] && delayStatus[0].status === 1)
      throw {
        forbidden:
          "Remote broadcasts are not allowed when the delay system is in critical status.",
      };

    // Disallow starting a remote broadcast if the host has belongsTo and the provided DJ either does not match belongsTo or is not scheduled to start a remote broadcast
    if (
      !(await sails.helpers.canStartRemoteBroadcast(
        this.req.payload.ID,
        "remote"
      ))
    ) {
      throw {
        forbidden:
          "Rejected; the org member this host belongs to does not have permission to start remote broadcasts.",
      };
    }

    if (this.req.payload.belongsTo) {
      // We need an additional check to ensure if belongsTo is specified, there is a scheduled remote broadcast where that member is one of the hosts.
      let dj = inputs.showname.split(" - ");
      let show = dj[1];
      var record = sails.models.calendar.calendardb.whatShouldBePlaying(
        null,
        false
      );
      record = record.filter(
        (event) =>
          event.type === "remote" &&
          event.name === show &&
          [
            event.hostDJ,
            event.cohostDJ1,
            event.cohostDJ2,
            event.cohostDJ3,
          ].indexOf(this.req.payload.belongsTo) !== -1
      );
      if (record.length < 1) {
        throw {
          forbidden:
            "Rejected; the org member belonging to this host is not scheduled to start a remote broadcast at this time.",
        };
      }

      // We also need to make sure at least one of the specified DJs for show hosts belongsTo this host
      let djs = dj[0].split("; ");
      let djRecord = await sails.models.djs.findOne({
        ID: this.req.payload.belongsTo,
      });
      if (!djRecord || djs.indexOf(djRecord.name) === -1) {
        throw {
          forbidden:
            "Rejected; none of the org member nicknames specified as show hosts belongs to this host.",
        };
      }
    }

    // Do not continue if not in automation mode; client should request automation before requesting remote
    if (
      !sails.models.meta.memory.state.startsWith("automation_") &&
      !sails.models.meta.memory.state.startsWith("prerecord_")
    ) {
      throw {
        forbidden:
          "Not allowed to start a remote broadcast outside of automation or prerecord. Please go to automation first.",
      };
    }

    // Block this request if we are changing states right now
    if (sails.models.meta.memory.changingState !== null) {
      throw {
        conflict:
          "Rejected; system is currently changing states. Please try again in several seconds.",
      };
    }

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict:
          "Rejected; system is airing an alert. Please try again in a minute.",
      };
    }

    try {
      // Lock so that other state changing requests get blocked until we are done.
      await sails.helpers.meta.change.with({
        changingState: `Switching to remote`,
      });

      // Filter profanity and sanitize
      if (inputs.topic !== "") {
        inputs.topic = await sails.helpers.filterProfane(inputs.topic);
        inputs.topic = await sails.helpers.sanitize(inputs.topic);
      }
      if (inputs.showname !== "") {
        inputs.showname = await sails.helpers.filterProfane(inputs.showname);
        inputs.showname = await sails.helpers.sanitize(inputs.showname);
      }

      // Skip RadioDJ stuff and go immediately to remote if not using RadioDJ.
      if (!sails.config.custom.radiodjs.length) {
        await sails.helpers.meta.changeDjs(inputs.showname);
        await sails.helpers.meta.change.with({
          state: "remote_on",
          host: this.req.payload.ID,
          show: inputs.showname,
          topic: inputs.topic,
          trackStamp: null,
          webchat: inputs.webchat,
        });
        await sails.helpers.meta.newShow();
        return;
      }

      // Send meta to prevent accidental interfering messages in Dj Controls
      await sails.helpers.meta.change.with({
        state: "automation_remote",
        host: this.req.payload.ID,
        show: inputs.showname,
        topic: inputs.topic,
        trackStamp: null,
      });

      // await sails.helpers.error.count('goRemote');

      // Operation: Remove all music tracks, queue clockwheel break, and disable auto DJ.
      await sails.helpers.rest.cmd("EnableAutoDJ", 0);
      await sails.helpers.songs.remove(
        true,
        sails.config.custom.subcats.noClearShow,
        false,
        true
      );
      await sails.helpers.rest.cmd("EnableAssisted", 1);
      await sails.helpers.break.checkClockwheel(false);
      await sails.helpers.break.executeArray(
        sails.config.custom.breaks.filter(
          (record) => record.type === "remote" && record.subtype === "start"
        ),
        "Remote Start"
      );

      // Queue a show opener if there is one
      if (
        typeof sails.config.custom.showcats[inputs.showname.split(" - ")[1]] !==
        "undefined"
      ) {
        await sails.helpers.songs.queue(
          [
            sails.config.custom.showcats[inputs.showname.split(" - ")[1]][
              "Show Openers"
            ],
          ],
          "Bottom",
          1
        );
      } else if (
        typeof sails.config.custom.showcats["Default"] !==
        "undefined"
      ) {
        await sails.helpers.songs.queue(
          [sails.config.custom.showcats["Default"]["Show Openers"]],
          "Bottom",
          1
        );
      }

      await sails.helpers.rest.cmd("EnableAssisted", 0);

      var queueLength = await sails.helpers.songs.calculateQueueLength();

      // If radioDJ queue is unacceptably long, try to make it shorter.
      if (queueLength >= sails.config.custom.basic.maxQueueRemote) {
        await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
        await sails.helpers.songs.remove(
          true,
          sails.config.custom.subcats.noClearShow,
          false,
          true
        );

        // Add a PSA to give the DJ a little more time
        await sails.helpers.songs.queue(
          sails.config.custom.subcats.PSAs,
          "Top",
          1
        );

        if (
          sails.config.custom.subcats.noClearShow &&
          sails.config.custom.subcats.noClearShow.indexOf(
            sails.models.meta.memory.trackIDSubcat
          ) === -1
        ) {
          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
        } // Skip currently playing track if it is not a noClearShow track
      }

      // Add a station ID
      await sails.helpers.songs.queue(
        sails.config.custom.subcats.IDs,
        "Top",
        1
      );

      queueLength = await sails.helpers.songs.calculateQueueLength();

      // Start the show
      await sails.helpers.meta.changeDjs(inputs.showname);
      await sails.helpers.meta.change.with({
        queueFinish: moment().add(queueLength, "seconds").toISOString(true),
        show: inputs.showname,
        topic: inputs.topic,
        trackStamp: null,
        webchat: inputs.webchat,
      });

      await sails.helpers.meta.change.with({ changingState: null });
      return;
    } catch (e) {
      await sails.helpers.meta.change.with({
        state: "automation_on",
        host: null,
        show: "",
        topic: "",
        trackStamp: null,
        changingState: null,
      });
      throw e;
    }
  },
};
