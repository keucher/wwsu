module.exports = {
  friendlyName: "POST api/state/automation",

  description: "Request to go into automation mode.",

  inputs: {
    transition: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, system will go into break mode instead of automation to allow for quick transitioning between radio shows.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller state/automation called.");

    try {
      // Prevent state changing if host is belongsTo and the specified belongsTo is not on the air
      if (
        !(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload))
      )
        throw {
          forbidden:
            "This host is currently not allowed to go to automation; this host did not start the current broadcast and does not have admin permission.",
        };

      // Block if we are in the process of changing states
      if (sails.models.meta.memory.changingState !== null) {
        throw {
          conflict:
            "Request rejected; the system is in the process of changing states right now. Please try again in several seconds.",
        };
      }

      // Block if running an alert
      if (sails.models.meta.memory.altRadioDJ !== null) {
        throw {
          conflict: "Request rejected; an alert is currently being broadcast.",
        };
      }

      // Lock system from any other state changing requests until we are done.
      await sails.helpers.meta.change.with({
        changingState: `Changing to automation`,
      });

      // Actually go to automation
      await sails.helpers.state.automation(inputs.transition);

      return;
    } catch (e) {
      await sails.helpers.meta.change.with({ changingState: null });
      throw e;
    }
  },
};
