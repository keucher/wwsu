module.exports = {
  friendlyName: "GET api/hosts",

  description:
    "Retrieve data about a specified host. Also provides an array of otherHosts, and subscribes to the hosts socket, if the host parameter is an admin host.",

  inputs: {
    host: {
      type: "string",
      required: true,
      description: "The host name to search for or authorize.",
    },
    app: {
      type: "string",
      description: "The application name and version this host is running.",
    },
  },

  exits: {
    success: {
      statusCode: 200,
    },
    notFound: {
      statusCode: 404,
    },
    error: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Find the hosts record
    let record = await sails.models.hosts.findOne({ host: inputs.host });
    sails.log.silly(record);

    if (!record) throw { notFound: "The host provided was not found." };

    // If app provided, update it
    if (inputs.app) {
      await sails.models.hosts.updateOne(
        { ID: record.ID },
        { app: inputs.app }
      );
    }

    // Subscribe to websockets if applicable
    if (record.authorized && this.req.isSocket && record.admin) {
      sails.sockets.join(this.req, "hosts");
      sails.log.verbose(
        "Request was a socket on an authorized admin. Joined hosts."
      );

      // Push the current hosts through the output
      let records = await sails.models.hosts.find();
      record.otherHosts = records;
    }

    return record;
  },
};
