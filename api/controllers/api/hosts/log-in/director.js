module.exports = {
  friendlyName: "POST api/hosts/:ID/log-in/director",

  description: "Log an entry for a Director using a locked down system",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the host from which this log comes.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Integrity check for existing host that is locked down
    let host = await sails.models.hosts.findOne({
      ID: inputs.ID,
    });
    if (!host) throw { notFound: "The provided host ID was not found." };
    if (!host.lockDown)
      throw { forbidden: "The provided host does not have lockdown enabled." };

    // error if there is already a clock-in right now on this host
    if (
      (await sails.models.lockdown.count({
        host: inputs.ID,
        clockOut: null,
      })) > 0
    ) {
      throw { forbidden: "Someone is already logged in on the provided host." };
    }

    // Clock in record
    let record = await sails.models.lockdown
      .create({
        host: inputs.ID,
        type: "director",
        typeID: this.req.payload.ID,
        clockIn: moment().format("YYYY-MM-DD HH:mm:ss"),
        clockOut: null,
      })
      .fetch();

    // Update lastSeen of the Director's DJ record, if applicable
    let dj = sails.models.djs.find({ realName: this.req.payload.name });
    if (dj[0]) {
      await sails.models.djs.updateOne(
        {
          ID: dj[0].ID,
        },
        { lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
      );
    }

    return record;
  },
};
