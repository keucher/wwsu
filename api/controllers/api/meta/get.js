module.exports = {
  friendlyName: "GET api/meta",

  description:
    "Get the current Meta. If the request is a socket, subscribe to meta changes.",

  inputs: {
    filter: {
      type: "json",
      description:
        "If provided (array of strings), we will only return / socket-subscribe meta info with the provided key names.",
    },
  },

  fn: async function (inputs) {
    // Subscribe to socket if applicable
    if (this.req.isSocket) {
      if (!inputs.filter || inputs.filter.constructor !== Array) {
        sails.sockets.join(this.req, "meta");
        sails.log.verbose("Request was a socket. Joining meta.");
      } else {
        inputs.filter.forEach((filter) => {
          sails.sockets.join(this.req, `meta-${filter}`);
        });
      }
    }

    // Return current meta, but update time to current time since it's not auto-updated automatically by changeMeta. Also include timezone of server.
    let _returnData = _.cloneDeep(sails.models.meta.memory);
    _returnData.time = moment().toISOString(true);
    _returnData.timezone = moment.tz.guess();

    let returnData = {};
    if (inputs.filter && inputs.filter.constructor === Array) {
      inputs.filter.forEach((key) => {
        returnData[key] = _returnData[key];
      });
    } else {
      returnData = _returnData;
    }
    return returnData;
  },
};
