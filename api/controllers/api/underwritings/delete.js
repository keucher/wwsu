module.exports = {
  friendlyName: "DELETE api/underwritings/:ID",

  description: "Remove an underwriting from the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the underwriting entry to remove.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Remove underwritings record
    let record = await sails.models.underwritings.destroyOne({ ID: inputs.ID });
    if (!record)
      throw { notFound: "The provided underwriting ID was not found." };

    return record;
  },
};
