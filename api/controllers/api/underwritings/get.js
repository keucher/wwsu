module.exports = {
  friendlyName: "GET api/underwritings",

  description:
    "Get all of the underwritings currently in the system, and subscribe to the underwritings socket.",

  inputs: {},

  fn: async function (inputs) {
    // Get underwritings records
    let records = await sails.models.underwritings.find();

    // Subscribe to websocket if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "underwritings");
      sails.log.verbose("Request was a socket. Joining underwritings.");
    }

    return records;
  },
};
