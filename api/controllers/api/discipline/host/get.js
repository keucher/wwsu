module.exports = {
  friendlyName: "GET api / Discipline / host",

  description:
    "Get an array of discipline in the system. Also subscribe to sockets.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    let records = await sails.models.discipline.find();

    // Subscribe to sockets if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "discipline");
      sails.log.verbose("Request was a socket. Joining discipline.");
    }

    return records;
  },
};
