var sh = require("shorthash");

module.exports = {
  friendlyName: "PUT api/discipline/acknowledge",

  description: "Mark a discipline as acknowledged by the client.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the disciplinary record to mark acknowledged.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let fromIP = await sails.helpers.getIp(this.req);
    let host = await sails.helpers.getHost(this.req);

    let record = await sails.models.discipline
      .update(
        {
          ID: inputs.ID,
          IP: [fromIP, `website-${host}`],
        },
        { acknowledged: 1 }
      )
      .fetch();

    if (!record)
      throw {
        notFound:
          "A discipline with that ID does not exist. Maybe it was for a different IP or it was already acknowledged?",
      };
  },
};
