module.exports = {
  friendlyName: "GET api/blogs/web/search",

  description:
    "Retrieve basic information on multiple blog entries; do not subscribe to sockets.",

  inputs: {
    lastID: {
      type: "number",
      defaultsTo: 0,
      min: 0,
      description:
        "Do not return records whose ID is the same or before this one",
    },
    limit: {
      type: "number",
      defaultsTo: 25,
      max: 100,
      min: 1,
      description: "Maximum number of entries to return",
    },
    category: {
      type: "string",
      description: "Only return blog entries in the specified category",
    },
    tags: {
      type: "json",
      description:
        "Only return blog entries containing one or more specified tags",
      custom: (value) => {
        // Must be array
        if (value.constructor !== Array) return false;

        // Every item in array must be a string
        let badRecord = value.find((item) => typeof item !== "string");
        if (badRecord) return false;

        return true;
      },
    },
    search: {
      type: "string",
      description:
        "Only return blog entries containing this search string in tags, title, summary, or content. OVERRIDES tags parameter",
    },
    author: {
      type: "number",
      description: "Only return blog entries written by this author",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Subscribe to websockets if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "blogs-web");
      sails.log.verbose("Request was a socket. Joined blogs-web.");
    }

    // Build query string
    let criteria = {
      ID: { ">": inputs.lastID },
      active: true,
      needsApproved: false,
    };

    if (inputs.category) criteria.categories = { contains: inputs.category };

    if (inputs.search) {
      criteria.or = [
        { tags: { contains: inputs.search } },
        { title: { contains: inputs.search } },
        { summary: { contains: inputs.search } },
        { contents: { contains: inputs.search } },
      ];
    } else if (inputs.tags) {
      criteria.tags = inputs.tags.map((tag) => {
        return { contains: tag };
      });
    }

    if (inputs.author) criteria.author = inputs.author;

    // Get records, sorted by newest first
    let criteriaB = _.cloneDeep(criteria);
    let records = await sails.models.blogs
      .find(criteriaB)
      .sort("createdAt DESC");

    // Cannot do this in Waterline, so do it in filter
    // NOTE: You should mark expired blog posts inactive within a relatively short time to conserve memory via daily cleanup
    records = records.filter(
      (record) =>
        (!record.starts || moment().isSameOrAfter(moment(record.starts))) &&
        (!record.expires || moment().isBefore(moment(record.expires)))
    );

    // Only return basic information; use blogs/get-one-web to retrieve full details.
    records = records.map((record) => {
      return {
        ID: record.ID,
        createdAt: record.createdAt,
        updatedAt: record.updatedAt,
        categories: record.categories,
        tags: record.tags,
        title: record.title,
        author: record.author,
        featuredImage: record.featuredImage,
        summary: record.summary,
        starts: record.starts,
        expires: record.expires,
      };
    });

    // Limit
    records.length =
      records.length > inputs.limit ? inputs.limit : records.length;

    return records;
  },
};
