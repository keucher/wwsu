module.exports = {
  friendlyName: "blogs/get-one-web",

  description: "Get a single blog entry",

  inputs: {
    ID: {
      type: "number",
      required: true,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Build query
    let criteria = { ID: inputs.ID };
    criteria.active = true;
    criteria.needsApproved = false;

    // Get records
    let criteriaB = _.cloneDeep(criteria);
    let record = await sails.models.blogs.findOne(criteriaB);

    if (!record) throw "notFound";

    // Throw not found if record is not active
    if (
      !(
        (!record.starts || moment().isSameOrAfter(moment(record.starts))) &&
        (!record.expires || moment().isBefore(moment(record.expires)))
      )
    )
      throw "notFound";

    return record;
  },
};
