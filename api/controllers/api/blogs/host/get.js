module.exports = {
  friendlyName: "GET api / blogs / host",

  description:
    "Get a single blog entry if ID is provided, or get basic info of multiple blog entries (and subscribe to sockets) if not provided.",

  inputs: {
    ID: {
      type: "number",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Get a specific blog and its full details if ID provided
    if (inputs.ID) {
      let record = await sails.models.blogs.findOne({ ID: inputs.ID });
      if (!record) throw "notFound";
      return record;
    }

    // Subscribe to websockets if not getting a single blog
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "blogs");
      sails.log.verbose("Request was a socket. Joined blogs.");
    }

    // Get all blogs
    let records = await sails.models.blogs.find();

    // Only include basic information when returning an array of blogs; conserve on data.
    records = records.map((record) => {
      return {
        ID: record.ID,
        createdAt: record.createdAt,
        updatedAt: record.updatedAt,
        active: record.active,
        needsApproved: record.needsApproved,
        categories: record.categories,
        title: record.title,
        starts: record.starts,
        expires: record.expires,
      };
    });

    return records;
  },
};
