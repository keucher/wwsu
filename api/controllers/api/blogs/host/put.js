module.exports = {
  friendlyName: "PUT api / blogs / host",

  description: "Edit a blog post.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "ID of the blog entry to edit",
    },

    active: {
      type: "boolean",
      description:
        "If false, this post will not be displayed regardless of starts, expires, and needsApproved settings.",
    },

    needsApproved: {
      type: "boolean",
      description:
        "Does this blog post need approval from directors before it is displayed?",
    },

    categories: {
      type: "json",
      description: "Which categories (blogs) does this post belong?",
      custom: (value) => {
        // Must be array
        if (value.constructor !== Array) return false;

        // Must be at least 1 item long
        if (value.length < 1) return false;

        // Every item in array must be a string
        let badRecord = value.find((item) => typeof item !== "string");
        if (badRecord) return false;

        return true;
      },
    },

    tags: {
      type: "json",
      description:
        "List of keywords for this post which can easily be searched.",
      custom: (value) => {
        // Must be array
        if (value.constructor !== Array) return false;

        // Every item in array must be a string
        let badRecord = value.find((item) => typeof item !== "string");
        if (badRecord) return false;

        return true;
      },
    },

    title: {
      type: "string",
      maxLength: 128,
      description: "Post title / headline",
    },

    author: {
      type: "number",
      description: "DJ ID of the author",
    },

    featuredImage: {
      type: "number",
      allowNull: true,
      description: "Uploads ID of the blog post featured image",
    },

    summary: {
      type: "string",
      maxLength: 255,
      description: "An excerpt of the post",
    },

    contents: {
      type: "string",
      description: "The post content itself (HTML format).",
    },

    starts: {
      type: "ref",
      custom: function (value) {
        return moment(value).isValid();
      },
      description:
        "If provided, post will not be visible until this date/time (providing it is active and does not need approved).",
    },

    expires: {
      type: "ref",
      custom: function (value) {
        return moment(value).isValid();
      },
      description: "If provided, post will disappear at this date/time.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Sanitize and filter profanity
    if (inputs.title) {
      inputs.title = await sails.helpers.sanitize(inputs.title);
      inputs.title = await sails.helpers.filterProfane(inputs.title);
    }
    if (inputs.summary) {
      inputs.summary = await sails.helpers.sanitize(inputs.summary);
      inputs.summary = await sails.helpers.filterProfane(inputs.summary);
    }
    if (inputs.contents) {
      inputs.contents = await sails.helpers.sanitize(inputs.contents);
      inputs.contents = await sails.helpers.filterProfane(inputs.contents);
    }

    // Update record
    let record = await sails.models.blogs.updateOne(
      { ID: inputs.ID },
      {
        active: inputs.active,
        needsApproved: inputs.needsApproved,
        categories: inputs.categories,
        tags: inputs.tags,
        title: inputs.title,
        author: inputs.author,
        featuredImage: inputs.featuredImage,
        summary: inputs.summary,
        contents: inputs.contents,
        starts: inputs.starts,
        expires: inputs.expires,
      }
    );
    if (!record) throw "notFound";
    return record;
  },
};
