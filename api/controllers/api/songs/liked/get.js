module.exports = {
  friendlyName: "GET api/songs/liked",

  description:
    "Retrieve an array of track IDs that the host has liked within the last sails.config.custom.basic.songsLikedCooldown days.",

  inputs: {},

  fn: async function (inputs) {
    // Get the hosts's IP address first
    let fromIP = await sails.helpers.getIp(this.req);
    let query = { IP: fromIP };

    // If config specifies users can like tracks multiple times, add a date condition to only return liked tracks within the configured days.
    if (sails.config.custom.basic.songsLikedCooldown > 0) {
      query.createdAt = {
        ">=": moment()
          .subtract(sails.config.custom.basic.songsLikedCooldown, "days")
          .format("YYYY-MM-DD HH:mm:ss"),
      };
    }

    // Retrieve track IDs liked by this IP
    let records = await sails.models.songsliked.find(query);

    return records;
  },
};
