module.exports = {
  friendlyName: "POST api/songs/liners/queue",

  description: "Queue and play a random Sports Liner.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    if (!sails.config.custom.radiodjs.length)
      throw {
        forbidden: "No RadioDJs are configured in the system.",
      };

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict:
          "An alert is currently being broadcast. Please try again in a minute.",
      };
    }

    // Prevent adding tracks if host is belongsTo and the specified belongsTo is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to queue a liner; this host did not start the current broadcast and does not have admin permission.",
      };

    // Error if we are not in a sports state
    if (sails.models.meta.memory.state.startsWith("sports")) {
      throw {
        forbidden: "Cannot play a sports liner when not in a sports broadcast.",
      };
    }

    // Log it
    await sails.models.logs
      .create({
        attendanceID: sails.models.meta.memory.attendanceID,
        logtype: "liner",
        loglevel: "info",
        logsubtype: sails.models.meta.memory.show,
        logIcon: `fas fa-angle-double-right`,
        title: `Host requested to play a random liner.`,
        event: "",
      })
      .fetch()
      .tolerate((err) => {
        // Do not throw for errors, but log it
        sails.log.error(err);
      });

    // Queue it
    if (
      typeof sails.config.custom.sportscats[sails.models.meta.memory.show] !==
      "undefined"
    ) {
      await sails.helpers.songs.queue(
        [
          sails.config.custom.sportscats[sails.models.meta.memory.show][
            "Sports Liners"
          ],
        ],
        "Top",
        1
      );
    }

    // Play it
    await sails.helpers.rest.cmd("EnableAssisted", 0);
    await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);

    return;
  },
};
