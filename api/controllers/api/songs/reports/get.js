module.exports = {
  friendlyName: "GET /api/songs/reports",

  description:
    "Get spin, requests, and track liking reports on songs and tracks in the system.",

  inputs: {
    ignoreAutomation: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, only tracks manually logged by show hosts via DJ Controls will be considered. Defaults to false.",
    },
    search: {
      type: "string",
      allowNull: true,
      description:
        "Filter by provided artist or title. Null = do not filter by this.",
    },
    category: {
      type: "string",
      custom: (value) => {
        if (typeof sails.config.custom.subcats[value] === `undefined`) {
          return false;
        }
        return true;
      },
      description:
        "Optionally filter by configured Node music category. Manual logs ignore this.",
    },
    subcategory: {
      type: "number",
      allowNull: true,
      description:
        "Optionally filter returned songs by provided subcategory ID. Manual logs ignore this.",
    },
    genre: {
      type: "number",
      allowNull: true,
      description:
        "Optionally filter returned songs by provided genre ID. Manual logs ignore this.",
    },
    start: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when we should begin considering song spins. Defaults to the beginning of time. Recommended ISO string.`,
    },
    end: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when we should stop considering song spins. Defaults to the end of time. Recommended ISO string.`,
    },
  },

  exits: {
    success: {
      statusCode: 200,
    },
    notFound: {
      statusCode: 404,
    },
    internalError: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Object... key = Artist - Title, value is object of data
    let songInfo = {};
    let queryString = {};

    /**
     * Initialize songInfo object on a track.
     * @param {string} artist Artist of the track
     * @param {string} title Title of the track
     * @param {string} album Album of the track
     * @param {string} label Record label of the track
     */
    const initSongInfo = (artist = "", title = "", album = "", label = "") => {
      if (typeof songInfo[`${artist || ""} - ${title || ""}`] === "undefined") {
        songInfo[`${artist || ""} - ${title || ""}`] = {
          artist: artist || "Unknown Artist",
          title: title || "Unknown Title",
          album: album || "",
          label: label || "",
          spins: 0,
          spinTimes: [],
          spinsRadioDJ: 0,
          spinsMeta: 0,
          spinsLog: 0,
          likes: 0,
          requests: 0,
        };
      }

      if (
        !songInfo[`${artist || ""} - ${title || ""}`].album ||
        songInfo[`${artist || ""} - ${title || ""}`].album === ""
      )
        songInfo[`${artist || ""} - ${title || ""}`].album = album;

      if (
        !songInfo[`${artist || ""} - ${title || ""}`].label ||
        songInfo[`${artist || ""} - ${title || ""}`].label === ""
      )
        songInfo[`${artist || ""} - ${title || ""}`].label = label;
    };

    /**
     * Add a spin count for a track.
     * @param {string} artist Artist of the track
     * @param {string} title Title of the track
     * @param {string} album Album of the track
     * @param {string} label Record label of the track
     * @param {string} type The type of spin (spinsRadioDJ, spinsMeta, or spinsLog)
     * @param {string} time ISO string of the time the spin occurred
     * @returns nothing
     */
    const addSpin = (
      artist = "",
      title = "",
      album = "",
      label = "",
      type = "spinsLog",
      time = undefined
    ) => {
      initSongInfo(artist, title, album, label);

      // Do not count this spin if it happened within +- 1 minute of another spin (e.g. radioDJ and meta logs might happen at the same time)
      if (
        songInfo[`${artist || ""} - ${title || ""}`].spinTimes.find(
          (spinTime) => {
            return (
              moment(spinTime).isAfter(moment(time).subtract(1, "minutes")) &&
              moment(spinTime).isBefore(moment(time).add(1, "minutes"))
            );
          }
        )
      )
        return;

      songInfo[`${artist || ""} - ${title || ""}`].spinTimes.push(time);
      songInfo[`${artist || ""} - ${title || ""}`].spins++;
      songInfo[`${artist || ""} - ${title || ""}`][type]++;
    };

    if (sails.config.custom.radiodjs.length > 0 && !inputs.ignoreAutomation) {
      // If specific categories or subcategories are provided, grab tracks from those.
      if (
        (typeof inputs.subcategory !== "undefined" &&
          inputs.subcategory !== null) ||
        (typeof inputs.category !== "undefined" && inputs.category !== null)
      ) {
        queryString.id_subcat = [];
      }
      if (
        typeof inputs.subcategory !== "undefined" &&
        inputs.subcategory !== null
      ) {
        queryString.id_subcat.push(inputs.subcategory);
      }
      if (
        typeof inputs.category !== "undefined" &&
        inputs.category !== null &&
        typeof sails.config.custom.subcats[inputs.category] !== `undefined`
      ) {
        queryString.id_subcat = queryString.id_subcat.concat(
          sails.config.custom.subcats[inputs.category]
        );
      }

      // If a genre is provided, get tracks from that genre.
      if (typeof inputs.genre !== "undefined" && inputs.genre !== null) {
        queryString.id_genre = inputs.genre;
      }

      // Filter by search string, if provided
      if (
        typeof inputs.search !== "undefined" &&
        inputs.search !== null &&
        inputs.search !== ""
      ) {
        queryString.or = [
          { artist: { contains: inputs.search } },
          { original_artist: { contains: inputs.search } },
          { title: { contains: inputs.search } },
          { album: { contains: inputs.search } },
          { composer: { contains: inputs.search } },
        ];
      }

      // Search within a range
      if (
        typeof inputs.start !== "undefined" &&
        inputs.start !== null &&
        inputs.start !== ""
      ) {
        if (typeof queryString.date_played === "undefined")
          queryString.date_played = {};

        queryString.date_played[">="] = moment(inputs.start).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      }
      if (
        typeof inputs.end !== "undefined" &&
        inputs.end !== null &&
        inputs.end !== ""
      ) {
        if (typeof queryString.date_played === "undefined")
          queryString.date_played = {};

        queryString.date_played["<="] = moment(inputs.end).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      }

      // Get our spins
      await sails.models.history
        .stream(queryString)
        .eachRecord(async (song) => {
          addSpin(
            song.artist,
            song.title,
            song.album,
            song.label,
            "spinsRadioDJ",
            song.date_played
          );
        });
    }

    // Now, get our other spins
    queryString = {
      logtype: inputs.ignoreAutomation ? ["manual"] : ["manual", "track"],
    };

    // Filter by search string, if provided
    if (
      typeof inputs.search !== "undefined" &&
      inputs.search !== null &&
      inputs.search !== ""
    ) {
      queryString.or = [
        { trackArtist: { contains: inputs.search } },
        { trackTitle: { contains: inputs.search } },
      ];
    }

    // Search within a range
    if (
      typeof inputs.start !== "undefined" &&
      inputs.start !== null &&
      inputs.start !== ""
    ) {
      if (typeof queryString.createdAt === "undefined")
        queryString.createdAt = {};

      queryString.createdAt[">="] = moment(inputs.start).format(
        "YYYY-MM-DD HH:mm:ss"
      );
    }
    if (
      typeof inputs.end !== "undefined" &&
      inputs.end !== null &&
      inputs.end !== ""
    ) {
      if (typeof queryString.createdAt === "undefined")
        queryString.createdAt = {};

      queryString.createdAt["<="] = moment(inputs.end).format(
        "YYYY-MM-DD HH:mm:ss"
      );
    }

    // Get our spins
    await sails.models.logs.stream(queryString).eachRecord(async (song) => {
      addSpin(
        song.trackArtist,
        song.trackTitle,
        song.trackAlbum,
        song.trackLabel,
        song.logtype === "track" ? "spinsMeta" : "spinsLog",
        song.createdAt
      );
    });

    // Check track requests
    if (sails.config.custom.radiodjs.length > 0) {
      queryString = {};

      // Search within a range
      if (
        typeof inputs.start !== "undefined" &&
        inputs.start !== null &&
        inputs.start !== ""
      ) {
        if (typeof queryString.createdAt === "undefined")
          queryString.createdAt = {};

        queryString.createdAt[">="] = moment(inputs.start).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      }
      if (
        typeof inputs.end !== "undefined" &&
        inputs.end !== null &&
        inputs.end !== ""
      ) {
        if (typeof queryString.createdAt === "undefined")
          queryString.createdAt = {};

        queryString.createdAt["<="] = moment(inputs.end).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      }

      // Get our requests
      await sails.models.requests
        .stream(queryString)
        .populate("songID")
        .eachRecord(async (song) => {
          if (
            song.songID &&
            typeof song.songID === "object" &&
            typeof songInfo[
              `${song.songID.artist || ""} - ${song.songID.title || ""}`
            ] !== "undefined"
          ) {
            songInfo[`${song.songID.artist || ""} - ${song.songID.title || ""}`]
              .requests++;
          }
        });
    }

    queryString = {};

    // Search within a range
    if (
      typeof inputs.start !== "undefined" &&
      inputs.start !== null &&
      inputs.start !== ""
    ) {
      if (typeof queryString.createdAt === "undefined")
        queryString.createdAt = {};

      queryString.createdAt[">="] = moment(inputs.start).format(
        "YYYY-MM-DD HH:mm:ss"
      );
    }
    if (
      typeof inputs.end !== "undefined" &&
      inputs.end !== null &&
      inputs.end !== ""
    ) {
      if (typeof queryString.createdAt === "undefined")
        queryString.createdAt = {};

      queryString.createdAt["<="] = moment(inputs.end).format(
        "YYYY-MM-DD HH:mm:ss"
      );
    }

    // Get liked songs
    await sails.models.songsliked
      .stream(queryString)
      .populate("trackID")
      .eachRecord(async (song) => {
        if (
          song.trackID &&
          typeof song.trackID === "object" &&
          typeof songInfo[
            `${song.trackID.artist || ""} - ${song.trackID.title || ""}`
          ] !== "undefined"
        ) {
          songInfo[`${song.trackID.artist || ""} - ${song.trackID.title || ""}`]
            .likes++;
        } else if (
          song.track &&
          typeof songInfo[`${song.track}`] !== "undefined"
        ) {
          songInfo[`${song.track}`].likes++;
        }
      });

    // Return our data as an array
    let returnData = [];
    for (let key in songInfo) {
      if (!Object.prototype.hasOwnProperty.call(songInfo, key)) return;

      returnData.push(songInfo[key]);
    }
    return returnData;
  },
};
