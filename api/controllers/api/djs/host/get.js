module.exports = {
  friendlyName: "GET api/djs/host",

  description:
    "Retrieve a list of DJs in the system (and subscribe to sockets), or get information about a single DJ.",

  inputs: {
    ID: {
      type: "number",
      description: `If provided, instead of returning an array of DJs, will return information about the specified DJ.`,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    if (!inputs.ID) {
      // Grab DJs
      var records = await sails.models.djs.find();

      // Return only true or false for login if a login was set for the DJ
      records = records.map((record) => {
        record.login = record.login === null ? false : true;
        delete record.doorCode;
        return record;
      });

      sails.log.verbose(`DJ records retrieved: ${records.length}`);

      // Subscribe to sockets if applicable
      if (this.req.isSocket) {
        sails.sockets.join(this.req, "djs");
        sails.log.verbose("Request was a socket. Joining djs.");
      }

      // Return records
      if (!records || records.length < 1) {
        return [];
      } else {
        return records;
      }
    } else {
      var record = await sails.models.djs.findOne({ ID: inputs.ID });
      if (!record) throw { notFound: "The provided DJ ID was not found." };

      var returnData = {
        startOfSemester: moment(
          sails.config.custom.analytics.startOfSemester
        ).toISOString(true),
      };

      record.login = record.login === null ? false : true;
      delete record.doorCode;

      returnData.DJ = record;
      returnData.notes = await sails.models.djnotes.find({ dj: inputs.ID });
      returnData.attendance = await sails.models.attendance.find({
        or: [
          { dj: inputs.ID },
          { cohostDJ1: inputs.ID },
          { cohostDJ2: inputs.ID },
          { cohostDJ3: inputs.ID },
        ],
      });

      let weekStats = await sails.helpers.analytics.showtime(
        [inputs.ID],
        undefined,
        moment().subtract(7, "days").toISOString(true),
        undefined,
        true
      );
      let semesterStats = await sails.helpers.analytics.showtime(
        [inputs.ID],
        undefined,
        moment(sails.config.custom.analytics.startOfSemester).toISOString(true),
        undefined,
        true
      );
      let yearStats = await sails.helpers.analytics.showtime(
        [inputs.ID],
        undefined,
        moment().subtract(1, "years").toISOString(true),
        undefined,
        true
      );

      returnData.stats = {
        week: weekStats,
        semester: semesterStats,
        year: yearStats,
      };

      return returnData;
    }
  },
};
