module.exports = {
  friendlyName: "DELETE api/djs/host/:ID",

  description: "Permanently remove a DJ from the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The DJ ID to remove.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Do not allow removing of master member
    if (inputs.ID === 1)
      throw { forbidden: "Not allowed to delete the default member (ID 1)." };

    // Remove DJ.
    let record = await sails.models.djs.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The DJ ID was not found." };

    return record;
  },
};
