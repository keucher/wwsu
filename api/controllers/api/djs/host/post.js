const bcrypt = require("bcrypt");

module.exports = {
  friendlyName: "POST api/djs/host",

  description:
    "Add a new DJ into the system. Call is ignored if a DJ with the same name already exists.",

  inputs: {
    name: {
      type: "string",
      required: true,
      custom: function (value) {
        // Prevent use of space dash space, or "; ", in names as this will cause problems in the system
        var temp2 = value.split(" - ");
        if (temp2.length > 1) {
          return false;
        }
        var temp3 = value.split("; ");
        if (temp3.length > 1) {
          return false;
        }
        return true;
      },
      description: "The DJ handle to add.",
    },

    realName: {
      type: "string",
      description: "The full real name of the DJ.",
    },

    email: {
      type: "string",
      description: "The email address of the DJ.",
      required: true,
    },

    login: {
      type: "string",
      required: true,
      description:
        "The login used for DJ-related things such as accessing locked-down computers and the DJ web panel.",
    },

    avatar: {
      type: "number",
      allowNull: true,
      description: "The ID of the Uploads file for the dj's avatar",
    },

    profile: {
      type: "string",
      description: "Profile information for the DJ.",
    },

    permissions: {
      type: "json",
      description: "The permissions granted to this DJ",
      custom: (value) => {
        if (value.constructor !== Array) return false;

        for (let key in value) {
          if (
            [
              "org",
              "sportsbroadcaster",
              "book",
              "live",
              "remote",
              "sports",
            ].indexOf(value[key]) === -1
          ) {
            return false;
          }
        }

        return true;
      },
    },

    doorCode: {
      type: "string",
      description: "The door code for the DJ",
    },

    doorCodeAccess: {
      type: "json",
      description: "The doors which the door code for this DJ grants access",
      custom: (value) => {
        if (value.constructor !== Array) return false;

        for (let key in value) {
          if (
            ["018", "018A", "018B", "018C", "018D"].indexOf(value[key]) === -1
          ) {
            return false;
          }
        }

        return true;
      },
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
  },

  fn: async function (inputs) {
    // login should always be null if blank
    if (!inputs.login || inputs.login === "") {
      inputs.login = null;
    }

    // Check if a DJ with the same name exists.
    let record = await sails.models.djs.find({ name: inputs.name });
    if (record.length)
      throw {
        badRequest:
          "A DJ / org member with that DJ name already exists. Please specify a different DJ name for this member.",
      };

    record = await sails.models.djs
      .create({
        name: inputs.name,
        active: true,
        realName: inputs.realName || null,
        email: inputs.email,
        login: bcrypt.hashSync(inputs.login, 10),
        avatar: inputs.avatar,
        profile: await sails.helpers.sanitize(inputs.profile),
        permissions: inputs.permissions,
        doorCode: inputs.doorCode,
        doorCodeAccess: inputs.doorCodeAccess,
        lastSeen: moment().format("YYYY-MM-DD HH:mm:ss"), // Even though the DJ hasn't made a broadcast yet, set this to now so the auto clean-up does not mark the DJ as inactive.
      })
      .fetch();

    return record;
  },
};
