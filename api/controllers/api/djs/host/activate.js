module.exports = {
  friendlyName: "PUT api/djs/host/:ID/activate",

  description: "Mark a DJ / org member as active in the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The DJ ID to mark active.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Mark DJ as active.
    let record = await sails.models.djs.updateOne(
      { ID: inputs.ID },
      {
        active: true,
        // Set last seen to now so auto-cleanup does not re-mark DJ as inactive. It is assumed when marking a DJ active again, they have been "seen".
        lastSeen: moment().format("YYYY-MM-DD HH:mm:ss"),
      }
    );

    if (!record)
      throw { notFound: "The provided DJ / org member was not found." };

    delete record.login; // Do not return login hash.
    return record;
  },
};
