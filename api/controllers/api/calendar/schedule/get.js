module.exports = {
  friendlyName: "GET api / calendar / schedule",

  description:
    "Get all calendar schedules to use in CalendarDb. Also subscribe to sockets.",

  inputs: {
    ID: {
      type: "number",
      description:
        "Only get this schedule ID if provided, and do not subscribe to sockets.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Get the individual schedule if requesting
    if (inputs.ID) {
      let record = await sails.models.schedule.findOne({ ID: inputs.ID });
      if (!record) throw "notFound";
      return record;
    }

    // Get all the schedules otherwise
    let scheduleRecords = await sails.models.schedule.find();

    // Subscribe to sockets if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "schedule");
      sails.log.verbose("Request was a socket. Joining schedule.");
    }

    return scheduleRecords;
  },
};
