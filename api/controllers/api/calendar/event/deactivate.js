module.exports = {
  friendlyName: "PUT api / calendar / event / deactivate",

  description: "Mark an event in the main calendar inactive",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID number of the calendar event to mark inactive.",
    },
  },

  exits: {
    success: {
      statusCode: 202, // Accepted
    },
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Do not allow marking the master event as inactive
    if (inputs.ID === 1)
      throw { forbidden: "You cannot deactivate the master event (ID 1)." };

    // Exit if the record does not exist
    if (!(await sails.models.calendar.count({ ID: inputs.ID })))
      throw { notFound: "The calendar ID was not found." };

    // Bail if we are still processing something for this Calendar ID
    if (sails.models.status.tasks.calendar.has(`calendar`))
      throw {
        conflict:
          "Another calendar operation is in progress. This request was blocked to prevent conflicts. Please try again in a minute.",
      };

    // Reserve the operation to prevent conflicts
    sails.models.status.tasks.calendar.set(`calendar`, true);
    try {
      // Check for event conflicts
      sails.models.calendar.calendardb.checkConflicts(
        async (conflicts) => {
          // Do not continue for conflict errors
          if (conflicts.errors.length > 0) {
            sails.models.status.tasks.calendar.delete(`calendar`);
            sails.log.error(new Error(conflicts.errors.join("; ")));
            return;
          }

          // Mark the calendar as inactive
          await sails.models.calendar.updateOne(
            { ID: inputs.ID },
            { active: false }
          );

          // Remove records which should be removed
          if (conflicts.removals.length > 0) {
            await sails.models.schedule
              .destroy({
                ID: conflicts.removals.map((removal) => removal.scheduleID),
              })
              .fetch();
          }

          // Now, add overrides
          if (conflicts.additions.length > 0) {
            let cfMaps = conflicts.additions.map(async (override) => {
              await sails.models.schedule.create(override).fetch();
            });
            await Promise.all(cfMaps);
          }

          // Finally, re-check the calendar events and update cache after 5 seconds
          setTimeout(async () => {
            await sails.helpers.calendar.check(false, true);
          }, 5000);

          sails.models.status.tasks.calendar.delete(`calendar`);
        },
        [{ removeCalendar: inputs.ID }]
      );

      // Will process in the background.
      return "Request accepted. The event will be deactivated after conflict resolution is run.";
    } catch (e) {
      sails.models.status.tasks.calendar.delete(`calendar`);
      throw e;
    }
  },
};
