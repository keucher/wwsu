module.exports = {
  friendlyName: "POST api / calendar / event",

  description: "Add a main calendar event.",

  inputs: {
    type: {
      type: "string",
      isIn: [
        "show",
        "sports",
        "remote",
        "prerecord",
        "genre",
        "playlist",
        "event",
        "onair-booking",
        "prod-booking",
        "office-hours",
      ],
      defaultsTo: "event",
    },

    active: {
      type: "boolean",
      defaultsTo: true,
    },

    priority: {
      type: "number",
      allowNull: true,
    },

    hostDJ: {
      type: "number",
      allowNull: true,
    },

    cohostDJ1: {
      type: "number",
      allowNull: true,
    },

    cohostDJ2: {
      type: "number",
      allowNull: true,
    },

    cohostDJ3: {
      type: "number",
      allowNull: true,
    },

    eventID: {
      type: "number",
      allowNull: true,
    },

    playlistID: {
      type: "number",
      allowNull: true,
    },

    director: {
      type: "number",
      allowNull: true,
    },

    name: {
      type: "string",
      allowNull: true,
    },

    logo: {
      type: "number",
      allowNull: true,
    },

    banner: {
      type: "number",
      allowNull: true,
    },

    description: {
      type: "string",
      allowNull: true,
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
  },

  fn: async function (inputs) {
    // Construct the event
    let event = {
      type: inputs.type,
      active: inputs.active,
      priority:
        inputs.priority && inputs.priority !== null
          ? inputs.priority
          : sails.models.calendar.calendardb.getDefaultPriority({
              type: inputs.type,
            }),
      hostDJ: inputs.hostDJ,
      cohostDJ1: inputs.cohostDJ1,
      cohostDJ2: inputs.cohostDJ2,
      cohostDJ3: inputs.cohostDJ3,
      eventID: inputs.eventID,
      playlistID: inputs.playlistID,
      director: inputs.director,
      name: inputs.name,
      description: inputs.description,
      logo: inputs.logo,
      banner: inputs.banner,
      // Set lastAired to now even though it has not aired yet; this prevents auto cleanup from deleting or marking the event inactive too soon.
      lastAired: moment().format("YYYY-MM-DD HH:mm:ss"),
    };

    // Verify the event is valid
    try {
      event = await sails.helpers.calendar.verify(event);
    } catch (e) {
      throw { badRequest: e.message };
    }

    // Re-check the calendar events and update cache after 5 seconds
    setTimeout(async () => {
      await sails.helpers.calendar.check(false, true);
    }, 5000);

    // Add the event into the calendar
    return await sails.models.calendar.create(event).fetch();
  },
};
