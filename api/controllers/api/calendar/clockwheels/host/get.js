module.exports = {
  friendlyName: "GET api/calendar/clockwheels/host",

  description: "Get all clockwheels and subscribe to sockets.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    let clockwheels = await sails.models.clockwheels.find();

    // Subscribe to sockets if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "clockwheels");
      sails.log.verbose("Request was a socket. Joining clockwheels.");
    }

    return clockwheels;
  },
};
