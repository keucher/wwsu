module.exports = {
  friendlyName: "GET api/calendar/clockwheels/web",

  description: "Get the clockwheels for the authorized DJ.",

  inputs: {
    calendarID: {
      type: "number",
      description:
        "If provided, will only return the clockwheels for the provided calendar ID.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Build a search query for calendar events
    let query = {
      or: [
        {
          hostDJ: this.req.payload.ID,
          cohostDJ1: this.req.payload.ID,
          cohostDJ2: this.req.payload.ID,
          cohostDJ3: this.req.payload.ID,
        },
      ],
      active: true,
    };
    if (inputs.calendarID) query.ID = inputs.calendarID;

    let events = await sails.models.calendar.find(query);
    if (!events || events.length < 1)
      throw {
        notFound:
          "Did not find any calendar events where the authorized member was a host.",
      };

    // Get the clockwheels for each calendar event
    let eventIDs = events.map((event) => event.ID);
    let clockwheels = await sails.models.clockwheels.find({
      calendarID: eventIDs,
    });

    return clockwheels;
  },
};
