module.exports = {
  friendlyName: "GET api/calendar/events-playlists",

  description:
    "Get arrays of events and playlists that can be used in calendar selection",

  inputs: {},

  fn: async function (inputs) {
    let events = sails.config.custom.radiodjs.length
      ? await sails.models.events.find({
          type: 3, // Manual Event
          data: { contains: "Load Rotation" },
          enabled: "True",
        })
      : [];
    let playlists = sails.config.custom.radiodjs.length
      ? await sails.models.playlists.find()
      : [];

    return {
      events: events.map((event) => {
        return { ID: event.ID, name: event.name };
      }),
      playlists: playlists.map((playlist) => {
        return { ID: playlist.ID, name: playlist.name };
      }),
    };
  },
};
