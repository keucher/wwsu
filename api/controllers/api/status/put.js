module.exports = {
  friendlyName: "PUT api/status/:name",

  description: "Change a status manually. Currently, can only change the status of recorder.",

  inputs: {
    name: {
      type: "string",
      required: true,
      isIn: ["recorder"]
    },
    status: {
      type: "number",
      min: 1,
      max: 5,
      required: true,
      description: "The status code of the recorder.",
    },
    summary: {
      type: "string",
      description: "Short blurb.",
    },
    data: {
      type: "string",
      required: true,
      description: "Information about the current state of the recorder.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    let record = await sails.helpers.status.modify.with({
      name: inputs.name,
      status: inputs.status,
      summary: inputs.summary || inputs.data,
      data: inputs.data,
    });

    // All done.
    return record;
  },
};
