let sh = require("shorthash");

module.exports = {
  friendlyName: "POST api/recipients/web",

  description: "Registers a public / web recipient in the recipients database.",

  inputs: {
    device: {
      type: "string",
      allowNull: true,
      description: `If this recipient comes from the WWSU mobile app, provide their OneSignal ID here.`,
    },
  },

  fn: async function (inputs) {
    // Get client IP address and form a host hash from it
    let host = await sails.helpers.getHost(this.req);

    // Mark the client as online and retrieve their nickname
    let response = await sails.helpers.recipients.add(
      sails.sockets.getId(this.req),
      `website-${host}`,
      "website",
      await sails.helpers.recipients.generateNick(),
      inputs.device
    );

    // Return the nickname of this client as a label object
    return response;
  },
};
