module.exports = {
  friendlyName: "DELETE api/directors",

  description: "Remove a director from the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The director ID to remove.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Do not allow removing of master director
    if (inputs.ID === 1)
      throw { forbidden: "The master director may not be deleted." };

    // Determine if we need to lock out of editing admin
    let lockout = await sails.models.directors.count({ admin: true });

    // Figure out what director we are going to be removing
    let toDestroy = await sails.models.directors.find({ ID: inputs.ID });

    // Block requests to remove this director if there are 1 or less admin directors and this director is an admin.
    if (lockout <= 1 && toDestroy.admin) {
      throw {
        conflict:
          "To prevent accidental lockout, this request was denied because there are 1 or less admin directors. Make another director admin first before removing this director.",
      };
    }

    let record = await sails.models.directors.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "That director ID does not exist." };

    // Also remove the director's push notification subscriptions
    await sails.models.subscribers
      .destroy({
        type: [
          "emergencies",
          "accountability-shows",
          "accountability-directors",
        ],
        subtype: inputs.ID,
      })
      .fetch();

    delete record.login;
    return record;
  },
};
