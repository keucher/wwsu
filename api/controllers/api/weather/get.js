module.exports = {
  friendlyName: "GET api / weather",

  description: "Get the current weather information and subscribe to sockets",

  inputs: {},

  fn: async function (inputs) {
    // Get records
    let records = await sails.models.climacell.find();

    // Subscribe to sockets, if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "climacell");
      sails.log.verbose("Request was a socket. Joining climacell.");
    }

    return records;
  },
};

// TODO: rename model and module from climacell to weather
