module.exports = {
  friendlyName: "PUT api/inventory/checkout/:ID",

  description: "Edit a checkout record",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The record to edit",
    },

    name: {
      type: "string",
    },

    checkOutDate: {
      type: "string",
      description: "The date/time the item was checked out. Defaults to now.",
      custom: function (value) {
        return moment(value).isValid();
      },
    },

    checkOutCondition: {
      type: "string",
      isIn: ["Excellent", "Very Good", "Good", "Fair", "Poor", "Broken"],
    },

    checkOutQuantity: {
      type: "number",
    },

    checkOutNotes: {
      type: "string",
    },

    checkInDue: {
      type: "string",
      description: "When the item is expected to be checked back in.",
      custom: function (value) {
        return moment(value).isValid();
      },
    },

    checkInDate: {
      type: "string",
      description: "When the item was checked in.",
      custom: function (value) {
        return moment(value).isValid();
      },
    },

    checkInCondition: {
      type: "string",
      isIn: ["Excellent", "Very Good", "Good", "Fair", "Poor", "Broken"],
    },

    checkInQuantity: {
      type: "number",
    },

    checkInNotes: {
      type: "string",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let criteria = {
      name: inputs.name,
      checkOutDate: inputs.checkOutDate
        ? moment(inputs.checkOutDate).format("YYYY-MM-DD HH:mm:ss")
        : undefined,
      checkOutCondition: inputs.checkOutCondition,
      checkOutQuantity: inputs.checkOutQuantity,
      checkOutNotes: inputs.checkOutNotes,
      checkInDue: inputs.checkInDue
        ? moment(inputs.checkInDue).format("YYYY-MM-DD HH:mm:ss")
        : undefined,
      checkInDate: inputs.checkInDate
        ? moment(inputs.checkInDate).format("YYYY-MM-DD HH:mm:ss")
        : undefined,
      checkInCondition: inputs.checkInCondition,
      checkInQuantity: inputs.checkInQuantity,
      checkInNotes: inputs.checkInNotes,
    };

    let criteriaB = _.cloneDeep(criteria);

    let record = await sails.models.checkout.updateOne(
      { ID: inputs.ID },
      criteriaB
    );
    if (!record)
      throw { notFound: "The provided checkout record was not found." };

    return record;
  },
};
