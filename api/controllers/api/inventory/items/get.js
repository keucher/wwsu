module.exports = {
  friendlyName: "GET api/inventory/items/:ID?",

  description: "Get inventory items.",

  inputs: {
    ID: {
      type: "number",
      description:
        "If getting a single item and its checkout records, specify it here.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let records;

    // Single item
    if (inputs.ID) {
      records = await sails.models.items
        .findOne({ ID: inputs.ID })
        .populate("checkoutRecords"); // Also populate / get checkoutRecords for this item.
      if (!records) throw { notFound: "The provided item was not found." };

      records.availableQuantity =
        await sails.helpers.inventory.getAvailableQuantity(inputs.ID); // Add on available quantity

        // All items
    } else {
      records = await sails.models.items.find();
    }

    // Subscribe to sockets if applicable
    if (this.req.isSocket && !inputs.ID) {
      sails.sockets.join(this.req, "items");
      sails.log.verbose("Request was a socket. Joining items.");
    }

    return records;
  },
};
