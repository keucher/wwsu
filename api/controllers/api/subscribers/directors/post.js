const sh = require("shorthash");

module.exports = {
  friendlyName: "POST api/subscribers/directors",

  description:
    "Add a push notification subscription to director-related matters.",

  inputs: {
    device: {
      type: "string",
      required: true,
      description: "The OneSignal device ID of the subscriber.",
    },

    type: {
      type: "string",
      required: true,
      isIn: ["accountability-shows", "accountability-directors", "emergencies"],
      description: "The main type of the subscription",
    },
  },

  fn: async function (inputs) {
    // Get the client IP address
    let host = await sails.helpers.getHost(this.req);

    // Use find or create so that duplicate subscriptions do not happen (ignore host when checking for duplicates).
    let record = await sails.models.subscribers.findOrCreate(inputs, {
      host: `website-${host}`,
      device: inputs.device,
      type: inputs.type,
      subtype: this.req.payload.ID,
      description: `Director notifications: ${inputs.subtype}`,
    });

    return record;
  },
};
