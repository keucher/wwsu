module.exports = {
  friendlyName: "GET api/subscribers/web",

  description:
    "Retrieve an array of the active push notification subscriptions for the specified device, and subscribe to sockets.",

  inputs: {
    device: {
      type: "string",
      required: true,
      description: "The OneSignal ID of the device to get subscriptions for.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Get subscriptions from this host
    let records = await sails.models.subscribers.find({
      device: inputs.device,
    });

    // Subscribe to websocket if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, `subscribers-${inputs.device}`);
    }

    return records;
  },
};
