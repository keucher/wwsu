const sh = require("shorthash");

module.exports = {
  friendlyName: "POST api/subscribers/web",

  description: "Add a push notification subscription.",

  inputs: {
    device: {
      type: "string",
      required: true,
      description: "The OneSignal device ID of the subscriber.",
    },

    type: {
      type: "string",
      required: true,
      isIn: ["calendar", "blog"],
      description: "The main type of the subscription",
    },

    subtype: {
      type: "string",
      required: true,
      description: "The subtype of the subscription",
    },

    description: {
      type: "string",
      required: true,
      description: "Human friendly description of this subscription",
    },
  },

  fn: async function (inputs) {
    // Get the client host
    let host = await sails.helpers.getHost(this.req);

    // Calendar subscriptions? Careful not to double-subscribe!
    if (inputs.type === "calendar") {
      // Function for calculating which calendar subscriptions trump others.
      const priority = (subtype) => {
        let temp = subtype.split("-");
        if (!temp[1] || (temp[1] === "null" && !temp[2])) return 1; // Only one part? We are subscribing to a whole calendar event, which is top priority.

        if (temp[1] !== "null") return 3; // If the second part contains a value, it's a one-time value, so we are only subscribing to a specific date/time.

        if (temp[2]) return 2; // At this point, part 2 is null and we have a schedule ID defined; we are subscribing to a schedule.

        throw new Error("Problem calculating priority");
      };

      let records = await sails.models.subscribers.find({
        device: inputs.device,
        type: "calendar",
      });

      // This is a top-level subscription, so remove all other subscriptions by the same calendar ID as this trumps them.
      if (priority(inputs.subtype) === 1) {
        let maps = records.map(async (record) => {
          let temp = record.subtype.split("-");
          if (temp[0] === inputs.subtype)
            await sails.models.subscribers.destroyOne({ ID: record.ID });
        });
        await Promise.all(maps);
      } else if (priority(inputs.subtype) === 2) {
        // Schedule subscription
        let temp = inputs.subtype.split("-");

        // Bail adding this subscription if we are already subscribed to the top calendar event
        if (records.find((record) => record.subtype === temp[0]))
          return "Already subscribed";

        // Remove single occurrence subscriptions for the same schedule
        let maps = records.map(async (record) => {
          let temp2 = record.subtype.split("-");
          if (
            temp2[1] &&
            temp2[1] !== "null" &&
            temp2[2] &&
            temp2[2] === temp[2]
          )
            await sails.models.subscribers.destroyOne({ ID: record.ID });
        });
        await Promise.all(maps);
      } else {
        // Single occurrence subscriptions
        let temp = inputs.subtype.split("-");

        // Bail adding this subscription if we are already subscribed to the top calendar event or the schedule
        if (
          records.find(
            (record) =>
              record.subtype === temp[0] ||
              (temp[2] && record.subtype === `${temp[0]}-null-${temp[2]}`)
          )
        )
          return "Already subscribed.";
      }
    }

    // Add the subscription but only if it does not already exist.
    let record = await sails.models.subscribers.findOrCreate(
      {
        device: inputs.device,
        type: inputs.type,
        subtype: inputs.subtype,
      },
      {
        host: `website-${host}`,
        device: inputs.device,
        type: inputs.type,
        subtype: inputs.subtype,
        description: inputs.description,
      }
    );

    return record;
  },
};
