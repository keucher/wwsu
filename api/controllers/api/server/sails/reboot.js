module.exports = {
  friendlyName: "POST api/server/sails/reboot",

  description: "Reboot sails app.",

  inputs: {},

  exits: {
    forbodden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    if (
      !sails.config.custom.basic.maintenance &&
      [
        "automation_on",
        "automation_break",
        "automation_playlist",
        "automation_genre",
        "unknown",
        "",
      ].indexOf(sails.models.meta.memory.state) === -1
    ) {
      throw {
        forbidden:
          "Rejected; a broadcast is in progress. If you ABSOLUTELY MUST reboot Sails right now, activate maintenance mode and try again.",
      };
    }

    sails.lower((err) => {
      if (err) throw err;
      process.exit(err ? 1 : 0);
    });
  },
};
