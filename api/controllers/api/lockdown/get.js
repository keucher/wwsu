module.exports = {
  friendlyName: "GET api/lockdown",

  description:
    "Get lockdown records. If no filters specified, will only return a list of currently clocked in records (and subscribe to sockets).",

  inputs: {
    date: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of a date to get logs.`,
    },
    start: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of a date which the returned logs should start from.`,
    },
    end: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of a date which the returned logs should end at.`,
    },
    host: {
      type: "number",
      allowNull: true,
      description: "Search for records for a specific host.",
    },
    type: {
      type: "string",
      allowNull: true,
      description: "Search by member type who logged in",
    },
    typeID: {
      type: "number",
      allowNull: true,
      description:
        "Search for records for a specific ID (may also want to use type based search).",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Nothing specified; return only clocked in records and subscribe to sockets.
    if (
      !inputs.start &&
      !inputs.end &&
      !inputs.date &&
      !inputs.host &&
      !inputs.type &&
      !inputs.typeID
    ) {
      
      // Subscribe to sockets if applicable
      if (this.req.isSocket) {
        sails.sockets.join(this.req, "lockdown");
        sails.log.verbose("Request was a socket. Joining lockdown.");
      }

      return await sails.models.lockdown.find({ clockOut: null });
    }

    // Get date range
    let start = inputs.date
      ? moment(inputs.date).startOf("day")
      : moment().startOf("day");
    let end = moment(start).add(1, "days");
    if (inputs.start) {
      start = moment(inputs.start);
    }
    if (inputs.end) {
      end = moment(inputs.end);
    }

    // Prepare query
    let query = {};

    if (inputs.start || inputs.end || inputs.date)
      query.createdAt = {
        ">=": start.format("YYYY-MM-DD HH:mm:ss"),
        "<": end.format("YYYY-MM-DD HH:mm:ss"),
      };
    if (inputs.host) query.host = inputs.host;
    if (inputs.type) query.type = inputs.type;
    if (inputs.typeID) query.typeID = inputs.typeID;

    return await sails.models.lockdown.find(query).sort("createdAt ASC");
  },
};
