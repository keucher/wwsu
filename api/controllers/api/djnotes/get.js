module.exports = {
  friendlyName: "GET api/djnotes",

  description:
    "Join websockets for dj notes. Specify a specific dj to retrieve records for them.",

  inputs: {
    dj: {
      type: "number",
      allowNull: true,
      description: "The DJ ID which to view information.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Get records if dj was specified and return them.
    if (inputs.dj && inputs.dj !== null) {
      let records = await sails.models.djnotes.find({ dj: inputs.dj });
      return records;
    }

    // Join socket if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "djnotes");
      sails.log.verbose("Request was a socket. Joining djnotes.");
    }

    return; // Do not return DJ notes if we did not specify a dj.
  },
};
