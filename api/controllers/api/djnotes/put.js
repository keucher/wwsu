module.exports = {
  friendlyName: "PUT api/djnotes",

  description: "Edit djnotes record.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the record to modify.",
    },
    dj: {
      type: "number",
      description:
        "The DJ ID this record belongs to. If provided, will overwrite the original value.",
    },
    date: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `If provided, the moment() parsable string of a date in which the record took effect.`,
    },
    type: {
      type: "string",
      allowNull: true,
      description:
        "The type of note record (remote-* for remote credits, warning-* for warning points, public-* for public notes visible to the DJ, private-* for records not visible to the DJ.).",
    },
    description: {
      type: "string",
      allowNull: true,
      description:
        "If provided, the description for this record will be edited to what is provided.",
    },
    amount: {
      type: "number",
      allowNull: true,
      description: "If provided, the amount will be edited to this.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Determine what needs updating
    let criteria = {};
    if (inputs.dj !== null && typeof inputs.dj !== "undefined") {
      criteria.dj = inputs.dj;
    }
    if (inputs.type !== null && typeof inputs.type !== "undefined") {
      criteria.type = inputs.type;
    }
    if (
      inputs.description !== null &&
      typeof inputs.description !== "undefined"
    ) {
      criteria.description = inputs.description;
    }
    if (inputs.amount !== null && typeof inputs.amount !== "undefined") {
      criteria.amount = inputs.amount;
    }
    if (inputs.date !== null && typeof inputs.date !== "undefined") {
      criteria.date = inputs.date;
    }

    // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
    let criteriaB = _.cloneDeep(criteria);

    // Edit it
    let record = await sails.models.djnotes.updateOne(
      { ID: inputs.ID },
      criteriaB
    );
    if (!record) throw { notFound: "DJ note was not found." };

    return record;
  },
};
