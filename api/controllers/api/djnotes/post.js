module.exports = {
  friendlyName: "POST api/djnotes",

  description: "Add a djnotes record.",

  inputs: {
    djs: {
      type: "json",
      required: true,
      description: "An array of DJ IDs this note applies to.",
      custom: (value) => {
        if (!value.constructor === Array) return false; // Must be array
        if (!value.length) return false; // Must contain at least 1 item
        return value.find((val) => typeof val !== "number") ? false : true; // Every item must be a number
      },
    },
    date: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of a date in which the record took effect.`,
    },
    type: {
      type: "string",
      required: true,
      description:
        "The type of note record (remote-* for remote credits, warning-* for warning points, public-* for public notes visible to the DJ, private-* for records not visible to the DJ.).",
    },
    description: {
      type: "string",
      required: true,
      description: "A description for this record.",
    },
    amount: {
      type: "number",
      description:
        "For remote credits, the number of credits earned. For warning, the warning points assigned.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    let date =
      inputs.date !== null && typeof inputs.date !== "undefined"
        ? moment(inputs.date).format("YYYY-MM-DD HH:mm:ss")
        : moment().format("YYYY-MM-DD HH:mm:ss"); // Default to now if date not specified

    let records = [];

    // Process the records.
    let maps = inputs.djs.map(async (dj) => {
      // Add the note
      records.push(
        await sails.models.djnotes
          .create({
            dj: dj,
            type: inputs.type,
            description: inputs.description,
            amount: inputs.amount,
            date: date,
          })
          .fetch()
      );

      // Update lastSeen of the DJs
      await sails.models.djs.updateOne(
        {
          ID: dj,
          lastSeen: { "<": date },
        },
        { lastSeen: date }
      );
    });

    await Promise.all(maps);

    return records;
  },
};
