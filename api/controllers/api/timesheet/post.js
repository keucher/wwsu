module.exports = {
  friendlyName: "POST api/timesheet",

  description: "Add a timesheet entry for a director.",

  inputs: {
    timestamp: {
      type: "string",
      required: true,
      custom: function (value) {
        return moment(value).isValid();
      },
      description: "A moment.js compatible timestamp for the timesheet entry.",
    },
    notes: {
      type: "string",
    },
    remote: {
      type: "boolean",
      defaultsTo: false,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
    internalError: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    let toapprove = 0;
    let dj;

    // Get the director
    let record = await sails.models.directors.findOne({
      ID: this.req.payload.ID,
    });
    let thetime;

    // No director? return not found.
    if (typeof record === "undefined" || record.length <= 0) {
      throw { internalError: "The authorized director was not found." };
    }

    // Filter disallowed HTML
    inputs.notes = await sails.helpers.sanitize(inputs.notes);

    // If the director is present, this is a clock-out entry.
    if (record.present > 0) {
      toapprove = 0;
      thetime = moment(inputs.timestamp);

      // If the entry is less than 30 minutes off from the current time, approve automatically
      if (
        thetime.isAfter(moment().subtract(30, "minutes")) &&
        thetime.isBefore(moment().add(30, "minutes"))
      ) {
        toapprove = 1;
      }

      // Edge case example: It is 5PM. A director clocks in for 1PM. Director has scheduled hours for 1-3PM and 4-6PM. Ensure there's a record for all in-between hours.
      let records = await sails.models.timesheet
        .find({
          name: record.name,
          approved: { ">=": 0 },
          timeIn: null,
          timeOut: null,
          scheduledIn: { "<": moment(thetime).format("YYYY-MM-DD HH:mm:ss") },
          scheduledOut: {
            ">=": moment(record.since).format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .sort("scheduledIn ASC");

      if (records.length > 0) {
        let theStart;
        let theEnd;

        // Sequential async
        records.reduce(async (prevReturn, record) => {
          return await (async (recordB) => {
            if (!theEnd) {
              theStart = recordB.scheduledIn;
            } else {
              theStart = theEnd;
            }
            theEnd = recordB.scheduledOut;
            recordsX = recordsX.concat(
              await sails.models.timesheet
                .update(
                  { ID: recordB.ID },
                  {
                    timeIn: moment(theStart).format("YYYY-MM-DD HH:mm:ss"),
                    timeOut: moment(theEnd).format("YYYY-MM-DD HH:mm:ss"),
                    approved: 0,
                    notes: inputs.notes,
                  }
                )
                .fetch()
            );
          })(record);
        }, null);

        // Add special clock-out entry
        await sails.models.timesheet
          .update(
            {
              timeIn: { "!=": null },
              timeOut: null,
              name: record.name,
              approved: { ">=": 1 },
            },
            {
              timeIn: moment(theEnd).format("YYYY-MM-DD HH:mm:ss"),
              timeOut: thetime.format("YYYY-MM-DD HH:mm:ss"),
              approved: toapprove,
              notes: inputs.notes,
            }
          )
          .fetch();
        await sails.models.timesheet
          .update(
            {
              timeIn: { "!=": null },
              timeOut: null,
              name: record.name,
              approved: { "<": 1 },
            },
            {
              timeIn: moment(theEnd).format("YYYY-MM-DD HH:mm:ss"),
              timeOut: thetime.format("YYYY-MM-DD HH:mm:ss"),
              notes: inputs.notes,
            }
          )
          .fetch();
      } else {
        // Add normal clock-out entry
        await sails.models.timesheet
          .update(
            {
              timeIn: { "!=": null },
              timeOut: null,
              name: record.name,
              approved: { ">=": 1 },
            },
            {
              timeOut: thetime.format("YYYY-MM-DD HH:mm:ss"),
              approved: toapprove,
              notes: inputs.notes,
            }
          )
          .fetch();
        await sails.models.timesheet
          .update(
            {
              timeIn: { "!=": null },
              timeOut: null,
              name: record.name,
              approved: { "<": 1 },
            },
            {
              timeOut: thetime.format("YYYY-MM-DD HH:mm:ss"),
              notes: inputs.notes,
            }
          )
          .fetch();
      }

      dj = await sails.models.djs.find({
        realName: record.name,
      });
      if (dj[0]) {
        // Update lastSeen of the DJ
        await sails.models.djs.updateOne(
          {
            ID: dj[0].ID,
          },
          { lastSeen: thetime.format("YYYY-MM-DD HH:mm:ss") }
        );
      }

      // Update the director presence
      await sails.models.directors
        .update(
          { ID: record.ID },
          { present: 0, since: thetime.format("YYYY-MM-DD HH:mm:ss") }
        )
        .fetch();

      if (toapprove === 0) {
        await sails.models.logs
          .create({
            attendanceID: null,
            logtype: "director-unapproved",
            loglevel: "warning",
            logsubtype: record.name,
            logIcon: `fas fa-user-times`,
            title: `A director's logged clock-out hours need reviewed / approved!`,
            event: `Director: ${record.name}<br />Clock-out time: ${moment(
              thetime
            ).format("LLLL")}`,
            createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
          })
          .fetch()
          .tolerate((err) => {
            sails.log.error(err);
          });
        await sails.helpers.onesignal.sendMass(
          "accountability-directors",
          "Timesheet Needs Approved in DJ Controls",
          `${record.name}'s timesheet ending on ${moment(thetime).format(
            "LLLL"
          )} has been flagged and needs reviewed/approved because the director set their clock-out time more than 30 minutes from the current time.`
        );
      }
    } else {
      // If the director is not present, this is a clock-in entry.
      toapprove = 0;
      thetime = moment(inputs.timestamp);

      // Check if an office hours record exists.
      let calendar = sails.models.calendar.calendardb.whoShouldBeIn();

      if (calendar.length > 0)
        calendar = calendar.find((cal) => cal.director === record.ID);

      // If the entry is less than 30 minutes off from the current time, approve automatically
      if (
        thetime.isAfter(moment().subtract(30, "minutes")) &&
        thetime.isBefore(moment().add(30, "minutes"))
      ) {
        toapprove = 1;
      }

      // Clock-ins need a new entry
      await sails.models.timesheet
        .create({
          name: record.name,
          unique: calendar ? calendar.unique : null,
          scheduledIn: calendar
            ? moment(calendar.start).format("YYYY-MM-DD HH:mm:ss")
            : null,
          scheduledOut: calendar
            ? moment(calendar.end).format("YYYY-MM-DD HH:mm:ss")
            : null,
          timeIn: thetime.format("YYYY-MM-DD HH:mm:ss"),
          approved: toapprove,
          remote: inputs.remote,
        })
        .fetch();

      dj = await sails.models.djs.find({
        realName: record.name,
      });
      if (dj[0]) {
        // Update lastSeen of the DJ
        await sails.models.djs.updateOne(
          {
            ID: dj[0].ID,
          },
          { lastSeen: thetime.format("YYYY-MM-DD HH:mm:ss") }
        );
      }

      // Update the director presence
      await sails.models.directors
        .update(
          { ID: record.ID },
          {
            present: inputs.remote ? 2 : 1,
            since: thetime.format("YYYY-MM-DD HH:mm:ss"),
          }
        )
        .fetch();

      if (toapprove === 0) {
        await sails.models.logs
          .create({
            attendanceID: null,
            logtype: "director-unapproved",
            loglevel: "warning",
            logsubtype: record.name,
            logIcon: `fas fa-user-times`,
            title: `A director's logged clock-in hours need reviewed / approved!`,
            event: `Director: ${record.name}<br />Clock-in time: ${moment(
              thetime
            ).format("LLLL")}`,
            createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
          })
          .fetch()
          .tolerate((err) => {
            sails.log.error(err);
          });
        await sails.helpers.onesignal.sendMass(
          "accountability-directors",
          "Timesheet Needs Approved in DJ Controls",
          `${record.name}'s timesheet, beginning on ${moment(thetime).format(
            "LLLL"
          )}, has been flagged and needs reviewed/approved because the director set their clock-in time more than 30 minutes from the current time.`
        );
      }
    }
    return;
  },
};
