module.exports = {
  friendlyName: "GET api/timesheet",

  description:
    "Get a week of timesheet entries, and subscribe to sockets if start not provided.",

  inputs: {
    start: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      description: `moment() parsable string of the start date/time to get records. Defaults to 14 days ago.`,
    },
    end: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      description: `moment() parsable string of the end date/time to get records. Defaults to now.`,
    },
    endInclusive: {
      type: "boolean",
      deafultsTo: true,
      description:
        "If true, will use start of next day for end instead of exact date/time. Set this to false when specifying a specific time for end or when end date should be exclusive.",
    },
  },

  fn: async function (inputs) {
    if (!inputs.start || inputs.start === null) {
      // Join timesheet socket if applicable
      if (this.req.isSocket) {
        sails.sockets.join(this.req, "timesheet");
        sails.log.verbose("Request was a socket. Joining timesheet.");
      }
    }

    // Get the time ranges
    var start = inputs.start
      ? moment(inputs.start)
      : moment().subtract(2, "weeks");
    var end = inputs.end
      ? inputs.endInclusive
        ? moment(inputs.end).add(1, "days")
        : moment(inputs.end)
      : moment();

    // Get timesheet records
    var records = await sails.models.timesheet
      .find({
        or: [
          {
            timeIn: {
              ">=": start.format("YYYY-MM-DD HH:mm:ss"),
              "<": end.format("YYYY-MM-DD HH:mm:ss"),
            },
          },
          {
            timeOut: {
              ">=": start.format("YYYY-MM-DD HH:mm:ss"),
              "<": end.format("YYYY-MM-DD HH:mm:ss"),
            },
          },
          {
            timeIn: null,
            timeOut: null,
            scheduledIn: {
              ">=": start.format("YYYY-MM-DD HH:mm:ss"),
              "<": end.format("YYYY-MM-DD HH:mm:ss"),
            },
          },
          {
            timeIn: null,
            timeOut: null,
            scheduledOut: {
              ">=": start.format("YYYY-MM-DD HH:mm:ss"),
              "<": end.format("YYYY-MM-DD HH:mm:ss"),
            },
          },
        ],
      })
      .sort([{ timeIn: "ASC" }, { scheduledIn: "ASC" }]);

    // return the records
    return records;
  },
};
