module.exports = {
  friendlyName: "DELETE api/timesheet/:ID",

  description: "Remove a timesheet entry.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID number of the Timesheet to remove.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Delete the timesheet record
    let record = await sails.models.timesheet.destroyOne({ ID: inputs.ID });

    // Force a re-load of all directors to update any possible changes in presence
    await sails.helpers.directors.update();

    return record;
  },
};
