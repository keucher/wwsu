const bcrypt = require("bcrypt");
const sh = require("shorthash");

module.exports = {
  friendlyName: "GET api / auth / director",

  description: "Authorize a director and get a token.",

  inputs: {
    username: {
      type: "string",
      description: "The name of the director to authorize.",
      required: true,
    },

    password: {
      type: "string",
      description: "Director login to authorize.",
      required: true,
    },
  },

  exits: {
    success: {
      statusCode: 200,
    },
    wrong: {
      statusCode: 403,
    },
    error: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Verify the Director exists first
    let director = await sails.models.directors.findOne({
      name: inputs.username,
      login: { "!=": null },
    });
    if (!director) {
      throw {
        wrong:
          "The username or password is wrong, or it points to a director that does not exist or is not allowed to authorize.",
      };
    }

    // Now check the password
    let match = await bcrypt.compare(inputs.password, director.login);

    if (!match) {
      throw {
        wrong:
          "The username or password is wrong, or it points to a director that does not exist or is not allowed to authorize.",
      };
    }

    // Sometimes sockets will have an undefined session in Sails, which triggers errors. Make an empty object if this happens.
    //if (typeof this.req.session === "undefined") this.req.session = {};

    // Generate the token valid for 10 minutes
    this.req.session.authDirector = {
      ID: director.ID,
      name: director.name,
      exp: moment().add(10, "minutes").toISOString(true),
    };

    // Return the token as an object
    return {
      token: sh.unique(`${director.ID}-${this.req.session.authDirector.exp}`),
      expires: 60000 * 10,
    };
  },
};
