module.exports = {
  friendlyName: "PUT api/logs/:ID",

  description:
    "Edit the excused or acknowledged state of a log entry (nothing else may be edited).",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the log entry to edit.",
    },

    acknowledged: {
      type: "boolean",
      description: "Change the acknowledged state of the log entry.",
    },

    excused: {
      type: "boolean",
      description: "Change the excused state of the log entry.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let criteria = {};
    if (typeof inputs.acknowledged !== "undefined")
      criteria.acknowledged = inputs.acknowledged;
    if (typeof inputs.excused !== "undefined")
      criteria.excused = inputs.excused;

    let criteriaB = _.cloneDeep(criteria);

    let record = await sails.models.logs.updateOne(
      { ID: inputs.ID },
      criteriaB
    );
    if (!record) throw { notFound: "The provided log ID was not found." };

    return record;
  },
};
