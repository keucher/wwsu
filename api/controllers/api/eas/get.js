module.exports = {
  friendlyName: "GET api/eas",

  description: "Get the currently active EAS alerts.",

  inputs: {
    jsonp: {
      type: "string",
      description:
        "If using a cross-site origin request (JSONP) from The Guardian Media Group, specify the callback name here.",
    },
  },

  fn: async function (inputs) {
    // Get records
    let records = await sails.models.eas.find();
    sails.log.verbose(`Retrieved Eas records: ${records.length}`);
    sails.log.silly(records);

    // Subscribe to sockets, if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "eas");
      sails.log.verbose("Request was a socket. Joining eas.");
    }

    // If JSONP function name provided, return as a JavaScript function to eval with the data as a parameter.
    if (inputs.jsonp) {
      return `${inputs.jsonp}(${JSON.stringify(records)});`;
    }

    return records;
  },
};
