module.exports = {
  friendlyName: "POST /api/eas/test",

  description: "Issue a test in the WWSU Emergency Alert System.",

  inputs: {},

  fn: async function (inputs) {
    // Add test alert
    let value = moment().valueOf();
    let record = await sails.helpers.eas.addAlert(
      value,
      "WWSU 106.9 FM",
      "WWSU listening area at Wright State University Dayton campus",
      "Test",
      "Minor",
      moment().format("YYYY-MM-DD HH:mm:ss"),
      moment().add(3, "minutes").format("YYYY-MM-DD HH:mm:ss"),
      "#FFFFFF",
      `This is a test of the WWSU Emergency Alert System. This is only a test. The WWSU Emergency Alert System is designed to provide the public with timely emergency information, Wright State Campus Alerts, and National Weather Service alerts for the following locations: ${sails.config.custom.nws.map((nws) => nws.name).join(", ")}. Had this been an actual emergency, you would have received information and instructions regarding the emergency. This concludes the test of the WWSU Emergency Alert System.`,
      true
    );

    // Post EAS tasks (what actually pushes the new alert)
    await sails.helpers.eas.postParse();

    return record;
  },
};
