module.exports = {
  friendlyName: "PUT api/eas",

  description: "Resends / edits an EAS alert with new information.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the eas to edit.",
    },
    information: {
      type: "string",
      description: "If provided, the new information for this alert.",
    },
    counties: {
      type: "string",
      description:
        "If provided, the new list of counties this alert is in effect.",
    },
    starts: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      description: `If provided, a new moment() parsable string of when the alert starts. Recommended ISO string.`,
    },

    expires: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `If provided, a new moment() parsable string of when the alert expires. Recommended ISO string.`,
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Do not allow editing of automatic alerts
    let record = await sails.models.eas.findOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The EAS ID was not found." };
    if (
      ["The National Weather Service", "Wright State Alert"].indexOf(
        record.source
      ) !== -1
    )
      throw { forbidden: "Not allowed to edit an automatic alert." };

    // Get (and remove) the original record
    let recordB = await sails.models.eas.destroyOne({ ID: inputs.ID });
    if (!recordB) throw { notFound: "The record ID was not found." };

    // Add the alert to EAS
    let recordC = await sails.helpers.eas.addAlert(
      moment().valueOf(),
      "WWSU",
      inputs.counties || recordB.counties,
      recordB.alert,
      recordB.severity,
      inputs.starts !== null && typeof inputs.starts !== "undefined"
        ? moment(inputs.starts).format("YYYY-MM-DD HH:mm:ss")
        : recordB.starts,
      typeof inputs.expires !== "undefined"
        ? inputs.expires !== null
          ? moment(inputs.expires).format("YYYY-MM-DD HH:mm:ss")
          : null
        : recordB.expires,
      recordB.color,
      inputs.information || recordB.information
    );

    // Process post tasks (this is what actually pushes the new alert out)
    await sails.helpers.eas.postParse();

    return recordC;
  },
};
