module.exports = {
  friendlyName: "GET api/version/:app",

  description: "Check version of an application.",

  inputs: {
    app: {
      type: "string",
      required: true,
      description: "The application to check version.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Get record
    let record = await sails.models.version.findOne({ app: inputs.app });
    if (!record) throw { notFound: "The provided app does not exist." };

    // Subscribe to socket
    if (this.req.isSocket) {
      sails.sockets.join(this.req, `version-${inputs.app}`);
      sails.log.verbose("Request was a socket. Joining version.");
    }

    // Return version record
    return [record];
  },
};
