module.exports = {
  friendlyName: "PUT api/version/:app",

  description: "Change latest version of an application.",

  inputs: {
    app: {
      type: "string",
      required: true,
      description: "The application to change version.",
    },
    version: {
      type: "string",
      description: "The new latest semantic version",
    },
    downloadURL: {
      type: "string",
      isURL: true,
      description: "The URL to download the new version",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    var criteria = {};
    if (inputs.version) criteria.version = inputs.version;
    if (inputs.downloadURL) criteria.downloadURL = inputs.downloadURL;
    var criteriaB = _.cloneDeep(criteria);

    let record = await sails.models.version.updateOne(
      { app: inputs.app },
      criteriaB
    );
    if (!record) throw { notFound: "The provided app does not exist." };

    return record;
  },
};
