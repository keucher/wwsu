module.exports = {
  friendlyName: "GET api / analytics / yearly",

  description: "Receive analytics from the last year, combined by month.",

  inputs: {},

  fn: async function (inputs) {
    var months = {};

    // Loop through the last 12 months
    for (var i = 12; i > 0; i--) {
      var start = moment().subtract(i, "months");
      var end = moment().subtract(i - 1, "months");

      // Initialize the month
      months[moment(start).format("MMMM YYYY")] = {
        streamHits: 0,
        videoHits: 0,
        listenerHoursTotal: 0,
        listenerHoursShows: 0,
        listenerToShowtimeRatioTotal: 0,
        listenerToShowtimeRatioShows: 0,
        viewerHoursTotal: 0,
        viewerHoursShows: 0,
        viewerToShowtimeRatioTotal: 0,
        viewerToShowtimeRatioShows: 0,
        tracksLiked: 0,
        webMessages: 0,
        requestsPlaced: 0,
      };

      // Get listener analytics for the month
      var records = await sails.models.listeners
        .find({
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .sort(`createdAt ASC`);

      // Determine the times that listeners or viewers went up; count these as tune-ins.
      var prevAmount = 0;
      var prevAmount2 = 0;
      records.map((record) => {
        if (record.listeners > prevAmount) {
          months[moment(start).format("MMMM YYYY")].streamHits +=
            record.listeners - prevAmount;
        }
        if (record.viewers > prevAmount2) {
          months[moment(start).format("MMMM YYYY")].videoHits +=
            record.viewers - prevAmount2;
        }
        prevAmount2 = record.viewers;
      });

      // Calculate listenership
      months[moment(start).format("MMMM YYYY")].listenerHoursTotal =
        (await sails.models.attendance.sum("listenerMinutes", {
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        })) / 60;
      var showTime =
        (await sails.models.attendance.sum("showTime", {
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        })) / 60;
      months[moment(start).format("MMMM YYYY")].listenerToShowtimeRatioTotal =
        showTime > 0
          ? months[moment(start).format("MMMM YYYY")].listenerHoursTotal /
            showTime
          : 0;
      months[moment(start).format("MMMM YYYY")].listenerHoursShows =
        (await sails.models.attendance.sum("listenerMinutes", {
          dj: { "!=": null },
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        })) / 60;
      var showTimeB =
        (await sails.models.attendance.sum("showTime", {
          dj: { "!=": null },
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        })) / 60;
      months[moment(start).format("MMMM YYYY")].listenerToShowtimeRatioShows =
        showTime > 0
          ? months[moment(start).format("MMMM YYYY")].listenerHoursShows /
            showTimeB
          : 0;

      // Calculate viewership
      months[moment(start).format("MMMM YYYY")].viewerHoursTotal =
        (await sails.models.attendance.sum("viewerMinutes", {
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        })) / 60;
      months[moment(start).format("MMMM YYYY")].viewerToShowtimeRatioTotal =
        showTime > 0
          ? months[moment(start).format("MMMM YYYY")].viewerHoursTotal /
            showTime
          : 0;
      months[moment(start).format("MMMM YYYY")].viewerHoursShows =
        (await sails.models.attendance.sum("viewerMinutes", {
          dj: { "!=": null },
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        })) / 60;
      months[moment(start).format("MMMM YYYY")].viewerToShowtimeRatioShows =
        showTime > 0
          ? months[moment(start).format("MMMM YYYY")].viewerHoursShows /
            showTimeB
          : 0;

      // Liked tracks
      months[moment(start).format("MMMM YYYY")].tracksLiked =
        await sails.models.songsliked.count({
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        });

      // Messages exchanged
      months[moment(start).format("MMMM YYYY")].webMessages =
        await sails.models.messages.count({
          or: [
            { from: { startsWith: "website-" } },
            { to: ["DJ", "DJ-private"] },
          ],
          createdAt: {
            ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
            "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
          },
        });

      // Track requests
      months[moment(start).format("MMMM YYYY")].requestsPlaced = sails.config
        .custom.radiodjs.length
        ? await sails.models.requests.count({
            createdAt: {
              ">=": moment(start).format("YYYY-MM-DD HH:mm:ss"),
              "<": moment(end).format("YYYY-MM-DD HH:mm:ss"),
            },
          })
        : 0;
    }

    return months;
  },
};
