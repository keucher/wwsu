module.exports = {
  friendlyName: "GET api/rss",

  description: "Get rss feeds and subscribe to sockets.",

  inputs: {
    sources: {
      type: "json",
      required: true,
      description: "The RSS feed sources to get and subscribe.",
      custom: function (value) {
        if (value.constructor !== Array || value.length === 0) return false;
        let valid = true;
        value.forEach((source) => {
          if (typeof source !== "string") valid = false;
        });

        return valid;
      },
    },
  },

  exits: {},

  fn: async function (inputs) {
    let records = await sails.models.rss.find({ source: inputs.source });

    // If applicable, subscribe to the sockets
    if (this.req.isSocket) {
      inputs.sources.forEach((source) =>
        sails.sockets.join(this.req, `rss-${source}`)
      );
      sails.log.verbose("Request was a socket. Joining rss sockets.");
    }

    return records;
  },
};
