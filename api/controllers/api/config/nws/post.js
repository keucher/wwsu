module.exports = {
  friendlyName: "POST /api/config/nws",

  description: "Add a new NWS feed to check in the internal EAS.",

  inputs: {
    code: {
      type: "string",
      required: true,
      description: "The CAPS county or zone code",
    },

    name: {
      type: "string",
      required: true,
      description:
        "The human readable name for this location, such as the name of the county.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.confignws
      .create({
        code: inputs.code,
        name: inputs.name,
      })
      .fetch();
  },
};
