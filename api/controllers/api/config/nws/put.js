module.exports = {
  friendlyName: "PUT /api/config/nws/:ID",

  description: "Edit a NWS feed.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID to remove.",
    },

    code: {
      type: "string",
      description: "The CAPS county or zone code",
    },

    name: {
      type: "string",
      description:
        "The human readable name for this location, such as the name of the county.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.confignws.updateOne(
      { ID: inputs.ID },
      {
        code: inputs.code,
        name: inputs.name,
      }
    );
    if (!record) throw { notFound: "The provided NWS ID was not found." };

    return record;
  },
};
