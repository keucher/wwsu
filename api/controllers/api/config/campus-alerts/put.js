module.exports = {
  friendlyName: "PUT /api/config/campus-alerts",

  description: "Change the URL for the campus alerts feed.",

  inputs: {
    campusAlerts: {
      type: "string",
      isURL: true,
      allowNull: true,
      description:
        "The URL to the Wright State Campus Alerts JSON feed. Must NOT contain query parameters.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        campusAlerts: inputs.campusAlerts || null,
      }
    );
  },
};
