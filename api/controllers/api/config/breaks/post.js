module.exports = {
  friendlyName: "POST /api/config/breaks",

  description: "Add a break task to the system.",

  inputs: {
    type: {
      type: "string",
      required: true,
      description: "Type of the break",
      isIn: ["clockwheel", "automation", "live", "remote", "sports"],
    },

    subtype: {
      type: "string",
      required: true,
      custom: (value) => {
        // If the value is a number, it must be between 0 and 59 (for the minute of the hour)
        if (!isNaN(parseInt(value))) {
          let numericValue = parseInt(value);
          if (numericValue < 0 || numericValue > 59) return false;
          return true;
        }

        // If the value is otherwise an actual string, it must be an allowed value
        if (
          [
            "start",
            "before",
            "during",
            "duringHalftime",
            "after",
            "end",
          ].indexOf(value) === -1
        )
          return false;
        return true;
      },
      description:
        "The subtype (for clockwheel, the minute of the hour... for all other types, either start, before, during, duringHalftime, after, or end).",
    },

    order: {
      type: "number",
      defaultsTo: 1,
      description:
        "Tasks are executed in order from lowest to highest (ID is used when records have the same order)",
    },

    task: {
      type: "string",
      required: true,
      isIn: [
        "log",
        "queue",
        "queueDuplicates",
        "queueUnderwritings",
        "queueRequests",
        "queueAlerts"
      ],
    },

    event: {
      type: "string",
      allowNull: true,
      description: "For log tasks, this is the text to be logged.",
    },

    category: {
      type: "string",
      allowNull: true,
      description:
        "For queue tasks, the category (configured in the node server) to queue tracks from.",
    },

    quantity: {
      type: "number",
      defaultsTo: 1,
      description: "The number of tracks to queue for any queue related tasks.",
    },

    rules: {
      type: "string",
      isIn: ["noRules", "lenientRules", "strictRules"],
      defaultsTo: "noRules",
      description:
        "noRules = do not follow playlist rotation rules; lenientRules = follow rotation rules unless there are no more tracks that conform; strictRules = abandon queuing any more tracks if no more conform to rotation rules",
    },

    doWhen: {
      type: "json",
      defaultsTo: [],
      custom: (value) => {
        if (value.constructor !== Array) return false; // Must be an array

        let invalid = value.find(
          (item) =>
            typeof item !== "string" ||
            [
              "show",
              "remote",
              "sports",
              "prerecord",
              "genre",
              "playlist",
              "default",
            ].indexOf(item) === -1
        );
        return !invalid;
      },
      description:
        "Only execute this task during the specified types of broadcasts (empty array = all of them, the default). Accepted values: show, remote, sports, prerecord, genre, playlist, default (Default automation rotation).",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbreaks
      .create({
        type: inputs.type,
        subtype: inputs.subtype,
        order: inputs.order,
        task: inputs.task,
        event: inputs.event,
        category: inputs.category,
        quantity: inputs.quantity,
        rules: inputs.rules,
        doWhen: inputs.doWhen,
      })
      .fetch();
  },
};
