module.exports = {
  friendlyName: "DELETE /api/config/breaks/:ID",

  description: "Delete a break task.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the break task to delete.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configbreaks.destroyOne({ ID: inputs.ID });
    if (!record)
      throw { notFound: "The provided break task ID was not found." };

    return record;
  },
};
