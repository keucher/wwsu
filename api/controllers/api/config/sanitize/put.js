module.exports = {
  friendlyName: "PUT /api/config/sanitize",

  description: "Change configuration for sanitizeHtml.",

  inputs: {
    sanitize: {
      description:
        "Options for sanitize-html to prevent annoying or unsafe HTML from being used in messages and public locations.",
      type: "json",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        sanitize: inputs.sanitize,
      }
    );
  },
};
