module.exports = {
  friendlyName: "PUT /api/config/owncast",

  description: "Change Owncast settings.",

  inputs: {
    owncastStream: {
      description: "The URL to the Owncast video stream server for WWSU",
      type: "string",
      isURL: true,
      allowNull: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        owncastStream: inputs.owncastStream || null,
      }
    );
  },
};
