module.exports = {
  friendlyName: "PUT /api/config/status",

  description: "Configure status alarm thresholds.",

  inputs: {
    musicLibraryVerify1: {
      type: "number",
      min: 1,
      description:
        "Music library should go into critical status when this many tracks in RadioDJ are flagged as invalid / corrupt.",
    },

    musicLibraryVerify2: {
      type: "number",
      min: 1,
      defaultsTo: 50,
      description:
        "Music library should go into major status when this many tracks in RadioDJ are flagged as invalid / corrupt.",
    },

    musicLibraryVerify3: {
      type: "number",
      min: 1,
      defaultsTo: 10,
      description:
        "Music library should go into minor status when this many tracks in RadioDJ are flagged as invalid / corrupt.",
    },

    musicLibraryLong1: {
      type: "number",
      min: 1,
      description:
        "Music library should go into critical status when this many tracks in the music system category are longer than breakCheck minutes long.",
    },

    musicLibraryLong2: {
      type: "number",
      min: 1,
      description:
        "Music library should go into major status when this many tracks in the music system category are longer than breakCheck minutes long.",
    },

    musicLibraryLong3: {
      type: "number",
      min: 1,
      description:
        "Music library should go into minor status when this many tracks in the music system category are longer than breakCheck minutes long.",
    },

    server1MinLoad1: {
      type: "number",
      min: 0,
      description:
        "Server should go into critical status when the 1-minute CPU load exceeds this value (generally, set this to number of CPU cores * 8).",
    },

    server1MinLoad2: {
      type: "number",
      min: 0,
      description:
        "Server should go into major status when the 1-minute CPU load exceeds this value (generally, set this to number of CPU cores * 4).",
    },

    server1MinLoad3: {
      type: "number",
      min: 0,
      description:
        "Server should go into minor status when the 1-minute CPU load exceeds this value (generally, set this to number of CPU cores * 2).",
    },

    server5MinLoad1: {
      type: "number",
      min: 0,
      description:
        "Server should go into critical status when the 5-minute CPU load exceeds this value (generally, set this to number of CPU cores * 6).",
    },

    server5MinLoad2: {
      type: "number",
      min: 0,
      description:
        "Server should go into major status when the 5-minute CPU load exceeds this value (generally, set this to number of CPU cores * 3).",
    },

    server5MinLoad3: {
      type: "number",
      min: 0,
      description:
        "Server should go into minor status when the 5-minute CPU load exceeds this value (generally, set this to number of CPU cores * 1.5).",
    },

    server15MinLoad1: {
      type: "number",
      min: 0,
      description:
        "Server should go into critical status when the 15-minute CPU load exceeds this value (generally, set this to number of CPU cores * 4).",
    },

    server15MinLoad2: {
      type: "number",
      min: 0,
      description:
        "Server should go into major status when the 15-minute CPU load exceeds this value (generally, set this to number of CPU cores * 2).",
    },

    server15MinLoad3: {
      type: "number",
      min: 0,
      description:
        "Server should go into critical status when the 15-minute CPU load exceeds this value (generally, set this to number of CPU cores).",
    },

    serverMemory1: {
      type: "number",
      min: 0,
      description:
        "Server should go into critical status when the free / available memory (RAM) (in bytes) drops below this value (generally, set this to 5% total RAM capacity).",
    },

    serverMemory2: {
      type: "number",
      min: 0,
      description:
        "Server should go into major status when the free / available memory (RAM) (in bytes) drops below this value (generally, set this to 10% total RAM capacity).",
    },

    serverMemory3: {
      type: "number",
      min: 0,
      description:
        "Server should go into minor status when the free / available memory (RAM) (in bytes) drops below this value (generally, set this to 25% total RAM capacity).",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configstatus.updateOne(
      { ID: 1 },
      {
        musicLibraryVerify1: inputs.musicLibraryVerify1,
        musicLibraryVerify2: inputs.musicLibraryVerify2,
        musicLibraryVerify3: inputs.musicLibraryVerify3,
        musicLibraryLong1: inputs.musicLibraryLong1,
        musicLibraryLong2: inputs.musicLibraryLong2,
        musicLibraryLong3: inputs.musicLibraryLong3,
        server1MinLoad1: inputs.server1MinLoad1,
        server1MinLoad2: inputs.server1MinLoad2,
        server1MinLoad3: inputs.server1MinLoad3,
        server5MinLoad1: inputs.server5MinLoad1,
        server5MinLoad2: inputs.server5MinLoad2,
        server5MinLoad3: inputs.server5MinLoad3,
        server15MinLoad1: inputs.server15MinLoad1,
        server15MinLoad2: inputs.server15MinLoad2,
        server15MinLoad3: inputs.server15MinLoad3,
        serverMemory1: inputs.serverMemory1,
        serverMemory2: inputs.serverMemory2,
        serverMemory3: inputs.serverMemory3,
      }
    );
  },
};
