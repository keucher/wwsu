module.exports = {
  friendlyName: "PUT /api/config/scores",

  description:
    "Change settings pertaining to how broadcast scores are calculated to determine most performing broadcasts.",

  inputs: {
    minShowTime: {
      type: "number",
      min: 0,
      required: true,
      description:
        "Minimum number of minutes a broadcast must have been on the air to receive a score; broadcasts shorter than this will receive a score of 0.",
    },

    listenerRatioPoints: {
      type: "number",
      min: 0,
      required: true,
      description:
        "Number of points to award to a broadcast for every 1 online listener to showTime ratio (eg. every 1 average online listener).",
    },

    messagePoints: {
      type: "number",
      min: 0,
      required: true,
      description:
        "Award this many points for every 1 message exchanged per hour on average during the broadcast.",
    },

    idealBreaks: {
      type: "number",
      min: 1, // Top of hour break is required by the FCC; do not allow values below 1.
      required: true,
      description:
        "How many breaks per hour is the ideal number for a broadcast?",
    },

    breakPoints: {
      type: "number",
      min: 0,
      required: true,
      description:
        "Award this many points if a broadcast took the ideal number of breaks (or more) per hour on average; less points will be awarded for less breaks depending on the number of breaks taken. Broadcasts will never be awarded more than this many points for breaks.",
    },

    viewerRatioPoints: {
      type: "number",
      min: 0,
      required: true,
      description:
        "Award this many points for every 1 video viewer to showTime ratio (eg. every 1 average video stream viewer).",
    },

    videoStreamPoints: {
      type: "number",
      min: 0,
      required: true,
      description:
        "Award this many points if a broadcast streamed video their entire broadcast; less points will be awarded for less video stream time.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configanalytics.updateOne(
      { ID: 1 },
      {
        minShowTime: inputs.minShowTime,
        listenerRatioPoints: inputs.listenerRatioPoints,
        messagePoints: inputs.messagePoints,
        idealBreaks: inputs.idealBreaks,
        breakPoints: inputs.breakPoints,
        viewerRatioPoints: inputs.viewerRatioPoints,
        videoStreamPoints: inputs.videoStreamPoints,
      }
    );
  },
};
