const systems = [
  "analytics",
  "basic",
  "breaks",
  "categories",
  "discord",
  "displaysigns",
  "meta",
  "nws",
  "radiodjs",
  "status",
  "sports",
];

module.exports = {
  friendlyName: "GET api/config",

  description:
    "Retrieve the entire current configuration. Also subscribe to receive changes via the config websocket.",

  inputs: {
    systems: {
      type: "json",
      defaultsTo: [],
      custom: (value) => {
        // Property must be array
        if (value.constructor !== Array) return false;

        if (value.length > 0) {
          for (let item of value) {
            // Every item in array must be a string
            if (typeof item !== "string") return false;

            // Every item must be a valid system.
            if (systems.indexOf(item) === -1) return false;
          }
        }

        return true;
      },
      description:
        "Filter by config system / database, or use empty array (default) for all of them.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    let returnData = {};

    // Empty array? Use all systems
    if (inputs.systems.length === 0) inputs.systems = systems;

    // Load in the data
    for (let system of inputs.systems) {
      returnData[system] = await sails.models[`config${system}`].find();

      // Subscribe to websockets if applicable
      if (this.req.isSocket) {
        sails.sockets.join(this.req, `config-${system}`);
      }
    }

    if (typeof returnData["basic"] !== "undefined") {
      // Delete sensitive properties from return so they do not get exposed to the public
      returnData["basic"] = returnData["basic"].map((record) => {
        delete record.hostSecret;
        delete record.metaSecret;
        delete record.tomorrowioAPI;
        delete record.skywaySecret;
        delete record.oneSignalRest;
        return record;
      });
    }

    if (typeof returnData["radiodjs"] !== "undefined") {
      // Delete sensitive properties from return
      returnData["radiodjs"] = returnData["radiodjs"].map((record) => {
        delete record.restPassword;
        return record;
      });
    }

    return returnData;
  },
};
