module.exports = {
  friendlyName: "PUT api/config/secrets",

  description: "Change secrets in configuration.",

  inputs: {
    hostSecret: {
      description:
        "A random string to encode hosts and IP addresses. WARNING! Changing this will invalidate all active discipline!",
      type: "string",
      allowNull: true
    },

    metaSecret: {
      description:
        "The secret token parameter that should be provided for all calls to PUT /api/meta or GET /api/meta/update.",
      type: "string",
      allowNull: true
    },
  },

  exits: {},

  fn: async function (inputs) {
    let criteria = {
      hostSecret: inputs.hostSecret ? inputs.hostSecret : undefined, // Null = do not change
      metaSecret: inputs.metaSecret ? inputs.metaSecret : undefined, // Null = do not change
    };

    return await sails.models.configbasic.updateOne({ ID: 1 }, criteria);
  },
};
