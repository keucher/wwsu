module.exports = {
  friendlyName: "PUT /api/config/max-queue",

  description:
    "Change configuration for maximum allowed queue length when starting certain broadcasts.",

  inputs: {
    maxQueueLive: {
      description:
        "When starting a live show, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up going live.",
      type: "number",
      min: 0,
    },

    maxQueuePrerecord: {
      description:
        "When starting a prerecord, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting the prerecord.",
      type: "number",
      min: 0,
    },

    maxQueueSports: {
      description:
        "When starting a sports broadcast, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting.",
      type: "number",
      min: 0,
    },

    maxQueueSportsReturn: {
      description:
        "When returning from break during a sports broadcast, if total queue is greater than this many seconds, all non-commercials and IDs will be skipped/removed immediately to speed up going back on air.",
      type: "number",
      min: 0,
    },

    maxQueueRemote: {
      description:
        "When starting a remote broadcast, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting.",
      type: "number",
      min: 0,
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        maxQueueLive: inputs.maxQueueLive,
        maxQueuePrerecord: inputs.maxQueuePrerecord,
        maxQueueSports: inputs.maxQueueSports,
        maxQueueSportsReturn: inputs.maxQueueSportsReturn,
        maxQueueRemote: inputs.maxQueueRemote,
      }
    );
  },
};
