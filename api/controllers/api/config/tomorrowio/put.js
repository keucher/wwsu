module.exports = {
  friendlyName: "PUT /api/config/tomorrowio",

  description: "Change tomorrow.io settings for weather.",

  inputs: {
    tomorrowioAPI: {
      description:
        "API key for tomorrow.io weather platform to get current weather and forecasts.",
      type: "string",
      allowNull: true,
      encrypt: true,
    },

    tomorrowioLocation: {
      description:
        "Location parameter for tomorrow.io weather (such as latitude,longitude)",
      type: "string",
      allowNull: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        tomorrowioAPI: inputs.tomorrowioAPI || null,
        tomorrowioLocation: inputs.tomorrowioLocation || null,
      }
    );
  },
};
