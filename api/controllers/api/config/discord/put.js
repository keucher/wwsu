module.exports = {
  friendlyName: "PUT /api/config/discord",

  description: "Change Discord configuration.",

  inputs: {
    token: {
      type: "string",
      allowNull: true,
      description: "The Discord bot token from the developer portal.",
    },

    clientOptions: {
      type: "json",
      description: "Discord.js client options for the bot.",
    },

    guildWWSU: {
      type: "string",
      description: "The snowflake ID of the WWSU discord server.",
    },

    guildWNF: {
      type: "string",
      description: "The snowflake ID of the Wright News Feeds discord server.",
    },

    channelLive: {
      type: "string",
      description:
        "The snowflake ID of the text channel to post broadcasts that go on the air.",
    },

    channelScheduleChanges: {
      type: "string",
      description:
        "The snowflake ID of the text channel to post all broadcast schedule changes.",
    },

    channelRadio: {
      type: "string",
      description:
        "The snowflake ID of the voice channel for the bot to join to play the internet radio stream.",
    },

    channelSports: {
      type: "string",
      description:
        "The snowflake ID of the text channel where general sports discussions take place; this is where sports-related stuff will be posted / read.",
    },

    channelGeneral: {
      type: "string",
      description:
        "The snowflake ID of the text channel where general discussions take place (messages posted in here when a show is not broadcasting will be displayed in the messages / chat system).",
    },

    channelBlog: {
      type: "string",
      description:
        "The snowflake ID of the text channel where published blog posts should be posted.",
    },

    channelAnalytics: {
      type: "string",
      description:
        "The snowflake ID of the text channel where weekly analytics should be posted.",
    },

    messageAnalytics: {
      type: "string",
      description:
        "The snowflake ID of the message where weekly analytics should be posted / edited.",
    },

    channelWNF: {
      type: "string",
      description:
        "The snowflake ID of the text channel in the Wright News feeds to post things.",
    },

    categoryShowDefault: {
      type: "string",
      description:
        "The snowflake ID of the channel category to place new text channels for shows added to the calendar.",
    },

    categoryGeneral: {
      type: "string",
      description:
        "The snowflake ID of the channel category where general discussions take place.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configdiscord.updateOne(
      { ID: 1 },
      {
        token: inputs.token ? inputs.token : undefined, // Null = do not change
        clientOptions: inputs.clientOptions,
        guildWWSU: inputs.guildWWSU,
        guildWNF: inputs.guildWNF,
        channelLive: inputs.channelLive,
        channelScheduleChanges: inputs.channelScheduleChanges,
        channelRadio: inputs.channelRadio,
        channelSports: inputs.channelSports,
        channelGeneral: inputs.channelGeneral,
        channelBlog: inputs.channelBlog,
        channelAnalytics: inputs.channelAnalytics,
        messageAnalytics: inputs.messageAnalytics,
        channelWNF: inputs.channelWNF,
        categoryShowDefault: inputs.categoryShowDefault,
        categoryGeneral: inputs.categoryGeneral,
      }
    );
  },
};
