module.exports = {
  friendlyName: "PUT /api/config/radiodjs/:ID",

  description: "Put radiodjs.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The RadioDJ ID record to modify.",
    },

    name: {
      type: "string",
      description: "Alphanumeric name",
      regex: /^[a-zA-Z0-9-_]+$/
    },

    label: {
      type: "string",
      description: "Friendly name",
    },

    restURL: {
      type: "string",
      isURL: true,
      description: "The URL to the REST server for this RadioDJ",
    },

    restPassword: {
      type: "string",
      description: "The password for the RadioDJ REST server",
    },

    errorLevel: {
      type: "number",
      min: 1,
      max: 5,
      description:
        "The error level that should be reported when this RadioDJ is not working (5 = good, 4 = info, 3 = minor, 2 = major, 1 = critical)",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configradiodjs.updateOne(
      { ID: inputs.ID },
      {
        name: inputs.name,
        label: inputs.label,
        restURL: inputs.restURL,
        restPassword: inputs.restPassword,
        errorLevel: inputs.errorLevel,
      }
    );

    if (!record) throw { notFound: "The provided RadioDJ ID was not found." };

    return record;
  },
};
