module.exports = {
  friendlyName: "PUT /api/config/maintenance",

  description: "Turn maintenance mode on or off.",

  inputs: {
    maintenance: {
      description:
        "When true, many regular checks will be disabled. Enable when performing maintenance.",
      type: "boolean",
    },
  },

  exits: {},

  fn: async function (inputs) {
    let criteria = {
      maintenance: inputs.maintenance,
    };

    return await sails.models.configbasic.updateOne({ ID: 1 }, criteria);
  },
};
