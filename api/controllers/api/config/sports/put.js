module.exports = {
  friendlyName: "PUT /api/config/sports/:ID",

  description: "Change a sport in the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The sport ID to edit.",
    },

    name: {
      type: "string",
      required: true,
      description:
        "Name of the sport. MUST exist as a subcategory in each of the RadioDJ categories of Sports Openers, Sports Liners, Sports Returns, and Sports Closers for it to work properly.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configsports.updateOne(
      { ID: inputs.ID },
      {
        name: inputs.name,
      }
    );
    if (!record) throw { notFound: "The provided sport ID was not found." };

    return record;
  },
};
