module.exports = {
  friendlyName: "POST /api/config/sports",

  description: "Add a sport to the system.",

  inputs: {
    name: {
      type: "string",
      required: true,
      description:
        "Name of the sport. MUST exist as a subcategory in each of the RadioDJ categories of Sports Openers, Sports Liners, Sports Returns, and Sports Closers for it to work properly.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configsports
      .create({ name: inputs.name })
      .fetch();
  },
};
