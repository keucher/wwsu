module.exports = {
  friendlyName: "PUT /api/config/websites",

  description: "Change websotes / social media links.",

  inputs: {
    website: {
      description: "URL to the WWSU website",
      type: "string",
      allowNull: true,
      isURL: true,
    },

    recordings: {
      description: "URL where broadcast recordings can be downloaded (redirected from /recordings)",
      type: "string",
      allowNull: true,
      isURL: true,
    },

    facebook: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU Facebook page.",
    },

    twitter: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU Twitter page.",
    },

    youtube: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU YouTube page.",
    },

    instagram: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "URL to the WWSU Instagram page.",
    },

    discord: {
      type: "string",
      isURL: true,
      allowNull: true,
      description: "Permanent invite link to the WWSU Discord server.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        website: inputs.website || null,
        recordings: inputs.recordings || null,
        facebook: inputs.facebook || null,
        twitter: inputs.twitter || null,
        youtube: inputs.youtube || null,
        instagram: inputs.instagram || null,
        discord: inputs.discord || null,
      }
    );
  },
};
