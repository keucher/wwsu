module.exports = {
  friendlyName: "PUT /api/config/shoutcast",

  description: "Change shoutcast settings",

  inputs: {
    shoutcastStream: {
      description:
        "The URL to the Shoutcast v2 internet stream server for WWSU. WWSU's stream must be the first one.",
      type: "string",
      isURL: true,
      allowNull: true,
    },
    shoutcastStreamID: {
      description:
        "The id number of the Shoutcast stream for WWSU on the Shoutcast server.",
      type: "number",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        shoutcastStream: inputs.shoutcastStream || null,
        shoutcastStreamID: inputs.shoutcastStreamID,
      }
    );
  },
};
