module.exports = {
  friendlyName: "POST /api/config/categories",

  description: "Add a new category.",

  inputs: {
    name: {
      type: "string",
      required: true,
    },

    categories: {
      type: "json",
      required: true,
      description:
        "Map of RadioDJ categories and subcategories assigned to this system category. Should be an object... each key is the name of the RadioDJ main category, and value is an array of subcategory names in the main category (or empty array for all of them).",
      custom: (value) => {
        if (typeof value !== "object") return false;

        let valid = true;
        let hasOneKey = false; // There must exist at least one key

        for (let key in value) {
          if (!Object.prototype.hasOwnProperty.call(value, key)) continue;

          // All property keys must be a string
          if (typeof key !== "string") {
            valid = false;
            break;
          }

          hasOneKey = true;

          // Values must be an array
          if (value[key].constructor !== Array) {
            valid = false;
            break;
          }

          // Every item in the value array must be a string
          if (value[key].length) {
            value[key].forEach((subcat) => {
              if (typeof subcat !== "string") valid = false;
            });
          }
        }

        return hasOneKey && valid;
      },
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configcategories
      .create({
        name: inputs.name,
        categories: inputs.categories,
      })
      .fetch();
  },
};
