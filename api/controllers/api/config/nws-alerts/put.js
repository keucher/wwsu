module.exports = {
  friendlyName: "PUT /api/config/nws-alerts",

  description:
    "Change which NWS alerts we process in the internal EAS and the color code associated with each alert.",

  inputs: {
    nwsAlerts: {
      type: "json",
      required: true,
      custom: (value) => {
        // Must be an object
        if (typeof value !== "object") return false;

        // Every key must be a string, and value must be a string that evaluates to a valid color hex code.
        var pattern = new RegExp("^#([a-fA-F0-9]){3}$|[a-fA-F0-9]{6}$");

        for (let key in value) {
          if (!Object.prototype.hasOwnProperty.call(value, key)) continue;
          if (typeof key !== "string") return false;
          if (!pattern.test(value[key])) return false;
        }

        return true;
      },
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        nwsAlerts: inputs.nwsAlerts,
      }
    );
  },
};
