module.exports = {
  friendlyName: "PUT /api/config/break-times",

  description: "Change timing of breaks.",

  inputs: {
    breakCheck: {
      type: "number",
      min: 3,
      max: 50,
      description:
        "Error deviation for breaks in minutes; prevents airing breaks (except top of hour ID) less than this many minutes apart from each other. Ideally, this number should not be greater than the smallest duration between two clockwheel breaks. Also, any music tracks longer than this many minutes will be considered too long by the system.",
    },

    linerTime: {
      type: "number",
      min: 1,
      max: 59,
      description:
        "Do not play a track from the liners category in automation any more often than once every defined number of minutes.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        breakCheck: inputs.breakCheck,
        linerTime: inputs.linerTime,
      }
    );
  },
};
