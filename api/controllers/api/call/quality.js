module.exports = {
  friendlyName: "PUT api/call/quality",

  description:
    "Transmit socket event indicating the current call quality percent of a remote broadcast. Should be called by the DJ Controls receiving the call.",

  inputs: {
    quality: {
      type: "number",
      required: true,
      min: 0,
      max: 100,
      description: `The call quality %.`,
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Transmit event through sockets; DJ Controls should manage what to do from there.
    sails.sockets.broadcast("call-quality", "call-quality", inputs.quality);
  },
};
