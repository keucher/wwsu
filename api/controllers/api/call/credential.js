const hmac = require("crypto-js/hmac-sha256");
const CryptoJS = require("crypto-js");
const sh = require("shorthash");

const ttl = 60 * 60 * 24;

module.exports = {
  friendlyName: "GET api/call/credential",

  description:
    "Receive a credential to connect to Skyway.js from an authorized host. ",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  // TODO: force credentials in Skyway
  fn: async function () {
    // Reject if skyway.js settings not defined.
    if (
      !sails.config.custom.basic.skywayAPI ||
      !sails.config.custom.basic.skywaySecret
    )
      throw {
        forbidden:
          "Skyway.js options have not been set in DJ Controls -> System Settings -> Skyway.js . You must set the Skyway.js API key and API secret.",
      };

    // Get current time
    const unixTimestamp = Math.floor(Date.now() / 1000);

    // Create the Peer ID that should be used
    let peerId = `WWSU-computer-${sh.unique(
      this.req.payload.host + sails.config.custom.basic.hostSecret
    )}-${sh.unique(Date.now() + sails.config.custom.basic.hostSecret)}`;

    // Create the credential
    const credential = {
      peerId: peerId,
      timestamp: unixTimestamp,
      ttl: ttl, // 24 hours
      authToken: calculateAuthToken(peerId, unixTimestamp),
      apiKey: sails.config.custom.basic.skywayAPI,
    };

    return credential;
  },
};

function calculateAuthToken(peerId, timestamp) {
  // calculate the auth token hash
  const hash = CryptoJS.HmacSHA256(
    `${timestamp}:${ttl}:${peerId}`,
    sails.config.custom.basic.skywaySecret
  );

  // convert the hash to a base64 string
  return CryptoJS.enc.Base64.stringify(hash);
}
