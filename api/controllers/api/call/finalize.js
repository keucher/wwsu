module.exports = {
  friendlyName: "POST api/call/finalize",

  description:
    "In a remote call, the DJ Controls receiving the call should call this API once the call was answered and audio detected (or not).",

  inputs: {
    success: {
      type: "boolean",
      required: true,
      description:
        "Success = true if audio was detected, false if no audio after 1 second.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // DJ Controls should handle this socket event.
    // If success = true, DJ Controls should process state changes / start or resume the broadcast.
    // If success = false, DJ Controls should abort the broadcast process and show an error for no incoming audio.
    sails.sockets.broadcast("finalize-call", "finalize-call", inputs.success);
  },
};
