module.exports = {
  friendlyName: "GET api/messages/web",

  description:
    "Web and mobile clients will use this endpoint to get public messages and private messages they sent or received.",

  inputs: {},

  fn: async function (inputs) {
    // Get the client host string
    let host = await sails.helpers.getHost(this.req);

    if (this.req.isSocket) {
      // Subscribe the client to receiving web messages over websockets
      sails.sockets.join(this.req, "messages-website"); // Public website messages
      sails.sockets.join(this.req, `messages-website-${host}`); // Private website messages
      sails.log.verbose(
        `Request was a socket. Joining messages-website and messages-website-${host}.`
      );
    }

    let searchto = moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss"); // Do not return messages more than 1 hour old

    // get records
    let records = await sails.models.messages.find({
      status: "active",
      createdAt: { ">": searchto },
      or: [
        { to: ["website", `website-${host}`] },
        { from: { startsWith: "website" }, to: "DJ" },
        { from: { startsWith: "discord-" }, to: "DJ" },
        { from: `website-${host}`, to: "DJ-private" },
        { from: { startsWith: "discord-" }, to: "DJ-private" },
      ],
    });

    // Return an empty array if no records were returned.
    if (typeof records === "undefined" || records.length === 0) {
      return [];
    } else {
      // Remove IP addresses from response!
      records
        .filter((record, index) => typeof records[index].fromIP !== "undefined")
        .map((record, index) => delete records[index].fromIP);

      return records;
    }
  },
};
