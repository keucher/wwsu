const sh = require("shorthash");

module.exports = {
  friendlyName: "POST api/messages/web",

  description: "Web and mobile clients use this endpoint to send messages.",

  inputs: {
    nickname: {
      type: "string",
      description: "Nickname of the client sending the message.",
    },

    private: {
      type: "boolean",
      required: true,
      description:
        "If this message is only for the DJ, then private will be true, otherwise false.",
    },

    message: {
      type: "string",
      required: true,
    },
  },

  exits: {
    tooManyRequests: {
      statusCode: 429,
    },
  },

  fn: async function (inputs) {
    // Get the client's IP address
    let fromIP = await sails.helpers.getIp(this.req);

    // Prepare data
    let opts = {
      message: inputs.message,
      fromIP: fromIP,
      nickname: inputs.nickname || null,
      private: inputs.private,
    };
    opts.host = await sails.helpers.getHost(this.req);

    let channel;
    let discordMessage;

    let theid = opts.host;
    // If no nickname provided, use host as the nickname
    if (inputs.nickname === null) {
      inputs.nickname = theid;
    }
    let record = null;

    // Check how many messages were sent by this host within the last minute. If more than configured limit are returned, the host is not allowed to send messages yet.
    let searchto = moment()
      .subtract(1, "minutes")
      .format("YYYY-MM-DD HH:mm:ss");
    let check = await sails.models.messages.find({
      fromIP: opts.fromIP,
      createdAt: { ">": searchto },
    });
    sails.log.verbose(
      `IP address sent ${check.length} messages within the last minute.`
    );

    // TODO: Move to sails.config
    if (check.length > 2) {
      throw {
        tooManyRequests: `This IP has exceeded the number of messages allowed per minute. Please wait one minute to send another message.`,
      };
    }

    // Filter disallowed HTML
    inputs.message = await sails.helpers.sanitize(inputs.message);

    // Filter profanity
    inputs.message = await sails.helpers.filterProfane(inputs.message);

    // Truncate after 1024 characters
    inputs.message = await sails.helpers.truncateText(inputs.message, 1024);

    // Create and broadcast the message, depending on whether or not it was private
    if (inputs.private) {
      record = await sails.models.messages
        .create({
          status: "active",
          from: `website-${theid}`,
          fromFriendly: `Web (${inputs.nickname})`,
          fromIP: opts.fromIP,
          to: "DJ-private",
          toFriendly: "Privately to DJ",
          message: inputs.message,
        })
        .fetch();
      delete record.fromIP; // We do not want to broadcast IP addresses!
    } else {
      // Send public messages in Discord as well if the webchat is enabled.

      try {
        if (
          typeof DiscordClient !== "undefined" &&
          DiscordClient &&
          DiscordClient.readyTimestamp &&
          DiscordClient.ws.status !== 5 &&
          sails.models.meta.memory.webchat
        ) {
          if (sails.models.meta.memory.discordChannel) {
            channel = DiscordClient.channels.resolve(
              sails.models.meta.memory.discordChannel
            );
            if (channel)
              discordMessage = await channel.send(
                `__Message from **Web (${inputs.nickname})**__` +
                  "\n" +
                  `${await sails.helpers.discord.cleanContent(
                    inputs.message,
                    channel
                  )}`
              );
          } else if (sails.models.meta.memory.state.startsWith("sports")) {
            // Sports channel
            channel = DiscordClient.channels.resolve(
              sails.config.custom.discord.channelSports
            );
            if (channel)
              discordMessage = await channel.send(
                `__Message from **Web (${inputs.nickname})**__` +
                  "\n" +
                  `${await sails.helpers.discord.cleanContent(
                    inputs.message,
                    channel
                  )}`
              );
          } else {
            // General channel
            channel = DiscordClient.channels.resolve(
              sails.config.custom.discord.channelGeneral
            );
            if (channel)
              discordMessage = await channel.send(
                `__Message from **Web (${inputs.nickname})**__` +
                  "\n" +
                  `${await sails.helpers.discord.cleanContent(
                    inputs.message,
                    channel
                  )}`
              );
          }
        }
      } catch (e) {
        sails.log.error(e);
        // Log errors but continue execution.
      }

      record = await sails.models.messages
        .create({
          status: "active",
          from: `website-${theid}`,
          fromFriendly: `Web (${inputs.nickname})`,
          fromIP: opts.fromIP,
          to: "DJ",
          toFriendly: "Public",
          message: inputs.message,
          discordChannel: channel ? channel.id : null,
          discordMessage: discordMessage ? discordMessage.id : null,
        })
        .fetch();
      delete record.fromIP; // We do not want to broadcast IP addresses!
    }

    return record;
  },
};
