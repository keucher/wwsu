module.exports = {
  friendlyName: "DELETE api/messages/host/:ID",

  description: "Delete a message by ID.",

  inputs: {
    ID: {
      type: "number",
      required: true,
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Prevent removing messages if host is belongsTo and the specified belongsTo is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to delete messages; this host did not start the current broadcast and does not have admin permission.",
      };

    // Mark message as removed. Do not actually destroy it; we want it in the database for archive.
    let record = await sails.models.messages.updateOne(
      { ID: inputs.ID },
      { status: "deleted" }
    );
    if (!record) throw { notFound: "The provided message ID was not found." };

    // If there was a discord message, edit it to say the message was deleted
    try {
      if (
        typeof DiscordClient !== "undefined" &&
        DiscordClient &&
        DiscordClient.readyTimestamp &&
        DiscordClient.ws.status !== 5 &&
        record.discordChannel &&
        record.discordMessage
      ) {
        let channel = DiscordClient.channels.resolve(record.discordChannel);
        if (channel) {
          let message;
          try {
            message = await channel.messages.fetch(record.discordMessage);
          } catch (e) {
            /* Ignore errors */
          }
          if (message) {
            await message.edit(`:x: **Original message was deleted** :x:`);
          }
        }
      }
    } catch (e) {
      sails.log.error(e);
      // Ignore discord errors; just log them.
    }

    return record;
  },
};
