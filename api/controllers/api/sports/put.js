module.exports = {
  friendlyName: "PUT api/sports/:name",

  description: "Update sports.",

  inputs: {
    name: {
      type: "string",
      required: true,
      description: "the name of the sports variable / data to change.",
    },

    value: {
      type: "string",
      allowNull: true,
      description: "the new value for the data variable.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    let record = await sails.models.sports
      .update({ name: inputs.name }, { value: inputs.value })
      .fetch();

    // All done.
    return record;
  },
};
