module.exports = {
  friendlyName: "/",

  description: "Home page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/home",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `WWSU 106.9 FM is a non-profit radio station broadcast from Wright State University. Since 1977, we have delivered a mix of music, sports, and talk shows to the Fairborn and Dayton communities.`,
        author: "WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}`,
        "og:type": "website",
        "og:description": `WWSU 106.9 FM is a non-profit radio station broadcast from Wright State University. Since 1977, we have delivered a mix of music, sports, and talk shows to the Fairborn and Dayton communities.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
