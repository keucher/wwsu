module.exports = {
  friendlyName: "blogs/",

  description: "Blogs page",

  inputs: {
    category: {
      type: "string",
      description: "The category of blogs to view",
      defaultsTo: "all",
    },
  },

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/blogs",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      category: inputs.category,
      meta: {
        description: `Browse ${inputs.category} blog posts on WWSU 106.9 FM.`,
        author: "WWSU 106.9 FM",
        "og:title": `${inputs.category} blog posts - WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/blogs/${inputs.category}`,
        "og:type": "website",
        "og:description": `Browse ${inputs.category} blog posts on WWSU 106.9 FM.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
