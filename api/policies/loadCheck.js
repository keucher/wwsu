// Check the load of the server and throttle or block requests as necessary.

module.exports = async function (req, res, next) {
  let status = await sails.helpers.throttle(next);

  if (status.result === 'REJECTED') {
      // Too many pending requests, or throttleAPI set to 2? 503 the request instead.
      sails.config.log.api.warn({
        message: `Request was REJECTED. ${status.queue} pending throttled requests. throttleAPI status was ${status.status}.`,
        rejected: true,
      });
      return res
        .status(503)
        .send(
          "The server is currently overloaded. Please try again in 5 minutes. If you continue to get this error, please contact wwsu4@wright.edu ."
        );
  }
};
