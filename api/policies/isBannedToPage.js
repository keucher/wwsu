/**
 * isAuthorized
 *
 * @description :: Policy to check if user is banned
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */
const moment = require("moment");

module.exports = async function (req, res, next) {
  let searchto = moment().subtract(1, "days").format("YYYY-MM-DD HH:mm:ss"); // Dayban search

  // Get the requesting client IP address
  let theip = await sails.helpers.getIp(req);
  let theid = await sails.helpers.getHost(req);

  try {
    let records = await sails.models.discipline.count({
      where: {
        active: 1,
        or: [
          { action: "permaban" },
          { action: "dayban", createdAt: { ">": searchto } },
          { action: "showban" },
        ],
        IP: [theip, `website-${theid}`],
      },
    });

    if (records > 0) {
      // Discipline records active? Load the discipline page instead of the requested page.
      return res.view("discipline/home", {
        layout: "discipline/layout",
      });
    } else {
      return next();
    }
  } catch (e) {
    sails.log.error(e);
    return res
      .status(500)
      .send(
        "There was an error checking IP blacklist. Please try again in a few minutes. If this problem continues, email wwsu4@wright.edu."
      );
  }
};
