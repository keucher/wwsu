module.exports = async function (req, res, next) {
  if (req.param("metaSecret")) {
    if (req.param("metaSecret") !== sails.config.custom.basic.metaSecret) {
      return res.status(403).json("The specified metaSecret is incorrect.");
    }
  } else {
    return res
      .status(401)
      .json("You must provide a metaSecret to use this endpoint.");
  }

  return next();
};
