module.exports = {
  friendlyName: "status.checkReported",

  description:
    "Check if any issues were reported and update status accordingly.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    var reports = await sails.models.logs.find({
      logtype: "status-reported",
      acknowledged: false,
    });

    if (reports.length > 0) {
      await sails.helpers.status.modify.with({
        name: "reported",
        label: "Reported Problems",
        summary: `${reports.length} problems were reported by DJs and/or website visitors. See DJ Controls.`,
        data: `These problems were reported (when complete, mark them off in DJ Controls Administration -> To-Do): <ul><li>${reports
          .map(
            (report) =>
              `${report.title} (${moment(report.createdAt).format("llll")}): ${
                report.event
              }`
          )
          .join("</li><li>")}</li></ul>`,
        status: 2,
      });
    } else {
      await sails.helpers.status.modify.with({
        name: "reported",
        label: "Reported Problems",
        summary: `No problems reported.`,
        data: `No problems have been reported at this time.`,
        status: 5,
      });
    }

    return true;
  },
};
