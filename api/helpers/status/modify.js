module.exports = {
  friendlyName: "status.change",

  description: "Change the status of something",

  inputs: {
    name: {
      type: "string",
      required: true,
      description:
        "The unique alphanumeric name of the status system to change.",
    },
    label: {
      type: "string",
      description: "The human readable label for this system.",
    },
    status: {
      type: "number",
      required: true,
      min: 1,
      max: 5,
      description:
        "The status which to change the system. 1 = critical, 2 = major, 3 = minor, 4 = offline, 5 = good.",
    },
    summary: {
      type: "string",
      description:
        "Short blurb of the current status.",
    },
    data: {
      type: "string",
      description:
        "Any additional information regarding this system, such as instructions to fix the issue.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    let criteria = {
      name: inputs.name,
      status: inputs.status,
      summary: inputs.summary,
      data: inputs.data || inputs.summary,
      label: inputs.label,
    };
    if (inputs.status === 5) {
      criteria.time = moment().toISOString(true);
    }

    // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
    let criteriaB = _.cloneDeep(criteria);

    // Find or create the status record
    let record = await sails.models.status.findOrCreate(
      { name: inputs.name },
      Object.assign({ label: inputs.name }, criteriaB)
    );

    // Search to see if any changes are made to the status; we only want to update if there is a change.
    let updateIt = false;
    for (let key in criteria) {
      if (Object.prototype.hasOwnProperty.call(criteria, key)) {
        if (criteria[key] !== record[key]) {
          // We don't want to fetch() on time-only updates; this will flood websockets
          if (!updateIt && key === "time") {
            updateIt = 2;
          } else {
            updateIt = 1;
          }
        }
      }
    }
    if (
      updateIt === 1 &&
      typeof criteria.status !== "undefined" &&
      criteria.status <= 3 &&
      (!record.status || record.status !== criteria.status)
    ) {
      let loglevel = `warning`;
      if (criteria.status < 2) {
        loglevel = `danger`;

        // Push notification for danger/critical statuses
        if (
          (await sails.models.emails.count({
            subject: `Critical Problem detected for ${
              criteria.label ||
              record.label ||
              criteria.name ||
              record.name ||
              `Unknown System`
            }`,
            createdAt: {
              ">=": moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss"),
            },
          })) === 0
        ) {
          await sails.helpers.onesignal.sendMass(
            "emergencies",
            "Critical Problem Detected",
            `${
              criteria.label ||
              record.label ||
              criteria.name ||
              record.name ||
              `Unknown System`
            } experienced a critical problem on ${moment().format("LLL")}: ${
              criteria.summary ? criteria.summary : `Unknown Issue`
            }`
          );
          await sails.helpers.emails.queueEmergencies(
            `Critical Problem detected for ${
              criteria.label ||
              record.label ||
              criteria.name ||
              record.name ||
              `Unknown System`
            }`,
            `Directors,<br /><br />

  A critical problem has been detected in the WWSU system with <strong>${
    criteria.label ||
    record.label ||
    criteria.name ||
    record.name ||
    `Unknown System`
  }</strong> on ${moment().format(
              "LLLL"
            )}. Please fix this issue immediately (if a fix is still needed).<br /><br />

  Additional information: ${criteria.data ? criteria.data : `Unknown Issue`}`,
            true
          );
        }
      } else if (criteria.status < 3) {
        loglevel = `orange`;
      }

      // Log changes in status
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: `status-${loglevel}`,
          loglevel: loglevel,
          logsubtype: sails.models.meta.memory.show,
          logIcon: `fas fa-exclamation-triangle`,
          title: `Status changed for ${
            criteria.label ||
            record.label ||
            criteria.name ||
            record.name ||
            `Unknown System`
          }`,
          event: `${criteria.data ? criteria.data : `Unknown Issue`}`,
        })
        .fetch()
        .tolerate((err) => {
          // Don't throw errors, but log them
          sails.log.error(err);
        });
    }
    if (
      updateIt === 1 &&
      record.status &&
      criteria.status &&
      record.status <= 3 &&
      criteria.status > 3
    ) {
      // Log when bad statuses are now good.
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "status-success",
          loglevel: "success",
          logsubtype: sails.models.meta.memory.show,
          logIcon: `fas fa-exclamation-triangle`,
          title: `Status: ${
            criteria.label ||
            record.label ||
            criteria.name ||
            record.name ||
            `Unknown System`
          } is now operational.`,
          event: ``,
        })
        .fetch()
        .tolerate((err) => {
          // Don't throw errors, but log them
          sails.log.error(err);
        });
    }

    if (updateIt === 1) {
      // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
      criteriaB = _.cloneDeep(criteria);
      sails.log.verbose(
        `Updating status ${inputs.name} and pushing to sockets via fetch.`
      );
      record = await sails.models.status
        .update({ name: inputs.name }, criteriaB)
        .fetch();

      // ALWAYS update even if there were no changes because we want updatedAt to update. But, if there are no changes, do not websocket / fetch it.
    } else {
      // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
      criteriaB = _.cloneDeep(criteria);
      sails.log.verbose(
        `Updating status ${inputs.name} without using fetch / pushing to sockets.`
      );
      record = await sails.models.status.update(
        { name: inputs.name },
        criteriaB
      );
    }

    // For delay system, if gone into critical status and we are in a remote broadcast, forcefully go to break.
    if (
      inputs.name === "delay-system" &&
      inputs.status === 1 &&
      [
        "remote_on",
        "sportsremote_on",
        "automation_remote",
        "automation_sportsremote",
        "remote_returning",
        "sportsremote_returning",
      ].indexOf(sails.models.meta.memory.state) !== -1
    ) {
      sails.sockets.broadcast(
        "remote-dropped-delay",
        "remote-dropped-delay",
        true
      );
      await sails.helpers.state.goBreak(false, true);
    }

    return record;
  },
};
