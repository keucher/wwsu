const { schedule } = require("later");

module.exports = {
  friendlyName: "sails.helpers.emails.queueEvent",

  description:
    "Queue emails for shows / programs that have started or have been changed.",

  inputs: {
    event: {
      type: "json",
      description:
        "Event object triggering the notification. Should be the event exception when updating or canceling.",
    },
    started: {
      type: "boolean",
      defaultsTo: true,
      description:
        "If true, this is a notification that the event is on the air rather than updated or canceled.",
    },
    newSchedule: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, this is a permanent schedule (for informing subscribers that the show has a regular broadcast change).",
    },
    removedException: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, the event object, which is an exception, was removed. For example, reversal of cancelations or updates.",
    },
  },

  fn: async function (inputs) {
    try {
      var to = [];
      var cc = [];
      var records;

      // No notifications for empty events
      if (!inputs.event || inputs.event === null) return false;

      // Load in DJs to send to
      var maps = ["hostDJ", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map(
        async (key) => {
          if (!inputs.event[key]) return;

          var query = {};

          var dj = await sails.models.djs.findOne({
            ID: inputs.event[key],
            active: true,
          });
          if (dj && dj.email && dj.email !== "") {
            to.push(dj.email);
          }
        }
      );
      await Promise.all(maps);

      // Load in directors to be CCd
      var records = await sails.models.directors.find({ emailCalendar: true });
      cc = records
        .filter((record) => record.email && record.email !== "")
        .map((record) => record.email);

      if (inputs.started) {
        // Do nothing
      } else if (inputs.newSchedule) {
        // Remove pending emails of the same type because sometimes schedules are edited en masse; we only want the newest to email out
        await sails.models.emails
          .destroy({
            subject: `Changed regular schedule for ${inputs.event.name}`,
            sent: false,
          })
          .fetch();

        // Get all schedules for this event and generate human text for them
        let schedules = await sails.models.schedule.find({
          calendarID: inputs.event.calendarID,
          scheduleType: null,
        });
        let scheduleHTML = ``;
        if (!schedules || !schedules.length) {
          scheduleHTML = `<li>NONE</li>`;
        } else {
          schedules.map((schedule) => {
            scheduleHTML += `<li>${sails.models.calendar.calendardb.generateScheduleText(
              schedule
            )}</li>`;
          });
        }

        await sails.helpers.emails.queue(
          to,
          cc,
          null,
          `Changed regular schedule for ${inputs.event.name}`,
          `Dear ${inputs.event.hosts},<br /><br />

The regular broadcast schedule for your ${inputs.event.type}, <strong>${inputs.event.name}</strong>, was changed.<br /><br />

<strong>New regular broadcast schedule (in the station timezone):</strong> <ul>${scheduleHTML}</ul><br /><br />

This only regards the regular schedule; it does not list nor change re-schedules or cancellations (except if the re-schedule or cancellation applied to a regular schedule that was removed).<br /><br />

If you have any questions or concerns, please reply all to this email.`,
          false,
          moment().add(15, "minutes").format("YYYY-MM-DD HH:mm:ss")
        );
      } else if (!inputs.newSchedule && !inputs.removedException) {
        // Changed date/time
        if (
          inputs.event.scheduleType === "updated" ||
          inputs.event.scheduleType === "updated-system"
        ) {
          await sails.helpers.emails.queue(
            to,
            cc,
            null,
            `Changed date/time for ${inputs.event.name}`,
            `Dear ${inputs.event.hosts},<br /><br />

  A time slot for your ${inputs.event.type}, <strong>${
              inputs.event.name
            }</strong>, was <strong>re-scheduled</strong>.<br /><br />

  Original time: ${moment(inputs.event.originalTime).format("LLLL")}<br />
  New time: <strong>${moment(inputs.event.start).format("LLLL")} - ${moment(
              inputs.event.end
            ).format("LT")}</strong><br /><br />
  
  Reason for change (if specified): ${inputs.event.scheduleReason}<br /><br />

  This re-schedule only applies to the date/time listed above; it does not apply to future time slots.<br /><br />
  
  If you have any questions or concerns, please reply all to this email.`
          );

          // Canceled date/time
        } else if (
          inputs.event.scheduleType === "canceled" ||
          inputs.event.scheduleType === "canceled-system"
        ) {
          await sails.helpers.emails.queue(
            to,
            cc,
            null,
            `Canceled date/time for ${inputs.event.name}`,
            `Dear ${inputs.event.hosts},<br /><br />

  A time slot for your ${inputs.event.type}, <strong>${
              inputs.event.name
            }</strong>, was <strong>canceled</strong>.<br /><br />

  Canceled time: ${moment(inputs.event.originalTime).format("LLLL")}<br />
  
  Reason for cancellation (if specified): ${
    inputs.event.scheduleReason
  }<br /><br />

  This cancellation only applies to the date/time listed above; it does not apply to future time slots.<br /><br />
  
  If you have any questions or concerns, please reply all to this email.`
          );
        }
      } else if (!inputs.newSchedule && inputs.removedException) {
        // reversed Changed date/time
        if (
          inputs.event.scheduleType === "updated" ||
          inputs.event.scheduleType === "updated-system"
        ) {
          await sails.helpers.emails.queue(
            to,
            cc,
            null,
            `Reversal of a re-schedule for ${inputs.event.name}`,
            `Dear ${inputs.event.hosts},<br /><br />

  Your ${inputs.event.type}, <strong>${
              inputs.event.name
            }</strong>, was originally re-scheduled to a different time. However, that re-schedule was reversed and your ${
              inputs.event.type
            } is now scheduled for its <strong>original time</strong>.<br /><br />

  Rescheduled time: ${moment(inputs.event.start).format("LLLL")} - ${moment(
              inputs.event.end
            ).format("LT")}<br />
  <strong>The ${
    inputs.event.type
  } should now air on its originally scheduled start time of ${moment(
              inputs.event.originalTime
            ).format("LLLL")} and end at its original end time.</strong>
  
  If you have any questions or concerns, please reply all to this email.`
          );

          // reversed Canceled date/time
        } else if (
          inputs.event.scheduleType === "canceled" ||
          inputs.event.scheduleType === "canceled-system"
        ) {
          await sails.helpers.emails.queue(
            to,
            cc,
            null,
            `Reversal of cancellation for ${inputs.event.name}`,
            `Dear ${inputs.event.hosts},<br /><br />

  Your ${inputs.event.type}, <strong>${
              inputs.event.name
            }</strong>, was originally canceled on a date/time. However, that cancellation was reversed and your ${
              inputs.event.type
            } is now scheduled for its <strong>original time</strong>.<br /><br />

  <strong>The ${
    inputs.event.type
  } should now air on its originally scheduled start time of ${moment(
              inputs.event.originalTime
            ).format(
              "LLLL"
            )} and end at its original end time; the cancellation of this date/time was reversed.</strong>
  
  If you have any questions or concerns, please reply all to this email.`
          );
        }
      }

      return true;
    } catch (e) {
      // No erroring if there's an error; just log it and return false to indicate it was not successful
      sails.log.error(e);
      return false;
    }
  },
};
