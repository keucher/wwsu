module.exports = {
  friendlyName: "attendance.createRecord",

  description:
    "Create a new attendance record, or end the current one. Also manages stats / XP awarding when applicable.",

  inputs: {
    event: {
      type: "json",
      description:
        "Event object triggering the new attendance record. If undefined, the current attendance will be closed, but a new one will not be created. If null, an attendance record for default genre rotation will be started.",
    },
    useMeta: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, will use the DJs and show name in the current meta information instead of in the provided event.",
    },
    unscheduled: {
      type: "boolean",
      defaultsTo: false,
      description: "If true, this is an unscheduled show.",
    },
    problemTerminated: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, this attendance record is being changed because of a major system problem or lofi being activated. Accountability will not be logged.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper attendance.createRecord called.");

    var returnData = { newID: null, unique: null };

    // Store the current ID in a variable; we want to start a new record before processing the old one
    var currentID = sails.models.meta.memory.attendanceID;

    // Create default rotation if inputs.event is null
    if (inputs.event === null) {
      inputs.event = {
        type: "genre",
        hosts: "Unknown Hosts",
        name: "Default Rotation",
        hostDJ: null,
        cohostDJ1: null,
        cohostDJ2: null,
        cohostDJ3: null,
      };
    }

    // Add a new attendance record if event is specified.
    if (inputs.event) {
      sails.log.silly("attendance.createRecord: Creating new attendance record.");

      // If useMeta provided, make a separate event using the info provided in meta
      var nowEvent = _.cloneDeep(inputs.event);
      if (inputs.useMeta) {
        nowEvent = Object.assign(nowEvent, {
          hostDJ: sails.models.meta.memory.hostDJ,
          cohostDJ1: sails.models.meta.memory.cohostDJ1,
          cohostDJ2: sails.models.meta.memory.cohostDJ2,
          cohostDJ3: sails.models.meta.memory.cohostDJ3,
          logo: sails.models.meta.memory.showLogo,
          description: sails.models.meta.memory.topic,
        });

        nowEvent = Object.assign(nowEvent, {
          hosts: await sails.helpers.calendar.generateHosts(nowEvent),
        });
      }

      // Create the new attendance record
      var created = null;

      if (nowEvent.unique && nowEvent.calendarID) {
        returnData.unique = nowEvent.unique;
        created = await sails.models.attendance
          .create({
            calendarID: nowEvent.calendarID,
            unique: nowEvent.unique,
            dj: nowEvent.hostDJ,
            cohostDJ1: nowEvent.cohostDJ1,
            cohostDJ2: nowEvent.cohostDJ2,
            cohostDJ3: nowEvent.cohostDJ3,
            happened: inputs.unscheduled ? 2 : 1,
            event: `${nowEvent.type}: ${nowEvent.hosts} - ${nowEvent.name}`,
            scheduledStart: moment(nowEvent.start).format(
              "YYYY-MM-DD HH:mm:ss"
            ),
            scheduledEnd: moment(nowEvent.end).format("YYYY-MM-DD HH:mm:ss"),
            actualStart: moment().format("YYYY-MM-DD HH:mm:ss"),
          })
          .fetch();

        sails.log.silly("attendance.createRecord: Done creating new attendance record; updating sign-offs.");

        // Update any early sign-offs that might have been caused by this broadcast (but only if this broadcast is a show, sports, prerecord, playlist, or remote)
        if (
          ["show", "sports", "prerecord", "playlist", "remote"].indexOf(
            nowEvent.type
          ) !== -1
        ) {
          let conflictingLog = await sails.models.logs
            .find({
              logtype: "sign-off-early",
              createdAt: {
                ">=": moment(nowEvent.start)
                  .subtract(5, "minutes")
                  .format("YYYY-MM-DD HH:mm:ss"),
              },
            })
            .limit(1);
          if (conflictingLog && conflictingLog.length > 0) {
            await sails.models.logs.updateOne(
              { ID: conflictingLog[0].ID },
              {
                excused: true,
                event:
                  conflictingLog[0].event +
                  `<br />This may have been caused by another show that went on the air: ${nowEvent.type}: ${nowEvent.hosts} - ${nowEvent.name}. This log was marked excused by default.`,
              }
            );
          }
        }

        // Log if any specified DJs do not match the DJs that were on the schedule
        let scheduledDJs = [];
        let onAirDJs = [];
        ["hostDJ", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
          if (inputs.event[dj]) scheduledDJs.push(inputs.event[dj]);
          if (sails.models.meta.memory[dj])
            onAirDJs.push(sails.models.meta.memory[dj]);
        });

        if (_.difference(onAirDJs, scheduledDJs).length > 0) {
          await sails.models.logs
            .create({
              attendanceID: created.ID,
              logtype: "unspecified-djs",
              loglevel: "info",
              logsubtype: `${nowEvent.hosts} - ${nowEvent.name}`,
              logIcon: sails.models.calendar.calendardb.getIconClass(nowEvent),
              title: `One or more specified hosts for this broadcast were not on the event schedule.`,
              event: `Listed on the schedule: ${inputs.event.hosts} - ${inputs.event.name}<br />Specified on the air: ${sails.models.meta.memory.show}`,
              createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
            })
            .fetch()
            .tolerate((err) => {
              sails.log.error(err);
            });
        }

        // Log if actualStart was 5 or more minutes before scheduledStart
        if (
          moment().add(5, "minutes").isSameOrBefore(moment(nowEvent.start)) &&
          ["show", "sports", "remote", "prerecord", "playlist"].indexOf(
            nowEvent.type
          ) !== -1
        ) {
          await sails.models.logs
            .create({
              attendanceID: created.ID,
              logtype: "sign-on-early",
              loglevel: "orange",
              logsubtype: `${nowEvent.hosts} - ${nowEvent.name}`,
              logIcon: sails.models.calendar.calendardb.getIconClass(nowEvent),
              title: `The broadcast started 5 or more minutes early.`,
              event: `${nowEvent.type}: ${nowEvent.hosts} - ${nowEvent.name}.<br />Unauthorized early starts should warrant intervention.`,
              createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
            })
            .fetch()
            .tolerate((err) => {
              sails.log.error(err);
            });
        }

        // Log if actualStart was 5 or more minutes after scheduledStart
        if (
          moment()
            .subtract(5, "minutes")
            .isSameOrAfter(moment(nowEvent.start)) &&
          ["show", "sports", "remote", "prerecord", "playlist"].indexOf(
            nowEvent.type
          ) !== -1
        ) {
          // But excuse if a previous broadcast might have interfered.
          let conflictingAttendance = await sails.models.attendance
            .find({
              actualEnd: {
                ">=": moment(nowEvent.start)
                  .add(5, "minutes")
                  .format("YYYY-MM-DD HH:mm:ss"),
              },
              or: [
                { event: { startsWith: "show:" } },
                { event: { startsWith: "sports:" } },
                { event: { startsWith: "remote:" } },
                { event: { startsWith: "prerecord:" } },
                { event: { startsWith: "playlist:" } },
              ],
            })
            .limit(1);
          await sails.models.logs
            .create({
              attendanceID: created.ID,
              logtype: "sign-on-late",
              loglevel: "warning",
              logsubtype: `${nowEvent.hosts} - ${nowEvent.name}`,
              logIcon: sails.models.calendar.calendardb.getIconClass(nowEvent),
              title: `The broadcast started 5 or more minutes late.`,
              event: `${nowEvent.type}: ${nowEvent.hosts} - ${nowEvent.name}${
                conflictingAttendance && conflictingAttendance.length > 0
                  ? `<br />This may have been caused by a previous show that ended late: ${conflictingAttendance[0].event}. This record was marked excused by default.`
                  : ``
              }`,
              excused:
                conflictingAttendance && conflictingAttendance.length > 0,
              createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
            })
            .fetch()
            .tolerate((err) => {
              sails.log.error(err);
            });
        }
      } else {

        created = await sails.models.attendance
          .create({
            unique: "",
            dj: nowEvent.hostDJ,
            cohostDJ1: nowEvent.cohostDJ1,
            cohostDJ2: nowEvent.cohostDJ2,
            cohostDJ3: nowEvent.cohostDJ3,
            happened: inputs.unscheduled ? 2 : 1,
            event: `${nowEvent.type}: ${nowEvent.hosts} - ${nowEvent.name}`,
            actualStart: moment().format("YYYY-MM-DD HH:mm:ss"),
            scheduledStart: null,
            scheduledEnd: null,
          })
          .fetch();
      }

      returnData.newID = created.ID;

      // Switch to the new record in the system
      await sails.helpers.meta.change.with({
        attendanceID: created.ID,
        calendarUnique: nowEvent.unique || null,
        calendarID: nowEvent.calendarID || null,
        scheduledStart: nowEvent.start || null,
        scheduledEnd: nowEvent.end || null,
      });

      sails.log.silly("attendance.createRecord: Done creating new attendance record fully.");
    } else if (inputs.event === null) {
      sails.log.silly("attendance.createRecord: Creating a default rotation record.");

      var created = await sails.models.attendance
        .create({
          unique: "",
          happened: 1,
          event: `genre: Unknown Hosts - Default Rotation`,
          actualStart: moment().format("YYYY-MM-DD HH:mm:ss"),
        })
        .fetch();
      returnData.newID = created.ID;
      await sails.helpers.meta.change.with({
        attendanceID: created.ID,
        calendarUnique: null,
        calendarID: null,
        scheduledStart: null,
        scheduledEnd: null,
      });
    } else {
      returnData.newID = null;
      await sails.helpers.meta.change.with({
        attendanceID: null,
        calendarUnique: null,
        calendarID: null,
        scheduledStart: null,
        scheduledEnd: null,
      });
    }

    // Add actualEnd to the previous attendance record, calculate showTime, calculate listenerMinutes, viewerMinutes, and calculate new weekly DJ stats to broadcast
    if (currentID !== null) {
      sails.log.silly("attendance.createRecord: Ending current record.");

      sails.models.status.tasks.createRecord++;

      // Add actualEnd
      var _currentRecord = await sails.models.attendance
        .updateOne(
          { ID: currentID },
          { actualEnd: moment().format("YYYY-MM-DD HH:mm:ss") }
        )
        .tolerate((e) => {
          sails.log.error(e);
        });

      if (_currentRecord) {
        sails.log.silly("attendance.createRecord: Wrapping up attendance record in the background.");

        // Calculate attendance information in the background as it may take some time
        var temp = (async (currentRecord) => {
          try {
            sails.log.silly("attendance.createRecord: Started wrapping up attendance record in the background.");

            sails.models.status.tasks.createRecord++; // Block process shut down until this is complete.
            var event = currentRecord.event.split(": ");
            if (!inputs.problemTerminated) {
              // Log if actualEnd was 5 or more minutes before scheduledEnd
              if (
                currentRecord &&
                currentRecord.scheduledEnd &&
                moment()
                  .add(5, "minutes")
                  .isSameOrBefore(moment(currentRecord.scheduledEnd)) &&
                (currentRecord.event.toLowerCase().startsWith("show:") ||
                  currentRecord.event.toLowerCase().startsWith("sports:") ||
                  currentRecord.event.toLowerCase().startsWith("remote:") ||
                  currentRecord.event.toLowerCase().startsWith("prerecord:") ||
                  currentRecord.event.toLowerCase().startsWith("playlist:"))
              ) {
                await sails.models.logs
                  .create({
                    attendanceID: currentRecord.ID,
                    logtype: "sign-off-early",
                    loglevel: "warning",
                    logsubtype: `${event[1]}`,
                    logIcon: sails.models.calendar.calendardb.getIconClass({
                      type: event[0],
                    }),
                    title: `The broadcast signed off 5 or more minutes early.`,
                    event: `${currentRecord.event}`,
                    createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                  })
                  .fetch()
                  .tolerate((err) => {
                    sails.log.error(err);
                  });
              }

              // Log if actualEnd was 5 or more minutes after scheduledEnd
              if (
                currentRecord &&
                currentRecord.scheduledEnd &&
                moment()
                  .subtract(5, "minutes")
                  .isSameOrAfter(moment(currentRecord.scheduledEnd)) &&
                (currentRecord.event.toLowerCase().startsWith("show:") ||
                  currentRecord.event.toLowerCase().startsWith("sports:") ||
                  currentRecord.event.toLowerCase().startsWith("remote:") ||
                  currentRecord.event.toLowerCase().startsWith("prerecord:") ||
                  currentRecord.event.toLowerCase().startsWith("playlist:"))
              ) {
                await sails.models.logs
                  .create({
                    attendanceID: currentRecord.ID,
                    logtype: "sign-off-late",
                    loglevel: "orange",
                    logsubtype: `${event[1]}`,
                    logIcon: sails.models.calendar.calendardb.getIconClass({
                      type: event[0],
                    }),
                    title: `The broadcast signed off 5 or more minutes late.<br />Unauthorized late ends should warrant intervention.`,
                    event: `${currentRecord.event}`,
                    createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                  })
                  .fetch()
                  .tolerate((err) => {
                    sails.log.error(err);
                  });
              }
            } else {
              await sails.models.logs
                .create({
                  attendanceID: currentRecord.ID,
                  logtype: "sign-off-problem",
                  loglevel: "yellow",
                  logsubtype: `${event[1]}`,
                  logIcon: sails.models.calendar.calendardb.getIconClass({
                    type: event[0],
                  }),
                  title: `The broadcast was signed off due to a system problem or CRON being disabled.`,
                  event: `${currentRecord.event}`,
                  createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                })
                .fetch()
                .tolerate((err) => {
                  sails.log.error(err);
                });
            }

            sails.log.silly("attendance.createRecord: Finished logging end-broadcast accountability.");

            // Stats for this broadcast
            var stats = await sails.helpers.attendance.recalculate(
              currentRecord.ID
            );

            sails.log.silly("attendance.createRecord: Finished calculating analytics.");

            // Re-calculate 7-day top analytics
            var topStats = await sails.helpers.analytics.calculateStats();

            sails.log.silly("attendance.createRecord: Finished re-calculating 7-day stats.");

            if (currentRecord.calendarID) {
              sails.log.silly("attendance.createRecord: Sending analytic email to DJs.");

              let semesterStats = await sails.helpers.analytics.showtime(
                undefined,
                [currentRecord.calendarID],
                moment(sails.config.custom.analytics.startOfSemester).format(
                  "YYYY-MM-DD HH:mm:ss"
                ),
                undefined,
                true
              );
              sails.log.silly("attendance.createRecord: Finished re-calculating semester stats.");

              let yearStats = await sails.helpers.analytics.showtime(
                undefined,
                [currentRecord.calendarID],
                moment().subtract(1, "years").format("YYYY-MM-DD HH:mm:ss"),
                undefined,
                true
              );
              sails.log.silly("attendance.createRecord: Finished re-calculating year stats.");

              // Send analytic emails to DJs
              await sails.helpers.emails.queueDjs(
                {
                  hostDJ: currentRecord.dj,
                  cohostDJ1: currentRecord.cohostDJ1,
                  cohostDJ2: currentRecord.cohostDJ2,
                  cohostDJ3: currentRecord.cohostDJ3,
                },
                `Analytics for ${currentRecord.event}`,
                `<p>Hello!</p>

  <p>Congratulations on a successful broadcast of ${currentRecord.event}!</p>
  <p><strong>Recordings:</strong> You can download recordings of your broadcast at <a href="https://server.wwsu1069.org/recordings">https://server.wwsu1069.org/recordings</a> . Please be aware recordings are only stored for a few weeks; download them as soon as possible! Also, please read the README.txt file in your folder for instructions on converting the WEBM files to MP3/WAV/etc .</p>
  <p>Below, you will find analytics for this broadcast.</p>
  <ul>
  <li><strong>Signed On:</strong> ${moment(currentRecord.actualStart).format(
    "LLLL"
  )}</li>
  <li><strong>Signed Off:</strong> ${moment(currentRecord.actualEnd).format(
    "LLLL"
  )}</li>
  <li><strong>Showtime:</strong> ${moment
    .duration(stats.showTime, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Online Listener Time*:</strong> ${moment
    .duration(stats.listenerMinutes, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Listener to Showtime Ratio (higher ratio = better performing broadcast):</strong> ${
    stats.showTime > 0 ? stats.listenerMinutes / stats.showTime : 0
  }</li>
  <li><strong>Video stream time:</strong> ${moment
    .duration(stats.videoTime, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li>
  <li><strong>Video stream watch Time*:</strong> ${moment
    .duration(stats.viewerMinutes, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Video watch time to Showtime Ratio (higher ratio = better performing broadcast):</strong> ${
    stats.showTime > 0 ? stats.viewerMinutes / stats.showTime : 0
  }</li>
  <li><strong>Messages sent / received:</strong> ${stats.webMessages}</li>
  ${
    topStats[0].topShows.findIndex((show) => show.name === event[1]) !== -1
      ? `<li><strong>Congratulations! Your broadcast placed ${
          topStats[0].topShows.findIndex((show) => show.name === event[1]) + 1
        } in the top ${
          topStats[0].topShows.length
        } shows of the last week!</strong></li>`
      : ``
  }
  </ul>

  <hr>

  <p>If any issues were discovered during your broadcast, they will be listed below. These issues were logged for the directors; repeat issues could result in intervention. If an issue was caused by a technical problem, please email the directors.</p>
  <ul>
  ${
    inputs.problemTerminated
      ? `<li><strong>This broadcast was terminated early automatically due to a critical system problem.</strong> This will not be held against you.</li>`
      : ``
  }
  ${
    stats.unauthorized
      ? `<li><strong>This broadcast was unscheduled / unauthorized.</strong> You should ensure the directors scheduled your show in and that you go on the air during your scheduled time (or request a re-schedule if applicable).</li>`
      : ``
  }
  ${
    stats.missedIDs.length > 0 && typeof stats.missedIDs.map === "function"
      ? `<li><strong>You failed to take a required top-of-the-hour ID break at these times</strong>; it is mandatory by the FCC to take a break at the top of every hour no later than :05 past the hour. For prerecords and playlists, ensure your audio cut-offs allow for the top-of-hour ID break to air on time:<br />
  ${stats.missedIDs.map((record) => moment(record).format("LT")).join("<br />")}
  </li>`
      : ``
  }
  ${
    stats.silence.length > 0 && typeof stats.silence.map === "function"
      ? `<li><strong>There was excessive silence / very low audio at these times;</strong> please avoid excessive silence on the air as this violates FCC regulations:</strong><br />
  ${stats.silence.map((record) => moment(record).format("LT")).join("<br />")}
  </li>`
      : ``
  }
  ${
    stats.badPlaylist
      ? `<li><strong>The broadcast did not successfully air due to a bad playlist.</strong> Please ensure the tracks you added/uploaded for this broadcast were not corrupted. If they are not corrupt, please let the directors know.</li>`
      : ``
  }
  ${
    stats.signedOnEarly
      ? `<li><strong>You signed on 5 or more minutes early.</strong> Please avoid doing this, especially if there's a scheduled show before yours.</li>`
      : ``
  }
  ${
    stats.signedOnLate
      ? `<li><strong>You signed on 5 or more minutes late.</strong> Please inform directors in advance if you are going to be late for your show.</li>`
      : ``
  }
  ${
    stats.signedOffEarly
      ? `<li><strong>You signed off 5 or more minutes early.</strong> Please inform directors in advance if you are going to end your show early. Ignore this if you signed off early for another broadcast.</li>`
      : ``
  }
  ${
    stats.signedOffLate
      ? `<li><strong>You signed off 5 or more minutes late.</strong> Please avoid doing this, especially if there's a scheduled show after yours.</li>`
      : ``
  }
  </ul>

  <hr>

  <p>Here is a break-down of the number of online listeners and video stream viewers tuned in during your broadcast and at what time:</p>
  <ul>${stats.listeners
    .map(
      (stat) =>
        `<li><strong>${moment(stat.time).format("LT")}</strong>: ${
          stat.listeners
        } listeners${
          stat.viewers !== null ? ` | ${stat.viewers} video watchers` : ``
        }</li>`
    )
    .join("")}</ul>

  <hr>

  <p>Here are the messages (if any) sent/received between you and listeners:</p>
  <ul>${stats.messages
    .map(
      (message) =>
        `<li><strong>${moment(message.createdAt).format("LT")} by ${
          message.fromFriendly
        }</strong>: ${message.message}</li>`
    )
    .join("")}</ul>

  <hr>

  <p>Here is your full show log:</p>
  <ul>${stats.logs
    .map(
      (log) =>
        `<li><strong>${moment(log.createdAt).format("LT")}: ${
          log.title
        }</strong><ul><li>${log.event}</li>${
          log.trackArtist ? `<li>Artist: ${log.trackArtist}</li>` : ``
        }${log.trackTitle ? `<li>Title: ${log.trackTitle}</li>` : ``}${
          log.trackAlbum ? `<li>Album: ${log.trackAlbum}</li>` : ``
        }${log.trackLabel ? `<li>Label: ${log.trackLabel}</li>` : ``}</ul></li>`
    )
    .join("")}</ul>

  ${
    semesterStats && semesterStats[1][currentRecord.calendarID]
      ? `<hr>
      <p>Here are analytics for ${
        currentRecord.event
      } for <strong>this semester</strong> so far:</p>
  <ul>
  <li><strong>On-air time:</strong> ${moment
    .duration(semesterStats[1][currentRecord.calendarID].showTime, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Online listener time*:</strong> ${moment
    .duration(
      semesterStats[1][currentRecord.calendarID].listenerMinutes,
      "minutes"
    )
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Listeners to Showtime Ratio (higher is better):</strong> ${
    semesterStats[1][currentRecord.calendarID].ratio
  }</li>
  <li><strong>Video stream time:</strong> ${moment
    .duration(semesterStats[1][currentRecord.calendarID].videoTime, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Video stream watch time*:</strong> ${moment
    .duration(
      semesterStats[1][currentRecord.calendarID].viewerMinutes,
      "minutes"
    )
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Video stream watch time to Showtime Ratio (higher is better):</strong> ${
    semesterStats[1][currentRecord.calendarID].viewerRatio
  }</li>
  <li><strong>Live episodes aired:</strong> ${
    semesterStats[1][currentRecord.calendarID].shows
  }</li>
  <li><strong>Prerecorded episodes aired:</strong> ${
    semesterStats[1][currentRecord.calendarID].prerecords
  }</li>
  <li><strong>Remote episodes aired:</strong> ${
    semesterStats[1][currentRecord.calendarID].remotes
  }</li>
  <li><strong>Sports broadcasts aired:</strong> ${
    semesterStats[1][currentRecord.calendarID].sports
  }</li>
  <li><strong>Playlist airings:</strong> ${
    semesterStats[1][currentRecord.calendarID].playlists
  }</li>
  <li><strong>Messages sent/received:</strong> ${
    semesterStats[1][currentRecord.calendarID].webMessages
  }</li>

  </ul>`
      : ``
  }

  ${
    yearStats && yearStats[1][currentRecord.calendarID]
      ? `<hr>
  <p>Here are analytics for ${
    currentRecord.event
  } for <strong>the past year</strong>:</p>
  <ul>
  <li><strong>On-air time:</strong> ${moment
    .duration(yearStats[1][currentRecord.calendarID].showTime, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Online listener time*:</strong> ${moment
    .duration(yearStats[1][currentRecord.calendarID].listenerMinutes, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Listeners to Showtime Ratio (higher is better):</strong> ${
    yearStats[1][currentRecord.calendarID].ratio
  }</li>
  <li><strong>Video stream time:</strong> ${moment
    .duration(yearStats[1][currentRecord.calendarID].videoTime, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Video stream watch time*:</strong> ${moment
    .duration(yearStats[1][currentRecord.calendarID].viewerMinutes, "minutes")
    .format("h [hours], m [minutes]")}</li>
  <li><strong>Video stream watch time to Showtime Ratio (higher is better):</strong> ${
    yearStats[1][currentRecord.calendarID].viewerRatio
  }</li>
  <li><strong>Live episodes aired:</strong> ${
    yearStats[1][currentRecord.calendarID].shows
  }</li>
  <li><strong>Prerecorded episodes aired:</strong> ${
    yearStats[1][currentRecord.calendarID].prerecords
  }</li>
  <li><strong>Remote episodes aired:</strong> ${
    yearStats[1][currentRecord.calendarID].remotes
  }</li>
  <li><strong>Sports broadcasts aired:</strong> ${
    yearStats[1][currentRecord.calendarID].sports
  }</li>
  <li><strong>Playlist airings:</strong> ${
    yearStats[1][currentRecord.calendarID].playlists
  }</li>
  <li><strong>Messages sent/received:</strong> ${
    yearStats[1][currentRecord.calendarID].webMessages
  }</li>

  </ul>`
      : ``
  }

              <hr>

<p>*Time only includes those listening to the WWSU internet radio stream or watching the video stream from the WWSU website, and is calculated per-listener/viewer, per-minute. For example, 5 online listeners / viewers tuned in for an entire 30 minute show is (5*30) 2 hours, 30 minutes listener / viewer time.</p>`,
                false,
                true
              );

              sails.log.silly("attendance.createRecord: Finished sending analytic emails.");
            }

            sails.log.silly("attendance.createRecord: Finished wrapping up attendance record.");

            sails.models.status.tasks.createRecord--;
          } catch (e) {
            sails.log.error(e);
            sails.models.status.tasks.createRecord--;
          }
        })(_currentRecord);
      }

      sails.models.status.tasks.createRecord--;
    }

    return returnData;
  },
};
