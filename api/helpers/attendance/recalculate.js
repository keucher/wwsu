module.exports = {
  friendlyName: "attendance.recalculate",

  description: "Re-calculate information about the specified attendance record",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "ID of the attendance record to recalculate.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper attendance.recalculate called for " & inputs.ID);

    var record = await sails.models.attendance.findOne({ ID: inputs.ID });
    if (!record) {
      sails.log.debug("Attendance ID " & inputs.ID & " not found.");
      return;
    }

    var toUpdate = {
      showTime: null,
      tuneIns: null,
      listenerMinutes: null,
      listenerPeak: null,
      listenerPeakTime: null,
      videoTime: null,
      viewerTuneIns: null,
      viewerMinutes: null,
      viewerPeak: null,
      viewerPeakTime: null,
      webMessages: null,
      discordMessages: null,
      missedIDs: [],
      breaks: 0,
      cancellation: false,
      absent: false,
      unauthorized: false,
      silence: [],
      signedOnEarly: false,
      signedOnLate: false,
      signedOffEarly: false,
      signedOffLate: false,
      badPlaylist: false,
    };

    // Get logs
    var logs = await sails.models.logs.find({ attendanceID: inputs.ID });

    sails.log.silly(
      "attendance.recalculate: Retrieved logs for attendance record " &
        inputs.ID
    );

    // Calculate accountability
    logs
      .filter((log) => !log.excused)
      .map((log) => {
        switch (log.logtype) {
          case "cancellation":
            toUpdate.cancellation = true;
            break;
          case "silence":
            toUpdate.silence.push(log.createdAt);
            break;
          case "absent":
            toUpdate.absent = true;
            break;
          case "unauthorized":
            toUpdate.unauthorized = true;
            break;
          case "id":
            toUpdate.missedIDs.push(log.createdAt);
            break;
          case "sign-on-early":
            toUpdate.signedOnEarly = true;
            break;
          case "sign-on-late":
            toUpdate.signedOnLate = true;
            break;
          case "sign-off-early":
            toUpdate.signedOffEarly = true;
            break;
          case "sign-off-late":
            toUpdate.signedOffLate = true;
            break;
          case "break":
            toUpdate.breaks++;
            break;
          case "bad-playlist":
            toUpdate.badPlaylist = true;
        }
      });

    sails.log.silly(
      "attendance.recalculate: Completed log hunting for accountability."
    );

    // Calculate show stats if it has ended
    if (record.actualEnd !== null) {
      sails.log.silly("attendance.recalculate: Calculating show stats.");

      // Pre-calculations
      toUpdate.showTime = moment(record.actualEnd).diff(
        moment(record.actualStart),
        "minutes"
      );

      // Calculate listener minutes and viewer minutes and video time
      toUpdate.listenerMinutes = 0;
      toUpdate.viewerMinutes = 0;
      var listeners = [];
      var prevListeners = 0;
      var prevViewers = null;

      // Fetch listenerRecords since beginning of sails.models.attendance, as well as the listener count prior to start of attendance record.
      var listenerRecords = await sails.models.listeners
        .find({
          createdAt: {
            ">": moment(record.actualStart).format("YYYY-MM-DD HH:mm:ss"),
            "<=": moment(record.actualEnd).format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .sort("createdAt ASC");
      var record2 =
        (await sails.models.listeners
          .find({
            createdAt: {
              "<=": moment(record.actualStart).format("YYYY-MM-DD HH:mm:ss"),
            },
          })
          .sort("createdAt DESC")
          .limit(1)) || 0;
      if (record2[0]) {
        prevListeners = record2[0].listeners || 0;
        prevViewers = record2[0].viewers || null;
        listeners.push({
          time: record.actualStart,
          listeners: prevListeners,
          viewers: prevViewers,
        });
      }

      // Calculate
      var prevTime = moment(record.actualStart);
      var listenerMinutes = 0;
      var listenerPeak = 0;
      var listenerPeakTime = null;
      var tuneIns = 0;
      var viewerMinutes = 0;
      var viewerPeak = 0;
      var viewerPeakTime = null;
      var viewerTuneIns = 0;
      var videoTime = 0;

      if (listenerRecords && listenerRecords.length > 0) {
        listenerRecords.map((listener) => {
          listeners.push({
            time: listener.createdAt,
            listeners: listener.listeners,
            viewers: listener.viewers,
          });
          listenerMinutes +=
            (moment(listener.createdAt).diff(moment(prevTime), "seconds") /
              60) *
            prevListeners;
          if (listener.listeners > prevListeners) {
            tuneIns += listener.listeners - prevListeners;
          }
          if (listener.listeners > listenerPeak) {
            listenerPeak = listener.listeners;
            listenerPeakTime = moment(listener.createdAt).format(
              "YYYY-MM-DD HH:mm:ss"
            );
          }

          viewerMinutes +=
            (moment(listener.createdAt).diff(moment(prevTime), "seconds") /
              60) *
            prevViewers;
          if (listener.viewers > prevViewers) {
            viewerTuneIns += listener.viewers - prevViewers;
          }
          if (listener.viewers > viewerPeak) {
            viewerPeak = listener.viewers;
            viewerPeakTime = moment(listener.createdAt).format(
              "YYYY-MM-DD HH:mm:ss"
            );
          }

          if (prevViewers !== null) {
            videoTime +=
              moment(listener.createdAt).diff(moment(prevTime), "seconds") / 60;
          }

          prevListeners = listener.listeners;
          prevViewers = listener.viewers;
          prevTime = moment(listener.createdAt);
        });
      }

      // This is to ensure listener minutes and viewer minutes from the most recent entry up until the current time is also accounted for
      listenerMinutes +=
        (moment(record.actualEnd).diff(moment(prevTime), "seconds") / 60) *
        prevListeners;
      viewerMinutes +=
        (moment(record.actualEnd).diff(moment(prevTime), "seconds") / 60) *
        prevViewers;
      if (prevViewers !== null) {
        videoTime +=
          moment(record.actualEnd).diff(moment(prevTime), "seconds") / 60;
      }

      toUpdate.listenerMinutes = Math.round(listenerMinutes);
      toUpdate.listenerPeak = listenerPeak;
      toUpdate.listenerPeakTime = listenerPeakTime;
      toUpdate.tuneIns = tuneIns;
      toUpdate.viewerMinutes = Math.round(viewerMinutes);
      toUpdate.viewerPeak = viewerPeak;
      toUpdate.viewerPeakTime = viewerPeakTime;
      toUpdate.viewerTuneIns = tuneIns;
      toUpdate.videoTime = videoTime;

      sails.log.silly("attendance.recalculate: Calculated listeners.");

      // Calculate web and discord messages
      var messages = await sails.models.messages.find({
        status: "active",
        or: [
          { to: { startsWith: "website-" } },
          { to: { startsWith: "discord-" } },
          { to: "DJ" },
          { to: "DJ-private" },
        ],
        createdAt: {
          ">=": moment(record.actualStart).format("YYYY-MM-DD HH:mm:ss"),
          "<=": moment(record.actualEnd).format("YYYY-MM-DD HH:mm:ss"),
        },
      });
      toUpdate.webMessages = messages.length;

      sails.log.silly("attendance.recalculate: Calculated web messages.");

      sails.log.silly("attendance.recalculate: Finished calculating.");
    }

    var toUpdate2 = _.cloneDeep(toUpdate);

    await sails.models.attendance.updateOne({ ID: inputs.ID }, toUpdate2);

    toUpdate.attendance = record;
    toUpdate.listeners = listeners;
    toUpdate.logs = logs;
    toUpdate.messages = messages;

    sails.log.silly("attendance.recalculate: Done.");

    return toUpdate;
  },
};
