module.exports = {
  friendlyName: "Invalidated",

  description:
    "Invalidated event in Discord; emitted when disconnecting and not allowed to reconnect.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    await sails.helpers.status.modify.with({
      name: "discord",
      status: 3,
      summary: `Bot disconnected and refuses to re-connect. Schedules / programming, messages, analytics, etc will not be posted to Discord until fixed.`,
      data: `The Discord Bot has been disconnected and refuses to re-connect due to an error. The bot will no longer update the WWSU server with schedule changes, messages, analytics, etc.
      <br /><strong>TO FIX / CHECK:</strong>
      <ul>
        <li>Network connection / https://discordstatus.com/</li>
        <li>Is the bot token correct? If not, set it in administration DJ Controls -> System Settings -> Discord. Then, restart the application in administration DJ Controls -> Maintenance -> Restart Node / SailsJS Application.</li>
        <li>Did the bot hit an API / rate limit?</li>
        <li>Did Discord ban the bot?</li>
      </ul>`,
    });
  },
};
