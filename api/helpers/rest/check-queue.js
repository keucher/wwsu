module.exports = {
  friendlyName: "rest.checkQueue",

  description:
    "This helper will resolve the provided exits parameter when sails.helpers.rest.getQueue confirms the provided track ID is in the RadioDJ queue.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "the track ID to monitor for in RadioDJ",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper rest.checkQueue called.");

    // Skip if there are no RadioDJs configured.
    if (!sails.config.custom.radiodjs.length) return;

    return await new Promise((resolve, reject) => {
      sails.models.songs.queueCheck.push({
        ID: inputs.ID,
        time: moment(),
        success: resolve,
        error: reject,
      });
    });
  },
};
