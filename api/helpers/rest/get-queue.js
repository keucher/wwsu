var running = false;

module.exports = {
  friendlyName: "rest.getQueue",

  description:
    "Get the current RadioDJ queue. Also, update it in the sails.models.meta.automation variable for local access.",

  inputs: {
    altREST: {
      type: "json",
      description:
        "Execute the command on the provided RadioDJ instance (configradiodjs record) instead of the active RadioDJ one.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper rest.getQueue called.");
    try {
      // Skip / return empty if there are no RadioDJs configured. or we are waiting for a good radioDJ.
      if (
        !inputs.altREST &&
        (!sails.config.custom.radiodjs.length ||
          !sails.models.meta.memory.radiodj ||
          sails.models.meta.memory.radiodj === "" ||
          sails.models.status.errorCheck.waitForGoodRadioDJ)
      )
        return [];

      running = true;
      // Query for the radioDJ queue and update sails.models.meta.automation with the queue.
      // LINT: Do not camel case; parameters are for needle.
      // eslint-disable-next-line camelcase
      var fn = () => {
        return new Promise((resolve, reject) => {
          let instance =
            inputs.altREST ||
            sails.config.custom.radiodjs.find(
              (instance) => instance.name === sails.models.meta.memory.radiodj
            );
          if (!instance || !instance.restURL || !instance.restPassword)
            throw new Error("Internal error: Could not load RadioDJ instance.");

          needle(
            "get",
            instance.restURL + "/p?auth=" + instance.restPassword,
            {},
            {
              open_timeout: 3000,
              response_timeout: 3000,
              read_timeout: 3000,
              headers: { "Content-Type": "application/json" },
            }
          )
            .then((resp) => resolve(resp))
            .catch((e) => reject(e));
        });
      };

      try {
        var newQueue = [];
        var resp = await fn();
      } catch (e) {
        running = false;
        return [];
      }

      // No queue? Return empty array
      if (
        !resp ||
        typeof resp.body === "undefined" ||
        typeof resp.body.name === "undefined" ||
        (resp.body.name !== "ArrayOfSongData" && resp.body.name !== "SongData")
      ) {
        running = false;
        return [];
      }

      // RadioDJ will not return an array if there is only one song in the queue. But we ALWAYS want an array.
      if (resp.body.name === "ArrayOfSongData") {
        resp.body.children.map((trackA) => {
          var theTrack = {};
          trackA.children.map((track) => {
            theTrack[track.name] = track.value;
          });
          newQueue.push(theTrack);
        });
      } else {
        var theTrack = {};
        resp.body.children.map((track) => {
          theTrack[track.name] = track.value;
        });
        newQueue.push(theTrack);
      }

      // Stop here if altREST provided; we do not want to update automation memory data with alts.
      if (inputs.altREST) {
        return newQueue;
      }

      sails.models.meta.automation = newQueue;

      // Run through sails.models.songs.queueCheck and resolve any songs detected as in the queue.
      var inQueue = [];
      sails.models.meta.automation.map((track) =>
        inQueue.push(parseInt(track.ID))
      );
      sails.log.verbose(JSON.stringify(inQueue));

      sails.models.songs.queueCheck.map((check, index) => {
        sails.log.verbose(`queueCheck ${check.ID}`);
        if (inQueue.indexOf(check.ID) !== -1) {
          sails.log.verbose(`IN QUEUE. Resolving success.`);
          check.success();
          delete sails.models.songs.queueCheck[index];
        } else if (moment().diff(moment(check.time), "seconds") >= 10) {
          check.error(
            new Error(
              `Did not find track ${check.ID} in the queue after 10 seconds.`
            )
          );
          delete sails.models.songs.queueCheck[index];
        }
      });

      running = false;
      return sails.models.meta.automation;
    } catch (e) {
      running = false;
      throw e;
    }
  },
};
