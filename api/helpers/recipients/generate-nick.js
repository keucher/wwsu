// LINT: Disabled no-unreachable; break statements are necessary in switches.
// LINT: disabled no-redeclare because variables are not actually being redeclared; they are conditional to each case of the switches.
/* eslint-disable no-redeclare */
/* eslint-disable no-unreachable */

module.exports = {
  friendlyName: "recipients.generateNick",

  description: "Generate a nickname for new clients",

  inputs: {},

  fn: async function (inputs) {
    let day = moment().date();

    // Generate a random nickname based on themes depending on the day of the month. Change this.nicknameThemes in WWSUmessagesweb if you edit this.
    switch (day) {
      case 1:
        return await sails.helpers.nicknames.starConstellations();
      case 2:
        return await sails.helpers.nicknames.pirates();
      case 3:
        return await sails.helpers.nicknames.instruments();
      case 4:
        return await sails.helpers.nicknames.fruitsVegetables();
      case 5:
        return await sails.helpers.nicknames.brands();
      case 6:
        return await sails.helpers.nicknames.medievalTitles();
      case 7:
        return await sails.helpers.nicknames.robots();
      case 8:
        return await sails.helpers.nicknames.animalSpecies();
      case 9:
        return await sails.helpers.nicknames.enchantments();
      case 10:
        return await sails.helpers.nicknames.dragons();
      case 11:
        return await sails.helpers.nicknames.herbs();
      case 12:
        return await sails.helpers.nicknames.knights();
      case 13:
        return await sails.helpers.nicknames.songTitles();
      case 14:
        return await sails.helpers.nicknames.love();
      case 15:
        return await sails.helpers.nicknames.animatronics();
      case 16:
        return await sails.helpers.nicknames.potions();
      case 17:
        return await sails.helpers.nicknames.irish();
      case 18:
        return await sails.helpers.nicknames.guilds();
      case 19:
        return await sails.helpers.nicknames.aliens();
      case 20:
        return await sails.helpers.nicknames.bands();
      case 21:
        return await sails.helpers.nicknames.galaxies();
      case 22:
        return await sails.helpers.nicknames.crops();
      case 23:
        return await sails.helpers.nicknames.food();
      case 24:
        return await sails.helpers.nicknames.christmasElves();
      case 25:
        return await sails.helpers.nicknames.angelicNames();
      case 26:
        return await sails.helpers.nicknames.wizards();
      case 27:
        return await sails.helpers.nicknames.animals();
      case 28:
        return await sails.helpers.nicknames.sports();
      case 29:
        return await sails.helpers.nicknames.bandits();
      case 30:
        return await sails.helpers.nicknames.monsters();
      case 31:
        return await sails.helpers.nicknames.spells();
    }

    return 'unknown';
  },
};
