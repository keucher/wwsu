var sh = require("shorthash");

module.exports = {
  friendlyName: "recipients.remove",

  description:
    "Remove a recipient socket from memory, and set recipient to offline if no sockets no longer active. Certain recipients should be actually removed after one hour of offline via cron.",

  inputs: {
    socket: {
      type: "string",
      required: true,
      description:
        "The socket ID of the recipient that was removed / disconnected.",
    },

    host: {
      type: "string",
      description: "The alphanumeric host / name of the recipient.",
      allowNull: true,
    },
  },

  fn: async function (inputs) {
    // This helper gets called when shutting down sockets, but after the database is already closed. So, skip this helper if we are shutting down.
    if (sails._exiting) return;

    var where = {};
    // No host name? Try to find it based on provided socket.
    if (typeof inputs.host === "undefined" || inputs.host === null) {
      sails.log.verbose(
        `No host specified. Trying to find recipient ID instead.`
      );
      for (var key in sails.models.recipients.sockets) {
        if (
          Object.prototype.hasOwnProperty.call(
            sails.models.recipients.sockets,
            key
          )
        ) {
          if (_.includes(sails.models.recipients.sockets[key], inputs.socket)) {
            where.ID = key;
            sails.log.verbose(`ID found: ${key}.`);
            break;
          }
        }
      }
    } else {
      where.host = inputs.host;
    }

    // If we could not find the recipient, exit the helper.
    if (typeof where.ID === "undefined" && typeof where.host === "undefined") {
      sails.log.verbose(
        `Could not find recipient. Assuming they do not exist. Terminating helper.`
      );
      return;
    }

    // Get the recipient entry
    var recipient = await sails.models.recipients
      .findOne(where)
      .tolerate((err) => {
        sails.log.error(err);
      });

    if (
      typeof recipient !== "undefined" &&
      typeof sails.models.recipients.sockets[recipient.ID] !== "undefined"
    ) {
      // Remove the socket ID from the array of sockets in memory
      _.remove(sails.models.recipients.sockets[recipient.ID], (e) => {
        return e === inputs.socket;
      });

      // If there are no socket IDs left, that means the recipient is offline. Update accordingly.
      if (sails.models.recipients.sockets[recipient.ID].length <= 0) {
        sails.log.verbose(
          `Recipient is no longer connected. Setting to offline.`
        );
        await sails.models.recipients
          .update(
            { host: recipient.host },
            {
              host: recipient.host,
              status: 0,
              peer: null,
              time: moment().format("YYYY-MM-DD HH:mm:ss"),
            }
          )
          .fetch();

        if (recipient.hostID !== null) {
          var hostRecord = await sails.models.hosts.findOne({
            ID: recipient.hostID,
          });
          if (hostRecord) {
            var offStatus = 4;
            var additionalData = ``;
            var additionalSummary = ``;
            if (
              hostRecord.silenceDetection ||
              hostRecord.recordAudio ||
              hostRecord.answerCalls
            ) {
              if (hostRecord.silenceDetection || hostRecord.recordAudio) {
                offStatus = 2;
                if (hostRecord.silenceDetection) {
                  additionalSummary += ` On-air silence is not being monitored!`;
                  additionalData += ` Host is responsible for silence detection; you will not be alerted of silence until this host is back online.`;
                }
                if (hostRecord.recordAudio) {
                  additionalSummary += ` On-air programming is not automatically being recorded; prepare to manually record!`;
                  additionalData += ` Host is responsible for recording on-air programming; until this host is back online, programming is not being recorded.`;
                }
              } else {
                offStatus = 3;
              }
              await sails.helpers.status.modify.with({
                name: `computer-${sh.unique(
                  hostRecord.host + sails.config.custom.basic.hostSecret
                )}`,
                label: `Host ${recipient.label}`,
                status: offStatus,
                summary: `Host / DJ Controls offline; it should always be running.${additionalSummary}`,
                data: `Host is offline; please restart DJ Controls on this computer/host.${additionalData}`,
              });
            }

            if (hostRecord.recordAudio) {
              await sails.helpers.status.modify.with({
                name: `recorder`,
                status: 2,
                label: `Recorder`,
                summary: `DJ Controls responsible for recording is offline. Record on-air programming manually until fixed.`,
                data: `The host responsible for audio recording, ${recipient.label}, is offline.<br /><strong>Be prepared to manually record your broadcasts</strong> until this is resolved.`,
              });
            }
            if (hostRecord.silenceDetection) {
              await sails.helpers.status.modify.with({
                name: `silence`,
                status: 2,
                label: `Silence`,
                summary: `DJ Controls responsible for silence detection is offline. Silence is not being monitored and will not be reported until fixed.`,
                data: `The host responsible for silence detection, ${recipient.label}, is offline.`,
              });
            }
            if (hostRecord.delaySystem) {
              await sails.helpers.status.modify.with({
                name: "delay-system",
                label: "Delay System",
                summary: `DJ Controls responsible for monitoring Delay System is offline. This could turn critical if not fixed in the next minute.`,
                data: `The host responsible for the delay system, ${recipient.label}, is offline.<strong>You will not be able to remotely dump audio in DJ Controls, nor can you host remote broadcasts,</strong> until this issue is resolved.`,
                status: 2, // Periodic network interruptions or quick reboots should not trigger critical status; leave this up to the cron checking for > 1 minute elapses in status reporting.
              });
            }
          }
        }
      }

      // If the recipient name is found in display sign config, reflect status if there are insufficient number of connections.
      var maps = sails.config.custom.displaysigns
        .filter(
          (display) =>
            recipient.host === `display-${display.name}` &&
            sails.models.recipients.sockets[recipient.ID].length <
              display.instances
        )
        .map(async (display) => {
          await sails.helpers.status.modify.with({
            name: `display-${display.name}`,
            label: `Display ${display.label}`,
            status: display.level,
            summary: `Displays connected: ${
              sails.models.recipients.sockets[recipient.ID].length
            } / ${display.instances}`,
            data: `${
              sails.models.recipients.sockets[recipient.ID].length
            } out of ${
              display.instances
            } displays are operational. Please reboot any display signs not working correctly.`,
          });
          return true;
        });
      await Promise.all(maps);
    } else {
      sails.log.verbose(
        `Recipient not found in database. Assuming already removed.`
      );
    }
  },
};
