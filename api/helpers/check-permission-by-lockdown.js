module.exports = {
  friendlyName: "helpers.checkPermissionByLockdown",

  description: "Check the permissions of a logged-in user for a provided host",

  inputs: {
    host: {
      type: "number",
      required: true,
      description: "The ID of the host to check",
    },
    permission: {
      type: "string",
      required: true,
      description: "The permission to check on the Dj",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // sails.js modifies query strings; dumb decision TBH. We need to clone inputs to avoid bugs.
    let criteria = _.cloneDeep({
      host: inputs.host,
      permission: inputs.permission,
    });

    // Grab who is logged in on the host
    let lockdownRecord = await sails.models.lockdown
      .find({ host: criteria.host, clockOut: null })
      .limit(1);
    if (!lockdownRecord[0]) return false;

    let dj;

    // Directors... we need to get their dj equivalent
    if (lockdownRecord[0].type === "director") {
      let director = await sails.models.directors.findOne({
        ID: lockdownRecord[0].typeID,
      });
      if (!director) return false;

      dj = await sails.models.djs
        .find({ realName: director.name, active: true })
        .limit(1);
      if (!dj[0]) return false;
    } else if (lockdownRecord[0].type === "DJ") {
      dj = await sails.models.djs
        .find({ ID: lockdownRecord[0].typeID, active: true })
        .limit(1);
      if (!dj[0]) return false;
    }

    if (dj[0].permissions.indexOf(criteria.permission) === -1) return false;

    return true;
  },
};
