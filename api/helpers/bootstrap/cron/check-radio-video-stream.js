module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkRadioVideoStream",

  description:
    "Check the status of the internet radio and video streams, and log current listeners / viewers.",

  schedule: "3,33 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if shutting down or in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "stream-public",
        label: "Radio Stream",
        summary: `Checks and analytics disabled; system in maintenance mode.`,
        data: "Shoutcast Radio stream checks are disabled; maintenance mode is active. Status will take up to 30 seconds to clear after maintenance mode is disabled.",
        status: 4,
      });
      await sails.helpers.status.modify.with({
        name: "stream-video",
        label: "Video Stream",
        summary: `Checks and analytics disabled; system in maintenance mode.`,
        data: "Owncast Video stream checks are disabled; maintenance mode is active. Status will take up to 30 seconds to clear after maintenance mode is disabled.",
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    // Initialize with current stats in case a check fails (certain checks will set to 0 is we reasonably believe no one is connected)
    let listeners = sails.models.meta.memory.listeners;
    let viewers = sails.models.listeners.memory.viewers;

    // SHOUTCAST 2.6
    if (
      sails.config.custom.basic.shoutcastStream &&
      sails.config.custom.basic.shoutcastStream !== ""
    ) {
      try {
        let resp = await needle(
          "get",
          sails.config.custom.basic.shoutcastStream + `/statistics?json=1`,
          {},
          { headers: { "Content-Type": "application/json" } }
        );

        if (resp && typeof resp.body !== "undefined") {
          if (typeof resp.body.streams !== "undefined") {
            let streams = resp.body.streams || [];
            let stream = streams.find(
              (str) => str.id === sails.config.custom.basic.shoutcastStreamID
            );

            // Check public stream
            if (
              typeof stream !== "undefined" &&
              typeof stream.streamstatus !== "undefined" &&
              stream.streamstatus !== 0
            ) {
              // Mark stream as good
              await sails.helpers.status.modify.with({
                name: "stream-public",
                label: "Radio Stream",
                summary: `Online`,
                data: "Shoutcast v2 internet radio server is online and operational.",
                status: 5,
              });
              listeners = stream.uniquelisteners;
            } else {
              await sails.helpers.status.modify.with({
                name: "stream-public",
                label: "Radio Stream",
                summary: `No active encoder encoding audio to the stream. Online listening not possible. Listener analytics not being tracked.`,
                data: `Shoutcast v2 server is operational, but there is no audio data being received by the encoder. Others may not be able to tune in to WWSU from the internet at this time.
                <br /><strong>TO FIX: </strong>
                <ul>
                  <li>Make sure the encoder (such as BUTT) is connected / streaming audio to the Shoutcast server and to the correct stream configured in DJ Controls -> System Settings -> Shoutcast v2.</li>
                  <li>Make sure the credentials in the encoder are correct, such as the stream username and password.</li>
                </ul>`,
                status: 2,
              });

              // If the stream is actually offline, we know no one is listening.
              listeners = 0;
            }
          } else {
            await sails.helpers.status.modify.with({
              name: "stream-public",
              label: "Radio Stream",
              summary: `Configured radio stream is offline. Online listening not possible. Listener analytics not being tracked.`,
              data: `Configured Shoutcast v2 stream is not active. Others may not be able to tune in to WWSU from the internet at this time.
              <br /><strong>TO FIX: </strong>
              <ul>
              <li>Make sure the encoder (such as BUTT) is connected / streaming audio to the Shoutcast server and to the correct stream configured in DJ Controls -> System Settings -> Shoutcast v2.</li>
              <li>Make sure the credentials in the encoder are correct, such as the stream username and password.</li>
              </ul>`,
              status: 2,
            });

            // If the stream is actually offline, we know no one is listening.
            listeners = 0;
          }
        } else {
          await sails.helpers.status.modify.with({
            name: "stream-public",
            label: "Radio Stream",
            summary: `Error parsing data / server offline. Online listening not possible. Listener analytics not being tracked.`,
            data: `Error parsing data from the Shoutcast v2 internet radio server. It might be offline, and others may not be able to tune in to WWSU from the internet at this time.
            <br /><strong>TO FIX: </strong>
            <ul>
              <li>Make sure the Shoutcast v2 server is online and working correctly.</li>
              <li>Make sure the Shoutcast v2 configuration is set correctly in DJ Controls -> System Settings -> Shoutcast v2.</li>
              <li>Make sure the encoder (such as BUTT) is connected / streaming audio to the Shoutcast server.</li>
              <li>Make sure the credentials in the encoder are correct, such as the stream username and password.</li>
            </ul>`,
            status: 2,
          });
        }
      } catch (e) {
        sails.log.error(e);
        await sails.helpers.status.modify.with({
          name: "stream-public",
          label: "Radio Stream",
          summary: `Internal server error or server offline. Online listening not possible. Listener analytics not being tracked.`,
          data: `Error checking the Shoutcast v2 internet radio server. It might be offline, and others may not be able to tune in to WWSU from the internet at this time.
          <br /><strong>TO FIX: </strong>
          <ul>
            <li>Check server logs for more information about the error.</li>
            <li>Make sure the Shoutcast v2 server is online and working correctly.</li>
            <li>Make sure the Shoutcast v2 configuration is set correctly in DJ Controls -> System Settings -> Shoutcast v2.</li>
            <li>Make sure the encoder (such as BUTT) is connected / streaming audio to the Shoutcast server.</li>
            <li>Make sure the credentials in the encoder are correct, such as the stream username and password.</li>
          </ul>`,
          status: 2,
        });
      }
    } else {
      await sails.helpers.status.modify.with({
        name: "stream-public",
        label: "Radio Stream",
        summary: `Shoutcast URL not defined in settings. Listener analytics not being tracked.`,
        data: `The Shoutcast v2 URL has not been defined in system settings! The system cannot track listener analytics nor internet stream status.
        <br /><strong>TO FIX: </strong>
        <ul>
          <li>Set the shoutcast v2 configuration in DJ Controls -> System Settings -> Shoutcast v2.</li>
          <li>Make sure the encoder (such as BUTT) is connected / streaming audio to the Shoutcast server.</li>
          <li>Make sure the credentials in the encoder are correct, such as the stream username and password.</li>
        </ul>`,
        status: 4,
      });
      listeners = 0;
    }

    // OWNCAST
    // NOTE: Unlike Shoutcast, we do not consider an offline video stream as an error because we do not expect the video stream to always be on / streaming. But it should ALWAYS return viewerCount.
    if (
      sails.config.custom.basic.owncastStream &&
      sails.config.custom.basic.owncastStream !== ""
    ) {
      try {
        let resp = await needle(
          "get",
          `${sails.config.custom.basic.owncastStream}/api/status`,
          {},
          { headers: { "Content-Type": "application/json" } }
        );

        if (resp && typeof resp.body !== "undefined") {
          if (typeof resp.body.online !== "undefined") {
            // Mark stream as good
            await sails.helpers.status.modify.with({
              name: "stream-video",
              label: "Video Stream",
              summary: `Online`,
              data: "Owncast Video stream server is operational.",
              status: 5,
            });
            viewers = resp.body.online ? resp.body.viewerCount : null; // Use "null" if the stream is offline
          } else {
            await sails.helpers.status.modify.with({
              name: "stream-video",
              label: "Video Stream",
              summary: `Owncast error or offline. Video streaming not possible. Viewer analytics not being tracked.`,
              data: `Owncast video stream server is offline or not working properly. Video streaming on WWSU may not be possible at this time.
            <br /><strong>TO FIX: </strong>
            <ul>
              <li>Make sure the Owncast server is online and working correctly.</li>
              <li>Make sure the Owncast URL is set correctly in DJ Controls -> System Settings -> Owncast.</li>
            </ul>`,
              status: 2,
            });

            // We have a response, but the video server is offline. We can reasonably believe no one is connected, including the encoder.
            viewers = null;
          }
        } else {
          await sails.helpers.status.modify.with({
            name: "stream-video",
            label: "Video Stream",
            summary: `Error parsing data / Owncast server offline. Video streaming not possible. Viewer analytics not being tracked.`,
            data: `Error parsing data from the Owncast server. The video stream server might be offline or not working. Video streaming on WWSU may not be possible at this time.
          <br /><strong>TO FIX: </strong>
          <ul>
            <li>Make sure the Owncast server is online and working correctly.</li>
            <li>Make sure the Owncast URL is set correctly in DJ Controls -> System Settings -> Owncast.</li>
          </ul>`,
            status: 2,
          });
        }
      } catch (e) {
        sails.log.error(e);
        await sails.helpers.status.modify.with({
          name: "stream-video",
          label: "Video Stream",
          summary: `Owncast server error or offline. Video streaming not possible. Viewer analytics not being tracked.`,
          data: `Error checking Owncast server. Video streaming might not be working at this time.
        <br /><strong>TO FIX: </strong>
        <ul>
          <li>Check server error logs for more information.</li>
          <li>Make sure the Owncast server is online and working correctly.</li>
          <li>Make sure the Owncast URL is set correctly in DJ Controls -> System Settings -> Owncast.</li>
        </ul>`,
          status: 2,
        });
      }
    } else {
      await sails.helpers.status.modify.with({
        name: "stream-video",
        label: "Video Stream",
        summary: `Owncast URL not defined in settings. Viewer analytics not being tracked.`,
        data: `The Owncast URL has not been defined in system settings! The system cannot track video viewer analytics nor video stream server status.
      <br /><strong>TO FIX: </strong>
      <ul>
        <li>Set the Owncast URL in DJ Controls -> System Settings -> Owncast.</li>
      </ul>`,
        status: 4,
      });
      viewers = null;
    }

    // Log listeners or viewers if there are any changes to them or to the host DJ
    if (
      sails.models.meta.memory.hostDJ !== sails.models.listeners.memory.dj ||
      (typeof listeners !== "undefined" &&
        listeners !== sails.models.listeners.memory.listeners) ||
      (typeof viewers !== "undefined" &&
        viewers !== sails.models.listeners.memory.viewers)
    ) {
      await sails.models.listeners
        .create({
          dj: sails.models.meta.memory.hostDJ,
          listeners:
            typeof listeners !== "undefined"
              ? listeners
              : sails.models.listeners.memory.listeners,
          viewers:
            typeof viewers !== "undefined"
              ? viewers
              : sails.models.listeners.memory.viewers,
        })
        .tolerate(() => {});
      await sails.helpers.meta.change.with({
        listeners:
          typeof listeners !== "undefined"
            ? listeners
            : sails.models.listeners.memory.listeners,
        viewers:
          typeof viewers !== "undefined"
            ? viewers
            : sails.models.listeners.memory.viewers,
      });
    }
    sails.models.listeners.memory = {
      dj: sails.models.meta.memory.hostDJ,
      listeners:
        typeof listeners !== "undefined"
          ? listeners
          : sails.models.listeners.memory.listeners,
      viewers:
        typeof viewers !== "undefined"
          ? viewers
          : sails.models.listeners.memory.viewers,
    };
  },
};
