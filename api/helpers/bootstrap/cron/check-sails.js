module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkCriticalLogs",

  description:
    "Check sails.js and the number of critical logs generated. If 10 unacknowledged danger logs were generated in the last hour, activate maintenance mode.",

  schedule: "*/15 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    let criticalLogs = await sails.models.logs.count({
      loglevel: "danger",
      acknowledged: false,
      createdAt: {
        ">=": moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss"),
      },
    });

    if (criticalLogs >= 10 && !sails.config.custom.basic.maintenance) {
      sails.config.custom.basic.maintenance = true;
      await sails.models.configbasic.updateOne(
        { ID: 1 },
        { maintenance: true }
      );
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "system-lockout",
          loglevel: "danger",
          logsubtype: "",
          logIcon: `fas fa-exclamation-triangle`,
          title: `The system was sent into maintenance mode due to excessive problems.`,
          event: `There have been at least 10 critical issues in the last hour. The system was put into maintenance mode. Please resolve the issues in DJ Controls -> To-Do (in the Administration menu), mark them off, and then deactivate maintenance mode.`,
        })
        .fetch()
        .tolerate((err) => {
          sails.log.error(err);
        });

      await sails.helpers.emails.queueEmergencies(
        `[URGENT] WWSU system locked out in maintenance mode`,
        `Directors,<br /><br />
Several critical issues occurred in the WWSU system in the last hour. As a precaution, <strong>WWSU was sent into maintenance mode.</strong>. This means error / failsafe checks are no longer running, and you could be off the air!<br /><br />
Please review the logs in DJ Controls -> To-Do (Administration menu) immediately. Fix the issues. Mark the logs off as complete. And then de-activate maintenance mode.`,
        true
      );
    }

    if (sails.models.status.tasks.cleanup) {
      await sails.helpers.status.modify.with({
        name: `app`,
        status: 4,
        summary: `Maintenance mode active: Daily cleanup in progress. System operations not permitted until complete.`,
        data: `Daily Cleanup is currently running. Maintenance mode is active until this is complete. This should take no more than a minute.`,
      });
    } else if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: `app`,
        status: 1,
        summary: `Maintenance mode active! All programming disabled / shows cannot air. Please disable maintenance mode via DJ Controls when maintenance is complete.`,
        data: `System is in maintenance mode! Many features / error checks are currently disabled. When maintenance is done, be sure to acknowledge applicable critical issues in Admin DJ Controls -> To-Do, and then disable maintenance mode in admin DJ Controls -> Maintenance -> Maintenance Mode.`,
      });
    } else if (criticalLogs >= 6) {
      await sails.helpers.status.modify.with({
        name: `app`,
        status: 2,
        summary: `Unacknowledged critical issues in the last hour: ${criticalLogs}. Maintenance mode will activate at 10.`,
        data: `There are ${criticalLogs} unacknowledged critical issues logged in the last hour! If this reaches 10, the system will enter maintenance mode automatically.`,
      });
    } else if (criticalLogs >= 4) {
      await sails.helpers.status.modify.with({
        name: `app`,
        status: 3,
        summary: `Unacknowledged critical issues in the last hour: ${criticalLogs}. Maintenance mode will activate at 10.`,
        data: `There are ${criticalLogs} unacknowledged critical issues logged in the last hour. If this reaches 10, the system will enter maintenance mode automatically.`,
      });
    } else if (criticalLogs >= 1) {
      await sails.helpers.status.modify.with({
        name: `app`,
        status: 4,
        summary: `Unacknowledged critical issues in the last hour: ${criticalLogs}. Maintenance mode will activate at 10.`,
        data: `There are ${criticalLogs} unacknowledged critical issues logged in the last hour. If this reaches 10, the system will enter maintenance mode automatically.`,
      });
    } else {
      await sails.helpers.status.modify.with({
        name: `app`,
        status: 5,
        summary: `Operational.`,
        data: `SailsJS is operational.`,
      });
    }
  },
};
