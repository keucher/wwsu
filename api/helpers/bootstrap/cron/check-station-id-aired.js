module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkStationIdAired",

  description: "Check if a top of the hour ID aired.",

  schedule: "5 5 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance)
      return "SKIPPED; maintenance mode active.";

    if (!sails.config.custom.radiodjs.length)
      return "SKIPPED; not using RadioDJ.";

    if (
      moment()
        .startOf(`hour`)
        .subtract(5, `minutes`)
        .isAfter(moment(sails.models.meta.memory.lastID))
    ) {
      // A station ID probably did not air when it should have

      // Check if a break was executed; if so, this is probably a system issue and not a DJ / host failing to do a break.
      let probablySystemProblem = moment()
        .subtract(10, "minutes")
        .isAfter(sails.models.status.errorCheck.prevBreak);

      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "id",
          loglevel: "orange",
          logsubtype:
            sails.models.meta.memory.show !== ""
              ? sails.models.meta.memory.show
              : sails.models.meta.memory.genre,
          logIcon: `fas fa-coffee`,
          excused: probablySystemProblem,
          title: `Required top-of-hour ID was not aired!`,
          event: `Broadcast: ${
            sails.models.meta.memory.show &&
            sails.models.meta.memory.show !== ""
              ? sails.models.meta.memory.show
              : sails.models.meta.memory.genre
          }${
            probablySystemProblem
              ? `<br />This is probably a system issue; logs indicate a break was taken less than 10 minutes ago, but a top of hour ID was not aired at that break. This record was marked excused by default.`
              : ``
          }`,
        })
        .fetch()
        .tolerate((err) => {
          sails.log.error(err);
        });
    }
  },
};
