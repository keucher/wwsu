module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkDelayStatus",

  description:
    "Update the delay status to critical if it has not been updated in 1 minute by DJ Controls (assumed it is offline).",

  schedule: "13 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "delay-system",
        label: "Delay System",
        summary: `Checks disabled; system in maintenance mode.`,
        data: `Delay System checks are disabled; maintenance mode is active. Status will take up to one minute to clear after maintenance mode is disabled.`,
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    // Delay system; send to critical status if no status data for over 1 minute
    let delay = await sails.models.status.findOne({ name: "delay-system" });
    let responsible = await sails.models.hosts.count({ delaySystem: true });
    if (
      moment(delay.updatedAt).add(1, "minutes").isBefore(moment()) &&
      responsible > 0
    ) {
      await sails.helpers.status.modify.with({
        name: "delay-system",
        label: "Delay System",
        summary: `Delay System status not being reported; it may be offline. Remote broadcasts cannot air. Shows strongly discouraged from airing: possibly no safety net if something inappropriate is said.`,
        data: `There has been no information received about the delay system for over 1 minute.<br /><strong>Remote broadcasts will not be allowed</strong> until this issue is resolved because it is currently impossible to remotely dump.
        <br /><strong>TO FIX: </strong>
        <ul>
          <li>Make sure the delay system is on and running. Try pressing the START button.</li>
          <li>Make sure the RF232 serial port / cord is properly connected from the delay system to the computer.</li>
          <li>Make sure the computer to which the delay system is connected is on and connected to the internet.</li>
          <li>Make sure DJ Controls is open / running on the same computer as the delay system.</li>
          <li>Make sure the DJ Controls running on the same computer as the delay system is set as responsible for it (DJ Controls Administration -> Hosts -> (edit host) -> Delay System Connected).<li>
          <li>Make sure the DJ Controls running on the same computer as the delay system has the correct serial port set in DJ Controls Settings -> Serial Ports -> Delay System.<li>
          <li>Be aware DJ Controls is only compatible with Airtools 6100 broadcast delay. If you have a different delay system, DJ Controls must be re-programmed for it.</li>
        </ul>`,
        status: 1,
      });
      await sails.helpers.meta.change.with({ delaySystem: null });
      return "No data received recently from the DJ Controls monitoring the delay system!";
    } else if (responsible < 1) {
      await sails.helpers.status.modify.with({
        name: "delay-system",
        label: "Delay System",
        summary: `No hosts are configured to monitor the delay system. Remote broadcasts cannot air. Shows strongly discouraged from airing: possibly no safety net if something inappropriate is said.`,
        data: `There are no hosts currently set for delay system monitoring. <strong>Remote dumping for remote broadcasts is not possible until this is fixed.</strong>
        <br /><strong>TO FIX: </strong> On a DJ Controls with access to the administration menu, go under Hosts, edit the host to which the delay system is connected, and check "Delay System Connected". You may have to edit and un-check this from the previous host if changing computers. Also make sure the correct serial port is selected for the delay system on the DJ Controls running on that computer -> Serial Ports.`,
        status: 1,
      });
      await sails.helpers.meta.change.with({ delaySystem: null });
      return "No configured hosts for delay system!";
    }
  },
};
