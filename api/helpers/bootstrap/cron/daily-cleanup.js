module.exports = {
  friendlyName: "helpers.bootstrap.cron.dailyCleanup",

  description:
    "Keep the application running smooth by pruning old records from the datastores daily.",

  schedule: "52 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      return "SKIPPED; maintenance mode active.";
    }

    // Do not execute if we do not know when daily cleanup was last run.
    if (!sails.models.status.lastCleanup) {
      sails.models.status.lastCleanup = moment();
      return "SKIPPED; lastCleanup was not set yet.";
    }

    // Do not run when not in automation (eg. if a broadcast is on the air).
    if (!sails.models.meta.memory.state.startsWith("automation_")) {
      return "SKIPPED; not in automation right now.";
    }

    // Do not run if daily cleanup already ran today.
    if (
      moment()
        .startOf("day")
        .isSameOrBefore(moment(sails.models.status.lastCleanup))
    ) {
      return "SKIPPED; cleanup already ran today.";
    }

    sails.models.status.lastCleanup = moment();

    let cleanedUp = ``;
    let records;
    let wasLofi = sails.config.custom.basic.maintenance;

    sails.models.status.tasks.cleanup++;

    try {
      // Activate lofi; clean-up tends to freeze Node for several seconds. We don't want accidental false positive errors tripping.
      sails.config.custom.basic.maintenance = true;

      await sails.helpers.status.modify.with({
        name: `app`,
        status: 4,
        summary: `Maintenance mode active: Daily cleanup running. System operations not permitted until complete.`,
        data: `Daily Cleanup is currently running. Maintenance mode is active until this is complete. This should take no more than a minute.`,
      });

      // Delete announcements that expired over a month ago
      records = await sails.models.announcements
        .destroy({
          expires: {
            "<": moment().subtract(1, "months").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Announcements deleted (expired over 1 months ago): ${records
          .map((record) => record.title)
          .join()}</li>`;

      // Delete attendance records older than 2 years
      records = await sails.models.attendance
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Attendance records / operation logs deleted (over 2 years old): ${records.length}</li>`;

      // Mark blog posts inactive that expired.
      records = await sails.models.blogs
        .update(
          {
            active: true,
            expires: {
              "!=": null,
              "<": moment().format("YYYY-MM-DD HH:mm:ss"),
            },
          },
          { active: false }
        )
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Blog posts marked inactive (expired): ${records.length}</li>`;

      // Delete blog posts that have been updated over 1 year ago and have no author (usually means org member was removed).
      records = await sails.models.blogs
        .destroy({
          author: null,
          updatedAt: {
            "<": moment().subtract(1, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Blog posts deleted (no active author; unedited for over a year): ${records.length}</li>`;

      // Delete on-air programming calendar events (excluding sports) that did not air for over a year
      records = await sails.models.calendar
        .destroy({
          ID: { "!=": 1 }, // Do not delete the test event
          type: ["show", "remote", "prerecord", "genre", "playlist"],
          lastAired: {
            "<": moment().subtract(1, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
          active: false,
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Calendar events deleted (did not air over a year): ${records
          .map((record) => `${record.type}: ${record.hosts} - ${record.name}`)
          .join()}</li>`;

      // Clean up old schedules
      records = await sails.helpers.calendar.cleanSchedules(
        moment().subtract(1, "weeks")
      );
      if (records > 0)
        cleanedUp += `<li>Schedules/occurrences/overrides deleted (ended 1+ weeks ago): ${records}</li>`;

      // Sometimes, garbage weather records exist. Remove these so DJ Controls doesn't freeze up trying to process weather.
      records = await sails.models.climacell
        .destroy({
          updatedAt: {
            "<": moment().subtract(2, "days").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Climacell garbage weather records deleted (2+ days without updates): ${records.length}</li>`;

      // Mark DJs inactive who have not been seen for over a year
      records = await sails.models.djs
        .update(
          {
            active: true,
            lastSeen: {
              "<": moment().subtract(1, "years").format("YYYY-MM-DD HH:mm:ss"),
            },
            ID: { "!=": 1 }, // Never mark master member inactive
          },
          { active: false }
        )
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>DJs / Org Members marked inactive (did not air any shows / authorize anything in the last year): ${records
          .map((record) => `${record.name} (${record.realName})`)
          .join()}</li>`;

      // Clean up old email records from over 2 years ago
      records = await sails.models.emails
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Email logs deleted (over 2 years old): ${records.length}</li>`;

      // Delete flagged records older than 2 years
      records = await sails.models.flaggedlist
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Flagged track / broadcast records deleted (over 2 years old): ${records.length}</li>`;

      // Delete RadioDJ history records older than 2 years
      if (sails.config.custom.radiodjs.length) {
        records = await sails.models.history
          .destroy({
            createdAt: {
              "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
            },
          })
          .fetch();
        if (records.length > 0)
          cleanedUp += `<li>History records from RadioDJ deleted (over 2 years old): ${records.length}</li>`;
      }

      // Delete listener analytics older than 2 years
      records = await sails.models.listeners
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Listener / viewer records deleted (over 2 years old): ${records.length}</li>`;

      // Delete lockdown records older than 2 years
      records = await sails.models.lockdown
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Lockdown / log-in records deleted (over 2 years old): ${records.length}</li>`;

      // Delete logs older than 2 years
      records = await sails.models.logs
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Log records deleted (over 2 years old): ${records.length}</li>`;

      // Delete old message records older than 1 years
      records = await sails.models.messages
        .destroy({
          createdAt: {
            "<": moment().subtract(1, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Message records deleted (over 1 year old): ${records.length}</li>`;

      // Delete track request records older than 2 years
      if (sails.config.custom.radiodjs.length) {
        records = await sails.models.requests
          .destroy({
            createdAt: {
              "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
            },
          })
          .fetch();
        if (records.length > 0)
          cleanedUp += `<li>Request records deleted (over 2 years old): ${records.length}</li>`;
      }

      // Delete liked songs records older than 2 years
      records = await sails.models.songsliked
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Songs liked records deleted (over 2 years old): ${records.length}</li>`;

      // Delete timesheet records older than 2 years
      records = await sails.models.timesheet
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").format("YYYY-MM-DD HH:mm:ss"),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Timesheet records deleted (over 2 years old): ${records.length}</li>`;

      // Add a log with the clean-up results
      await sails.models.logs
        .create({
          attendanceID: null,
          logtype: "cleanup",
          loglevel: "info",
          logsubtype: "",
          logIcon: `fas fa-broom`,
          title: `Daily clean-up was run.`,
          event: `<ul>${cleanedUp}</ul>`,
        })
        .fetch()
        .tolerate((err) => {
          // Don't throw errors, but log them
          sails.log.error(err);
        });

      // Turn lofi back off if it was off when this process started.
      sails.config.custom.basic.maintenance = wasLofi;

      sails.models.status.tasks.cleanup--;
    } catch (e) {
      // Add a log indicating an error
      await sails.models.logs
        .create({
          attendanceID: null,
          logtype: "cleanup",
          loglevel: "warning",
          logsubtype: "",
          logIcon: `fas fa-broom`,
          title: `There was a problem running the daily cleanup!`,
          event: `${e.message}<br />See node logs for more information.`,
        })
        .fetch()
        .tolerate((err) => {
          // Don't throw errors, but log them
          sails.log.error(err);
        });

      sails.config.custom.basic.maintenance = wasLofi;
      sails.log.error(e);

      sails.models.status.tasks.cleanup--;
    }
  },
};
