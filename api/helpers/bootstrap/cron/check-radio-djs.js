module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkRadioDjs",

  description:
    "Check the REST servers on each configured RadioDJ; also stop playing RadioDJs that are playing something when they should not be.",

  schedule: "4,34 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    if (!sails.config.custom.radiodjs.length)
      return "SKIPPED; No RadioDJs configured in the system.";

    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      let maps = sails.config.custom.radiodjs.map(async (radiodj) => {
        await sails.helpers.status.modify.with({
          name: `radiodj-${radiodj.name}`,
          label: `RadioDJ ${radiodj.label}`,
          summary: `Checks disabled; system in maintenance mode.`,
          data: "Checks are disabled; maintenance mode is active. Status will take up to 30 seconds to clear after maintenance mode is disabled.",
          status: 4,
        });
      });
      await Promise.all(maps);
      return "SKIPPED; maintenance mode active.";
    }

    // eslint-disable-next-line no-async-promise-executor
    await sails.helpers.asyncForEach(
      sails.config.custom.radiodjs,
      (radiodj) => {
        // eslint-disable-next-line promise/param-names
        return new Promise((resolve2) => {
          sails.models.status
            .findOne({ name: `radiodj-${radiodj.name}` })
            .then(async (status) => {
              try {
                var resp = await needle(
                  "get",
                  `${radiodj.restURL}/p?auth=${radiodj.restPassword}`,
                  {},
                  { headers: { "Content-Type": "application/json" } }
                );
                if (
                  resp &&
                  typeof resp.body !== "undefined" &&
                  typeof resp.body.children !== "undefined"
                ) {
                  await sails.helpers.status.modify.with({
                    name: `radiodj-${radiodj.name}`,
                    label: `RadioDJ ${radiodj.label}`,
                    summary: `Online`,
                    data: "RadioDJ is online.",
                    status: 5,
                  });

                  // We were waiting for a good RadioDJ to switch to. Switch to it immediately and re-queue what we have in memory.
                  if (sails.models.status.errorCheck.waitForGoodRadioDJ) {
                    sails.models.status.errorCheck.waitForGoodRadioDJ = false;
                    var queue = sails.models.meta.automation;
                    await sails.helpers.meta.change.with({
                      radiodj: radiodj.name,
                    });
                    await sails.helpers.rest.cmd("ClearPlaylist", 1);
                    await sails.helpers.error.post(queue);
                  }

                  // If this RadioDJ is inactive, check to see if it is playing anything
                  if (
                    sails.models.meta.memory.radiodj !== radiodj.name &&
                    sails.models.meta.memory.altRadioDJ !== radiodj.name
                  ) {
                    let automation = await sails.helpers.rest.getQueue(radiodj);
                    // If this if condition passes, the RadioDJ is playing something.
                    if (
                      typeof automation[0] !== "undefined" &&
                      parseInt(automation[0].ID) !== 0
                    ) {
                      // If the active radioDJ is playing something too, we should stop the inactive RadioDJ or we will have more than one thing going out on the air.
                      if (sails.models.meta.memory.queueFinish !== null) {
                        await sails.helpers.rest.cmd(
                          "StopPlayer",
                          null,
                          10000,
                          radiodj
                        );
                        // If the active RadioDJ is NOT playing anything, we should switch the active RadioDJ to the one playing something if not in a show.
                      } else if (
                        sails.models.meta.memory.changingState === null &&
                        [
                          "live_on",
                          "remote_on",
                          "sports_on",
                          "sportsremote_on",
                        ].indexOf(sails.models.meta.memory.state) === -1
                      ) {
                        await sails.helpers.meta.change.with({
                          changingState: `Switching radioDJ instances`,
                        });
                        await sails.helpers.rest.cmd("EnableAssisted", 1, 0);
                        await sails.helpers.rest.cmd("EnableAutoDJ", 1, 0);
                        await sails.helpers.rest.cmd("StopPlayer", 0, 0);
                        await sails.helpers.rest.changeRadioDj(radiodj);
                        await sails.helpers.error.post();
                        await sails.helpers.meta.change.with({
                          changingState: null,
                        });
                      }
                    }
                  }
                } else {
                  await sails.helpers.status.modify.with({
                    name: `radiodj-${radiodj.name}`,
                    label: `RadioDJ ${radiodj.label}`,
                    summary:
                      `RadioDJ is not reporting what is in the queue.` &
                      (radiodj.errorLevel < 3
                        ? ` This may affect on-air programming.`
                        : ``),
                    data: `RadioDJ REST did not return queue data!
                    <br /><strong>TO FIX: </strong>
                    <ul>
                    <li>If you recently started / restarted RadioDJ, you may have to start and stop a track for REST to begin working.</li>
                    <li>Make sure the REST server is configured correctly (RadioDJ wrench -> Plugins -> REST Server), is started / running, and can be accessed by the node / sailsjs server (check the network connection).</li>
                    <li>Make sure configuration for accessing RadioDJ is correct in an administration DJ Controls -> System Settings -> RadioDJ Instances.</li>
                  </ul>`,
                    status: radiodj.errorLevel,
                  });
                }
                return resolve2(false);
              } catch (unusedE) {
                await sails.helpers.status.modify.with({
                  name: `radiodj-${radiodj.name}`,
                  label: `RadioDJ ${radiodj.label}`,
                  summary: `Communication error; RadioDJ might be offline or have crashed. This may affect programming.`,
                  data: `RadioDJ REST returned an error or is not responding!
                  <br /><strong>TO FIX: </strong>
                  <ul>
                  <li>Sometimes a bad track in the RadioDJ queue (such as one with unicode characters in its name) will cause this error. Review what is in the queue and remove / edit bad tracks.</li>
                  <li>Make sure this RadioDJ is open, is not frozen, and has access to the music library (check any applicable network drives to make sure they are online). Restart RadioDJ if it is frozen.</li>
                  <li>If you recently started / restarted RadioDJ, you may need to play and then stop a track for REST to begin working.</li>
                  <li>Make sure the REST server is configured correctly (RadioDJ wrench -> Plugins -> REST Server), is started / running, and can be accessed by the node / sailsjs server (check the network connection).</li>
                  <li>Make sure configuration for accessing RadioDJ is correct in an administration DJ Controls -> System Settings -> RadioDJ Instances.</li>
                </ul>`,
                  status: radiodj.errorLevel,
                });
                return resolve2(false);
              }
            });
        });
      }
    );
  },
};
