module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkWebsite",

  description: "Check the status of the WWSU website",

  schedule: "5 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if no website is defined
    if (
      !sails.config.custom.basic.website ||
      sails.config.custom.basic.website === ""
    ) {
      await sails.helpers.status.modify.with({
        name: `website`,
        label: `Website`,
        summary: `Checks disabled; website URL not configured.`,
        data: "Website checks are disabled; website is not configured. <br /><strong>TO FIX:</strong> Set the website in admin DJ Controls -> System Settings -> Websites.",
        status: 4,
      });
      return "SKIPPED; website not defined in configuration.";
    }

    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: `website`,
        label: `Website`,
        summary: `Checks disabled; system in maintenance mode.`,
        data: "Website checks are disabled; maintenance mode is active. Status will take up to one minute to clear after maintenance mode is disabled.",
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    try {
      var resp = await needle(
        "get",
        sails.config.custom.basic.website,
        {},
        { headers: { "Content-Type": "application/json" } }
      );
      if (resp && typeof resp.body !== "undefined") {
        await sails.helpers.status.modify.with({
          name: `website`,
          label: `Website`,
          summary: `Online.`,
          data: "Website is online.",
          status: 5,
        });
      } else {
        await sails.helpers.status.modify.with({
          name: `website`,
          label: `Website`,
          summary: `No data received; website might be offline.`,
          data: `Website ${sails.config.custom.basic.website} did not return body data.
          <br /><strong>TO FIX:</strong> Make sure the website is online and working correctly, and the URL is set correctly in administration DJ Controls -> System Settings -> Website.`,
          status: 2,
        });
      }
    } catch (e) {
      sails.log.error(e);
      await sails.helpers.status.modify.with({
        name: `website`,
        label: `Website`,
        summary: `Error or website offline.`,
        data: `Error checking the status of the website. The website might be offline. Please check server error logs.
        <br /><strong>TO FIX:</strong> Make sure the website is online and working correctly, and the URL is set correctly in administration DJ Controls -> System Settings -> Website.`,
        status: 2,
      });
    }
  },
};
