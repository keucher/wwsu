module.exports = {
  friendlyName: "helpers.bootstrap.cron.updateWeather",

  description: "Fetch and process weather data",

  schedule: "11 */5 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "tomorrowio",
        summary: `Disabled; system in maintenance mode.`,
        data: `Tomorrow.io weather fetching is disabled; maintenance mode is active. Status will take up to 5 minutes to clear after maintenance mode is disabled.`,
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    // Update weather
    await sails.helpers.climacell.update();
  },
};
