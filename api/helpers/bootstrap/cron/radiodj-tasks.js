module.exports = {
  friendlyName: "helpers.bootstrap.cron.radiodjTasks",

  description:
    "Cron job for performing tasks regarding RadioDJ / the automation system.",

  schedule: "* * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    var queueLength = 0;
    var trackLength = 0;
    var countDown = 0;
    var theplaylist;
    var playlistTracks;
    var change = {}; // Instead of doing a bunch of changeMetas, put all non-immediate changes into this object and changeMeta at the end of this operation.

    // Skip all checks and use default meta template if lofi is activated or we have no healthy RadioDJs.
    if (
      sails.config.custom.basic.maintenance ||
      sails.models.status.errorCheck.waitForGoodRadioDJ
    ) {
      try {
        change = sails.models.meta.memoryDefault;
        change.time = moment().toISOString(true);
        await sails.helpers.meta.change.with(change);
      } catch (e) {
        sails.log.error(e);
      }
      return;
    }

    // If we do not know current state, we may need to populate the info from the database.
    if (
      sails.models.meta.memory.state === "" ||
      sails.models.meta.memory.state === "unknown"
    ) {
      try {
        sails.log.verbose(`Unknown meta. Retrieving from database.`);

        var meta = await sails.models.meta.find().limit(1);
        meta = meta[0];

        // If we found and know our meta state, process it back into memory.
        if (
          meta &&
          meta.state &&
          meta.state !== "unknown" &&
          meta.state !== ""
        ) {
          delete meta.createdAt;
          delete meta.updatedAt;
          delete meta.ID;
          meta.time = moment().toISOString(true);
          // sails.log.verbose(meta)
          await sails.helpers.meta.change.with(meta);
          if (
            sails.config.custom.radiodjs.length &&
            meta.playlist !== null &&
            meta.playlist !== ""
          ) {
            theplaylist = await sails.models.playlists.findOne({
              name: meta.playlist,
            });
            // LINT: RadioDJ table that is not camel case
            // eslint-disable-next-line camelcase
            playlistTracks = await sails.models.playlists_list
              .find({ pID: theplaylist.ID })
              .tolerate(() => {});
            sails.models.playlists.active.tracks = [];
            if (typeof playlistTracks !== "undefined") {
              playlistTracks.forEach((playlistTrack) => {
                sails.models.playlists.active.tracks.push(playlistTrack.sID);
              });
            }
          }
        } else {
          // We still do not know what state we should be
          // After having checked the meta in the database, set it to blank so the calendar subsystem knows it is safe to run.
          await sails.helpers.meta.change.with({ state: "" });
        }
      } catch (e) {
        sails.log.error(e);
      }
    }

    // Check if we should timeout
    if (
      sails.models.meta.memory.timeout !== null &&
      moment().isAfter(sails.models.meta.memory.timeout) &&
      sails.models.meta.memory.changingState === null
    ) {
      try {
        await sails.helpers.meta.change.with({
          changingState: `Break timed out; switching to automation`,
        });
        await sails.models.logs
          .create({
            attendanceID: sails.models.meta.memory.attendanceID,
            logtype: "timeout",
            loglevel: "warning",
            logsubtype: sails.models.meta.memory.show,
            logIcon: `fas fa-hourglass-end`,
            title: `A break timed out, and the system was sent to automation.`,
            event: `Broadcast: ${sails.models.meta.memory.show}`,
          })
          .fetch()
          .tolerate((err) => {
            // Do not throw for errors, but log it.
            sails.log.error(err);
          });
        await sails.helpers.state.automation(false);
      } catch (e) {
        await sails.helpers.meta.change.with({ changingState: null });
      }
    }

    // Do not proceed any further if no configured radioDJs in the system
    if (!sails.config.custom.radiodjs.length) {
      await sails.helpers.status.modify.with({
        name: "underwritings",
        label: "Underwritings",
        summary: `Disabled; no RadioDJ instances configured.`,
        data: `Underwritings system is disabled because there are no RadioDJs configured.
        <br /><strong>TO FIX:</strong> Go to administration DJ Controls -> System Settings -> RadioDJs.`,
        status: 4,
      });
      return;
    }

    // changingState error check
    if (
      sails.models.meta.memory.changingState !== null &&
      !sails.models.status.waitForGoodRadioDJ &&
      !sails.models.status.errorPostRunning
    ) {
      await sails.helpers.error.count("changingStateTookTooLong");
    } else {
      await sails.helpers.error.reset("changingStateTookTooLong");
    }

    var queue = [];

    // Only re-fetch the queue once every 3 seconds or when the system intelligently determines it is necessary to keep a real-time eye on the queue.
    // Should check whenever track length < 3 and something is playing, or time to check (every 5 seconds), or the genre might have run out of music, or there is not an accurate queue count, or something triggered for a queue check (such as duplicate track removal or change in state).
    if (
      (sails.models.status.errorCheck.prevTrackLength < 3 &&
        sails.models.meta.memory.playing) ||
      sails.models.status.errorCheck.queueWait < 1 ||
      (sails.models.meta.memory.state === "automation_genre" &&
        sails.models.status.errorCheck.prevQueueLength <= 20) ||
      sails.models.status.errorCheck.trueZero > 0
    ) {
      try {
        if (sails.models.meta.memory.changingState === null) {
          queue = await sails.helpers.rest.getQueue();

          if (!queue[0]) {
            throw new Error("Queue returned 0 tracks.");
          }

          sails.models.status.errorCheck.queueWait = 5;

          try {
            sails.log.silly(`queueCheck executed.`);
            var theTracks = [];
            change.trackID = parseInt(queue[0].ID || 0);
            change.trackIDSubcat = parseInt(queue[0].IDSubcat || 0);
            if (change.trackID !== sails.models.meta.memory.trackID) {
              logBreakDebug = true;
              breakDebug = [];
            }

            var breakQueueLength = -2;
            var firstNoMeta = 0;
            change.queueMusic = false;

            if (
              sails.models.meta.memory.state.includes("_returning") ||
              sails.models.meta.memory.state === "automation_live" ||
              sails.models.meta.memory.state === "automation_remote" ||
              sails.models.meta.memory.state === "automation_sports" ||
              sails.models.meta.memory.state === "automation_sportsremote"
            ) {
              breakQueueLength = -1;
              firstNoMeta = -1;
              queue.forEach((track, index) => {
                if (
                  sails.config.custom.subcats.noMeta &&
                  sails.config.custom.subcats.noMeta.indexOf(
                    parseInt(track.IDSubcat)
                  ) === -1
                ) {
                  if (firstNoMeta > -1 && breakQueueLength < 0) {
                    breakQueueLength = index;
                    change.queueMusic = true;
                  }
                } else if (firstNoMeta < 0) {
                  firstNoMeta = index;
                }
              });
            }

            // Determine if something is currently playing via whether or not track 0 has ID of 0.
            if (parseInt(queue[0].ID) === 0) {
              // If we apparently stopped, but there is another item in the queue, consider the queue inaccurate ATM.
              if (
                sails.models.meta.memory.playing &&
                queue[1] &&
                queue[1].ID !== 0
              ) {
                sails.models.status.errorCheck.trueZero = 3;
                change.queueCalculating = true;
              }
              change.playing = false;
              change.trackFinish = null;
              trackLength = 0;
            } else {
              change.playing = true;
              change.trackArtist = queue[0].Artist || null;
              change.trackTitle = queue[0].Title || null;
              change.trackAlbum = queue[0].Album || null;
              change.trackLabel = queue[0].Label || null;
              trackLength =
                parseFloat(queue[0].Duration) - parseFloat(queue[0].Elapsed);
              trackLength = parseFloat(trackLength);

              // Workaround for RadioDJ 2.0.3.4 bug; tracks with unknown duration sometimes return a massively large duration that triggers invalid dates
              try {
                change.trackFinish =
                  typeof trackLength === "number" && trackLength > 0
                    ? moment().add(trackLength, "seconds").toISOString(true)
                    : moment().toISOString(true);
              } catch (e) {
                change.trackFinish = moment().toISOString(true);
              }
            }

            // Remove duplicate tracks (ONLY remove one since cron goes every second; so one is removed each second). Do not remove any duplicates if changing states.
            if (sails.models.meta.memory.changingState === null) {
              sails.log.debug(
                `Calling asyncForEach in cron checks for removing duplicate tracks`
              );
              await sails.helpers.asyncForEach(queue, (track, index) => {
                // eslint-disable-next-line promise/param-names
                return new Promise((resolve2) => {
                  var title = `${track.Artist} - ${track.Title}`;
                  // If there is a duplicate, remove the track, store for later queuing if necessary.
                  // Also, calculate length of the queue
                  if (theTracks.indexOf(title) > -1) {
                    sails.log.debug(
                      `Track ${
                        track.ID
                      } on index ${index} is a duplicate of index (${
                        theTracks[theTracks.indexOf(title)]
                      }. Removing!`
                    );
                    if (track.TrackType !== "Music") {
                      sails.models.songs.pending.push(track.ID);
                    }
                    sails.models.status.errorCheck.queueWait = 0;
                    sails.helpers.rest
                      .cmd("RemovePlaylistTrack", index - 1)
                      .then(() => {
                        theTracks = [];
                        sails.helpers.rest.getQueue().then((theQueue) => {
                          queue = theQueue;
                          queueLength = 0;
                          return resolve2(true);
                        });
                      });
                  } else {
                    theTracks.push(title);
                    let tLength =
                      parseFloat(track.Duration) - parseFloat(track.Elapsed);
                    tLength = parseFloat(tLength);

                    // Workaround for RadioDJ 2.0.3.4 bug; tracks with unknown duration sometimes return a massively large duration that triggers invalid dates
                    try {
                      let justToTriggerAnErrorIfNecessary = moment()
                        .add(tLength, "seconds")
                        .toISOString(true);
                      if (typeof tLength === "number") queueLength += tLength;
                      if (
                        index < breakQueueLength ||
                        (breakQueueLength < 0 && firstNoMeta > -1)
                      ) {
                        if (typeof tLength === "number") countDown += tLength;
                      }
                    } catch (e) {
                      // Ignore
                    }
                    return resolve2(false);
                  }
                });
              });
            }

            /* Every now and then, querying now playing queue happens when RadioDJ is in the process of queuing a track, resulting in an inaccurate reported queue length.
             * This results in false transitions in system state. Run a check to detect if the queuelength deviated by more than 2 seconds since last run.
             * If so, we assume this was an error, so do not treat it as accurate, and trigger a 3 second error resolution wait.
             */
            if (
              queueLength >
                sails.models.status.errorCheck.prevQueueLength - 3 ||
              sails.models.status.errorCheck.trueZero > 0
            ) {
              // If the detected queueLength gets bigger, assume the issue resolved itself and immediately mark the queuelength as accurate
              if (
                queueLength >
                sails.models.status.errorCheck.prevQueueLength - 1
              ) {
                sails.models.status.errorCheck.trueZero = 0;
                change.queueCalculating = false;
              } else if (sails.models.status.errorCheck.trueZero > 0) {
                sails.models.status.errorCheck.trueZero -= 1;
                if (sails.models.status.errorCheck.trueZero < 1) {
                  sails.models.status.errorCheck.trueZero = 0;
                  change.queueCalculating = false;
                  // If not an accurate queue count, use previous queue - 1 instead
                } else {
                  queueLength =
                    sails.models.status.errorCheck.prevQueueLength - 1;
                  trackLength =
                    sails.models.status.errorCheck.prevTrackLength - 1;
                  countDown = sails.models.status.errorCheck.prevCountdown - 1;
                }
                if (queueLength < 0) {
                  queueLength = 0;
                }
                if (trackLength < 0) {
                  trackLength = 0;
                }
                if (countDown < 0) {
                  countDown = 0;
                }
              } else {
                // No error wait time [remaining]? Use actual detected queue time.
              }
            } else {
              sails.models.status.errorCheck.trueZero = 3; // Wait up to 3 seconds before considering the queue accurate
              change.queueCalculating = true;
              // Instead of using the actually recorded queueLength, use the previously detected length minus 1 second.
              queueLength = sails.models.status.errorCheck.prevQueueLength - 1;
              trackLength = sails.models.status.errorCheck.prevTrackLength - 1;
              countDown = sails.models.status.errorCheck.prevCountdown - 1;
              if (queueLength < 0) {
                queueLength = 0;
              }
              if (trackLength < 0) {
                trackLength = 0;
              }
              if (countDown < 0) {
                countDown = 0;
              }
            }

            // Enable assisted if we are in a live show and just finished playing stuff in the queue
            if (
              (sails.models.meta.memory.state === "live_on" ||
                sails.models.meta.memory.state === "sports_on") &&
              queueLength <= 0 &&
              sails.models.status.errorCheck.prevQueueLength > 0
            ) {
              await sails.helpers.rest.cmd("EnableAssisted", 1);
            }

            sails.models.status.errorCheck.prevQueueLength = queueLength;
            sails.models.status.errorCheck.prevTrackLength = trackLength;
            sails.models.status.errorCheck.prevCountdown = countDown;

            // When we are waiting to begin a broadcast, switch to the on state when all noMeta tracks have finished.
            if (
              sails.models.meta.memory.state.includes("_returning") ||
              sails.models.meta.memory.state === "automation_live" ||
              sails.models.meta.memory.state === "automation_remote" ||
              sails.models.meta.memory.state === "automation_sports" ||
              sails.models.meta.memory.state === "automation_sportsremote"
            ) {
              if (
                firstNoMeta < 0 &&
                breakQueueLength < 0 &&
                sails.models.status.errorCheck.trueZero <= 0
              ) {
                if (sails.models.meta.memory.state === "automation_live") {
                  await sails.helpers.meta.change.with({ state: "live_on" });
                  await sails.helpers.meta.newShow();
                }
                if (sails.models.meta.memory.state === "automation_sports") {
                  await sails.helpers.meta.change.with({
                    state: "sports_on",
                  });
                  await sails.helpers.meta.newShow();
                }
                if (sails.models.meta.memory.state === "automation_remote") {
                  await sails.helpers.meta.change.with({
                    state: "remote_on",
                  });
                  await sails.helpers.meta.newShow();
                }
                if (
                  sails.models.meta.memory.state === "automation_sportsremote"
                ) {
                  await sails.helpers.meta.change.with({
                    state: "sportsremote_on",
                  });
                  await sails.helpers.meta.newShow();
                }
                if (sails.models.meta.memory.state.includes("_returning")) {
                  switch (sails.models.meta.memory.state) {
                    case "live_returning":
                      await sails.helpers.meta.change.with({
                        state: "live_on",
                      });
                      break;
                    case "remote_returning":
                      await sails.helpers.meta.change.with({
                        state: "remote_on",
                      });
                      break;
                    case "sports_returning":
                      await sails.helpers.meta.change.with({
                        state: "sports_on",
                      });
                      break;
                    case "sportsremote_returning":
                      await sails.helpers.meta.change.with({
                        state: "sportsremote_on",
                      });
                      break;
                  }
                }
              }
            } else {
              sails.models.status.errorCheck.prevCountdown = 0;
              countDown = 0;
            }
          } catch (e) {
            sails.models.status.errorCheck.queueWait = 0;
            sails.log.error(e);
          }

          // Do automation system error checking and handling
          if (
            !sails.models.meta.memory.altRadioDJ &&
            ((queue.length > 0 &&
              queue[0].Duration ===
                sails.models.status.errorCheck.prevFetchedDuration &&
              queue[0].Elapsed ===
                sails.models.status.errorCheck.prevFetchedElapsed) ||
              queue.length < 1) &&
            (sails.models.meta.memory.state.startsWith("automation_") ||
              sails.models.meta.memory.state.endsWith("_break") ||
              sails.models.meta.memory.state.endsWith("_disconnected") ||
              sails.models.meta.memory.state.endsWith("_returning") ||
              sails.models.meta.memory.state.startsWith("prerecord_"))
          ) {
            await sails.helpers.error.count("frozen");
            sails.models.status.errorCheck.queueWait = 0;
          } else {
            sails.models.status.errorCheck.prevDuration = queue[0].Duration;
            sails.models.status.errorCheck.prevElapsed = queue[0].Elapsed;
            sails.models.status.errorCheck.prevFetchedDuration =
              queue[0].Duration;
            sails.models.status.errorCheck.prevFetchedElapsed =
              queue[0].Elapsed;
            await sails.helpers.error.reset("frozen");
          }

          // If we are set to queue random tracks as an emergency fall-back, do so when the queue runs low.
          if (
            sails.models.status.errorCheck.playRandomTracks &&
            queueLength <= 20
          ) {
            await sails.helpers.songs.queue(
              undefined,
              "Bottom",
              3,
              "lenientRules"
            );
          }

          // If we are in automation_genre, check to see if the queue is less than 20 seconds. If so, the genre rotation may be out of tracks to play.
          // In that case, flip to automation_on with Default rotation.
          if (
            sails.models.meta.memory.state === "automation_genre" &&
            queueLength <= 20
          ) {
            await sails.helpers.error.count("genreEmpty");
          } else {
            await sails.helpers.error.reset("genreEmpty");
          }

          // Playlist maintenance
          var thePosition = -1;
          if (
            sails.models.meta.memory.state === "automation_playlist" ||
            sails.models.meta.memory.state === "automation_prerecord" ||
            sails.models.meta.memory.state.startsWith("prerecord_")
          ) {
            // Go through each track in the queue and see if it is a track from our playlist. If so, log the lowest number as the position in our playlist
            sails.log.debug(
              `Calling asyncForEach in cron checks for checking if any tracks in queue are a part of an active playlist`
            );
            var playingTrack = false;
            await sails.helpers.asyncForEach(queue, (autoTrack, index) => {
              // eslint-disable-next-line promise/param-names
              return new Promise((resolve2) => {
                try {
                  for (
                    var i = 0;
                    i < sails.models.playlists.active.tracks.length;
                    i++
                  ) {
                    var name = sails.models.playlists.active.tracks[i];
                    if (name === parseInt(autoTrack.ID)) {
                      // Waiting for the playlist to begin, and it has begun? Switch states.
                      if (
                        sails.models.meta.memory.state ===
                          "automation_prerecord" &&
                        index === 0 &&
                        !sails.models.playlists.queuing &&
                        sails.models.meta.memory.changingState === null &&
                        sails.models.status.errorCheck.trueZero <= 0
                      ) {
                        // State switching should be pushed in sockets
                        sails.helpers.meta.change
                          .with({ state: "prerecord_on" })
                          .then(() => {
                            sails.helpers.meta.newShow().then(() => {});
                          });
                      }

                      // Flip to prerecord_break if not currently playing a track from the prerecord playlist, and back to prerecord_on otherwise
                      if (index === 0) {
                        playingTrack = true;
                        if (
                          sails.models.meta.memory.state ===
                            "prerecord_break" &&
                          sails.models.status.errorCheck.trueZero <= 0
                        ) {
                          (async () => {
                            await sails.helpers.meta.change.with({
                              state: `prerecord_on`,
                            });
                          })();
                        }
                      } else if (
                        index > 0 &&
                        sails.models.meta.memory.state === "prerecord_on" &&
                        !playingTrack &&
                        sails.models.status.errorCheck.trueZero <= 0
                      ) {
                        (async () => {
                          await sails.helpers.meta.change.with({
                            state: `prerecord_break`,
                          });
                        })();
                      }
                      if (thePosition === -1 || i < thePosition) {
                        thePosition = i;
                      }
                      break;
                    }
                  }
                  return resolve2(false);
                } catch (e) {
                  sails.log.error(e);
                  return resolve2(false);
                }
              });
            });

            try {
              // Finished the playlist? Go back to automation.
              if (
                thePosition === -1 &&
                sails.models.status.errorCheck.trueZero <= 0 &&
                ((parseFloat(queue[0].Duration) > 0 &&
                  parseFloat(queue[0].Elapsed) <
                    parseFloat(queue[0].Duration) &&
                  parseFloat(queue[0].Elapsed) > 0) ||
                  (parseFloat(queue[0].Duration) <= 0 &&
                    parseFloat(queue[0].Elapsed) > 0)) &&
                !sails.models.playlists.queuing &&
                sails.models.meta.memory.changingState === null
              ) {
                await sails.helpers.meta.change.with({
                  changingState: `Ending playlist`,
                });
                switch (sails.models.meta.memory.state) {
                  case "automation_playlist":
                    await sails.models.logs
                      .create({
                        attendanceID: sails.models.meta.memory.attendanceID,
                        logtype: "sign-off",
                        loglevel: "primary",
                        logsubtype: sails.models.meta.memory.playlist,
                        logIcon: `fas fa-play`,
                        title: `Playlist finished airing; no more tracks to play.`,
                        event: ``,
                      })
                      .fetch()
                      .tolerate((err) => {
                        // Do not throw for errors, but log it.
                        sails.log.error(err);
                      });
                    break;
                  case "prerecord_on":
                  case "prerecord_break":
                    await sails.models.logs
                      .create({
                        attendanceID: sails.models.meta.memory.attendanceID,
                        logtype: "sign-off",
                        loglevel: "primary",
                        logsubtype: sails.models.meta.memory.show,
                        logIcon: `fas fa-play-circle`,
                        title: `Prerecord finished airing; no more tracks to play.`,
                        event: ``,
                      })
                      .fetch()
                      .tolerate((err) => {
                        // Do not throw for errors, but log it.
                        sails.log.error(err);
                      });
                    break;

                  // Uh oh! This is a bad prerecord playlist! Log this and notify the directors.
                  case "automation_prerecord":
                    var eventNow =
                      sails.models.calendar.calendardb.whatShouldBePlaying(
                        null,
                        false
                      );
                    eventNow = eventNow.find(
                      (event) =>
                        event.type === "prerecord" &&
                        sails.models.meta.memory.show ===
                          `${event.hosts} - ${event.name}` &&
                        [
                          "canceled",
                          "canceled-system",
                          "canceled-changed",
                        ].indexOf(event.scheduleType) === -1
                    );
                    if (eventNow) {
                      var record = await sails.models.attendance
                        .create({
                          calendarID: eventNow.calendarID,
                          unique: eventNow.unique,
                          dj: eventNow.hostDJ,
                          cohostDJ1: eventNow.cohostDJ1,
                          cohostDJ2: eventNow.cohostDJ2,
                          cohostDJ3: eventNow.cohostDJ3,
                          event: `prerecord: ${eventNow.hosts} - ${eventNow.name}`,
                          happened: 0,
                          happenedReason: "Bad Playlist",
                          scheduledStart: moment(eventNow.start).format(
                            "YYYY-MM-DD HH:mm:ss"
                          ),
                          scheduledEnd: moment(eventNow.end).format(
                            "YYYY-MM-DD HH:mm:ss"
                          ),
                          badPlaylist: true,
                        })
                        .fetch();
                      await sails.models.logs
                        .create({
                          attendanceID: record.ID,
                          logtype: "bad-playlist",
                          loglevel: "orange",
                          logsubtype: sails.models.meta.memory.show,
                          logIcon: `fas fa-play-circle`,
                          title: `A prerecord failed to air!`,
                          event: `Prerecord: ${sails.models.meta.memory.show}<br />This is probably because the tracks in the prerecord's radioDJ playlist are corrupt.`,
                        })
                        .fetch()
                        .tolerate((err) => {
                          // Do not throw for errors, but log them.
                          sails.log.error(err);
                        });
                      await sails.helpers.onesignal.sendMass(
                        "emergencies",
                        "Prerecord failed to air!",
                        `${eventNow.hosts} - ${
                          eventNow.name
                        } failed to air on ${moment(eventNow.start).format(
                          "llll"
                        )} - ${moment(eventNow.end).format(
                          "llll"
                        )}; most likely, the tracks in the playlist are corrupted.`
                      );
                      await sails.helpers.emails.queueDjs(
                        eventNow,
                        `Prerecord failed to air: ${eventNow.hosts} - ${eventNow.name}`,
                        `Dear ${eventNow.hosts},<br /><br />

                                    A scheduled prerecord, <strong>${eventNow.name}</strong>, tried to go on the air. However, it failed to broadcast, and the system immediately went back to automation. Please check to ensure the tracks uploaded to the system and/or in the RadioDJ Playlist are valid and not corrupt.<br /><br />
                          You can reply all to this email if you need assistance from the directors.`
                      );
                    }
                    break;
                }

                await sails.helpers.state.automation(false);

                // Did not finish the playlist? Ensure the position is updated in meta.
              } else if (thePosition !== -1) {
                if (thePosition !== sails.models.meta.memory.playlistPosition) {
                  change.playlistPosition = thePosition;
                }
              }
            } catch (e) {
              await sails.helpers.meta.change.with({ changingState: null });
              sails.log.error(e);
            }
          }

          // Check for, and queue, breaks when necessary
          await sails.helpers.break.checkClockwheel(true);
        }

        // Error checks
        await sails.helpers.error.reset("queueFail");
        await sails.helpers.error.count("stationID", true);
      } catch (e) {
        sails.models.status.errorCheck.queueWait = 0;
        if (sails.models.status.errorCheck.trueZero < 2)
          sails.models.status.errorCheck.trueZero = 2;
        change.queueCalculating = true;
        await sails.helpers.error.count("queueFail");
        sails.log.error(e);
      }
    } else {
      queue = sails.models.meta.automation;
      change.playing = sails.models.meta.memory.playing;
      sails.models.status.errorCheck.queueWait -= 1;
      if (sails.models.meta.memory.playing) {
        queueLength = sails.models.status.errorCheck.prevQueueLength - 1;
        trackLength = sails.models.status.errorCheck.prevTrackLength - 1;
        countDown = sails.models.status.errorCheck.prevCountdown - 1;
        sails.models.status.errorCheck.prevQueueLength = queueLength;
        sails.models.status.errorCheck.prevTrackLength = trackLength;
        sails.models.status.errorCheck.prevCountdown = countDown;
        sails.models.status.errorCheck.prevElapsed += 1;
      }
    }

    // If we do not know active playlist, we need to populate the info
    if (
      sails.models.meta.memory.playlistID !== null &&
      sails.models.playlists.active.tracks.length <= 0 &&
      (sails.models.meta.memory.state === "automation_playlist" ||
        sails.models.meta.memory.state.startsWith("prerecord_"))
    ) {
      try {
        theplaylist = sails.config.custom.radiodjs.length
          ? await sails.models.playlists
              .findOne({ ID: sails.models.meta.memory.playlistID })
              .tolerate(() => {})
          : undefined;
        if (typeof theplaylist !== "undefined") {
          // LINT: RadioDJ table
          // eslint-disable-next-line camelcase
          playlistTracks = await sails.models.playlists_list
            .find({ pID: sails.models.meta.memory.playlistID })
            .tolerate(() => {});
          sails.models.playlists.active.tracks = [];
          if (typeof playlistTracks !== "undefined") {
            playlistTracks.forEach((playlistTrack) => {
              sails.models.playlists.active.tracks.push(playlistTrack.sID);
            });
          }
        } else {
          await sails.helpers.meta.change.with({
            playlist: null,
            playlistID: null,
          });
        }
      } catch (e) {
        sails.log.error(e);
      }
    }

    // Clear manual metadata if it is old
    if (
      sails.models.meta.memory.trackStamp !== null &&
      moment().isAfter(
        moment(sails.models.meta.memory.trackStamp).add(
          sails.config.custom.meta.manualClear,
          "minutes"
        )
      ) &&
      !sails.models.meta.memory.state.startsWith("automation_") &&
      !sails.models.meta.memory.state.startsWith("prerecord_")
    ) {
      change.trackStamp = null;
      change.trackArtist = null;
      change.trackTitle = null;
      change.trackAlbum = null;
      change.trackLabel = null;
    }

    try {
      var attendance;
      if (queue.length > 0) {
        // If we are preparing for live, so some stuff if queue is done
        if (
          sails.models.meta.memory.state === "automation_live" &&
          queueLength <= 0 &&
          sails.models.status.errorCheck.trueZero <= 0
        ) {
          await sails.helpers.meta.change.with({ state: "live_on" });
          await sails.helpers.rest.cmd("EnableAssisted", 1);
          await sails.helpers.meta.newShow();
        }
        // If we are preparing for sports, do some stuff if queue is done
        if (
          sails.models.meta.memory.state === "automation_sports" &&
          queueLength <= 0 &&
          sails.models.status.errorCheck.trueZero <= 0
        ) {
          await sails.helpers.meta.change.with({ state: "sports_on" });
          await sails.helpers.rest.cmd("EnableAssisted", 1);
          await sails.helpers.meta.newShow();
        }
        // If we are preparing for remote, do some stuff
        if (
          sails.models.meta.memory.state === "automation_remote" &&
          queueLength <= 0 &&
          sails.models.status.errorCheck.trueZero <= 0
        ) {
          await sails.helpers.meta.change.with({ state: "remote_on" });
          await sails.helpers.meta.newShow();
        }
        // If we are preparing for sportsremote, do some stuff if we are playing the stream track
        if (
          sails.models.meta.memory.state === "automation_sportsremote" &&
          queueLength <= 0 &&
          sails.models.status.errorCheck.trueZero <= 0
        ) {
          await sails.helpers.meta.change.with({ state: "sportsremote_on" });
          await sails.helpers.meta.newShow();
        }
        // If returning from break, do stuff once queue is empty
        if (
          sails.models.meta.memory.state.includes("_returning") &&
          queueLength <= 0 &&
          sails.models.status.errorCheck.trueZero <= 0
        ) {
          switch (sails.models.meta.memory.state) {
            case "live_returning":
              await sails.helpers.meta.change.with({ state: "live_on" });
              await sails.helpers.rest.cmd("EnableAssisted", 1);
              break;
            case "remote_returning":
              await sails.helpers.meta.change.with({ state: "remote_on" });
              break;
            case "sports_returning":
              await sails.helpers.meta.change.with({ state: "sports_on" });
              await sails.helpers.rest.cmd("EnableAssisted", 1);
              break;
            case "sportsremote_returning":
              await sails.helpers.meta.change.with({
                state: "sportsremote_on",
              });
              break;
          }
        }

        // If we are in break, queue something if the queue is under 2 items to keep the break going, and if we are not changing states
        if (
          sails.models.meta.memory.changingState === null &&
          queue.length < 2
        ) {
          switch (sails.models.meta.memory.state) {
            case "automation_break":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "automation" && record.subtype === "during"
                ),
                "Automation During"
              );
              await sails.helpers.break.checkClockwheel(false);
              break;
            case "live_break":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "live" && record.subtype === "during"
                ),
                "Live During"
              );
              await sails.helpers.break.checkClockwheel(false);
              break;
            case "remote_break":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "remote" && record.subtype === "during"
                ),
                "Remote During"
              );
              await sails.helpers.break.checkClockwheel(false);
              break;
            case "sports_break":
            case "sportsremote_break":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "sports" && record.subtype === "during"
                ),
                "Sports During"
              );
              await sails.helpers.break.checkClockwheel(false);
              break;
            case "sports_halftime":
            case "sportsremote_halftime":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "sports" &&
                    record.subtype === "duringHalftime"
                ),
                "Sports DuringHalftime"
              );
              await sails.helpers.break.checkClockwheel(false);
              break;
          }
        }
      }

      // Process queueFinish
      if (change.playing) {
        change.queueFinish =
          typeof queueLength === "number"
            ? moment().add(queueLength, "seconds").toISOString(true)
            : moment().toISOString(true);
      } else {
        change.queueFinish = null;
      }

      // Process countdown
      if (countDown > 0) {
        change.countdown =
          typeof countDown === "number"
            ? moment().add(countDown, "seconds").toISOString(true)
            : moment().toISOString(true);
      } else {
        change.countdown = null;
      }

      // Change applicable meta
      await sails.helpers.meta.change.with(change);

      // Check alt RadioDJ if applicable
      if (sails.models.meta.memory.altRadioDJ) {
        let queue2 = await sails.helpers.rest.getQueue(
          sails.config.custom.radiodjs.find(
            (instance) => instance.name === sails.models.meta.memory.altRadioDJ
          )
        );
        if (!queue2[0]) {
          await sails.helpers.error.count("altDone");
        } else {
          if (
            queue2.length > 0 &&
            queue2[0].Duration ===
              sails.models.status.errorCheck.prevAltDuration &&
            queue2[0].Elapsed === sails.models.status.errorCheck.prevAltElapsed
          ) {
            await sails.helpers.error.count("altDone");
          } else {
            await sails.helpers.error.reset("altDone");
            sails.models.status.errorCheck.prevAltDuration = queue2[0].Duration;
            sails.models.status.errorCheck.prevAltElapsed = queue2[0].Elapsed;
          }
        }
      }

      // All done
      return;
    } catch (e) {
      await sails.helpers.error.count("frozen");
      sails.log.error(e);
    }
  },
};
