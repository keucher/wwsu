module.exports = {
  friendlyName: "helpers.bootstrap.cron.clockOutDirectors",

  description:
    "Clock out directors that are still clocked in (and flag their timesheet).",

  schedule: "50 59 23 * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if shutting down or in maintenance mode.
    if (sails.config.custom.basic.maintenance) return "SKIPPED; maintenance mode is active.";

    // Sync meta time with server time
    await sails.helpers.meta.change.with({
      time: moment().toISOString(true),
    });

    let records = await sails.models.timesheet
      .find({ timeIn: { "!=": null }, timeOut: null })
      .sort("scheduledIn ASC");

    let recordsX = [];

    if (records.length > 0) {
      let theStart;
      let theEnd;

      // Sequential async to fill in an edge case
      records.reduce(async (prevReturn, record) => {
        return await (async (recordB) => {
          if (!theEnd) {
            theStart = recordB.scheduledIn;
          } else {
            theStart = theEnd;
          }
          theEnd = recordB.scheduledOut;
          recordsX = recordsX.concat(
            await sails.models.timesheet
              .update(
                { ID: recordB.ID },
                {
                  timeIn: moment(theStart).format("YYYY-MM-DD HH:mm:ss"),
                  timeOut: moment(theEnd).format("YYYY-MM-DD HH:mm:ss"),
                  approved: 0,
                }
              )
              .fetch()
          );
        })(record);
      }, null);

      // Add special clock-out entry
      recordsX = recordsX.concat(
        await sails.models.timesheet
          .update(
            { timeIn: { "!=": null }, timeOut: null },
            {
              timeIn: moment(theEnd).format("YYYY-MM-DD HH:mm:ss"),
              timeOut: moment().format("YYYY-MM-DD HH:mm:ss"),
              approved: 0,
            }
          )
          .fetch()
      );
    } else {
      // Add normal clock-out entry
      recordsX = recordsX.concat(
        await sails.models.timesheet
          .update(
            { timeIn: { "!=": null }, timeOut: null },
            { timeOut: moment().format("YYYY-MM-DD HH:mm:ss"), approved: 0 }
          )
          .fetch()
      );
    }

    if (recordsX.length > 0) {
      recordsX.map((record) => {
        (async () => {
          await sails.helpers.onesignal.sendMass(
            "accountability-directors",
            "Timesheet needs approved in DJ Controls",
            `${record.name}'s timesheet, ending on ${moment().format(
              "LLLL"
            )}, has been flagged and needs reviewed/approved because the director forgot to clock out at midnight.`
          );
        })();
      });
    }

    // Force reload all directors based on timesheets
    await sails.helpers.directors.update();
  },
};
