module.exports = {
  friendlyName: "helpers.bootstrap.cron.fetchEasAlerts",

  description:
    "Fetch active alerts from the National Weather Service and from Campus Alerts.",

  schedule: "6 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if shutting down or in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "EAS-NWS",
        label: "EAS NWS CAPS",
        summary: `Disabled; system in maintenance mode.`,
        data: `National Weather Service alert fetching is disabled; maintenance mode is active.`,
        status: 4,
      });
      await sails.helpers.status.modify.with({
        name: "EAS-WSU",
        label: "EAS Wright State Alert",
        summary: `Disabled; system in maintenance mode.`,
        data: `Wright State Alert fetching is disabled; maintenance mode is active.`,
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    // Initial housekeeping
    await sails.helpers.eas.preParse();

    let complete = 0;
    let bad = [];

    let asyncLoop = async function (array, callback) {
      for (let index = 0; index < array.length; index++) {
        // LINT: This is a loop; we do not want to return the callback.
        // eslint-disable-next-line callback-return
        await callback(array[index], index, array);
      }
    };

    // Iterate through every configured county and get their weather alerts
    if (sails.config.custom.nws && sails.config.custom.nws.length > 0) {
      await asyncLoop(sails.config.custom.nws, async (county) => {
        try {
          let resp = await needle(
            "get",
            `https://alerts.weather.gov/cap/wwaatmget.php?x=${
              county.code
            }&y=0&t=${moment().valueOf()}`,
            {},
            { headers: { "Content-Type": "application/json" } }
          );
          await sails.helpers.eas.parseCaps(county.name, resp.body);
          complete++;
        } catch (err) {
          bad.push(county.name);
          // Do not reject on error; just go to the next county
          sails.log.error(err);
        }
      });

      // If all counties succeeded, mark EAS-NWS as operational
      if (complete >= sails.config.custom.nws.length) {
        await sails.helpers.status.modify.with({
          name: "EAS-NWS",
          label: "EAS NWS CAPS",
          summary: `Online.`,
          data: `All NWS CAPS (${sails.config.custom.nws
            .map((cap) => cap.name)
            .join()}) are online.`,
          status: 5,
        });
      } else {
        await sails.helpers.status.modify.with({
          name: "EAS-NWS",
          label: "EAS NWS CAPS",
          summary: `Error fetching the CAPS feed for ${bad.join(", ")}. Alerts for those locations are not being received.`,
          data: `Could not fetch the following NWS CAPS locations: ${bad.join(
            ", "
          )}. This is usually caused by a network issue, or the NWS CAPS server is experiencing a temporary service disruption.`,
          status: 3,
        });
      }
    } else {
      await sails.helpers.status.modify.with({
        name: "EAS-NWS",
        label: "EAS NWS CAPS",
        summary: `Disabled; no locations configured.`,
        data: `There are no counties / zones defined in configuration; National Weather Service EAS checking is disabled.
        <br /><strong>TO FIX:</strong>
        <ol>
          <li>Go to https://alerts.weather.gov/ .</li>
          <li>Click "County List" or "Zone List" next to your state.</li>
          <li>Make note of the County / Zone code.</li>
          <li>Repeat steps 1 - 3 for all areas WWSU covers for Emergency Alerts.</li>
          <li>In administration DJ Controls, go to System Settings -> National Weather Service.</li>
          <li>Insert each county / zone into the configuration.</li>
        </ol>`,
        status: 4,
      });
    }

    // Now, query for Campus alerts
    if (sails.config.custom.basic.campusAlerts) {
      try {
        let resp = await needle(
          "get",
          `${sails.config.custom.basic.campusAlerts}?t=${moment().valueOf()}`,
          {},
          { headers: { "Content-Type": "application/json" } }
        );
        if (!resp || !resp.body || !resp.body.success) {
          throw new Error("Wright State Alert fetch was not successful.");
        }

        // Alerts in effect; parse them.
        if (resp.body.alerts && resp.body.alerts.length > 0) {
          let maps = resp.body.alerts.map(async (alert) => {
            await sails.helpers.eas.addAlert(
              alert.updated || alert.date || alert.title || alert.description,
              "Wright State Alert",
              "Wright State University - Dayton Campus",
              alert.title || "Unknown Campus Alert",
              "Severe",
              moment(alert.updated || alert.date || undefined).format(
                "YYYY-MM-DD HH:mm:ss"
              ),
              null,
              "#046A38",
              alert.description.join(" | "),
              true
            );
          });
          await Promise.all(maps);
        }

        // At this point, mark WSU EAS as good.
        await sails.helpers.status.modify.with({
          name: "EAS-WSU",
          label: "EAS Wright State Alert",
          summary: `Online.`,
          data: `Wright State Alert feed is operational.`,
          status: 5,
        });
      } catch (err) {
        await sails.helpers.status.modify.with({
          name: "EAS-WSU",
          label: "EAS Wright State Alert",
          summary: `Error fetching campus alerts. System will not air / trigger campus alerts until the error is resolved.`,
          data: `Could not fetch active Campus Alerts. This is usually caused by a network issue, or the Wright State Alert feed is experiencing a temporary service disruption. If this problem persists, please contact those in charge of the Campus Alerts System.`,
          status: 3,
        });
      }
    } else {
      await sails.helpers.status.modify.with({
        name: "EAS-WSU",
        label: "EAS Wright State Alert",
        summary: `Disabled; campus alert feed not configured.`,
        data: `Wright State Alerts feed is not configured; you are not receiving campus alerts. <br /><strong>TO FIX:</strong> Set the URL to the campus alerts feed in admin DJ Controls -> System Settings -> Wright State Alerts.`,
        status: 4,
      });
    }

    // Finish up
    await sails.helpers.eas.postParse();
  },
};
