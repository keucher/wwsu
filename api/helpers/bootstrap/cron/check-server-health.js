const os = require("os");

module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkServerHealth",

  description:
    "Check for server CPU and memory use and update status accordingly. Also determine if http requests should be throttled because of poor server performance.",

  schedule: "*/15 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Server health
    let load = os.loadavg();
    let mem = os.freemem();

    if (
      load[0] >= sails.config.custom.status.server1MinLoad1 ||
      load[1] >= sails.config.custom.status.server5MinLoad1 ||
      load[2] >= sails.config.custom.status.server15MinLoad1 ||
      mem <= sails.config.custom.status.serverMemory1
    ) {
      // Critical status? Reject requests.
      sails.models.status.errorCheck.throttleAPI = 2;

      await sails.helpers.status.modify.with({
        name: `server`,
        label: `Server`,
        status: 1,
        summary: `CPU or memory use dangerously high! System likely unstable. Rejecting all web / API requests.`,
        data: `Server resource use is dangerously high!!! <strong>Web and API requests are being rejected.</strong> Speed / performance is likely degraded, and server may be unstable. CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}
        <br /><strong>TO FIX:</strong>
        <ul>
          <li>Remotely connect to the server and check CPU and memory use on running processes. This might not always be abnormal (eg. system updates) unless it lasts a long while or significantly affects server performance.</li>
          <li>If this is abnormal, kill the offending processes or reboot the server. Consider running a system update or investigating the offending processes / programs.</li>
        </ul>`,
      });
    } else if (
      load[0] >= sails.config.custom.status.server1MinLoad2 ||
      load[1] >= sails.config.custom.status.server5MinLoad2 ||
      load[2] >= sails.config.custom.status.server15MinLoad2 ||
      mem <= sails.config.custom.status.serverMemory2
    ) {
      // Very high use? Throttle requests.
      sails.models.status.errorCheck.throttleAPI = 1;

      await sails.helpers.status.modify.with({
        name: `server`,
        label: `Server`,
        status: 2,
        summary: `CPU or memory use very high! System possibly slow or unstable. All web / API requests are being throttled.`,
        data: `Server resource use is very high! <strong>Web and API requests are being throttled.</strong> Speed / performance may be degraded at this time. CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}
        <br /><strong>TO FIX:</strong>
        <ul>
          <li>This is usually not abnormal unless CPU or memory use remains high for a long time or becomes critically high.</li>
          <li>Remotely connect to the server and check CPU and memory use on running processes.</li>
          <li>If this is abnormal, kill the offending processes or reboot the server. Consider running a system update or investigating the offending processes / programs.</li>
        </ul>`,
      });
    } else if (
      load[0] >= sails.config.custom.status.server1MinLoad3 ||
      load[1] >= sails.config.custom.status.server5MinLoad3 ||
      load[2] >= sails.config.custom.status.server15MinLoad3 ||
      mem <= sails.config.custom.status.serverMemory3
    ) {
      // Do not throttle / reject requests yet for a minor resource use issue.
      sails.models.status.errorCheck.throttleAPI = 0;

      await sails.helpers.status.modify.with({
        name: `server`,
        label: `Server`,
        status: 3,
        summary: `CPU or memory use high.`,
        data: `Server resource use is mildly high. This is usually nothing to worry about, but monitor to make sure it does not get higher. CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}`,
      });
    } else {
      sails.models.status.errorCheck.throttleAPI = 0;

      await sails.helpers.status.modify.with({
        name: `server`,
        label: `Server`,
        status: 5,
        summary: `Operational.`,
        data: `Server resource use is good. CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}`,
      });
    }
  },
};
