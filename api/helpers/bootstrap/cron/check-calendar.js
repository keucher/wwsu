module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkCalendar",

  description:
    "Check for, validate, and execute scheduled calendar events when applicable",

  schedule: "2 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "calendar",
        label: "Calendar",
        summary: `Integrity checks disabled; system in maintenance mode.`,
        data: `Calendar checks are disabled; maintenance mode is active. Status will take up to one minute to clear after maintenance mode is disabled.`,
        status: 4,
      });
      await sails.helpers.status.modify.with({
        name: "calendar-triggerer",
        status: 2,
        summary: `Genres / Playlists / Prerecords not automatically starting; system is in maintenance mode. These programs must be started manually.`,
        data: `The calendar is not triggering automatic programming (Genres / Playlists / Prerecords) as the system is in maintenance mode. Once out of maintenance mode, this status might not clear until a genre/playlist/prerecord starts.`,
      });
      return "SKIPPED; maintenance mode active.";
    }

    // Check integrity on every 5th minute; this is a CPU intensive process and therefore why we only check these every 5 minutes.
    if (moment().minute() % 5 === 0) {
      await sails.helpers.calendar.check(false, true);
      return "Ran 5-minute event integrity checks.";
    } else {
      await sails.helpers.calendar.check();
      return "Ran standard schedule check.";
    }
  },
};
