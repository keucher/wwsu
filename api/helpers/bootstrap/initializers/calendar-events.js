module.exports = {
  friendlyName: "helpers.bootstrap.initializers.calendarEvents",

  description: "Check and add default calendar events",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Add test event if it does not exist
    await sails.models.calendar.findOrCreate(
      { ID: 1 },
      {
        type: "show",
        active: true,
        priority: sails.models.calendar.calendardb.getDefaultPriority({
          type: "show",
        }),
        name: "Test Event",
        description: "This is not an actual show; this is only used to test the radio system.",
        hostDJ: 1,
      }
    );

    // Initialize studio booking events if they do not exist
    await sails.models.calendar.findOrCreate(
      { type: "onair-booking" },
      {
        type: "onair-booking",
        active: true,
        priority: sails.models.calendar.calendardb.getDefaultPriority({
          type: "onair-booking",
        }),
        name: "OnAir Studio Bookings",
      }
    );
    await sails.models.calendar.findOrCreate(
      { type: "prod-booking" },
      {
        type: "prod-booking",
        active: true,
        priority: sails.models.calendar.calendardb.getDefaultPriority({
          type: "prod-booking",
        }),
        name: "Production Studio Bookings",
      }
    );

    // Add directors in the calendar
    var records = await sails.models.directors.find();
    var IDs = [];

    records.map((director) => {
      IDs.push(director.ID);

      (async (_director) => {
        sails.models.calendar
          .findOrCreate(
            { type: "office-hours", director: _director.ID },
            {
              type: "office-hours",
              active: true,
              priority: sails.models.calendar.calendardb.getDefaultPriority({
                type: "office-hours",
              }),
              hosts: _director.name,
              name: _director.name,
              director: _director.ID,
              lastAired: moment().format("YYYY-MM-DD HH:mm:ss"),
            }
          )
          .exec(async (err, record, wasCreated) => {
            if (err) {
              throw err;
            }
            if (!wasCreated && record)
              await sails.models.calendar
                .update(
                  { ID: record.ID },
                  {
                    active: true,
                    priority:
                      sails.models.calendar.calendardb.getDefaultPriority({
                        type: "office-hours",
                      }),
                  }
                )
                .fetch();
          });
      })(director);
    });

    // De-activate directors that do not exist anymore.
    await sails.models.calendar
      .update(
        { type: "office-hours", director: { nin: IDs } },
        { active: false }
      )
      .fetch();

    // Refresh directors (re-calculate presence information)
    await sails.helpers.directors.update();
  },
};
