const bcrypt = require("bcrypt");
const sh = require("shorthash");

module.exports = {
  friendlyName: "Master dj director",

  description: "",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Add org member ID 1 in the system if it does not exist
    let director = await sails.models.directors.findOne({ ID: 1 });
    let tempPass = sh.unique(`${moment().valueOf()}`);
    await sails.models.djs.findOrCreate(
      { ID: 1 },
      {
        ID: 1,
        name: `Primary Member ${tempPass}`,
        realName: director ? director.name : `Primary Director ${tempPass}`,
        login: bcrypt.hashSync(tempPass, 10), // CAREFUL! This should be changed immediately.
      }
    );

    // Add director ID 1 in the system if it does not exist
    let member = await sails.models.djs.findOne({ ID: 1 });
    await sails.models.directors.findOrCreate(
      { ID: 1 },
      {
        ID: 1,
        name: member ? member.realName : `Primary Director ${tempPass}`,
        login: bcrypt.hashSync(tempPass, 10), // CAREFUL! This should be changed immediately.
        admin: true,
        canSendEmails: true,
      }
    );
  },
};
