module.exports = {
  friendlyName: "helpers.bootstrap.initializers.calendarIntegrity",

  description: "Check calendar event integrity",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Check calendar events and integrity
    await sails.helpers.meta.change.with({
      changingState: `Initializing program calendar`,
    });
    await sails.helpers.calendar.check(true, true);
    await sails.helpers.meta.change.with({
      changingState: null,
    });
  },
};
