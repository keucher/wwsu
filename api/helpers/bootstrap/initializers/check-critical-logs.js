module.exports = {
  friendlyName: "helpers.bootstrap.initializers.checkCriticalLogs",

  description:
    "Check the number of critical logs generated. If 10 unacknowledged danger logs were generated in the last hour, activate maintenance mode.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    let criticalLogs = await sails.models.logs.count({
      loglevel: "danger",
      acknowledged: false,
      createdAt: { ">=": moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss") },
    });

    // If 10 critical logs in 1 hour, activate maintenance mode.
    if (criticalLogs >= 10 && !sails.config.custom.basic.maintenance) {
      sails.config.custom.basic.maintenance = true;

      // If we do not have any logs about maintenance mode being activated in the last hour, log it.
      if (
        (await sails.models.logs.count({
          logtype: "system-lockout",
          acknowledged: false,
          createdAt: { ">=": moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss") },
        })) === 0
      ) {
        await sails.models.logs
          .create({
            attendanceID: sails.models.meta.memory.attendanceID,
            logtype: "system-lockout",
            loglevel: "danger",
            logsubtype: "",
            logIcon: `fas fa-exclamation-triangle`,
            title: `The system was sent into maintenance mode due to excessive problems.`,
            event: `There have been at least 10 critical issues in the last hour. The system was put into maintenance mode. Please resolve the issues in DJ Controls -> To-Do (in the Administration menu), mark them off, and then deactivate maintenance mode.`,
          })
          .fetch()
          .tolerate((err) => {
            sails.log.error(err);
          });

        await sails.helpers.emails.queueEmergencies(
          `[URGENT] WWSU system locked out in maintenance mode`,
          `Directors,<br /><br />
  Several critical issues occurred in the WWSU system in the last hour. As a precaution, <strong>WWSU was sent into maintenance mode.</strong>. This means error / failsafe checks are no longer running, and you could be off the air!<br /><br />
  Please review the logs in DJ Controls -> To-Do (Administration menu) immediately. Fix the issues. Mark the logs off as complete. And then de-activate maintenance mode.`,
          true
        );
      }
    }
  },
};
