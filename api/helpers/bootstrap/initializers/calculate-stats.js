module.exports = {
  friendlyName: "helpers.bootstrap.initializers.calculateStats",

  description:
    "Calculate weekly analytics (in the background) when server is booted.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not await; we want this done in the background.
    sails.helpers.analytics.calculateStats().exec(() => {});
  },
};
