module.exports = {
  friendlyName: "state.automation",

  description: "Request to go into automation mode.",

  inputs: {
    transition: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, system will go into break mode instead of automation to allow for quick transitioning between radio shows.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper state.automation called.");

    // Used for loops to ensure our queue always is in the right order despite doing things async.
    let queueDone = false;

    const executeAutomation = async () => {
      try {
        // Log the request
        await sails.models.logs
          .create({
            attendanceID: sails.models.meta.memory.attendanceID,
            logtype: "sign-off",
            loglevel: "primary",
            logsubtype: sails.models.meta.memory.show,
            logIcon: `fas fa-stop`,
            title: `Broadcast ended ${
              inputs.transition
                ? ` and went into break.`
                : ` and went into automation.`
            }`,
            event: "",
          })
          .fetch()
          .tolerate((err) => {
            // Do not throw for error, but log it
            sails.log.error(err);
          });

        // Close off the current attendance record in the background because we want to queue RadioDJ stuff ASAP.
        sails.helpers.attendance
          .createRecord(inputs.transition ? null : undefined)
          .then(async (resp) => {
            try {
              sails.log.silly("state.automation: Attendance createRecord finished.");

              // We are going to automation
              if (!inputs.transition || !sails.config.custom.radiodjs.length) {
                sails.log.silly("state.automation: Going to automation.");
                await sails.helpers.meta.change.with({
                  host: null,
                  hostDJ: null,
                  cohostDJ1: null,
                  cohostDJ2: null,
                  cohostDJ3: null,
                  genre: "",
                  state: "automation_on",
                  show: "",
                  track: "",
                  topic: "",
                  webchat: true,
                  playlist: null,
                  playlistPosition: -1,
                  playlistPlayed: moment("2002-01-01").format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  hostCalling: null,
                  hostCalled: null,
                });

                sails.log.silly("state.automation: Went to automation. Waiting for show closers and breaks to queue...");

                // Wait for show closers and end break to be queued before proceeding.
                await waitForQueue();

                sails.log.silly("state.automation: Show closers and end breaks queued. Continuing by checking breaks.");

                // Queue clockwheel break
                await sails.helpers.break.checkClockwheel(false);

                // Re-check and trigger any programs that should begin.
                try {
                  sails.log.silly("state.automation: Checking clockwheel.");
                  await sails.helpers.calendar.check(true);
                } catch (e2) {
                  // Couldn't load calendar? Fall back to Default automation
                  sails.log.error(e2);
                  await sails.helpers.genre.start(null, true);
                }

                sails.log.silly("state.automation: Finished going to automation.");

                // Enable Auto DJ
                await sails.helpers.rest.cmd("EnableAutoDJ", 1);

                // We are going to automation_break
              } else {
                sails.log.silly("state.automation: Going to break.");
                await sails.helpers.meta.change.with({
                  host: null,
                  hostDJ: null,
                  cohostDJ1: null,
                  cohostDJ2: null,
                  cohostDJ3: null,
                  genre: "",
                  state: "automation_break",
                  show: "",
                  showLogo: null,
                  track: "",
                  topic: "",
                  webchat: true,
                  playlist: null,
                  playlistPosition: -1,
                  playlistPlayed: moment("2002-01-01").format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                  hostCalling: null,
                  hostCalled: null,
                });

                sails.log.silly("state.automation: Waiting for show closers and end break to finish queuing...");

                // Wait for show closers and end break to be queued before proceeding.
                await waitForQueue();

                sails.log.silly("state.automation: Show closers and end break queued. Continuing with break clockwheel check.");

                // Queue clockwheel break
                await sails.helpers.break.checkClockwheel(false);
              }

              await sails.helpers.rest.cmd("EnableAssisted", 0);

              await sails.helpers.meta.change.with({ changingState: null });

              sails.log.silly("state.automation: Finished going to break.");
            } catch (e) {
              queueDone = true;
              await sails.helpers.meta.change.with({ changingState: null });
              sails.log.error(e);
            }
          });

        if (sails.config.custom.radiodjs.length) {
          sails.log.silly("state.automation: Determining what needs to be queued in RadioDJ.");

          // Prepare RadioDJ
          await sails.helpers.rest.cmd("EnableAssisted", 1);

          // If coming out of a sports broadcast, queue a closer if exists.
          if (sails.models.meta.memory.state.startsWith("sports")) {
            if (
              typeof sails.config.custom.sportscats[
                sails.models.meta.memory.show
              ] !== "undefined"
            ) {
              sails.log.silly("state.automation: Queuing Sports Closer.");
              await sails.helpers.songs.queue(
                [
                  sails.config.custom.sportscats[sails.models.meta.memory.show][
                    "Sports Closers"
                  ],
                ],
                "Bottom",
                1
              );
            }
          } else {
            // If coming out of a show, queue a closer if exists.
            if (
              (sails.models.meta.memory.state.startsWith("live_") ||
                sails.models.meta.memory.state.startsWith("remote_")) &&
              typeof sails.config.custom.showcats[
                sails.models.meta.memory.show.split(" - ")[1]
              ] !== "undefined"
            ) {
              sails.log.silly("state.automation: Queuing Show Closer.");
              await sails.helpers.songs.queue(
                [
                  sails.config.custom.showcats[
                    sails.models.meta.memory.show.split(" - ")[1]
                  ]["Show Closers"],
                ],
                "Bottom",
                1
              );
            }
          }

          // Queue ending stuff
          if (sails.models.meta.memory.state.startsWith("live_")) {
            sails.log.silly("state.automation: Queuing End Break for live.");
            await sails.helpers.break.executeArray(
              sails.config.custom.breaks.filter(
                (record) => record.type === "live" && record.subtype === "end"
              ),
              "Live End"
            );
          }

          if (sails.models.meta.memory.state.startsWith("remote_")) {
            sails.log.silly("state.automation: Queuing End Break for remote.");
            await sails.helpers.break.executeArray(
              sails.config.custom.breaks.filter(
                (record) => record.type === "remote" && record.subtype === "end"
              ),
              "Remote End"
            );
          }

          if (
            sails.models.meta.memory.state.startsWith("sports_") ||
            sails.models.meta.memory.state.startsWith("sportsremote_")
          ) {
            sails.log.silly("state.automation: Queuing End Break for sports.");
            await sails.helpers.break.executeArray(
              sails.config.custom.breaks.filter(
                (record) => record.type === "sports" && record.subtype === "end"
              ),
              "Sports End"
            );
          }
        }

        sails.log.silly("state.automation: Queue is finished.");

        queueDone = true;
      } catch (e) {
        queueDone = true;
        await sails.helpers.meta.change.with({ changingState: null });
        throw e;
      }
    };

    const waitForQueue = () => {
      return new Promise((resolve, reject) => {
        if (queueDone) {
          return resolve();
        }
  
        let counts = 0;
        let counter = setInterval(() => {
          counts++;

          if (queueDone) {
            return resolve();
          }

          if (counts >= 10) {
            sails.log.error(new Error("state.automation: Timed out after 10 seconds waiting for RadioDJ to queue show closers and end breaks."));
            resolve();
            clearInterval(counter);
          }
        }, 1000);
      });
    }

    // Because remote broadcasts are intentionally on a 1-second delay, delay actually going to automation by 1 second.
    if (
      sails.models.meta.memory.state.startsWith("remote_") ||
      sails.models.meta.memory.state.startsWith("sportsremote_")
    ) {
      setTimeout(async () => executeAutomation(), 1000);
    } else {
      executeAutomation();
    }
  },
};
