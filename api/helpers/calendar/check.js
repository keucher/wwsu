let fs = require("fs");
let readline = require("readline");
let breakdance = require("breakdance");

module.exports = {
  friendlyName: "calendar.check",

  description:
    "Check for absent shows/directors, trigger programs that should start, and optionally check integrity of events in the next 14 days.",

  inputs: {
    ignoreChangingState: {
      type: "boolean",
      defaultsTo: false,
      description:
        "When triggering new radio programming, if set to true, ignore checks for whether or not we are already changing states. Defaults to false.",
    },

    checkIntegrity: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, we will check the integrity of events that trigger something (playlists, genres, prerecords) for the next 14 days. Will also update calendar cache and check for / log shows that did not air.",
    },
  },

  fn: async function (inputs) {
    if (!sails.config.custom.radiodjs.length) {
      await sails.helpers.status.modify.with({
        name: "calendar-triggerer",
        status: 4,
        summary: `No Genres / Playlists / Prerecords will automatically trigger; no RadioDJ instances have been configured.`,
        data: `Since no RadioDJs are configured, the system is not automatically triggering calendar programming. You are responsible for configuring your automation system, if not using RadioDJ, to trigger automatic programming at their respective times. It should also be informing the system of now-playing metadata accordingly as indicated in the instructions under administration DJ Controls -> System Settings -> RadioDJs.
        <br /><strong>TO FIX:</strong> Go to administration DJ Controls -> System Settings -> RadioDJs. This status might not clear until a genre/playlist/prerecord starts.`,
      });
    }

    // Do not trigger programming if not using RadioDJ, maintenance mode active, no healthy RadioDJs, or meta state is unknown (but allow if blank because that means we tried to check)
    if (
      sails.config.custom.radiodjs.length &&
      !sails.config.custom.basic.maintenance &&
      !sails.models.status.errorCheck.waitForGoodRadioDJ &&
      sails.models.meta.memory.state !== "unknown"
    ) {
      await sails.log.verbose(`CALENDAR CHECK: Checking for programs to start`);

      // Check if it's time to trigger a program, and trigger it if so. Do so sync because we need this info ASAP
      let _eventNow = sails.models.calendar.calendardb.whatShouldBePlaying(
        undefined,
        true
      );

      await sails.log.verbose(
        `CALENDAR CHECK: whatShouldBePlaying returned: ${_eventNow.length}`
      );

      let triggered = false;
      let defaultError = false;

      for (let eventNow in _eventNow) {
        eventNow = _eventNow[eventNow];
        if (
          (eventNow.type === "prerecord" || eventNow.type === "playlist") &&
          eventNow.playlistID !== null
        ) {
          try {
            triggered = await sails.helpers.playlists.start(
              eventNow,
              inputs.ignoreChangingState
            );
          } catch (e) {
            sails.log.error(e);
            triggered = false;
          }
        }
        if (eventNow.type === "genre" && eventNow.eventID !== null) {
          try {
            triggered = await sails.helpers.genre.start(
              eventNow,
              inputs.ignoreChangingState
            );
          } catch (e) {
            sails.log.error(e);
            triggered = false;
          }
        }
        if (triggered) break;
      }

      if (!triggered) {
        sails.log.verbose(
          `CALENDAR CHECK: whatShouldBePlaying: no scheduled automation. Use Default rotation.`
        );
        try {
          triggered = await sails.helpers.genre.start(
            null,
            inputs.ignoreChangingState
          );
        } catch (e) {
          sails.log.error(e);
          triggered = false;
          defaultError = true;
        }
      }

      // If we tried and failed to queue Default rotation, this is a problem! If we are supposed to be playing something, fall back to random music as emergency.
      if (
        defaultError &&
        ["automation_on", "automation_genre", "unknown", ""].indexOf(
          sails.models.meta.memory.state
        ) !== -1 &&
        !sails.config.custom.basic.maintenance &&
        !sails.models.status.errorCheck.waitForGoodRadioDJ
      ) {
        sails.log.verbose(
          `CALENDAR CHECK: whatShouldBePlaying: Could not queue anything! Switching to emergency random tracks.`
        );
        sails.models.status.errorCheck.playRandomTracks = true;
        await sails.helpers.status.modify.with({
          name: "calendar-triggerer",
          status: 2,
          summary: `All scheduled programming for this time failed to run. Default rotation also failed to run.`,
          data: `Unable to trigger any automated programming! All scheduled genres/playlists/prerecords failed, and the default rotation also failed. The system is currently falling back to queuing random tracks.
          <br /><strong>TO FIX:</strong> Please make sure all scheduled genres/playlists/prerecords are set up correctly, have the correct RadioDJ event or playlist set, and have tracks that can be queued. Also make sure a manual event named "Default" exists in RadioDJ and triggers a rotation that queues default music when nothing else is scheduled.`,
        });
        if (sails.models.meta.memory.state !== "automation_on") {
          await sails.helpers.meta.change.with({
            state: "automation_on",
            genre: "Default",
            playlistPlayed: moment().format("YYYY-MM-DD HH:mm:ss"),
          });
          await sails.helpers.meta.newShow();
          await sails.models.logs
            .create({
              attendanceID: sails.models.meta.memory.attendanceID,
              logtype: "sign-on-emergency",
              loglevel: "orange",
              logsubtype: "",
              logIcon: `fas fa-music`,
              title: `Emergency random music started.`,
              event:
                "There were no scheduled automation programs correctly configured. Furthermore, there was no correctly configured Default manual event in RadioDJ to trigger a default rotation. System fell back to queuing random music.",
            })
            .fetch()
            .tolerate((err) => {
              sails.log.error(err);
            });
        }
      } else {
        sails.log.verbose(
          `CALENDAR CHECK: whatShouldBePlaying: We triggered a genre; we are good.`
        );
        sails.models.status.errorCheck.playRandomTracks = false;
        await sails.helpers.status.modify.with({
          name: "calendar-triggerer",
          status: 5,
          summary: `Operational.`,
          data: "Calendar is successfully triggering automated programming",
        });
      }

      await sails.log.verbose(`CALENDAR CHECK: whatShouldBePlaying finished`);
    } else if (!inputs.ignoreChangingState) {
      await sails.log.verbose(`CALENDAR CHECK: Ignored checking state`);
      await sails.helpers.meta.change.with({ changingState: null });
    }

    // Do this block only if checking for integrity (every 5 minutes; changes to calendar; ending a broadcast)
    await sails.log.verbose(`CALENDAR CHECK: Called`);
    if (inputs.checkIntegrity) {
      // Get events for the next 14 days
      await sails.log.verbose(`CALENDAR CHECK: loading events next 14 days`);
      sails.models.calendar.calendardb.getEvents(
        async (events) => {
          await sails.log.verbose(
            `CALENDAR CHECK: 14-day events returned: ${events.length}`
          );

          // Events cache; perform/update in background
          /*
          (async (_events) => {
            await sails.log.verbose(`CALENDAR CHECK: Cache check started`);
            let existing = [];

            await sails.log.verbose(
              `CALENDAR CHECK: Cache check find/create/update`
            );
            let maps = _events
              .filter((event) => event.unique)
              .map(async (event) => {
                // We use an event's unique for the record ID in cache. But canceled-changed events share unique strings, so add a c at the end of the ID for these events.
                let unique = event.unique;
                if (event.scheduleType === "canceled-changed") unique += "-c";

                existing.push(unique);
              });
            await Promise.all(maps);

            // Remove records that no longer exist
            await sails.log.verbose(
              `CALENDAR CHECK: Cache check removing nonexisting events`
            );
          })(events);
          */

          // Check integrity of events
          await sails.log.verbose(`CALENDAR CHECK: Integrity started`);
          let status = 5;
          let cannotAir = [];
          let issues = [];

          if (events && events.length > 0) {
            let playlists = {};
            let djevents = {};

            // Check playlists
            if (sails.config.custom.radiodjs.length) {
              let playlistsR = await sails.models.playlists.find();

              // Check each playlist for duration and duplicates
              sails.log.debug(
                `CALENDAR CHECK: Integrity playlists map initializing`
              );
              maps = playlistsR.map(async (playlist) => {
                let playlistSongs = [];
                let playlistDuplicates = 0;
                let duplicateTracks = [];
                let badTypes = 0;
                let badTypeTracks = [];

                // Get the playlist tracks
                // LINT: Playlists_list is valid
                // eslint-disable-next-line camelcase
                let pTracks = await sails.models.playlists_list
                  .find({
                    pID: playlist.ID,
                  })
                  .populate("sID");
                sails.log.verbose(
                  `Retrieved Playlists_list records: ${pTracks.length}`
                );
                sails.log.silly(pTracks);

                let temp = [];
                let duration = 0;

                // Check for duplicates, duration, etc.
                pTracks.map((track) => {
                  if (!track.sID || typeof track.sID !== "object") return;

                  if (temp.indexOf(track.sID.ID) > -1) {
                    playlistDuplicates++;
                  }
                  temp.push(track.sID.ID);

                  if (
                    playlistSongs.indexOf(
                      `${track.sID.artist} - ${track.sID.title}`
                    ) > -1
                  ) {
                    playlistDuplicates++;
                    duplicateTracks.push(
                      `${track.sID.artist} - ${track.sID.title}`
                    );
                  } else {
                    duration += track.sID.duration;
                  }

                  // Determine tracks which may have the wrong type for pre-records
                  if (
                    [5, 7, 8, 11, 12, 13].indexOf(track.sID.song_type) === -1
                  ) {
                    badTypes++;
                    badTypeTracks.push(
                      `${track.sID.artist} - ${track.sID.title}`
                    );
                  }

                  playlistSongs.push(
                    `${track.sID.artist} - ${track.sID.title}`
                  );
                });

                // Generate playlist object
                playlists[playlist.ID] = {
                  ID: playlist.ID,
                  name: playlist.name,
                  tracks: pTracks,
                  duration: duration,
                  duplicates: playlistDuplicates,
                  duplicateTracks: duplicateTracks.join("; "),
                  badTypes: badTypes,
                  badTypeTracks: badTypeTracks.join("; "),
                };

                return true;
              });
              await Promise.all(maps);
              sails.log.debug(
                `CALENDAR CHECK: integrity playlists map completed.`
              );

              // Load all manual RadioDJ events into memory
              let djeventsR = await sails.models.events.find({ type: 3 });
              djeventsR.map((_event) => {
                djevents[_event.ID] = _event;
              });
            }

            // Load directors, djs, and hosts into memory
            let directors = await sails.models.directors.find();
            let djs = await sails.models.djs.find();
            let hosts = await sails.models.hosts.find();

            // Now, check each event
            sails.log.debug(
              `CALENDAR CHECK: integrity beginning event check of ${events.length} events`
            );
            events
              .filter(
                (event) =>
                  (event.scheduleType === null ||
                    ["canceled", "canceled-system", "canceled-changed"].indexOf(
                      event.scheduleType
                    ) === -1) &&
                  moment(event.end).isAfter(moment())
              )
              .map((event) => {
                let members = [];
                switch (event.type) {
                  case "prerecord":
                    // No playlist was assigned
                    if (event.playlistID === null) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Prerecord "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: does not have a playlist assigned to it and will not air until fixed! Please set a playlist in administration DJ Controls -> Calendar and edit the event.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Playlist does not exist in RadioDJ
                    if (
                      sails.config.custom.radiodjs.length &&
                      typeof playlists[event.playlistID] === "undefined"
                    ) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Prerecord "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: has an assigned playlist that does not exist in RadioDJ, and so it will not air! Please set a valid playlist in administration DJ Controls -> Calendar and edit the event, or create the playlist in RadioDJ.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Playlist has no tracks
                    if (
                      sails.config.custom.radiodjs.length &&
                      (!playlists[event.playlistID].tracks ||
                        playlists[event.playlistID].tracks.length < 1)
                    ) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Prerecord "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: has no tracks in its assigned playlist and will not air! In RadioDJ playlist builder, make sure the playlist has at least 1 valid track.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Disabled for now; there is no way to re-check track durations automatically on a frequency in RadioDJ

                    /*

                        // Playlist is 15 or more minutes too short
                        if (
                          (event.duration - 15) * 60 >=
                          playlists[event.playlistID].duration * 1.05
                        ) {
                          if (status > 4) {
                            status = 4;
                          }
                          issues.push(
                            `Prerecord playlist "${event.hosts} - ${
                              event.name
                            }" for ${moment(event.start).format(
                              "llll"
                            )} is short on content by about ${moment
                              .duration(
                                event.duration * 60 -
                                  playlists[event.playlistID].duration * 1.05,
                                "seconds"
                              )
                              .humanize()}`
                          );
                        }

                        // Playlist is 5 or more minutes too long
                        if (
                          (event.duration + 5) * 60 <=
                          playlists[event.playlistID].duration * 1.05
                        ) {
                          if (status > 4) {
                            status = 4;
                          }
                          issues.push(
                            `Prerecord playlist "${event.hosts} - ${
                              event.name
                            }" for ${moment(event.start).format(
                              "llll"
                            )} has about ${moment
                              .duration(
                                playlists[event.playlistID].duration * 1.05 -
                                  event.duration * 60,
                                "seconds"
                              )
                              .humanize()} too much content and might be cut off early by other scheduled broadcasts.`
                          );
                        }

                        */

                    // Duplicate tracks
                    if (
                      sails.config.custom.radiodjs.length &&
                      playlists[event.playlistID].duplicates > 0
                    ) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Prerecord "${event.name}" for ${moment(
                          event.start
                        ).format("llll")}: has ${
                          playlists[event.playlistID].duplicates
                        } duplicate tracks in the playlist which will be skipped. Edit the playlist in RadioDJ to review / fix.`
                      );
                    }

                    // Bad track types
                    if (
                      sails.config.custom.radiodjs.length &&
                      playlists[event.playlistID].badTypes > 0
                    ) {
                      if (status > 4) {
                        status = 4;
                      }
                      issues.push(
                        `Prerecord "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: These tracks are not set as type Variable Duration File, Podcast, Internet Stream, Playlist Event, File by Date, or Newest from Folder: ${
                          playlists[event.playlistID].badTypeTracks
                        }. Files which are regularly overwritten for the pre-record must be one of the specified types. You can ignore this for tracks which are not overwritten for each pre-record episode. TO FIX: Edit these tracks in RadioDJ tracks manager (or Playlist Builder) and set the Track Type to one of the specified applicable types.`
                      );
                    }

                    break;
                  case "playlist":
                    // No playlist was assigned
                    if (event.playlistID === null) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Playlist "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: does not have a playlist assigned to it and will not air until fixed! Please set a playlist in administration DJ Controls -> Calendar and edit the event.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Playlist does not exist in RadioDJ
                    if (
                      sails.config.custom.radiodjs.length &&
                      typeof playlists[event.playlistID] === "undefined"
                    ) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Playlist "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: has an assigned playlist that does not exist in RadioDJ, and so it will not air! Please set a valid playlist in administration DJ Controls -> Calendar and edit the event. Or, create the playlist in RadioDJ.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Playlist has no tracks
                    if (
                      sails.config.custom.radiodjs.length &&
                      (!playlists[event.playlistID].tracks ||
                        playlists[event.playlistID].tracks.length < 1)
                    ) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Playlist "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: has no tracks in its assigned playlist and will not air! In RadioDJ playlist builder, make sure the playlist has at least 1 valid track.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Disabled for now; there is no way to re-check track durations automatically on a requency in RadioDJ

                    /*

                        // Playlist is 15 or more minutes too short
                        if (
                          (event.duration - 15) * 60 >=
                          playlists[event.playlistID].duration * 1.05
                        ) {
                          if (status > 4) {
                            status = 4;
                          }
                          issues.push(
                            `Playlist event "${event.hosts} - ${
                              event.name
                            }" for ${moment(event.start).format(
                              "llll"
                            )} is short on content by about ${moment
                              .duration(
                                event.duration * 60 -
                                  playlists[event.playlistID].duration * 1.05,
                                "seconds"
                              )
                              .humanize()}`
                          );
                        }

                        // Playlist is 5 or more minutes too long
                        if (
                          (event.duration + 5) * 60 <=
                          playlists[event.playlistID].duration * 1.05
                        ) {
                          if (status > 4) {
                            status = 4;
                          }
                          issues.push(
                            `Playlist event "${event.hosts} - ${
                              event.name
                            }" for ${moment(event.start).format(
                              "llll"
                            )} has about ${moment
                              .duration(
                                playlists[event.playlistID].duration * 1.05 -
                                  event.duration * 60,
                                "seconds"
                              )
                              .humanize()} too much content and might be cut off early by other scheduled broadcasts.`
                          );
                        }

                        */

                    // Duplicate tracks
                    if (
                      sails.config.custom.radiodjs.length &&
                      playlists[event.playlistID].duplicates > 0
                    ) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Playlist event "${event.hosts} - ${
                          event.name
                        }" for ${moment(event.start).format("llll")}: has ${
                          playlists[event.playlistID].duplicates
                        } duplicate tracks which will be skipped. Use RadioDJ playlist builder to review / fix.`
                      );
                    }

                    break;
                  case "genre":
                    // No event was assigned
                    if (event.eventID === null) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Genre "${event.name}" for ${moment(event.start).format(
                          "llll"
                        )}: does not have a RadioDJ event assigned to it and will not air! TO FIX: In RadioDJ Options -> Events, create an event with "Manual Event" type. Add a "Load Rotation" action which loads the rotation for this genre. Then, in administration DJ Controls -> Calendar, edit this genre and assign the correct RadioDJ event.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Assigned event does not exist in RadioDJ
                    if (
                      sails.config.custom.radiodjs.length &&
                      typeof djevents[event.eventID] === "undefined"
                    ) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Genre "${event.name}" for ${moment(event.start).format(
                          "llll"
                        )}: has an assigned event that does not exist in RadioDJ, and will not air! TO FIX: In RadioDJ Options -> Events, create an event with "Manual Event" type. Add a "Load Rotation" action which loads the rotation for this genre. Then, in administration DJ Controls -> Calendar, edit this genre and assign the correct RadioDJ event.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Event does not actually load any rotations
                    if (
                      sails.config.custom.radiodjs.length &&
                      !djevents[event.eventID].data.includes("Load Rotation")
                    ) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Genre "${event.name}" for ${moment(event.start).format(
                          "llll"
                        )}: has an assigned RadioDJ event that does not contain a "Load Rotation" action, and will not air! TO FIX: In RadioDJ Options -> Events, edit the event for this genre, set it to "Manual Event" type, and add a "Load Rotation" action which loads the rotation for this genre.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                    }

                    // Event disabled
                    if (
                      sails.config.custom.radiodjs.length &&
                      djevents[event.eventID].enabled !== "True"
                    ) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Genre "${event.name}" for ${moment(event.start).format(
                          "llll"
                        )}: has an assigned RadioDJ event that is disabled, and thus will not air! TO FIX: In RadioDJ Options -> Events, edit the event for this genre and enable it.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                    }

                    break;

                  case "show":
                    // Get DJ records for each host
                    members = ["hostDJ", "cohostDJ1", "cohostDJ2", "cohostDJ3"]
                      .map((prop) => event[prop] || null)
                      .filter((member) => member !== null)
                      .map(
                        (member) =>
                          djs.find((record) => record.ID === member) || null
                      )
                      .filter((member) => member !== null);

                    // Are all the members missing?
                    if (members.length <= 0) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Live show "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: None of the hosts for this show could be found in the system's roster of org members. Therefore, no one can start the show. Please edit the show in the calendar to the correct hosts.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Are all the members inactive?
                    if (!members.find((member) => member.active)) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Live show "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: All of the hosts for this show either do not exist or are marked inactive. Therefore, no one can start the show. TO FIX: Edit the hosts of the show in the calendar, or mark the members active in DJ Controls Administration -> Org Members.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                    } else if (members.find((member) => !member.active)) {
                      if (status > 4) {
                        status = 4;
                      }
                      issues.push(
                        `Live show "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: These org members listed as a host are inactive in the system: ${members
                          .filter((member) => !member.active)
                          .map(
                            (member) => `${member.name} (${member.realName})`
                          )
                          .join(
                            "; "
                          )}. However, at least one other host for the show is active. TO FIX: Edit the hosts of the show in the calendar, or mark the members active in DJ Controls Administration -> Org Members.`
                      );
                    }

                    // Check permissions
                    if (
                      !members.find(
                        (member) => member.permissions.indexOf("live") !== -1
                      )
                    ) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Live show "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: None of the hosts for this show have permission to start live broadcasts. Therefore, no one can start the show. TO FIX: In DJ Controls, go to Administration / Org Members -> Edit the org member. Add permission "Can air live broadcasts from the WWSU onair studio".`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                    } else if (
                      members.find(
                        (member) => member.permissions.indexOf("live") === -1
                      )
                    ) {
                      if (status > 4) {
                        status = 4;
                      }
                      issues.push(
                        `Live show "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: These org members listed as a host do not have permission to start live broadcasts: ${members
                          .filter(
                            (member) =>
                              member.permissions.indexOf("live") === -1
                          )
                          .map(
                            (member) => `${member.name} (${member.realName})`
                          )
                          .join(
                            "; "
                          )}. However, at least one other host for the show has permission and can therefore start it instead. TO FIX: In DJ Controls, go to Administration / Org Members -> Edit the org member. Add permission "Can air live broadcasts from the WWSU onair studio".`
                      );
                    }

                    break;

                  case "remote":
                    // Get DJ records for each host
                    members = ["hostDJ", "cohostDJ1", "cohostDJ2", "cohostDJ3"]
                      .map((prop) => event[prop] || null)
                      .filter((member) => member !== null)
                      .map(
                        (member) =>
                          djs.find((record) => record.ID === member) || null
                      )
                      .filter((member) => member !== null);

                    // Are all the members missing?
                    if (members.length <= 0) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Remote broadcast "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: None of the hosts for this show could be found in the system's roster of org members. Therefore, no one can start the show. Please edit the show in the calendar to the correct hosts.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                      break;
                    }

                    // Are all the members inactive?
                    if (!members.find((member) => member.active)) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Remote broadcast "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: All of the hosts for this show either do not exist or are marked inactive. Therefore, no one can start the show. TO FIX: Edit the hosts of the show in the calendar, or mark the members active in DJ Controls Administration -> Org Members.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                    } else if (members.find((member) => !member.active)) {
                      if (status > 4) {
                        status = 4;
                      }
                      issues.push(
                        `Remote broadcast "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: These org members listed as a host are inactive in the system: ${members
                          .filter((member) => !member.active)
                          .map(
                            (member) => `${member.name} (${member.realName})`
                          )
                          .join(
                            "; "
                          )}. However, at least one other host for the show is active. TO FIX: Edit the hosts of the show in the calendar, or mark the members active in DJ Controls Administration -> Org Members.`
                      );
                    }

                    // Check permissions
                    if (
                      !members.find(
                        (member) => member.permissions.indexOf("remote") !== -1
                      )
                    ) {
                      if (status > 2) {
                        status = 2;
                      }
                      issues.push(
                        `Remote broadcast "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: None of the hosts for this show have permission to start remote broadcasts. Therefore, no one can start the show. TO FIX: In DJ Controls, go to Administration / Org Members -> Edit the org member. Add permission "Can air remote broadcasts from their own hosts / computers".`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                    } else if (
                      members.find(
                        (member) => member.permissions.indexOf("remote") === -1
                      )
                    ) {
                      if (status > 4) {
                        status = 4;
                      }
                      issues.push(
                        `Remote broadcast "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: These org members listed as a host do not have permission to start remote broadcasts: ${members
                          .filter(
                            (member) =>
                              member.permissions.indexOf("remote") === -1
                          )
                          .map(
                            (member) => `${member.name} (${member.realName})`
                          )
                          .join(
                            "; "
                          )}. However, at least one other host for the show has permission and can therefore start it instead. TO FIX: In DJ Controls, go to Administration / Org Members -> Edit the org member. Add permission "Can air remote broadcasts from their own hosts / computers".`
                      );
                    }

                    let remoteHosts = hosts.filter(
                      (host) =>
                        host.authorized &&
                        host.belongsTo > 0 &&
                        members.find((member) => member.ID === host.belongsTo)
                    );

                    // Check for hosts
                    if (remoteHosts.length <= 0) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Remote broadcast "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: None of the hosts for this show have an authorized host DJ Controls assigned to them. Therefore, no one can start the show except from a WWSU computer. Ignore this error if this broadcast is to be run on a WWSU computer. TO FIX: Make sure the org member installs DJ Controls on their computer and runs it. Then, in DJ Controls Administration -> Hosts, edit their host. Mark "is authorized" if not already, and set the belongs to option to their org member name.`
                      );
                      cannotAir.push(`${event.type}: ${event.name}`);
                    } else if (
                      members.filter((member) =>
                        remoteHosts.find((host) => host.belongsTo === member.ID)
                      ).length < members.length
                    ) {
                      if (status > 4) {
                        status = 4;
                      }
                      issues.push(
                        `Remote broadcast "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )}: These org members listed as a host do not have an authorized host DJ Controls assigned to them: ${members
                          .filter(
                            (member) =>
                              !remoteHosts.find(
                                (host) => host.belongsTo === member.ID
                              )
                          )
                          .map(
                            (member) => `${member.name} (${member.realName})`
                          )
                          .join(
                            "; "
                          )}. However, at least one other host for the show has a host DJ Controls assigned to them and can start the broadcast from their computer or a WWSU computer instead. Ignore this error if this broadcast is to be run on a WWSU computer. TO FIX: Make sure the org member installs DJ Controls on their computer and runs it. Then, in DJ Controls Administration -> Hosts, edit their host. Mark "is authorized" if not already, and set the belongs to option to their org member name.`
                      );
                    }

                    break;

                  case "office-hours":
                    if (event.director === null) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Director hours "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )} does not have a director ID assigned to it. A director must be assigned to office hour events. Please fix or remove.`
                      );
                      break;
                    }
                    let _director = directors.filter(
                      (director) => director.ID === event.director
                    );
                    if (_director.length < 1) {
                      if (status > 3) {
                        status = 3;
                      }
                      issues.push(
                        `Director hours "${event.name}" for ${moment(
                          event.start
                        ).format(
                          "llll"
                        )} were assigned a director that does not exist in the system. Please fix this or remove the office hours.`
                      );
                      break;
                    }
                }
              });

            if (issues.length === 0) {
              await sails.helpers.status.modify.with({
                name: "calendar",
                label: "Calendar",
                summary: `Operational.`,
                data: `Calendar is operational.`,
                status: 5,
              });
            } else {
              // Remove duplicates
              issues = issues.filter((v, i, a) => a.indexOf(v) === i);
              await sails.helpers.status.modify.with({
                name: "calendar",
                label: "Calendar",
                summary: `Issues detected (See DJ Controls).${
                  cannotAir.length > 0
                    ? ` These programs might not be able to air: ${cannotAir.join(
                        "; "
                      )}`
                    : ``
                }`,
                data: `<ul><li>${issues.join(`</li><li>`)}</li></ul>`,
                status: status,
              });
            }

            await sails.log.verbose(`CALENDAR CHECK: Integrity finished`);
          }
          await sails.log.verbose(
            `CALENDAR CHECK: 14-day event processing finished`
          );
        },
        undefined,
        moment().add(14, "days").toISOString(true)
      );

      // Check to see if any events did not air
      await sails.log.verbose(`CALENDAR CHECK: Absence checking started`);
      sails.models.calendar.calendardb.getEvents(
        async (eventCheck) => {
          await sails.log.verbose(
            `CALENDAR CHECK: Absence checking events returned`
          );
          if (eventCheck && eventCheck.length > 0) {
            // Radio shows
            eventCheck
              .filter(
                (event) =>
                  moment().isSameOrAfter(moment(event.end)) &&
                  (event.scheduleType === null ||
                    (event.scheduleType !== "canceled" &&
                      event.scheduleType !== "canceled-system" &&
                      event.scheduleType !== "canceled-changed")) &&
                  ["show", "sports", "prerecord", "remote", "playlist"].indexOf(
                    event.type
                  ) !== -1
              )
              .map(async (event) => {
                let check = await sails.models.attendance.find({
                  unique: event.unique,
                });
                if (check.length < 1) {
                  let record = await sails.models.attendance
                    .create({
                      calendarID: event.calendarID,
                      unique: event.unique,
                      dj: event.hostDJ,
                      cohostDJ1: event.cohostDJ1,
                      cohostDJ2: event.cohostDJ2,
                      cohostDJ3: event.cohostDJ3,
                      event: `${event.type}: ${event.hosts} - ${event.name}`,
                      happened: 0,
                      scheduledStart: moment(event.start).format(
                        "YYYY-MM-DD HH:mm:ss"
                      ),
                      scheduledEnd: moment(event.end).format(
                        "YYYY-MM-DD HH:mm:ss"
                      ),
                    })
                    .fetch();

                  if (event.type === "show") {
                    await sails.models.logs
                      .create({
                        attendanceID: record.ID,
                        logtype: "absent",
                        loglevel: "warning",
                        logsubtype: `${event.hosts} - ${event.name}`,
                        logIcon: `fas fa-calendar-times`,
                        title: `A scheduled show did not air!`,
                        event: `Show: ${event.hosts} - ${
                          event.name
                        }}<br />Scheduled time: ${moment(event.start).format(
                          "llll"
                        )} - ${moment(event.end).format("llll")}`,
                        createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                      })
                      .fetch()
                      .tolerate((err) => {
                        sails.log.error(err);
                      });
                    await sails.helpers.onesignal.sendMass(
                      "accountability-shows",
                      "Show did not air!",
                      `${event.hosts} - ${event.name} failed to air on ${moment(
                        event.start
                      ).format("llll")} - ${moment(event.end).format(
                        "llll"
                      )}; unexcused absence.`
                    );
                    await sails.helpers.emails.queueDjs(
                      event,
                      `Unexcused absence for ${event.hosts} - ${event.name}`,
                      `Dear ${event.hosts},<br /><br />

                          Your show, <strong>${
                            event.name
                          }</strong>, was scheduled to air, but you did not air your show. This counts as an <strong>unexcused absence</strong>. Repeat unexcused absences could result in disciplinary action, including loss of your show.<br /><br />

                          Scheduled time: ${moment(event.start).format(
                            "LLLL"
                          )} - ${moment(event.end).format("LTS")}<br />

                          Please reply all to this email if you feel this was not an unexcused absence (such as if you notified them ahead of time you would not be doing your show).`
                    );
                  }

                  if (event.type === "prerecord") {
                    await sails.models.logs
                      .create({
                        attendanceID: record.ID,
                        logtype: "absent",
                        loglevel: "warning",
                        logsubtype: `${event.hosts} - ${event.name}`,
                        logIcon: `fas fa-calendar-times`,
                        title: `A scheduled prerecord did not air!`,
                        event: `Prerecord: ${event.hosts} - ${
                          event.name
                        }<br />Scheduled time: ${moment(event.start).format(
                          "llll"
                        )} - ${moment(event.end).format(
                          "llll"
                        )}<br />Note: When prerecords do not air, this is usually because of a system problem or the prerecord was not correctly added to the system. If this is the case, please mark this as excused.`,
                        createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                      })
                      .fetch()
                      .tolerate((err) => {
                        sails.log.error(err);
                      });
                    await sails.helpers.onesignal.sendMass(
                      "emergencies",
                      "Prerecord failed to air!",
                      `${event.hosts} - ${event.name} failed to air on ${moment(
                        event.start
                      ).format("llll")} - ${moment(event.end).format(
                        "llll"
                      )}; this is likely a problem with the system.`
                    );
                    await sails.helpers.emails.queueDjs(
                      event,
                      `Prerecord did not air for ${event.hosts} - ${event.name}`,
                      `Dear ${event.hosts},<br /><br />

                          A prerecord, <strong>${
                            event.name
                          }</strong>, was scheduled to air, but it did not air. This counts as an <strong>unexcused absence</strong>. Repeat unexcused absences could result in disciplinary action, including loss of your show.<br /><br />

                          Scheduled time: ${moment(event.start).format(
                            "LLLL"
                          )} - ${moment(event.end).format("LTS")}<br />

                          <strong>This could have been a system problem rather than an unexcused absence.</strong> If that is the case, or if you emailed a director ahead of time asking to cancel the prerecord, please reply all to this email.`
                    );
                  }

                  if (event.type === "remote") {
                    await sails.models.logs
                      .create({
                        attendanceID: record.ID,
                        logtype: "absent",
                        loglevel: "warning",
                        logsubtype: `${event.hosts} - ${event.name}`,
                        logIcon: `fas fa-calendar-times`,
                        title: `A scheduled remote broadcast did not air!`,
                        event: `Remote: ${event.hosts} - ${
                          event.name
                        }<br />Scheduled time: ${moment(event.start).format(
                          "llll"
                        )} - ${moment(event.end).format("llll")}`,
                        createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                      })
                      .fetch()
                      .tolerate((err) => {
                        sails.log.error(err);
                      });
                    await sails.helpers.onesignal.sendMass(
                      "accountability-shows",
                      "Remote broadcast did not air!",
                      `${event.hosts} - ${event.name} failed to air on ${moment(
                        event.start
                      ).format("llll")} - ${moment(event.end).format(
                        "llll"
                      )}; unexcused absence.`
                    );
                    await sails.helpers.emails.queueDjs(
                      event,
                      `Unexcused absence for ${event.hosts} - ${event.name}`,
                      `Dear ${event.hosts},<br /><br />

                          Your remote broadcast, <strong>${
                            event.name
                          }</strong>, was scheduled to air, but you did not air your remote broadcast. This counts as an <strong>unexcused absence</strong>. Repeat unexcused absences could result in disciplinary action, including loss of your show.<br /><br />

                          Scheduled time: ${moment(event.start).format(
                            "LLLL"
                          )} - ${moment(event.end).format("LTS")}<br />

                          Please reply all to this email if you feel this was not an unexcused absence (such as if you notified them ahead of time you would not be doing your show, or if you experienced technical problems with WWSU preventing you from doing the remote broadcast).`
                    );
                  }

                  if (event.type === "sports") {
                    await sails.models.logs
                      .create({
                        attendanceID: record.ID,
                        logtype: "absent",
                        loglevel: "warning",
                        logsubtype: event.name,
                        logIcon: `fas fa-calendar-times`,
                        title: `A scheduled sports broadcast did not air!`,
                        event: `Sport: ${
                          event.name
                        }<br />Scheduled time: ${moment(event.start).format(
                          "llll"
                        )} - ${moment(event.end).format("llll")}`,
                        createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                      })
                      .fetch()
                      .tolerate((err) => {
                        sails.log.error(err);
                      });
                    await sails.helpers.onesignal.sendMass(
                      "accountability-shows",
                      "Sports broadcast did not air!",
                      `${event.name} failed to air on ${moment(
                        event.start
                      ).format("llll")} - ${moment(event.end).format(
                        "llll"
                      )}; unexcused absence.`
                    );
                    await sails.helpers.emails.queueDjs(
                      event,
                      `Sports broadcast did not air for ${event.name}`,
                      `Dear directors,<br /><br />

                          A sports broadcast, <strong>${
                            event.name
                          }</strong>, was scheduled to air, but it did not air.<br /><br />

                          Scheduled time: ${moment(event.start).format(
                            "LLLL"
                          )} - ${moment(event.end).format("LTS")}<br />

                          Please cancel sports broadcasts in the calendar in advance if they no longer are supposed to air. Otherwise, DJs who have had their shows canceled/rescheduled will unnecessarily miss part or all of their show.`
                    );
                  }

                  if (event.type === "playlist") {
                    await sails.models.logs
                      .create({
                        attendanceID: record.ID,
                        logtype: "absent",
                        loglevel: "warning",
                        logsubtype: event.name,
                        logIcon: `fas fa-calendar-times`,
                        title: `A scheduled playlist did not air!`,
                        event: `Playlist: ${
                          event.name
                        }<br />Scheduled time: ${moment(event.start).format(
                          "llll"
                        )} - ${moment(event.end).format(
                          "llll"
                        )}<br />Note: when playlists do not air, this is usually because of a system problem or the playlist was not correctly added to the system.`,
                        createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                      })
                      .fetch()
                      .tolerate((err) => {
                        sails.log.error(err);
                      });
                    await sails.helpers.onesignal.sendMass(
                      "emergencies",
                      "Playlist failed to air",
                      `${event.name} failed to air on ${moment(
                        event.start
                      ).format("llll")} - ${moment(event.end).format(
                        "llll"
                      )}; this is likely a problem with the system.`
                    );
                    await sails.helpers.emails.queueDjs(
                      event,
                      `Playlist did not air for ${event.hosts} - ${event.name}`,
                      `Dear ${event.hosts},<br /><br />

                          A playlist, <strong>${
                            event.name
                          }</strong>, was scheduled to air, but it did not air. This counts as an <strong>unexcused absence</strong>. Repeat unexcused absences could result in disciplinary action, including loss of your show.<br /><br />

                          Scheduled time: ${moment(event.start).format(
                            "LLLL"
                          )} - ${moment(event.end).format("LTS")}<br />

                          <strong>This could have been a system problem rather than an unexcused absence.</strong> If that is the case, or if you emailed a director ahead of time asking to cancel the playlist, please reply all to this email.`
                    );
                  }
                }
              });

            // Check director hours not tied to a calendar entry. Tie them to an entry if applicable.
            let check2 = await sails.models.timesheet.find({
              unique: null,
              timeIn: { "!=": null },
              timeOut: null,
            });
            if (check2 && check2.length > 0) {
              let maps = check2.map(async (check3) => {
                let eventCheck2 = eventCheck.find((event) => {
                  return (
                    event.name === check3.name &&
                    moment().isSameOrAfter(event.start) &&
                    moment(check3.timeIn).isBefore(event.end)
                  );
                });
                if (eventCheck2) {
                  await sails.models.timesheet
                    .update(
                      { ID: check3.ID },
                      {
                        unique: eventCheck2.unique,
                        scheduledIn: eventCheck2.start,
                        scheduledOut: eventCheck2.end,
                      }
                    )
                    .fetch();
                }
              });
              await Promise.all(maps);
            }

            // Director hours absence checking
            eventCheck
              .filter(
                (event) =>
                  moment().isSameOrAfter(moment(event.end)) &&
                  (event.scheduleType === null ||
                    (event.scheduleType !== "canceled" &&
                      event.scheduleType !== "canceled-system" &&
                      event.scheduleType !== "canceled-changed")) &&
                  event.type === "office-hours"
              )
              .map(async (event) => {
                let check2 = await sails.models.timesheet.find({
                  unique: event.unique,
                });
                if (check2.length < 1) {
                  await sails.models.timesheet
                    .create({
                      calendarID: event.calendarID,
                      unique: event.unique,
                      name: event.hosts,
                      scheduledIn: event.start,
                      scheduledOut: event.end,
                      approved: -1,
                    })
                    .fetch()
                    .tolerate((err) => {
                      sails.log.error(err);
                    });

                  await sails.models.logs
                    .create({
                      attendanceID: null,
                      logtype: "director-absent",
                      loglevel: "warning",
                      logsubtype: event.hosts,
                      logIcon: `fas fa-user-times`,
                      title: `A director failed to clock in for scheduled office hours!`,
                      event: `Director: ${
                        event.hosts
                      }<br />Scheduled time: ${moment(event.start).format(
                        "llll"
                      )} - ${moment(event.end).format("llll")}`,
                      createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
                    })
                    .fetch()
                    .tolerate((err) => {
                      sails.log.error(err);
                    });
                  await sails.helpers.onesignal.sendMass(
                    "accountability-directors",
                    "Director failed to do their hours!",
                    `${
                      event.hosts
                    } failed to show up for their scheduled hours at ${moment(
                      event.start
                    ).format("llll")} - ${moment(event.end).format("llll")}.`
                  );
                }
              });
          }
          await sails.log.verbose(`CALENDAR CHECK: Absence checking finished`);
        },
        moment(sails.models.meta.memory.attendanceChecked)
          .subtract(1, "days")
          .toISOString(true),
        moment().toISOString(true)
      );

      // Update attendance checked (do not wait for getEvents to do this; we don't want future getEvents calls to check for the same date ranges)
      await sails.helpers.meta.change.with({
        attendanceChecked: moment().format("YYYY-MM-DD HH:mm:ss"),
      });
    }

    return;
  },
};
