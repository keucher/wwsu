module.exports = {
  friendlyName: "discipline.add",

  description: "Add a disciplinary action to a specified host.",

  inputs: {
    host: {
      required: true,
      type: "string",
      description:
        "The unique ID or IP address assigned to the host that we are banning.",
    },

    action: {
      type: "string",
      required: true,
      isIn: ["dayban", "permaban", "showban"],
      description:
        "Type of ban: dayban (24 hours from createdAt), permaban (indefinite), show ban (until the current broadcast ends).",
    },

    message: {
      type: "string",
      defaultsTo: "Unspecified reason",
      description: "The reason for issuing the discipline",
    },

    active: {
      type: "boolean",
      defaultsTo: true,
      description:
        "whether or not the discipline should be active when created.",
    },
  },

  fn: async function (inputs) {

    // Try to find the actual IP address in the event inputs.host is a host string and not an IP
    let ip;
    ip = await sails.models.messages
      .find({
        or: [{ fromIP: inputs.host }, { from: `website-${inputs.host}` }],
      })
      .sort("createdAt DESC")
      .limit(1);

    if (ip && ip[0]) {
      ip = ip[0].fromIP;
    } else {
      ip = null;
    }

    // Remove all messages by the disciplined user
    await sails.helpers.messages.removeMass(ip || inputs.host);

    // Add discipline to the database
    let reference = await sails.models.discipline
      .create({
        active: inputs.active,
        acknowledged: !inputs.active,
        IP: ip || inputs.host,
        action: inputs.action,
        message: inputs.message,
      })
      .fetch();

    // Broadcast the ban to the client
    if (inputs.active) {
      sails.sockets.broadcast(
        `discipline-${ip || inputs.host.replace("website-", "")}`,
        `discipline-add`,
        {
          ID: reference.ID,
          active: inputs.active,
          acknowledged: !inputs.active,
          message: inputs.message,
          action: inputs.action,
          createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
        }
      );
    }

    return reference;
  },
};
