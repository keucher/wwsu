module.exports = {
  friendlyName: "eas.addAlert",

  description: "Prepares an alert to be pushed by the eas/postParse helper.",

  inputs: {
    reference: {
      type: "string",
      required: true,
      description: "A unique ID for the alert provided by the source.",
    },
    source: {
      type: "string",
      required: true,
      description:
        "This alert originates from the provided source. Use NWS for NWS sources so that this helper can retrieve alert information.",
    },
    county: {
      type: "string",
      required: true,
      description: "This alert applies to the specified county.",
    },
    alert: {
      type: "string",
      required: true,
      description: 'The alert name/event. Eg. "Severe Thunderstorm Warning".',
    },
    severity: {
      type: "string",
      required: true,
      isIn: ["Extreme", "Severe", "Moderate", "Minor"],
      description: `Severity of alert: One of the following in order from highest to lowest ['Extreme', 'Severe', 'Moderate', 'Minor']`,
    },
    starts: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the alert starts. Recommended ISO string.`,
    },
    expires: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the alert expires. Recommended ISO string.`,
    },
    color: {
      type: "string",
      regex: /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i,
      description: "Hex color representing this alert.",
      required: true,
    },
    information: {
      type: "string",
      allowNull: true,
      description: "Detailed information about this alert for the public.",
    },
    needsAired: {
      type: "boolean",
      defaultsTo: false,
      description: "Should this alert be sent over the air (text to speech)?",
    },
  },

  fn: async function (inputs) {
      let criteria = {};

      // Define a function for processing information into the sails.models.eas.pendingAlerts variable.
      // Notably, this function ensures the same alert from different counties are joined together.
      const processPending = (criteria) => {
        if (
          typeof sails.models.eas.pendingAlerts[
            `${inputs.source}.${inputs.reference}`
          ] === `undefined`
        ) {
          sails.models.eas.pendingAlerts[
            `${inputs.source}.${inputs.reference}`
          ] = criteria;
        } else {
          for (let key in criteria) {
            if (Object.prototype.hasOwnProperty.call(criteria, key)) {
              if (key !== `counties`) {
                sails.models.eas.pendingAlerts[
                  `${inputs.source}.${inputs.reference}`
                ][key] = criteria[key];
              } else {
                let temp =
                  sails.models.eas.pendingAlerts[
                    `${inputs.source}.${inputs.reference}`
                  ][key].split(", ");
                if (temp.indexOf(inputs.county) === -1) {
                  temp.push(inputs.county);
                }
                temp = temp.join(", ");
                sails.models.eas.pendingAlerts[
                  `${inputs.source}.${inputs.reference}`
                ][key] = temp;
              }
            }
          }
        }
      };

      // Get the alert if it already exists in the database
      let record = await sails.models.eas
        .findOne({ source: inputs.source, reference: inputs.reference })
        .tolerate((err) => {
          sails.log.error(err);
        });

      // exists
      if (record) {
        sails.log.verbose(
          `Alert ${inputs.county}/${inputs.alert} already exists.`
        );
        // Detect if the county issuing the alert is already in the alert. If not, add the county in.
        let counties = record.counties.split(", ");
        if (counties.indexOf(inputs.county) === -1) {
          counties.push(inputs.county);
        }
        counties = counties.join(", ");

        // Prepare criteria to update
        criteria = {
          source: inputs.source,
          reference: inputs.reference,
          alert: inputs.alert,
          severity: inputs.severity,
          color: inputs.color,
          counties: counties,
          starts:
            inputs.starts !== null
              ? moment(inputs.starts).format("YYYY-MM-DD HH:mm:ss")
              : moment().format("YYYY-MM-DD HH:mm:ss"),
          expires:
            inputs.expires !== null
              ? moment(inputs.expires).format("YYYY-MM-DD HH:mm:ss")
              : null,
          // Do not include needsAired; for updates, we will only include needsAired if specified true as a parameter AND the alert was changed.
        };
        if (
          typeof inputs.information !== "undefined" &&
          inputs.information !== null
        ) {
          criteria.information = inputs.information;
        }

        sails.log.verbose(`Criteria`, criteria);

        // Detect any changes in the alert. If a change is detected, we will do a database update.
        let updateIt = false;
        for (let key in criteria) {
          if (Object.prototype.hasOwnProperty.call(criteria, key)) {
            if (key === "starts" || key === "expires") {
              // Skip
            } else if (criteria[key] !== record[key]) {
              updateIt = true;
              // Mark to re-broadcast this alert if applicable.
              if (inputs.needsAired) {
                criteria.needsAired = true;
              }
            }
          }
        }
        sails.log.silly(
          `${inputs.county}/${inputs.alert} Needs updating?: ${updateIt}`
        );
        if (updateIt) {
          criteria._new = false;
          criteria._update = true;
        } else {
          criteria._update = false;
        }
        processPending(criteria);
        return criteria;
      } else {
        // Does not exist; new alert
        sails.log.verbose(`Alert ${inputs.county}/${inputs.alert} is new.`);

        // Prepare criteria
        criteria = {
          source: inputs.source,
          reference: inputs.reference,
          alert: inputs.alert,
          severity: inputs.severity,
          color: inputs.color,
          counties: inputs.county,
          starts:
            inputs.starts !== null
              ? moment(inputs.starts).format("YYYY-MM-DD HH:mm:ss")
              : moment().format("YYYY-MM-DD HH:mm:ss"),
          expires:
            inputs.expires !== null
              ? moment(inputs.expires).format("YYYY-MM-DD HH:mm:ss")
              : null,
          information: inputs.information || "",
          needsAired: inputs.needsAired,
        };

        sails.log.verbose(`Initial criteria`, criteria);

        // If this alert came from NWS, we need to GET a separate URL for alert information before we create the record.
        if (
          inputs.source === "The National Weather Service" &&
          (typeof sails.models.eas.pendingAlerts[
            `${inputs.source}.${inputs.reference}`
          ] === `undefined` ||
            sails.models.eas.pendingAlerts[
              `${inputs.source}.${inputs.reference}`
            ].information === "" ||
            sails.models.eas.pendingAlerts[
              `${inputs.source}.${inputs.reference}`
            ].information === null)
        ) {
          sails.log.verbose(
            "Alert is from NWS source. Retrieving alert information."
          );
          let resp = await needle("get", inputs.reference);

          // Go through each child
          let maps = resp.body.children
            .filter(
              (entry) =>
                typeof entry.name !== "undefined" && entry.name === "info"
            )
            .map(async (entry) => {
              let details = {
                description: "",
                instruction: "",
              };

              // Parse field information into the alert variable
              entry.children.map((entry2) => {
                switch (entry2.name) {
                  case "description":
                    details.description = entry2.value;
                    break;
                  case "instruction":
                    details.instruction = entry2.value;
                    break;
                }
              });

              // Sometimes, EAS will return "This alert has expired". If so, leave information blank.
              if (
                !details.description
                  .toLowerCase()
                  .includes("this alert has expired")
              ) {
                criteria.information = `${
                  details.description === ""
                    ? `No information is available for this alert.`
                    : details.description
                }.${
                  details.instruction !== ""
                    ? `Precautionary / preparedness actions: ${details.instruction}.`
                    : ``
                }`;
                sails.log.verbose(
                  `Alert ${inputs.county}/${inputs.alert} is still valid.`
                );
              } else {
                sails.log.verbose(
                  `Alert ${inputs.county}/${inputs.alert} has expired, according to NWS`
                );
              }
              sails.log.silly(`Criteria: ${criteria}`);
              criteria._new = true;
              criteria._update = false;
              processPending(criteria);
            });
          await Promise.all(maps);

          return criteria;
        } else {
          sails.log.verbose(`Criteria: ${criteria}`);
          criteria._new = true;
          criteria._update = false;
          processPending(criteria);
          return criteria;
        }
      }
  },
};
