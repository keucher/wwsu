let easTimer = false;

module.exports = {
  friendlyName: "eas.postParse",

  description: "Finalize and push out new/updated alerts.",

  inputs: {},

  fn: async function (inputs) {
    sails.log.debug("Helper eas.postParse called.");

    try {
      var sendit = [];
      let newAlerts = false;

      let activeAlerts = _.cloneDeep(sails.models.eas.pendingAlerts);

      // Get all active alerts from the database.
      var records = await sails.models.eas.find();
      sails.log.verbose(
        `sails.models.eas records retrieved: ${records.length}`
      );
      sails.log.silly(records);

      // Process sails.models.eas.pendingAlerts
      for (var key in sails.models.eas.pendingAlerts) {
        if (
          Object.prototype.hasOwnProperty.call(
            sails.models.eas.pendingAlerts,
            key
          )
        ) {
          sails.log.verbose(
            `Finalizing alert`,
            sails.models.eas.pendingAlerts[key]
          );

          // Do not process alerts containing no information
          if (
            sails.models.eas.pendingAlerts[key].information === "" ||
            sails.models.eas.pendingAlerts[key].information === null
          ) {
            sails.log.verbose(`Alert has no information. Bailing.`);
            delete sails.models.eas.pendingAlerts[key];
            continue;
          }

          // New alerts should be created
          if (sails.models.eas.pendingAlerts[key]._new) {
            newAlerts = true;
            sails.log.verbose(`Alert is new. Creating.`);
            delete sails.models.eas.pendingAlerts[key]._new;
            delete sails.models.eas.pendingAlerts[key]._update;
            await sails.models.eas
              .create(sails.models.eas.pendingAlerts[key])
              .fetch();
            delete sails.models.eas.pendingAlerts[key];

            // Non-new alerts should be updated if _update is true
          } else if (sails.models.eas.pendingAlerts[key]._update) {
            delete sails.models.eas.pendingAlerts[key]._new;
            delete sails.models.eas.pendingAlerts[key]._update;
            if (sails.models.eas.pendingAlerts[key].needsAired)
              newAlerts = true;
            await sails.models.eas
              .update(
                {
                  source: sails.models.eas.pendingAlerts[key].source,
                  reference: sails.models.eas.pendingAlerts[key].reference,
                },
                sails.models.eas.pendingAlerts[key]
              )
              .fetch();
            delete sails.models.eas.pendingAlerts[key];
          }
        }
      }

      var maps = records.map(async (record) => {
        // Trigger a non-fetched update of all NWS records still active so that the updatedAt is updated, and therefore the record isn't removed.
        if (
          record.source === "The National Weather Service" &&
          sails.models.eas.activeCAPS.indexOf(record.reference) !== -1
        ) {
          await sails.models.eas
            .update(
              { ID: record.ID },
              { updatedAt: moment().format("YYYY-MM-DD HH:mm:ss") }
            )
            .tolerate((err) => {
              sails.log.error(err);
            });
          return true;
        }

        // Do the same for WSU alerts
        if (
          record.source === "Wright State Alert" &&
          activeAlerts[`Wright State Alert.${record.reference}`]
        ) {
          await sails.models.eas
            .update(
              { ID: record.ID },
              { updatedAt: moment().format("YYYY-MM-DD HH:mm:ss") }
            )
            .tolerate((err) => {
              sails.log.error(err);
            });
          return true;
        }

        // Remove expired alerts. Also, NWS and WSU alerts not reported by CAPS in 5 or more minutes (via updatedAt checking) should also be considered expired or canceled.
        if (
          (["The National Weather Service", "Wright State Alert"].indexOf(
            record.source
          ) !== -1 &&
            moment().isAfter(moment(record.updatedAt).add(5, "minutes"))) ||
          (record.expires && moment().isAfter(moment(record.expires)))
        ) {
          sails.log.verbose(
            `Record ${record.ID} is to be deleted. It has expired.`
          );
          await sails.models.eas
            .destroy({ ID: record.ID })
            .fetch()
            .tolerate((err) => {
              sails.log.error(err);
            });
          return true;
        }

        return true;
      });
      await Promise.all(maps);

      // Check if any alerts need aired (skip if we have no RadioDJs configured)
      if (
        newAlerts &&
        !sails.models.meta.memory.altRadioDJ &&
        !easTimer &&
        sails.config.custom.radiodjs.length
      ) {
        let triggerAir = await sails.models.eas.count({ needsAired: true });
        if (triggerAir) {
          easTimer = true;
          // Actual executor for airing the new alert
          let queueFunc = async function (altREST) {
            // Only if the current track is not a station ID, skip it to immediately play the alert
            if (
              sails.config.custom.subcats.IDs.indexOf(
                sails.models.meta.memory.trackIDSubcat || 0
              ) === -1 ||
              altREST
            ) {
              // Queue alert intro and stream
              await sails.helpers.songs.queue(
                sails.config.custom.subcats["easNewAlertIntro"],
                "Top",
                1,
                "noRules",
                null,
                false,
                altREST
              );
              await sails.helpers.rest.cmd(
                "PlayPlaylistTrack",
                0,
                undefined,
                altREST
              );
              await sails.helpers.songs.queue(
                sails.config.custom.subcats["easNewAlertStream"],
                "Top",
                1,
                "noRules",
                null,
                false,
                altREST
              );
            } else {
              // Queue alert intro and stream
              await sails.helpers.songs.queue(
                sails.config.custom.subcats["easNewAlertStream"],
                "Top",
                1,
                "noRules",
                null,
                false,
                altREST
              );
              await sails.helpers.songs.queue(
                sails.config.custom.subcats["easNewAlertIntro"],
                "Top",
                1,
                "noRules",
                null,
                false,
                altREST
              );
            }

            // Log
            await sails.models.logs
              .create({
                attendanceID: sails.models.meta.memory.attendanceID,
                logtype: "eas",
                loglevel: "info",
                logsubtype: ``,
                logIcon: `fas fa-warning`,
                title: `Alerts were broadcast on the air via the internal EAS.`,
                event: `Note, this is the internal EAS (via DJ Controls, Campus Alerts, etc), not the radio EAS equipment required by the FCC.`,
              })
              .fetch()
              .tolerate((err) => {
                sails.log.error(err);
              });
          };

          if (
            sails.models.meta.memory.state.startsWith("automation_") ||
            sails.models.meta.memory.state.endsWith("_break") ||
            sails.models.meta.memory.state.endsWith("_returning") ||
            sails.models.meta.memory.state.endsWith("_halftime")
          ) {
            // If in automation or break, simply queue / play the new alert immediately
            await queueFunc();
            easTimer = false;
          } else if (sails.models.meta.memory.state.startsWith("prerecord_")) {
            // We need to do something special in this case; utilize an inactive (but good status) RadioDJ if possible.
            // Otherwise, we would have to skip part of the prerecord, which is not ideal.

            // Find healthy RadioDJs that are inactive
            let healthyRadioDJs = [];
            let maps = sails.config.custom.radiodjs.map(async (instance) => {
              if (instance.name === sails.models.meta.memory.radiodj) {
                return false;
              }
              var status = await sails.models.status.findOne({
                name: `radiodj-${instance.name}`,
              });
              if (status && status.status === 5) {
                healthyRadioDJs.push(instance);
              }
              return true;
            });
            await Promise.all(maps);

            // We have at least one inactive healthy RadioDJ
            if (healthyRadioDJs.length > 0) {
              let temp = await sails.helpers.pickRandom(healthyRadioDJs); // Pick a random RadioDJ
              let picked = temp.item;

              // Prepare the inactive RadioDJ
              await sails.helpers.rest.cmd(
                "EnableAssisted",
                0,
                undefined,
                picked
              );
              await sails.helpers.rest.cmd(
                "EnableAutoDJ",
                0,
                undefined,
                picked
              );
              await sails.helpers.songs.remove(true, [], false, true, picked);

              // Pause the active RadioDJ
              await sails.helpers.rest.cmd("PausePlayer", 1);

              // This is to ensure bootstrap.js does not trigger error fallbacks because active RadioDJ is paused.
              // It also enabled bootstrap.js to unpause active RadioDJ when the queue for the picked one is empty.
              await sails.helpers.meta.change.with({ altRadioDJ: picked.name });
              await queueFunc(picked);
            } else {
              // No other healthy radioDJs available? We have no choice... forcefully play the alert on the current one.
              // This will unfortunately skip the remainder of the current prerecord track playing.
              await sails.helpers.rest.cmd("EnableAssisted", 0);
              await queueFunc();
              // Log
              await sails.models.logs
                .create({
                  attendanceID: sails.models.meta.memory.attendanceID,
                  logtype: "eas-trackskip",
                  loglevel: "warning",
                  logsubtype: ``,
                  logIcon: `fas fa-warning`,
                  title: `A prerecord track had to be skipped to broadcast an alert.`,
                  event: `There were no other available RadioDJ instances to broadcast the alert without interrupting the pre-record. Therefore, the track that was airing had to be skipped. Please mark excused any logs about the prerecord ending early.`,
                })
                .fetch()
                .tolerate((err) => {
                  sails.log.error(err);
                });
            }
          } else {
            sails.sockets.broadcast("eas-pending", "eas-pending", 30);
            sails.log.debug("Waiting 30 seconds to send alert...");
            easTimer = setTimeout(async () => {
              try {
                clearTimeout(easTimer);
                easTimer = true;

                // Re-check state in case it changed during the wait
                if (
                  sails.models.meta.memory.state.startsWith("automation_") ||
                  sails.models.meta.memory.state.endsWith("_break") ||
                  sails.models.meta.memory.state.endsWith("_returning") ||
                  sails.models.meta.memory.state.endsWith("_halftime")
                ) {
                  // If in automation or break now, simply queue / play the new alert immediately
                  await queueFunc();
                } else {
                  // Trigger a break, then queue the alert
                  await sails.helpers.state.goBreak(false, false);
                  await queueFunc();
                }
                easTimer = false;
              } catch (e) {
                sails.log.error(e);
                easTimer = false;
              }
            }, 30000);
          }
        }
      }

      return sendit;
    } catch (e) {
      easTimer = false;
      throw e;
    }
  },
};
