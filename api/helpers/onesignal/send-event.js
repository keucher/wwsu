module.exports = {
  friendlyName: "sails.helpers.onesignal.sendEvent",

  description:
    "Send push notifications out for shows / programs that have started or have been changed.",

  inputs: {
    event: {
      type: "json",
      description:
        "Event object triggering the notification. Should be the event exception when updating or canceling.",
    },
    started: {
      type: "boolean",
      defaultsTo: true,
      description:
        "If true, this is a notification that the event is on the air rather than updated or canceled.",
    },
    newSchedule: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, event object is a main calendar event with a new regular event.schedule (for informing subscribers that the show has a permanent time change).",
    },
    removedException: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, the event object, which is an exception, was removed. For example, reversal of cancelations or updates.",
    },
  },

  fn: async function (inputs) {
    try {
      var devices = [];
      var records;

      // No notifications for empty events
      if (!inputs.event || inputs.event === null) return false;

      // Load in subscribers
      if (inputs.event.unique) {
        let temp = inputs.event.unique.split("-");
        var records = await sails.models.subscribers.find({
          type: `calendar`,
          subtype: [
            `${temp[0]}`, // Top calendar event
            `${temp[0]}-null${temp[2] ? `-${temp[2]}` : ``}`, // Calendar schedule
            inputs.event.unique, // Calendar occurrence
          ],
        });
        records.map((record) => devices.push(record.device));
      } else if (inputs.event.scheduleID) {
        records = await sails.models.subscribers.find({
          type: `calendar`,
          or: [
            { subtype: `${inputs.event.calendarID}` }, // Top calendar event
            {
              subtype: {
                startsWith: `${inputs.event.calendarID}-`,
                endsWith: `-${inputs.event.scheduleID}`,
              },
            }, // Schedule and oneTime subscriptions
          ],
        });
        records.map((record) => devices.push(record.device));
      } else {
        records = await sails.models.subscribers.find({
          type: `calendar`,
          or: [
            { subtype: `${inputs.event.calendarID}` }, // Top calendar event
            { subtype: { startsWith: `${inputs.event.calendarID}-` } }, // All other subscriptions
          ],
        });
        records.map((record) => devices.push(record.device));
      }

      // Exit if we have no devices
      if (!devices.length) return true;

      if (inputs.started) {
        await sails.helpers.onesignal.send(
          devices,
          `event`,
          `${inputs.event.name} started!`,
          `${inputs.event.name} is now on the air! Listen at wwsu1069.org or the WWSU app.`,
          (inputs.event.duration || 60) * 60
        );

        // Remove one-time subscriptions
        if (inputs.event.unique)
          await sails.models.subscribers
            .destroy({ type: `calendar`, subtype: inputs.event.unique })
            .fetch();
      } else if (inputs.newSchedule) {
        await sails.helpers.onesignal.send(
          devices,
          `event`,
          `${inputs.event.name} updated broadcast schedule`,
          `${inputs.event.name} updated their regular broadcast schedule. Go to wwsu1069.org or the WWSU app to review the new schedule.`,
          60 * 60 * 24 * 7
        );
      } else if (!inputs.newSchedule && !inputs.removedException) {
        // Changed date/time
        if (
          (inputs.event.scheduleType === "updated" ||
            inputs.event.scheduleType === "updated-system") &&
          (inputs.event.newTime ||
            (inputs.event.originalDuration &&
              inputs.event.originalDuration !== inputs.event.duration))
        ) {
          await sails.helpers.onesignal.send(
            devices,
            `event`,
            `${inputs.event.name} re-scheduled a broadcast`,
            `${inputs.event.name} re-scheduled their ${moment(
              inputs.event.originalTime
            ).format("llll")} broadcast to ${moment(inputs.event.start).format(
              "llll"
            )} - ${moment(inputs.event.end).format("LT")}`,
            60 * 60 * 24 * 7
          );
          // Canceled date/time
        } else if (
          inputs.event.scheduleType === "canceled" ||
          inputs.event.scheduleType === "canceled-system"
        ) {
          await sails.helpers.onesignal.send(
            devices,
            `event`,
            `${inputs.event.name} cancelled a broadcast`,
            `${inputs.event.name} was cancelled for ${moment(
              inputs.event.originalTime
            ).format("llll")}.`,
            60 * 60 * 24 * 7
          );
        }
      } else if (!inputs.newSchedule && inputs.removedException) {
        // reversed Changed date/time
        if (
          inputs.event.scheduleType === "updated" ||
          inputs.event.scheduleType === "updated-system"
        ) {
          await sails.helpers.onesignal.send(
            devices,
            `event`,
            `${inputs.event.name} re-schedule reversed`,
            `${inputs.event.name} had an updated broadcast time of ${moment(
              inputs.event.start
            ).format(
              "llll"
            )}. This was changed back to the original time of ${moment(
              inputs.event.originalTime
            ).format("llll")}.`,
            60 * 60 * 24 * 7
          );
          // reversed Canceled date/time
        } else if (
          inputs.event.scheduleType === "canceled" ||
          inputs.event.scheduleType === "canceled-system"
        ) {
          await sails.helpers.onesignal.send(
            devices,
            `event`,
            `${inputs.event.name} cancelation reversed`,
            `${inputs.event.name} has un-canceled their broadcast for ${moment(
              inputs.event.originalTime
            ).format("llll")}; it will now air at that time.`,
            60 * 60 * 24 * 7
          );
        }
      } else if (!inputs.event.active) {
        await sails.helpers.onesignal.send(
          devices,
          `event`,
          `${inputs.event.name} is discontinued on WWSU`,
          `${inputs.event.name} has been discontinued on WWSU Radio. We have removed your subscriptions to this broadcast automatically.`,
          60 * 60 * 24 * 7
        );
        // Remove all subscriptions
        await sails.models.subscribers
          .destroy({
            type: `calendar`,
            or: [
              { subtype: `${inputs.event.calendarID}` }, // Top calendar event
              { subtype: { startsWith: `${inputs.event.calendarID}-` } }, // All other subscriptions
            ],
          })
          .fetch();
      }

      return true;
    } catch (e) {
      // No erroring if there's an error; just log it and return false to indicate it was not successful
      sails.log.error(e);
      return false;
    }
  },
};
