module.exports = {
  friendlyName: "genre.start",

  description:
    "Start a RadioDJ rotation. Returns false if genre was not queued AND system should see if another genre can be queued. True means either queued, or not queued and we should not seek other genres at this time.",

  inputs: {
    event: {
      type: "json",
      description: "Event object triggering the genre.",
    },
    ignoreChangingState: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Set to true to ignore sails.models.meta.changingState conflict detection.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper genre.start called.");
    sails.log.verbose(inputs.event);
    try {
      // Bail silently if not running RadioDJ
      if (sails.config.custom.radiodjs.length <= 0) return;

      // Block if running an alert
      if (sails.models.meta.memory.altRadioDJ !== null) {
        sails.log.verbose(`genre.start: SKIPPED: An alert is being broadcast.`);
        return true;
      }

      // Do not start a genre if we're not in genre rotation or automation, unless we are in an unknown state, lofi is disabled, and we are not waiting for a good RadioDJ
      if (
        ["automation_on", "automation_genre", "unknown", ""].indexOf(
          sails.models.meta.memory.state
        ) !== -1 &&
        !sails.config.custom.basic.maintenance &&
        !sails.models.status.errorCheck.waitForGoodRadioDJ
      ) {
        sails.log.verbose(
          "genre.start: Passed first check; we are not in a state that disallows genre rotations."
        );
        // Do not start this genre if it was already started
        if (
          sails.models.meta.memory.calendarUnique !==
            (inputs.event === null ? null : inputs.event.unique) ||
          inputs.ignoreChangingState ||
          sails.models.status.errorCheck.playRandomTracks
        ) {
          sails.log.verbose(
            "genre.start: Passed second check; we are not already airing this genre."
          );
          // Do not start a genre if we are in the middle of changing states, unless ignoreChangingState was provided true.
          if (
            sails.models.meta.memory.changingState === null ||
            inputs.ignoreChangingState
          ) {
            sails.log.verbose(
              "genre.start: Passed third check; it is safe to start a genre."
            );
            // Lock future state changes until we are done if ignoreChangingState is false.
            if (!inputs.ignoreChangingState) {
              await sails.helpers.meta.change.with({
                changingState: `Switching to genre`,
              });
            }

            // Find the manual RadioDJ event for Node to trigger
            var query =
              inputs.event !== null
                ? { type: 3, ID: inputs.event.eventID, enabled: "True" }
                : { type: 3, name: "Default", enabled: "True" };
            var event = await sails.models.events.find(query);
            sails.log.verbose(
              `sails.models.events returned ${event.length} matched events, but we're only going to use the first one.`
            );
            sails.log.silly(event);

            // We cannot find the event. Throw an error.
            if (event.length <= 0) {
              if (!inputs.ignoreChangingState) {
                await sails.helpers.meta.change.with({ changingState: null });
              }
              throw new Error(
                `genre.start: FAILED; could not find an active manual event in RadioDJ to trigger the genre rotation for event ID ${
                  inputs.event
                    ? inputs.event.eventID
                    : "(whichever event has the name Default)"
                }`
              );
            }

            await sails.helpers.rest.cmd("RefreshEvents", 0, 10000); // Reload events in RadioDJ just in case

            await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Don't want RadioDJ queuing tracks until we have switched rotations
            await sails.helpers.songs.remove(
              true,
              sails.config.custom.subcats.noClearGeneral,
              true
            ); // We want the rotation change to be immediate; clear out any music tracks in the queue. But, leave requested tracks in the queue.
            await sails.helpers.rest.cmd("EnableAssisted", 0);

            await sails.helpers.rest.cmd("RunEvent", event[0].ID, 5000); // This triggers the change in rotation.
            if (!sails.models.meta.memory.playing) {
              await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
            }
            await sails.helpers.rest.cmd("EnableAutoDJ", 1);

            // If we are going back to default rotation, we don't want to activate genre mode; leave in automation_on mode
            if (inputs.event !== null) {
              await sails.helpers.meta.change.with({
                state: "automation_genre",
                genre: inputs.event.name,
                playlistPlayed: moment().format("YYYY-MM-DD HH:mm:ss"),
              });
              await sails.helpers.meta.newShow();
            } else {
              await sails.helpers.meta.change.with({
                state: "automation_on",
                genre: "Default",
                playlistPlayed: moment().format("YYYY-MM-DD HH:mm:ss"),
              });
              await sails.helpers.meta.newShow();
              await sails.models.logs
                .create({
                  attendanceID: sails.models.meta.memory.attendanceID,
                  logtype: "sign-on",
                  loglevel: "primary",
                  logsubtype: "",
                  logIcon: `fas fa-music`,
                  title: `Default rotation started.`,
                  event: "",
                })
                .fetch()
                .tolerate((err) => {
                  sails.log.error(err);
                });
            }
            if (!inputs.ignoreChangingState) {
              await sails.helpers.meta.change.with({ changingState: null });
            }
            return true;
          } else {
            sails.log.verbose(
              "genre.start: SKIPPED; we are changing states right now."
            );
            return true;
          }
        } else {
          sails.log.verbose("genre.start: SKIPPED; Genre is ongoing.");
          return true;
        }
      } else {
        sails.log.verbose("genre.start: SKIPPED; We are not in automation.");
        return true;
      }
    } catch (e) {
      sails.log.verbose("genre.start: ERROR.");
      if (!inputs.ignoreChangingState) {
        await sails.helpers.meta.change.with({ changingState: null });
      }
      throw e;
    }
  },
};
