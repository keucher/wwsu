module.exports = {
  friendlyName: "Update",

  description: "Update guardian RSS data.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    let problems = false;
    try {
      sails.log.debug("GUARDIAN RSS: Starting.");
      let resp = await needle("get", `https://wsuguardian.com/wprss`, {});
      sails.log.debug("GUARDIAN RSS: body returned.");
      sails.log.debug(resp);
      if (
        resp &&
        resp.body &&
        resp.body.children &&
        resp.body.children.length > 0
      ) {
        let entryIDs = [];

        resp.body.children.forEach((child) => {
          if (
            child.name !== "entry" ||
            !child.children ||
            child.children.length === 0
          )
            return;

          let returnData = { source: "wsuguardian" };
          child.children.forEach((child2) => {
            switch (child2.name) {
              case "id":
                entryIDs.push(child2.value);
                returnData.entryID = child2.value;
                break;
              case "title":
                returnData.title = child2.value;
                break;
              case "updated":
                returnData.date = child2.value;
                break;
              case "summary":
                returnData.summary = child2.value;
                break;
              case "author":
                if (child2.children && child2.children.length > 0) {
                  returnData.author = [];
                  child2.children.forEach((child3) => {
                    if (child3.name !== "name") return;
                    returnData.author.push(child3.value);
                  });
                  returnData.author = returnData.author.join(", ");
                }
                break;
            }
          });

          sails.log.debug("GUARDIAN RSS: RSS feed parsed.");
          sails.log.debug(returnData);

          let criteria = _.cloneDeep(returnData);
          sails.models.rss
            .findOrCreate({ entryID: returnData.entryID }, criteria)
            .exec((err, record, wasCreated) => {
              if (err) {
                sails.log.error(err);
                sails.helpers.status.modify
                  .with({
                    name: "rss-wsuguardian",
                    label: "WSUGuardian RSS",
                    status: 3,
                    summary: `Database error.`,
                    data: `There was an error with the RSS database when finding/creating entryID ${returnData.entryID}.`,
                  })
                  .exec(() => {});
                problems = true;
                return;
              }
              if (!wasCreated) {
                let updateIt = false;
                for (let key in returnData) {
                  if (
                    key === "date" &&
                    !moment(record[key]).isSame(moment(returnData[key]))
                  ) {
                    updateIt = true;
                    break;
                  }
                  if (
                    (key !== "date" && typeof record[key] === "undefined") ||
                    record[key] !== returnData[key]
                  ) {
                    updateIt = true;
                    break;
                  }
                }
                if (updateIt) {
                  criteria = _.cloneDeep(returnData);
                  sails.models.rss
                    .updateOne({ entryID: returnData.entryID }, criteria)
                    .exec((err) => {
                      if (err) {
                        sails.log.error(err);
                        sails.helpers.status.modify
                          .with({
                            name: "rss-wsuguardian",
                            label: "WSUGuardian RSS",
                            status: 3,
                            summary: `Database error.`,
                            data: `There was an error with the RSS database when updating entryID ${returnData.entryID}.`,
                          })
                          .exec(() => {});
                        problems = true;
                        return;
                      }
                    });
                }
              }
            });
        });

        // If we had a body, now we remove any entries no longer being returned from the source
        sails.models.rss
          .destroy({ entryID: { nin: entryIDs }, source: "wsuguardian" })
          .fetch()
          .exec((err) => {
            if (err) {
              sails.log.error(err);
              sails.helpers.status.modify
                .with({
                  name: "rss-wsuguardian",
                  label: "WSUGuardian RSS",
                  status: 3,
                  summary: `Database error.`,
                  data: `There was an error destroying RSS records in the database that no longer exist.`,
                })
                .exec(() => {});
              problems = true;
              return;
            }
          });

        setTimeout(async () => {
          if (!problems)
            await sails.helpers.status.modify.with({
              name: "rss-wsuguardian",
              label: "WSUGuardian RSS",
              status: 5,
              summary: `Operational.`,
              data: "The wsuguardian RSS feed is operational.",
            });
        }, 15000);
      } else {
        await sails.helpers.status.modify.with({
          name: "rss-wsuguardian",
          label: "WSUGuardian RSS",
          status: 3,
          summary: `Invalid RSS feed or RSS feed offline.`,
          data: `The wsuguardian feed URL did not return a proper RSS feed.
          <br /><strong>TO FIX / CHECK:</strong>
          <ul>
            <li>Make sure the URL for the feed to the Guardian Media Group's posts is correct in administration DJ Controls -> System Settings -> The Guardian Media Group.</li>
            <li>Make sure the URL is returning a proper RSS feed.</li>
            <li>Make sure the network connection on the server is good.</li>
          </ul>`,
        });
      }
    } catch (e) {
      sails.log.error(e);
      await sails.helpers.status.modify.with({
        name: "rss-wsuguardian",
        label: "WSUGuardian RSS",
        status: 3,
        summary: `Internal error; see server logs.`,
        data: "There was an error encountered when trying to load the wsuguardian RSS feed. Please see the server error logs.",
      });
    }
  },
};
