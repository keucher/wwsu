require("moment-duration-format");

module.exports = {
  friendlyName: "requests.checkRequestable",

  description: "Check to see if a track can be requested by the client.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID number of the Song to check for request ability.",
    },
    IP: {
      type: "string",
      required: true,
      description: "The IP address of the client to check for request limits.",
    },
  },

  fn: async function (inputs, exits) {
    // Bail silently if not running RadioDJ
    if (sails.config.custom.radiodjs.length <= 0) return exits.success({});

    let d = moment().startOf("day").format("YYYY-MM-DD HH:mm:ss");

    // First, check to see if the client has already exceeded their requests for the day
    let requests = await sails.models.requests.find({
      userIP: inputs.IP,
      requested: { ">=": d },
    });
    if (requests.length >= sails.config.custom.basic.requestsDailyLimit) {
      return {
        requestable: false,
        message: `You have reached your daily request limit (${sails.config.custom.basic.requestsDailyLimit}). Please check back tomorrow.`,
        listDiv: "warning",
        type: "requestRules",
      };
    }

    let record = await sails.models.songs.findOne({ ID: inputs.ID });

    // Next, confirm the track ID actually exists
    if (!record) {
      return exits.success({
        requestable: false,
        message: `The track ID provided was not found.`,
        listDiv: "danger",
        type: "internal",
      });
    }

    // Is the track disabled?
    if (record.enabled !== 1) {
      return exits.success({
        requestable: false,
        message: `The track is disabled and cannot be requested.`,
        listDiv: "warning",
        type: "disabled",
      });
    }

    // Next, check if the provided track has already been requested and is pending to air
    let requests2 = await sails.models.requests.find({
      songID: inputs.ID,
      played: 0,
    });
    if (requests2.length > 0) {
      return exits.success({
        requestable: false,
        message: `The track has recently been requested and is pending to air.`,
        listDiv: "warning",
        type: "inQueue",
      });
    }

    // Also check if the track is in the current automation queue.
    let inQueue = false;
    sails.models.meta.automation
      .filter((track) => parseInt(track.ID) === inputs.ID)
      .map(() => {
        inQueue = true;
      });
    if (inQueue) {
      return exits.success({
        requestable: false,
        message: `The track is in the automation queue and pending to air shortly.`,
        listDiv: "warning",
        type: "inQueue",
      });
    }

    // Check if the track exists in any of the music categories. If not, it cannot be requested.
    let subcat = await sails.models.subcategory.findOne({
      ID: record.id_subcat,
    });
    if (sails.config.custom.subcats.music.indexOf(subcat.ID) === -1) {
      return exits.success({
        requestable: false,
        message: `The track is not requestable.`,
        listDiv: "warning",
        type: "nonMusic",
      });
    }

    // The rest of the checks are based off of track rotation rule settings saved in the database via RadioDJ
    let thesettings = await sails.models.settings.find({
      source: "settings_general",
      setting: [
        "RepeatTrackInterval",
        "RepeatArtistInteval",
        "RepeatAlbumInteval",
        "RepeatTitleInteval",
      ],
    });
    let rotationRules = {};
    thesettings.map((thesetting) => {
      rotationRules[thesetting.setting] = thesetting.value;
    });

    // Check if we are past the end date of the track
    if (
      moment(record.end_date).isBefore() &&
      moment(record.end_date).isAfter("2002-01-01 00:00:01")
    ) {
      return exits.success({
        requestable: false,
        message: `The track has expired.`,
        listDiv: "warning",
        type: "expired",
      });
    }

    // Check if we have not yet reached the start date of the track
    if (moment(record.start_date).isAfter()) {
      return exits.success({
        requestable: false,
        message: `The track's start date/time has not been reached yet.`,
        listDiv: "warning",
        type: "expired",
      });
    }

    // Check if the track has exceeded the number of allowed spin counts
    if (record.limit_action > 0 && record.count_played >= record.play_limit) {
      return exits.success({
        requestable: false,
        message: `The track has reached its maximum allowed spins.`,
        listDiv: "dark",
        type: "expired",
      });
    }

    // Check rotation rules
    let passesRules = true;
    let rulesFailed = ``;
    if (
      moment(record.date_played).isAfter(
        moment().subtract(rotationRules.RepeatTrackInterval, "minutes")
      )
    ) {
      passesRules = false;
      rulesFailed += `<br>*This track played recently. Wait about ${moment().to(
        moment(record.date_played).add(
          rotationRules.RepeatTrackInterval,
          "minutes"
        ),
        true
      )}`;
    }
    if (
      moment(record.title_played).isAfter(
        moment().subtract(rotationRules.RepeatTitleInteval, "minutes")
      )
    ) {
      passesRules = false;
      rulesFailed += `<br>*A track with same title played recently. Wait about ${moment().to(
        moment(record.title_played).add(
          rotationRules.RepeatTitleInteval,
          "minutes"
        ),
        true
      )}`;
    }
    if (
      moment(record.artist_played).isAfter(
        moment().subtract(rotationRules.RepeatArtistInteval, "minutes")
      )
    ) {
      passesRules = false;
      rulesFailed += `<br>*A track from the same artist played recently. Wait about ${moment().to(
        moment(record.artist_played).add(
          rotationRules.RepeatArtistInteval,
          "minutes"
        ),
        true
      )}`;
    }
    if (
      moment(record.album_played).isAfter(
        moment().subtract(rotationRules.RepeatAlbumInteval, "minutes")
      )
    ) {
      passesRules = false;
      rulesFailed += `<br>*A track from the same album played recently. Wait about ${moment().to(
        moment(record.album_played).add(
          rotationRules.RepeatAlbumInteval,
          "minutes"
        ),
        true
      )}`;
    }
    if (!passesRules) {
      sails.log.verbose(
        `Track cannot be requested: Fails rotation rules: ${rulesFailed}`
      );
      return exits.success({
        requestable: false,
        message: `The track fails one or more playlist rotation rules and cannot be requested at this time:${rulesFailed}`,
        listDiv: "warning",
        type: "rotationRules",
      });

      // By this point, all rules passed and the track can be requested. Include the request form.
    } else {
      return exits.success({
        requestable: true,
        message: `This track can be requested`,
        listDiv: "success",
        type: "requestable",
      });
    }
  },
};
