module.exports = {
  friendlyName: "sails.helpers.songs.reloadSubcategories",

  description:
    "Re-generate sails.config.custom.subcats, sails.config.custom.basic.sportscats, and sails.config.custom.showcats",

  inputs: {},

  fn: async function (inputs) {
    // Bail if not using RadioDJ
    if (sails.config.custom.radiodjs.length <= 0) {
      return;
    }

    var categories;
    var subcategories;
    var catIDs = [];
    var cats = {};

    // Load subcats IDs for each consigured categories
    let maps = sails.config.custom.categories.map(async (record) => {
      sails.config.custom.subcats[record.name] = [];

      for (let cat in record.categories) {
        if (Object.prototype.hasOwnProperty.call(record.categories, cat)) {
          let thecategory = await sails.models.category
            .findOne({ name: cat })
            .tolerate(() => {});
          if (!thecategory || thecategory === null) {
            continue;
          }

          let thesubcategories;
          if (record.categories[cat].length <= 0) {
            thesubcategories = await sails.models.subcategory
              .find({ parentid: thecategory.ID })
              .tolerate(() => {});
          } else {
            thesubcategories = await sails.models.subcategory
              .find({
                parentid: thecategory.ID,
                name: record.categories[cat],
              })
              .tolerate(() => {});
          }
          if (!thesubcategories || thesubcategories.length <= 0) {
            continue;
          }

          thesubcategories.forEach((thesubcategory) => {
            sails.config.custom.subcats[record.name].push(thesubcategory.ID);
          });
        }
      }
    });
    await Promise.all(maps);

    // Load subcats IDs for each consigured sport
    sails.log.verbose(`BOOTSTRAP: Loading sportscats into configuration.`);
    sails.config.custom.sportscats = {};
    sails.config.custom.sports.forEach((sport) => {
      sails.config.custom.sportscats[sport.name] = {
        "Sports Openers": null,
        "Sports Liners": null,
        "Sports Closers": null,
        "Sports Returns": null,
      };
    });

    categories = await sails.models.category
      .find({
        name: [
          "Sports Openers",
          "Sports Liners",
          "Sports Closers",
          "Sports Returns",
        ],
      })
      .tolerate(() => {});

    catIDs = [];
    cats = {};

    if (categories.length > 0) {
      categories.forEach((category) => {
        catIDs.push(category.ID);
        cats[category.ID] = category.name;
      });
    }

    subcategories = await sails.models.subcategory
      .find({ parentid: catIDs })
      .tolerate(() => {});

    if (subcategories.length > 0) {
      subcategories.forEach((subcategory) => {
        if (
          typeof sails.config.custom.sportscats[subcategory.name] !==
          "undefined"
        ) {
          sails.config.custom.sportscats[subcategory.name][
            cats[subcategory.parentid]
          ] = subcategory.ID;
        }
      });
    }

    // Load subcats IDs for each show
    sails.log.verbose(`BOOTSTRAP: Loading showcats into configuration.`);

    categories = await sails.models.category
      .find({ name: ["Show Openers", "Show Returns", "Show Closers"] })
      .tolerate(() => {});

    catIDs = [];
    cats = {};

    if (categories.length > 0) {
      categories.forEach((category) => {
        catIDs.push(category.ID);
        cats[category.ID] = category.name;
      });
    }

    subcategories = await sails.models.subcategory
      .find({ parentid: catIDs })
      .tolerate(() => {});

    if (subcategories.length > 0) {
      sails.config.custom.showcats = {};
      subcategories.forEach((subcategory) => {
        if (
          typeof sails.config.custom.showcats[subcategory.name] === "undefined"
        ) {
          sails.config.custom.showcats[subcategory.name] = {
            "Show Openers": null,
            "Show Returns": null,
            "Show Closers": null,
          };
        }
        sails.config.custom.showcats[subcategory.name][
          cats[subcategory.parentid]
        ] = subcategory.ID;
      });
    }
  },
};
