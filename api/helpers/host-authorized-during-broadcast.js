module.exports = {
  friendlyName: "Host authorized during broadcast",

  description:
    "Does the current host have admin permission or it / its org member started the current broadcast?",

  inputs: {
    payload: {
      type: "json",
      required: true,
      description: "The payload of the authorized host.",
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    return (
      inputs.payload.admin || // Host has admin permission
      inputs.payload.ID === sails.models.meta.memory.host || // Host started current broadcast
      inputs.payload.belongsTo === null || // Host is a WWSU computer / not set to an org member
      inputs.payload.belongsTo === sails.models.meta.memory.hostDJ ||
      inputs.payload.belongsTo === sails.models.meta.memory.cohostDJ1 ||
      inputs.payload.belongsTo === sails.models.meta.memory.cohostDJ2 ||
      inputs.payload.belongsTo === sails.models.meta.memory.cohostDJ3 // Host's belongsTo member is the host or co-host of the current broadcast
    );
  },
};
