"use strict";
// Manager for the blogs system on the website.
// Does not use WWSUdb; would take too much memory for large blog databases.

// REQUIRES these WWSUmodules: WWSUevents, WWSUnavigation, WWSUMeta,

class WWSUblogsweb extends WWSUevents {
	/**
	 * The class constructor.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options = {}) {
		super();

		this.manager = manager;
		this.options = options;

		// DOM
		this.search = {
			button: undefined,
			box: undefined,
		};
		this.blogContainer;
		this.blogList = undefined;
		this.moreButton = undefined;

		this.lastID = 0;
		this.category = undefined;
		this.cache = new Map();

		this.endpoints = {
			add: "POST /api/blogs/web",
			getOne: "GET /api/blogs/web/:ID",
			get: "GET /api/blogs/web",
		};

		// Custom socket
		this.manager.socket.on("blogs-web", (data) => {
			for (let action in data) {
				if (!Object.prototype.hasOwnProperty.call(data, action)) continue;

				// TODO

				switch (action) {
					case "insert":
						break;
					case "update":
						break;
					case "remove":
						break;
				}
			}
		});

		// Navigation event emitter for replacing the search box with previous search queries
		this.manager
			.get("WWSUNavigation")
			.on("popState", "WWSUblogsweb", (data) => {
				if (!data.WWSUblogsweb || !data.WWSUblogsweb.search) return;
				$(this.search.box).val(data.WWSUblogsweb.search);
			});
	}

	/**
	 * Initialize web blogs system. Should be called immediately after loading the module.
	 *
	 * @param {string} blogContainer DOM query string of the entire container used for blog post browsing.
	 * @param {string} blogList DOM query string where blog cards are placed.
	 * @param {string} moreButton DOM query string of the "Load More Blog Posts" button.
	 * @param {string} searchButton DOM query string of the search button.
	 * @param {string} searchBox DOM query string of the input box for search terms.
	 */
	init(blogContainer, blogList, moreButton, searchButton, searchBox) {
		if (this.search.button) $(this.search.button).off("click");
		if (this.moreButton) $(this.moreButton).off("click");

		// Load DOMs into memory
		this.blogContainer = blogContainer;
		this.blogList = blogList;
		this.moreButton = moreButton;
		this.search.button = searchButton;
		this.search.box = searchBox;

		// Add click handler for blog searching
		$(this.search.button).on("click", () => {
			this.lastID = 0;
			$(this.blogList).html("");
			this.getBlogs({
				limit: 9,
				search: $(this.search.box).val(),
				category: this.category,
			});
		});

		// More button click
		$(this.moreButton).on("click", () => {
			this.getBlogs({
				limit: 9,
				search: $(this.search.box).val(),
				category: this.category,
			});
		});

		// Add pushState data to WWSUNavigation
		this.manager.get("WWSUNavigation").pushStateFns.set("WWSUblogsweb", () => {
			return {
				WWSUblogsweb: {
					search: $(this.search.box).val(), // We should remember search queries from past history
				},
			};
		});
	}

	/**
	 * This should be called whenever we want to view/refresh blogs of a category.
	 *
	 * @param {boolean} reload Whether or not to actually reload blog posts from the API. Otherwise, will load cached posts.
	 * @param {string} category The category of blog posts to retrieve; do not define to retrieve all.
	 */
	list(reload, category) {
		this.category = category;
		this.lastID = 0;
		$(this.blogList).html("");

		if (reload) {
			// Check the API for blog posts and load them.
			this.getBlogs({
				limit: 9,
				search: $(this.search.box).val(),
				category: this.category,
			});
		} else {
			// Display blog posts from the cache
			this.cache.forEach((post, id) => {
				// Skip blogs not in this category if a category was defined
				if (category && !id.startsWith(`${category}-`)) return;

				// Get the actual ID
				let ID = id.split("-")[1];

				// Mark the last ID when we fetch more posts.
				if (ID > this.lastID) this.lastID = ID;

				// Populate the cards
				$(this.blogList).append(this.summaryCard(post));
			});
			// Make sure button to load more blog posts is active; do even if cache is empty as there may have been a new post made that was not yet fetched.
			$(this.moreButton).prop("disabled", false);
			$(this.moreButton).removeClass("btn-outline-danger");
			$(this.moreButton).addClass("btn-secondary");
			$(this.moreButton).html("Load More Blog Posts");
		}
	}

	/**
	 * Get blog posts from the WWSU API and populate them on the page.
	 *
	 * @param {object} data Data to pass to the API.
	 * @param {function} cb Callback executed with the body passed as a parameter.
	 */
	getBlogs(data, cb) {
		if (typeof data.lastID === "undefined") data.lastID = this.lastID;
		this.manager.get("noReq").request(
			this.endpoints.get,
			{
				data: data,
			},
			{
				dom: this.blogContainer,
			},
			(body, res) => {
				if (res.statusCode >= 400 || body.constructor !== Array) {
					if (typeof cb === "function") cb(false);
				} else {
					if (body.length > 0) {
						this.lastID = 0;
						body.forEach((post) => {
							// Mark the last ID when we fetch more posts.
							if (post.ID > this.lastID) this.lastID = post.ID;

							// Populate the cards
							$(this.blogList).append(this.summaryCard(post));

							// Add to cache
							this.cache.set(`${data.category}-${post.ID}`, post);
						});

						// Re-add click handlers for the cards via WWSUnavigation
						this.manager.get("WWSUNavigation").addItem(
							".nav-blog",
							"#section-blog",
							(e) =>
								e
									? `${$(e).find(".card-title").first().html()} - WWSU 106.9 FM`
									: `Blog Post ${$(e).data("id")} - WWSU 106.9 FM`,
							(e) =>
								`/blog${
									e ? `/${$(e).data("id")}/${$(e).data("monikor")}` : ``
								}`,
							false,
							(e, popState) => {
								this.loadBlogPost($(e).data("id"));
							}
						);

						// Make sure button to load more blog posts is active
						$(this.moreButton).prop("disabled", false);
						$(this.moreButton).removeClass("btn-outline-danger");
						$(this.moreButton).addClass("btn-secondary");
						$(this.moreButton).html("Load More Blog Posts");
					} else {
						// No more blog posts; disable button to load more
						$(this.moreButton).prop("disabled", true);
						$(this.moreButton).addClass("btn-outline-danger");
						$(this.moreButton).removeClass("btn-secondary");
						$(this.moreButton).html("No More Blog Posts");
					}
					if (typeof cb === "function") cb(body);
				}
			}
		);
	}

	/**
	 * Load a single blog post entry.
	 *
	 * @param {number} id ID of the post to load / show
	 * @param {function} cb Optional callback with body data as parameter
	 */
	loadBlogPost(id, cb) {
		this.manager.get("noReq").request(
			this.endpoints.getOne,
			{
				data: { ID: id },
			},
			{
				dom: `#section-blog`,
			},
			(body, res) => {
				if (res.statusCode >= 400 || !body || !body.ID) {
					if (typeof cb === "function") cb(false);
				} else {
					if (typeof cb === "function") cb(body);

					this.manager.get("WWSUNavigation").addItem(
						".nav-blogs-subcategory",
						"#section-blogs",
						(e) =>
							e
								? `${$(e).data("category") || "All"} Blog Posts - WWSU 106.9 FM`
								: `All Blog Posts - WWSU 106.9 FM`,
						(e) =>
							`/blogs${
								e && $(e).data("category") ? `/${$(e).data("category")}` : ``
							}`,
						false,
						(e, popState) => {
							this.manager
								.get("WWSUNavigation")
								.processMenu(
									`#nav-blogs${
										e && $(e).data("category")
											? `-${$(e).data("category")}`
											: ``
									}`
								);
						}
					);

					// Author constant function

					const updateAuthorInfo = (author) => {
						// Author information
						if (author) {
							$("#section-blog-author-card").removeClass("d-none");
							$("#section-blog-author").html(
								author.realName || author.name || "Unknown Author"
							);
							$("#section-blog-author-card-realName").html(
								author.realName || `Unknown Author Name`
							);
							$("#section-blog-author-card-name").html(
								author.realName || `Unknown Author Nickname`
							);
							$("#section-blog-author-card-profile").html(
								author.profile || `No profile provided`
							);
							if (author.avatar) {
								$("#section-blog-author-avatar").removeClass("d-none");
								$("#section-blog-author-avatar").attr(
									"src",
									`${this.manager.socket.url}/api/uploads/${author.avatar}`
								);
								$("#section-blog-author-card-avatar").removeClass("d-none");
								$("#section-blog-author-card-avatar").attr(
									"src",
									`${this.manager.socket.url}/api/uploads/${author.avatar}`
								);
							} else {
								$("#section-blog-author-avatar").addClass("d-none");
								$("#section-blog-author-card-avatar").addClass("d-none");
							}
						} else {
							$("#section-blog-author-card").addClass("d-none");
							$("#section-blog-author-avatar").addClass("d-none");
							$("#section-blog-author").html("Unknown Author");
						}
					};

					// Get post author
					let author;
					if (body.author) {
						author = this.manager
							.get("WWSUdjs")
							.find({ ID: body.author }, true);

						// No author found? Maybe WWSUdjs has not loaded yet. Add a one-off event handler.
						if (!author) {
							this.manager
								.get("WWSUdjs")
								.once("change", "WWSUblogsweb", (db) => {
									author = this.manager
										.get("WWSUdjs")
										.find({ ID: body.author }, true);
									updateAuthorInfo(author);
								});
						}
					}

					updateAuthorInfo(author);

					// Post title
					$("#section-blog-title").html(body.title);

					// Publication date
					$("#section-blog-publication").html(
						`Published ${moment(body.starts || body.createdAt).format(
							"MMM D, YYYY, h:mm A"
						)}`
					);

					// Tags
					let tagsHtml = ``;
					if (body.tags && body.tags.length > 0) {
						tagsHtml = `Tags: ${body.tags
							.map(
								(tag) =>
									`<span class="p-1 m-1 badge badge-secondary nav-blogs-tag" style="cursor: pointer;" tabindex="-1" role="link" data-search="${tag}" aria-label="${tag} tag">${tag}</span>`
							)
							.join("")}`;
					}
					$("#section-blog-tags").html(tagsHtml);

					// Tags click event via navigation
					this.manager
						.get("WWSUNavigation")
						.addItem(
							".nav-blogs-tag",
							"#section-blogs",
							`Blog Posts - WWSU 106.9 FM`,
							`/blogs`,
							false,
							(e, popState) => {
								$(this.search.box).val($(e).data("search"));
								this.lastID = 0;
								$(this.blogList).html("");
								this.getBlogs({
									limit: 9,
									search: $(this.search.box).val(),
								});
							}
						);

					// Featured image
					if (body.featuredImage) {
						$("#section-blog-featuredImage").removeClass("d-none");
						$("#section-blog-featuredImage").attr(
							"src",
							`${this.manager.socket.url}/api/uploads/${body.featuredImage}`
						);
					} else {
						$("#section-blog-featuredImage").addClass("d-none");
					}

					// Post contents
					$("#section-blog-contents").html(body.contents);
				}
			}
		);
	}

	/**
	 * Generate HTML for a small blog summary card.
	 * Remember to re-bind nav-blog.
	 *
	 * @param {object} blog The blog record.
	 * @returns {string} HTML for the summary
	 */
	summaryCard(blog) {
		return `
    <div class="col-md-12 col-lg-6 col-xl-4">
    <div
      class="card mb-2"
      style="
        background: linear-gradient(
            rgba(0, 0, 0, 0.75),
            rgba(0, 0, 0, 0.75)
          )${
						blog.featuredImage
							? `, url('${this.manager.socket.url}/api/uploads/${blog.featuredImage}') center center`
							: ``
					};
        min-height: 275px;
      "
    >
      <a
        class="
          card-img-overlay
          d-flex
          flex-column
          justify-content-end
          nav-blog
        "
        data-id="${blog.ID}"
        data-monikor="${this.toMonikor(blog.title)}"
        href="#"
      >
        <h2
          class="
            card-title
            text-white
            font-weight-bold
            h5
          "
        >
          ${blog.title}
        </h2>
        <p class="card-text text-white pb-2 pt-1">
          ${blog.summary}
        </p>
        <div class="text-white">Published ${moment(
					blog.starts || blog.createdAt
				).format("MMM D, YYYY, h:mm A")}</div>
      </a>
    </div>
    </div>`;
	}

	/**
	 * Convert a blog title to a monikor
	 *
	 * @param {string} title Title of the blog
	 * @returns {string} URL monikor
	 */
	toMonikor(title) {
		return title
			.toLowerCase() // Make everything lower case
			.replace(/\s/g, "-") // Replace all whitespace with a hyphen
			.replace("&", "and") // Replace ampersands with the word and
			.replace("_", "-") // Replace underscores with hyphens
			.replace(/[^0-9a-z\-]/gi, ""); // Remove all remaining non-alphanumeric characters unless it's a hyphen
	}
}
