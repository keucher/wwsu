"use strict";

// This class manages the WWSU status system

// REQUIRES these WWSUmodules: noReq (WWSUreq), hostReq (if reporting issues with recorder)
class WWSUstatus extends WWSUdb {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 * @param {Array} options.filter If specified, we will only monitor statuses whose name is specified in the array of strings.
	 */
	constructor(manager, options = {}) {
		super(); // Create the db

		this.options = options;

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/status",
			report: "POST /api/status",
			status: "PUT /api/status/:name",
		};

		this.assignSocketEvent("status", this.manager.socket);

		this.statusModal = new WWSUmodal(
			`Problems Detected with WWSU`,
			null,
			``,
			true,
			{
				overlayClose: true,
				zindex: 1100,
				timeout: 180000,
				timeoutProgressbar: true,
			}
		);

    this.on("replace", "WWSUstatus", () => {
      // WWSUmodules loading DOM check
      this.initialized = true;
      this.manager.checkInitialized();
    });
	}

	// Start the connection. Call this in socket connect event.
	init() {
    this.initialized = false;
    this.manager.checkInitialized();

		this.replaceData(this.manager.get("noReq"), this.endpoints.get, {
			filter: this.options.filter,
		});
	}

	/**
	 * Report a problem to WWSU.
	 *
	 * @param {string} dom DOM query string that should be blocked when processing report (typically the report form).
	 * @param {object} data Data to pass to the API
	 * @param {?function} cb Callback function with true for success, false for failure.
	 */
	report(dom, data, cb) {
		this.manager
			.get("noReq")
			.request(this.endpoints.report, { data }, { dom }, (body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") {
						cb(false);
					}
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Problem reported",
						autohide: true,
						delay: 10000,
						body: `The issue has been reported, and directors will be notified.`,
					});
					if (typeof cb === "function") {
						cb(true);
					}
				}
			});
	}

	/**
	 * Report current status of a subsystem to WWSU.
	 *
	 * @param {object} data Data to send to the WWSU API.
	 * @param {?function} cb Callback function with true for success, false for failure.
	 */
	status(data, cb) {
		this.manager
			.get("hostReq")
			.request(
				this.endpoints.status,
				{ data },
				{ hideErrorToast: true },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") {
							cb(false);
						}
					} else {
						if (typeof cb === "function") {
							cb(true);
						}
					}
				}
			);
	}

	/**
	 * Initialize a "report problem" Alpaca form.
	 *
	 * @param {string} location Descriptor used in the report to determine where the problem was reported.
	 * @param {string} dom DOM query string where to put the form.
	 */
	initReportForm(location, dom) {
		$(dom).alpaca({
			schema: {
				type: "object",
				properties: {
					information: {
						type: "string",
						title: "Describe the problem",
						maxLength: 1024,
						required: true,
					},
				},
			},
			options: {
				fields: {
					information: {
						type: "textarea",
						helper:
							"Explain concisely what problem you are having, what you did leading up to the problem, and any errors you received. Do NOT include personal information / passwords / etc (reports are public). You have a 1024 character limit.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: "Submit Problem",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									form.focus();
									return;
								}
								let value = form.getValue();
								this.report(
									dom,
									{
										location: location,
										information: value.information,
									},
									(success) => {
										if (success) {
											form.clear();
										}
									}
								);
							},
						},
					},
				},
			},
		});
	}
}
