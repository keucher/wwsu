"use strict";

/**
 * This class manages WWSU sports scoreboard overlay data.
 *
 * @requires WWSUdb
 * @requires noReq from WWSUreq
 */
class WWSUsports extends WWSUdb {
  /**
   * Construct the class
   *
   * @param {WWSUmanager} manager The manager that initiated this class.
   * @param {object} options Object of options to pass to this class.
   */
  constructor(manager, options) {
    super();

    this.manager = manager;

    this.endpoints = {
      get: "GET /api/sports",
      update: "PUT /api/sports/:name",
    };

    this.assignSocketEvent("sports", this.manager.socket);

    this.on("replace", "WWSUsports", () => {
      // WWSUmodules loading DOM check
      this.initialized = true;
      this.manager.checkInitialized();
    });
  }

  /**
   * Initialize the connection to WWSU sockets and add event listeners.
   */
  init() {
    this.initialized = false;
    this.manager.checkInitialized();

    this.replaceData(this.manager.get("noReq"), this.endpoints.get, {});
  }

  /**
   * Update a value for sports.
   *
   * @param {string} key
   * @param {string} value
   */
  updateValue(key, value) {
    let temp = this.find({ name: key }, true);
    if (!temp || typeof temp.value === `undefined` || temp.value !== value) {
      this.manager.get("noReq").request(
        this.endpoints.update,
        {
          data: { name: key, value: value },
        },
        {},
        (body, resp) => {}
      );
    }
  }
}

/**
 * This class manages the actual scoreboard overlay.
 *
 * @requires $.animateCss jQuery animateCSS extension
 */
class WWSUscoreboard {
  /**
   * Create the scoreboard class.
   *
   * @param {WWSUmodules} manager The WWSUmodules that initiated this class.
   * @param {object} options Options to pass to the module.
   * @param {string} options.main DOM query string of the entire scoreboard
   * @param {string} options.wsuScore DOM query string of WSU's score
   * @param {string} options.oppScore DOM query string of opponent's score
   * @param {string} options.wsuNum DOM query string of secondary info number for WSU (eg. number of fouls / timeouts etc)
   * @param {string} options.oppNum DOM query string of secondary info number for opponent
   * @param {string} options.wsuText DOM query string of secondary info text for WSU
   * @param {string} options.oppText DOM query string of secondary info text for opponent
   */
  constructor(manager, options = {}) {
    this.manager = manager;

    this.ID = Math.floor(1000000 + Math.random() * 1000000);
    this._main = options.main;
    this._wsuScore = options.wsuScore;
    this._wsuScoreValue = 0;
    this._oppScore = options.oppScore;
    this._oppScoreValue = 0;
    this._wsuNum = options.wsuNum;
    this._wsuNumValue = null;
    this._oppNum = options.oppNum;
    this._oppNumValue = null;
    this._wsuText = options.wsuText;
    this._wsuTextValue = null;
    this._oppText = options.oppText;
    this._oppTextValue = null;
  }

  /**
   * Fade the scoreboard out
   */
  hide() {
    $(this._main).fadeTo(500, 0);
  }

  /**
   * Fade the scoreboard in
   */
  show() {
    $(this._main).fadeTo(500, 1);
  }

  /**
   * Sets WSU's score. Also flashes the number if the score increased via animate.css.
   * @param {string|number} value New score
   */
  set wsuScore(value) {
    var temp = document.querySelector(this._wsuScore);
    if (temp !== null) {
      temp.innerHTML = value;
      if (value === null || value === ``) {
        $(this._wsuScore).fadeTo(500, 0);
      }
      if (
        value !== null &&
        value !== `` &&
        (this._wsuScoreValue === null || this._wsuScoreValue === ``)
      ) {
        $(this._wsuScore).fadeTo(500, 1);
      }
      if (value > this._wsuScoreValue) {
        $(this._wsuScore).animateCss("heartBeat slower");
      }
    }
    this._wsuScoreValue = value;
  }

  /**
   * Sets opponent's score. Also flashes the number if the score increased via animate.css.
   * @param {string|number} value New score
   */
  set oppScore(value) {
    var temp = document.querySelector(this._oppScore);
    if (temp !== null) {
      temp.innerHTML = value;
      if (value === null || value === ``) {
        $(this._oppScore).fadeTo(500, 0);
      }
      if (
        value !== null &&
        value !== `` &&
        (this._oppScoreValue === null || this._oppScoreValue === ``)
      ) {
        $(this._oppScore).fadeTo(500, 1);
      }
      if (value > this._oppScoreValue) {
        $(this._oppScore).animateCss("heartBeat slower");
      }
    }
    this._oppScoreValue = value;
  }

  /**
   * Sets WSU's secondary info number. Also does fading via animate.css.
   * @param {?string|number} value New number. Null or empty hides it.
   */
  set wsuNum(value) {
    var temp = document.querySelector(this._wsuNum);
    if (temp !== null) {
      var _this = this;
      $(this._wsuNum).fadeTo(500, 0, () => {
        temp.innerHTML = value;
        if (value !== null && value !== ``) {
          $(_this._wsuNum).fadeTo(500, 1);
        }
      });
    }
    this._wsuNumValue = value;
  }

  /**
   * Sets opponent's secondary info number. Also does fading via animate.css.
   * @param {?string|number} value New number. Null or empty hides it.
   */
  set oppNum(value) {
    var temp = document.querySelector(this._oppNum);
    if (temp !== null) {
      var _this = this;
      $(this._oppNum).fadeTo(500, 0, () => {
        temp.innerHTML = value;
        if (value !== null && value !== ``) {
          $(_this._oppNum).fadeTo(500, 1);
        }
      });
    }
    this._oppNumValue = value;
  }

  /**
   * Sets WSU's secondary info text. Also does fading via animate.css.
   * @param {?string} value Text to show. Null or empty hides it.
   */
  set wsuText(value) {
    var temp = document.querySelector(this._wsuText);
    if (temp !== null) {
      var _this = this;
      $(this._wsuText).fadeTo(500, 0, () => {
        temp.innerHTML = value;
        if (value !== null && value !== ``) {
          $(_this._wsuText).fadeTo(500, 1);
        }
      });
    }
    this._wsuTextValue = value;
  }

  /**
   * Sets opponent's secondary info text. Also does fading via animate.css.
   * @param {?string} value Text to show. Null or empty hides it.
   */
  set oppText(value) {
    var temp = document.querySelector(this._oppText);
    if (temp !== null) {
      var _this = this;
      $(this._oppText).fadeTo(500, 0, () => {
        temp.innerHTML = value;
        if (value !== null && value !== ``) {
          $(_this._oppText).fadeTo(500, 1);
        }
      });
    }
    this._oppTextValue = value;
  }

  /**
   * Hides all secondary text and numbers.
   */
  hideTextNums() {
    $(this._wsuNum).fadeTo(500, 0);
    $(this._oppNum).fadeTo(500, 0);
    $(this._wsuText).fadeTo(500, 0);
    $(this._oppText).fadeTo(500, 0);
  }
}
