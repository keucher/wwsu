"use strict";

// AnimateCSS JQuery extension
$.fn.extend({
	animateCss: function (animationName, callback) {
		let animationEnd = (function (el) {
			let animations = {
				animation: "animationend",
				OAnimation: "oAnimationEnd",
				MozAnimation: "mozAnimationEnd",
				WebkitAnimation: "webkitAnimationEnd",
			};

			for (let t in animations) {
				if (el.style[t] !== undefined) {
					return animations[t];
				}
			}
		})(document.createElement("div"));

		this.addClass("animated " + animationName).one(animationEnd, function () {
			$(this).removeClass("animated " + animationName);

			if (typeof callback === "function") {
				callback();
			}
		});

		return this;
	},
});

// Socket connection
io.sails.url = "https://server.wwsu1069.org";
io.sails.reconnection = true;
io.sails.reconnectionAttempts = Infinity;
let socket = io.sails.connect();

// Create WWSU modules
let wwsumodules = new WWSUmodules(socket, '#modules-loading');
wwsumodules
	.add("noReq", WWSUreq, { host: null })
	.add("WWSUsports", WWSUsports)
	.add("WWSUscoreboard", WWSUscoreboard, {
		main: "#scoreboard",
		wsuScore: "#score-wsu",
		oppScore: "#score-opp",
		wsuNum: "#num-wsu",
		oppNum: "#num-opp",
		wsuText: "#text-wsu",
		oppText: "#text-opp",
	});
let sports = wwsumodules.get("WWSUsports");
let scoreboard = wwsumodules.get("WWSUscoreboard");

// Listener for when sports info changes
const changeData = (data) => {
  console.dir(data);
  switch (data.name) {
    case `wsuScore`:
      scoreboard.wsuScore = data.value;
      break;
    case `oppScore`:
      scoreboard.oppScore = data.value;
      break;
    case `wsuNum`:
      scoreboard.wsuNum = data.value;
      break;
    case `oppNum`:
      scoreboard.oppNum = data.value;
      break;
    case `wsuText`:
      scoreboard.wsuText = data.value;
      break;
    case `oppText`:
      scoreboard.oppText = data.value;
      break;
    case `scoreboardVisible`:
      if (data.value && data.value !== 0) {
        scoreboard.show();
      } else {
        scoreboard.hide();
      }
      break;
  }
}
sports.on("insert", "renderer", (data) => {
  changeData(data);
});
sports.on("update", "renderer", (data) => {
  changeData(data);
});
sports.on("replace", "renderer", (db) => {
	db.each((data) => {
    changeData(data);
	});
});

socket.on("connect", () => {
	sports.init();
});

socket.on("disconnect", () => {
	console.log("Lost connection");
	try {
		socket._raw.io._reconnection = true;
		socket._raw.io._reconnectionAttempts = Infinity;
	} catch (e) {
		console.error(e);
	}
});
