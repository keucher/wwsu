"use strict";

// This class manages analytics and generates analytic charts.
// NOTE: unlike most other WWSU models, this does not use traditional WWSUdb extends. Otherwise, memory can be quickly eaten up by analytics.

// REQUIRES these WWSUmodules: noReq (WWSUreq), hostReq (WWSUreq), WWSUMeta, WWSUutil, WWSUanimations, WWSUconfig, WWSUdjs, WWSUcalendar
// REQUIRES chart.js and the chartJS moment adapter when displaying charts, select2, randomColor, moment duration
class WWSUanalytics extends WWSUevents {
	/**
	 * Construct the class.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			getListeners: "GET /api/analytics/listeners",
			getShowtime: "GET /api/analytics/showtime",
		};

		this.dom = {
			start: undefined,
			end: undefined,
			startofsemester: undefined,
			djs: undefined,
			events: undefined,
			analytics: undefined,
			generate: undefined,
			chart: undefined,
		};

		this.chart;
		this.table;

		// match analytic keys with a human readable label (undefined = do not use that analytic in charts)
		this.analytics = {
			showTime: "On-Air time",
			tuneIns: "Online listener tune-ins",
			listenerMinutes: "Online listener time",
			listenerPeak: "Peak online listeners",
			ratio: "Ratio of listener minutes to on-air time",
			ratioPeak: "Highest listener : showTime ratio",
			videoTime: "Amount of Time Video Streamed",
			viewerTuneIns: "Video stream tune-ins",
			viewerMinutes: "Video stream watch time",
			viewerPeak: "Peak video stream viewers",
			viewerRatio: "Ratio of video stream viewer time to on-air time",
			viewerRatioPeak: "Highest viewer : showTime ratio",
			webMessages: "Messages sent / received",
			remoteCredits: "Remote credits earned",
			warningPoints: "Warning Points accumulated",
			shows: "Live show airs",
			prerecords: "Pre-recorded airs",
			remotes: "Remote broadcast airs",
			genres: "Genre rotation airs",
			playlists: "Playlist airs",
			sports: "Sports broadcast airs",
			breaks: "Breaks taken",
			earlyStart: "Broadcasts started early",
			lateStart: "Broadcasts started late",
			earlyEnd: "Broadcasts ended early",
			lateEnd: "Broadcasts ended late",
			absences: "Absences / no-shows",
			cancellations: "Cancellations",
			unauthorized: "Unscheduled / Unauthorized airs",
			missedIDs: "Missed top-of-hour ID breaks",
			silences: "Silence Detection triggers",
			dumps: "Delay system dumps",
			reputationPercent: "Reputation Percent",
		};

		this.tableModal = new WWSUmodal(`Analytics`, null, ``, true, {
			width: 800,
			zindex: 1100,
		});
	}

	/**
	 * Call this when all WWSU modules have been added
	 *
	 * @param {string} start DOM query string of the date selection for the start range of analytics (for charts).
	 * @param {string} end DOM query string of the date selection for the end range of analytics (for charts).
	 * @param {string} startofsemester DOM query string of the button that sets start to startOfSemester (via WWSUconfig) (for charts).
	 * @param {string} djs DOM query string of the select element for choosing members
	 * @param {string} events DOM query string of the select element for choosing events
	 * @param {string} analytics DOM query string of the select element for choosing which analytics to generate
	 * @param {string} generate DOM query string of the button when click is supposed to generate the chart
	 * @param {string} chart DOM query string of where the Chart.js chart should be placed (must be a canvas)
	 */
	init(start, end, startofsemester, djs, events, analytics, generate, chart) {
		this.dom = {
			start,
			end,
			startofsemester,
			djs,
			events,
			analytics,
			generate,
			chart,
		};

		// Populate member select box options
		let djChoices = `<option value="all">(All Members)</option>`;
		this.manager
			.get("WWSUdjs")
			.db()
			.get()
			.forEach((dj) => {
				djChoices += `<option value="${dj.ID}">${dj.name} (${dj.realName})</option>`;
			});
		$(djs).html(djChoices);
		$(djs).select2({
			dropdownAutoWidth: true,
			width: "100%",
		});

		// Add a listener to update the options whenever members change
		this.manager.get("WWSUdjs").on("change", "WWSUanalytics", (db) => {
			djChoices = `<option value="all">(All Members)</option>`;
			db.get().forEach((dj) => {
				djChoices += `<option value="${dj.ID}">${dj.name} (${dj.realName})</option>`;
			});
			$(djs).html(djChoices);
		});

		// Populate event/broadcast select boxes
		let eventChoices = `<option value="all">(All broadcasts)</option>
		<option value="0">(Totals of all selected programming)</option>
		<option value="-1">(Totals of selected live shows, remotes, and prerecords)</option>
		<option value="-2">(Totals of selected sports broadcasts)</option>
		<option value="-3">(Totals of selected genre rotations)</option>
		<option value="-4">(Totals of selected playlists)</option>
		`;
		this.manager
			.get("WWSUcalendar")
			.calendar.db()
			.get()
			.filter(
				(event) =>
					[
						"show",
						"remote",
						"sports",
						"prerecord",
						"genre",
						"playlist",
					].indexOf(event.type) !== -1
			)
			.forEach((event) => {
				eventChoices += `<option value="${event.ID}">${event.name}</option>`;
			});
		$(events).html(eventChoices);
		$(events).select2({
			dropdownAutoWidth: true,
			width: "100%",
		});

		// Add a listener to update the options whenever calendar events change
		this.manager
			.get("WWSUcalendar")
			.calendar.on("change", "WWSUanalytics", (db) => {
				eventChoices = `<option value="all">(All broadcasts)</option>
		<option value="0">(Totals of all selected programming)</option>
		<option value="-1">(Totals of selected live shows, remotes, and prerecords)</option>
		<option value="-2">(Totals of selected sports broadcasts)</option>
		<option value="-3">(Totals of selected genre rotations)</option>
		<option value="-4">(Totals of selected playlists)</option>
		`;
				db.get()
					.filter(
						(event) =>
							[
								"show",
								"remote",
								"sports",
								"prerecord",
								"genre",
								"playlist",
							].indexOf(event.type) !== -1
					)
					.forEach((event) => {
						eventChoices += `<option value="${event.ID}">${event.type}: ${event.name}</option>`;
					});
				$(events).html(eventChoices);
			});

		// Populate analytic select box options
		let analyticChoices = ``;
		for (let analytic in this.analytics) {
			if (!Object.prototype.hasOwnProperty.call(this.analytics, analytic))
				continue;

			if (!this.analytics[analytic]) continue;

			analyticChoices += `<option value="${analytic}">${this.analytics[analytic]}</option>`;
		}
		$(analytics).html(analyticChoices);
		$(analytics).select2({
			dropdownAutoWidth: true,
			width: "100%",
		});

		// Add click event for buttons
		$(startofsemester).unbind("click");
		$(startofsemester).on("click", () => {
			$(start).val(
				moment(this.manager.get("WWSUconfig").analytics.startOfSemester).format(
					"YYYY-MM-DD"
				)
			);
		});
		$(generate).unbind("click");
		$(generate).on("click", () => {
			let startRange = $(this.dom.start).val();
			let endRange = $(this.dom.end).val();

			let _djsChosen = $(this.dom.djs).select2("data");
			let _eventsChosen = $(this.dom.events).select2("data");
			let _analyticsChosen = $(this.dom.analytics).select2("data");

			// Filter down to ids only
			let djsChosen = _djsChosen.map((record) => parseInt(record.id));
			let eventsChosen = _eventsChosen.map((record) => parseInt(record.id));
			let analyticsChosen = _analyticsChosen.map((record) => record.id);

			// Error and bail if all was selected for both members and broadcasts; fetching all of them will heavily impact the server.
			if (
				djsChosen.indexOf("all") !== -1 &&
				eventsChosen.indexOf("all") !== -1
			) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				$(document).Toasts("create", {
					class: "bg-warning",
					title: "Cannot fetch all members and broadcasts",
					body: "Fetching all members and broadcasts will cause a heavy load on the server when getting analytics. Please choose at least one member or broadcast.",
					autohide: true,
					delay: 15000,
					icon: "fas fa-skull-crossbones fa-lg",
				});
				return;
			}

			// Also error if nothing was selected
			if (djsChosen.length === 0 && eventsChosen.length === 0) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				$(document).Toasts("create", {
					class: "bg-warning",
					title: "You did not choose anything!",
					body: "You did not select any members nor broadcasts.",
					autohide: true,
					delay: 10000,
					icon: "fas fa-skull-crossbones fa-lg",
				});
				return;
			}

			// Set default if startRange was not provided; default to startOfSemester
			if (!startRange || startRange === "") {
				startRange = moment(
					this.manager.get("WWSUconfig").analytics.startOfSemester
				);
			}

			// Check endRange
			if (!endRange || endRange === "") {
				endRange = undefined;
			} else {
				// specified end date should be inclusive; add a day
				endRange = moment(endRange).add(1, "days");
			}

			// Fetch analytics
			this.getShowtime(
				this.dom.chart,
				{
					djs:
						djsChosen.length > 0
							? djsChosen.indexOf("all") !== -1
								? []
								: djsChosen
							: undefined,
					calendarIDs:
						eventsChosen.length > 0
							? eventsChosen.indexOf("all") !== -1
								? []
								: eventsChosen.filter((event) => event > 0)
							: undefined,
					start: moment(startRange).toISOString(true),
					end: endRange ? moment(endRange).toISOString(true) : undefined,
				},
				(analytics) => {
					if (!analytics) return;

					// First, filter by our selected members and broadcasts
					let dataSets = [];
					for (let key in analytics[0]) {
						if (
							djsChosen.indexOf("all") !== -1 ||
							djsChosen.indexOf(parseInt(key)) !== -1
						)
							dataSets.push(analytics[0][key]);
					}
					for (let key in analytics[1]) {
						if (
							(key > 0 && eventsChosen.indexOf("all") !== -1) ||
							eventsChosen.indexOf(parseInt(key)) !== -1
						)
							dataSets.push(analytics[1][key]);
					}

					let maximums = {};

					dataSets = dataSets.map((dataSet) => {
						let returnData = { name: dataSet.name };

						// Filter by chosen analytics
						if (analyticsChosen.length > 0) {
							for (let key in dataSet) {
								if (!Object.prototype.hasOwnProperty.call(dataSet, key))
									continue;

								if (analyticsChosen.indexOf(key) !== -1)
									returnData[key] = dataSet[key];
							}
						} else {
							returnData = Object.assign({}, dataSet);
						}

						for (let key in returnData) {
							if (!Object.prototype.hasOwnProperty.call(returnData, key))
								continue;

							// Convert array analytics into their length
							if (returnData[key] && returnData[key].constructor === Array)
								returnData[key] = returnData[key].length;

							// Calculate maximum values
							if (
								typeof maximums[key] === "undefined" ||
								maximums[key] < returnData[key]
							)
								maximums[key] = returnData[key];
						}
						return returnData;
					});

					// Create a dataset for Chart.js
					let labels =
						_analyticsChosen.length > 0
							? _analyticsChosen.map((analytic) => analytic.text)
							: Object.values(this.analytics);

					let chartData = dataSets.map((set) => {
						let returnData = {
							label: set.name,
							data: [],
							backgroundColor: randomColor({ seed: set.name }),
							altData: [],
						};

						// Make sure data is in the correct order according to the order of labels
						for (let key in set) {
							if (!Object.prototype.hasOwnProperty.call(set, key)) continue;

							returnData.data[
								analyticsChosen.length > 0
									? analyticsChosen.indexOf(key)
									: Object.keys(this.analytics).indexOf(key)
							] = maximums[key] ? (set[key] / maximums[key]) * 100 : 0;

							// Real values
							returnData.altData[
								analyticsChosen.length > 0
									? analyticsChosen.indexOf(key)
									: Object.keys(this.analytics).indexOf(key)
							] = [
								"showTime",
								"listenerMinutes",
								"videoTime",
								"viewerMinutes",
							].includes(key)
								? moment
										.duration(set[key], "minutes")
										.format("h [hours], m [minutes]")
								: set[key];
						}

						return returnData;
					});

					// Prepare chart
					$(this.dom.chart).html("");

					let analyticChartCanvas = $(this.dom.chart).get(0).getContext("2d");

					try {
						this.chart.destroy();
					} catch (e) {}

					// Create chart
					this.chart = new Chart(analyticChartCanvas, {
						type: "bar",
						data: {
							labels: labels,
							datasets: chartData,
						},
						options: {
							responsive: true,
							plugins: {
								tooltip: {
									callbacks: {
										label: function (context) {
											let label = context.dataset.label || "";

											if (label) {
												label += ": ";
											}

											if (
												context.dataset.altData &&
												typeof context.dataset.altData[context.dataIndex] !==
													"undefined"
											) {
												label += `${
													context.dataset.altData[context.dataIndex]
												} (${context.dataset.data[context.dataIndex]}%)`;
											} else {
												label += `NA (${
													context.dataset.data[context.dataIndex]
												}%)`;
											}

											return label;
										},
									},
								},
							},
						},
					});
				}
			);
		});
	}

	/**
	 * Get listener information from WWSU.
	 *
	 * @param {string} dom The DOM query string to block while loading
	 * @param {object} data Data to pass to WWSU
	 * @param {function} cb Callback function with results as parameter. Does not fire if API fails.
	 */
	getListeners(dom, data, cb) {
		this.manager
			.get("hostReq")
			.request(this.endpoints.getListeners, { data }, { dom }, (body, res) => {
				if (res.statusCode < 400 && body && body.constructor === Array) {
					if (typeof cb === "function") {
						cb(body);
					}
				}
			});
	}

	/**
	 * Get show / member analytics from WWSU API.
	 *
	 * @param {string} dom The query string of the DOM to block while fetching
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter is returned data, or false if failed.
	 */
	getShowtime(dom, data, cb) {
		this.manager.get("hostReq").request(
			this.endpoints.getShowtime,
			{
				data: data,
			},
			{ dom: dom },
			(body, res) => {
				if (body.constructor === Array && body[0] && body[1]) {
					cb(body);
				} else if (res.statusCode < 400) {
					if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
					$(document).Toasts("create", {
						class: "bg-danger",
						title: "Error getting analytics",
						body: "There was an error getting analytics. Please report this to the engineer.",
						autohide: true,
						delay: 10000,
						icon: "fas fa-skull-crossbones fa-lg",
					});
					console.error(e);
					cb(false);
				}
			}
		);
	}

	/**
	 * Generate a line graph of online listeners.
	 *
	 * @param {string} dom DOM query string of the canvas where we should generate the Chart.js line graph
	 * @param {object} apiData Data to pass to the WWSU API in this.getListeners (requires apiData.start and apiData.end)
	 */
	createListenerChart(dom, apiData) {
		this.getListeners(dom, apiData, (listeners) => {
			let data1 = listeners.map((listener) => {
				return {
					x: moment
						.tz(
							listener.createdAt,
							this.manager.has("WWSUMeta")
								? this.manager.get("WWSUMeta").meta.timezone
								: moment.tz.guess()
						)
						.format(),
					y: listener.listeners,
				};
			});
			let data2 = listeners.map((listener) => {
				return {
					x: moment
						.tz(
							listener.createdAt,
							this.manager.has("WWSUMeta")
								? this.manager.get("WWSUMeta").meta.timezone
								: moment.tz.guess()
						)
						.format(),
					y: listener.viewers,
				};
			});

			let listenerChartCanvas = $(dom).get(0).getContext("2d");
			let listenerChart = new Chart(listenerChartCanvas, {
				type: "line",
				data: {
					datasets: [
						{
							label: "Online Listeners",
							data: data1,
							steppedLine: true,
							fill: false,
							borderColor: `#17a2b8`,
						},
						{
							label: "Video Stream Viewers",
							data: data2,
							steppedLine: true,
							fill: false,
							borderColor: `#61358b`,
						},
					],
				},
				options: {
					responsive: true,
					title: {
						display: false,
					},
					scales: {
						x: {
							type: "time",
							time: {
								unit: 'minute',
								displayFormats: {
									minute: 'h:mm A'
								}
							},
							display: true,
							min: moment
								.tz(
									apiData.start,
									this.manager.has("WWSUMeta")
										? this.manager.get("WWSUMeta").meta.timezone
										: moment.tz.guess()
								)
								.format("lll"),
							max: moment
								.tz(
									apiData.end,
									this.manager.has("WWSUMeta")
										? this.manager.get("WWSUMeta").meta.timezone
										: moment.tz.guess()
								)
								.format("lll"),
						},
						y: {
							min: 0
						}
					},
				},
			});
		});
	}

	/**
	 * Open a datatables modal of analytics for the provided member or event.
	 *
	 * @param {string} type Either member or event
	 * @param {number} ID The ID of the type
	 */
	showAnalytics(type, ID) {
		let record;
		if (type === "member") {
			record = this.manager.get("WWSUdjs").find({ ID: ID }, true);
		} else if (type === "event") {
			record = this.manager.get("WWSUcalendar").calendar.find({ ID: ID }, true);
		}
		if (!record) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Error loading analytics",
				body: `There was an error loading the ${type} record for ID ${ID}. Please report this to the engineer.`,
				autohide: true,
				delay: 15000,
				icon: "fas fa-skull-crossbones fa-lg",
			});
			return;
		}

		this.tableModal.iziModal("open");
		this.tableModal.title = `Analytics for ${record.name}${
			type === "member" ? ` (${record.realName || `(Unknown Person)`})` : ``
		}`;
		this.tableModal.body = ``;

		let makeTable = (dom, analytic) => {
			this.manager.get("WWSUutil").waitForElement(`${dom}`, () => {
				// Generate table
				this.table = $(`${dom}`).DataTable({
					paging: false,
					data: [
						[
							"01. Live Shows Aired",
							analytic.week.shows || 0,
							analytic.semester.shows || 0,
							analytic.year.shows || 0,
						],
						[
							"02. Prerecorded Shows Aired",
							analytic.week.prerecords || 0,
							analytic.semester.prerecords || 0,
							analytic.year.prerecords || 0,
						],
						[
							"03. Remote Shows Aired",
							analytic.week.remotes || 0,
							analytic.semester.remotes || 0,
							analytic.year.remotes || 0,
						],
						[
							"04. Sports Broadcasts Aired",
							analytic.week.sports || 0,
							analytic.semester.sports || 0,
							analytic.year.sports || 0,
						],
						[
							"05. Playlists Aired",
							analytic.week.playlists || 0,
							analytic.semester.playlists || 0,
							analytic.year.playlists || 0,
						],
						[
							"06. Genre Rotations Aired",
							analytic.week.genres || 0,
							analytic.semester.genres || 0,
							analytic.year.genres || 0,
						],
						[
							"07. On-Air Time",
							moment
								.duration(analytic.week.showTime || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.semester.showTime || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.year.showTime || 0, "minutes")
								.format("h [hours], m [minutes]"),
						],
						[
							"08. Video Stream Time",
							moment
								.duration(analytic.week.videoTime || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.semester.videoTime || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.year.videoTime || 0, "minutes")
								.format("h [hours], m [minutes]"),
						],
						[
							"09. Online Listener Time",
							moment
								.duration(analytic.week.listenerMinutes || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.semester.listenerMinutes || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.year.listenerMinutes || 0, "minutes")
								.format("h [hours], m [minutes]"),
						],
						[
							"10. Video Stream Watch Time",
							moment
								.duration(analytic.week.viewerMinutes || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.semester.viewerMinutes || 0, "minutes")
								.format("h [hours], m [minutes]"),
							moment
								.duration(analytic.year.viewerMinutes || 0, "minutes")
								.format("h [hours], m [minutes]"),
						],
						[
							"11. Ratio Listener Time to On-Air Time",
							parseInt((analytic.week.ratio || 0) * 1000) / 1000,
							parseInt((analytic.semester.ratio || 0) * 1000) / 1000,
							parseInt((analytic.year.ratio || 0) * 1000) / 1000,
						],
						[
							"12. Video Watch Time to On-Air Time",
							parseInt((analytic.week.viewerRatio || 0) * 1000) / 1000,
							parseInt((analytic.semester.viewerRatio || 0) * 1000) / 1000,
							parseInt((analytic.year.viewerRatio || 0) * 1000) / 1000,
						],
						[
							"13. Best Broadcast (by Highest Listener Ratio)",
							analytic.week.ratioPeakTime
								? `${moment(analytic.week.ratioPeakTime).format("lll")} (${
										parseInt((analytic.week.ratioPeak || 0) * 1000) / 1000
								  })`
								: `NA`,
							analytic.semester.ratioPeakTime
								? `${moment(analytic.semester.ratioPeakTime).format("lll")} (${
										parseInt((analytic.semester.ratioPeak || 0) * 1000) / 1000
								  })`
								: `NA`,
							analytic.year.ratioPeakTime
								? `${moment(analytic.year.ratioPeakTime).format("lll")} (${
										parseInt((analytic.year.ratioPeak || 0) * 1000) / 1000
								  })`
								: `NA`,
						],
						[
							"14. Best Broadcast (by Highest Video Viewer Ratio)",
							analytic.week.viewerRatioPeakTime
								? `${moment(analytic.week.viewerRatioPeakTime).format(
										"lll"
								  )} (${
										parseInt((analytic.week.viewerRatioPeak || 0) * 1000) / 1000
								  })`
								: `NA`,
							analytic.semester.viewerRatioPeakTime
								? `${moment(analytic.semester.viewerRatioPeakTime).format(
										"lll"
								  )} (${
										parseInt((analytic.semester.viewerRatioPeak || 0) * 1000) /
										1000
								  })`
								: `NA`,
							analytic.year.viewerRatioPeakTime
								? `${moment(analytic.year.viewerRatioPeakTime).format(
										"lll"
								  )} (${
										parseInt((analytic.year.viewerRatioPeak || 0) * 1000) / 1000
								  })`
								: `NA`,
						],
						[
							"15. Messages Exchanged",
							analytic.week.webMessages || 0,
							analytic.semester.webMessages || 0,
							analytic.year.webMessages || 0,
						],
						[
							"16. Remote Credits",
							analytic.week.remoteCredits || 0,
							analytic.semester.remoteCredits || 0,
							analytic.year.remoteCredits || 0,
						],
						[
							"17. Warning / Discipline Points",
							analytic.week.warningPoints || 0,
							analytic.semester.warningPoints || 0,
							analytic.year.warningPoints || 0,
						],
						[
							"18. Shows Started 5+ Minutes Early",
							(analytic.week.earlyStart || []).length,
							(analytic.semester.earlyStart || []).length,
							(analytic.year.earlyStart || []).length,
						],
						[
							"19. Shows Started 10+ Minutes Late",
							(analytic.week.lateStart || []).length,
							(analytic.semester.lateStart || []).length,
							(analytic.year.lateStart || []).length,
						],
						[
							"20. Shows Ended 10+ Minutes Early",
							(analytic.week.earlyEnd || []).length,
							(analytic.semester.earlyEnd || []).length,
							(analytic.year.earlyEnd || []).length,
						],
						[
							"21. Shows Ended 5+ Minutes Late",
							(analytic.week.lateEnd || []).length,
							(analytic.semester.lateEnd || []).length,
							(analytic.year.lateEnd || []).length,
						],
						[
							"22. Absences / No-Shows",
							(analytic.week.absences || []).length,
							(analytic.semester.absences || []).length,
							(analytic.year.absences || []).length,
						],
						[
							"23. Cancellations",
							(analytic.week.cancellations || []).length,
							(analytic.semester.cancellations || []).length,
							(analytic.year.cancellations || []).length,
						],
						[
							"24. Unauthorized / Unscheduled Broadcasts",
							(analytic.week.unauthorized || []).length,
							(analytic.semester.unauthorized || []).length,
							(analytic.year.unauthorized || []).length,
						],
						[
							"25. Missed Top-Of-Hour ID Breaks",
							(analytic.week.missedIDs || []).length,
							(analytic.semester.missedIDs || []).length,
							(analytic.year.missedIDs || []).length,
						],
						[
							"26. Silence Alarms Triggered",
							(analytic.week.silences || []).length,
							(analytic.semester.silences || []).length,
							(analytic.year.silences || []).length,
						],
						[
							"27. Delay System Dumps Triggered",
							(analytic.week.dumps || []).length,
							(analytic.semester.dumps || []).length,
							(analytic.year.dumps || []).length,
						],
						[
							"28. Reputation Percent (out of 100)",
							analytic.week.reputationPercent || 0,
							analytic.semester.reputationPercent || 0,
							analytic.year.reputationPercent || 0,
						],
					],
					columns: [
						{ title: "Analytic" },
						{ title: "Past Week" },
						{ title: "This Semester" },
						{ title: "Past Year" },
					],
					buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
				});

				this.table
					.buttons()
					.container()
					.appendTo(`${dom}_wrapper .col-md-6:eq(0)`);
			});
		};

		const callback = (analytics) => {
			if (!analytics || !analytics.stats) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				$(document).Toasts("create", {
					class: "bg-danger",
					title: "Error loading analytics",
					body: `There was an error loading analytics from WWSU. Please report this to the engineer.`,
					autohide: true,
					delay: 15000,
					icon: "fas fa-skull-crossbones fa-lg",
				});
				return;
			}

			let stats = analytics.stats;

			// The stats returned are not table-friendly. We need to convert them.
			let DJs = {};
			let shows = {};
			for (let [analyticKey, analyticValue] of Object.entries(stats)) {
				// DJs
				for (let [djID, djAnalytics] of Object.entries(analyticValue[0])) {
					if (typeof DJs[djID] === "undefined") {
						DJs[djID] = {
							week: {},
							semester: {},
							year: {},
						};
					}
					DJs[djID][analyticKey] = djAnalytics;
				}
				// Shows
				for (let [showID, showAnalytics] of Object.entries(analyticValue[1])) {
					if (typeof shows[showID] === "undefined") {
						shows[showID] = {
							week: {},
							semester: {},
							year: {},
						};
					}
					shows[showID][analyticKey] = showAnalytics;
				}
			}

			if (
				(type === "member" && (!ID || !DJs[ID])) ||
				(type === "event" && (!ID || !shows[ID]))
			) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				$(document).Toasts("create", {
					class: "bg-danger",
					title: "Error processing analytics",
					body: `There was an error processing analytics (API did not return analytics for the requested resource). Please report this to the engineer.`,
					autohide: true,
					delay: 15000,
					icon: "fas fa-skull-crossbones fa-lg",
				});
				return;
			}

			let html = ``;

			for (let DJ in DJs) {
				if (!Object.prototype.hasOwnProperty.call(DJs, DJ)) continue;

				html += `<div class="card card-widget widget-user-2 p-1">
			  <div class="widget-user-header bg-info">
				<h3 class="widget-user-username">Member Analytics (${
					DJs[DJ].week.name || DJs[DJ].semester.name || DJs[DJ].year.name
				})</h3>
			  </div>
			  <div class="card-footer p-1">
				  <table id="section-analytics-table-member-${DJ}" class="table table-striped display responsive" style="width: 100%;"></table>
			  </div>
			</div>`;

				makeTable(`#section-analytics-table-member-${DJ}`, DJs[DJ]);
			}

			for (let show in shows) {
				if (show <= 0 || !Object.prototype.hasOwnProperty.call(shows, show))
					continue;

				html += `<div class="card card-widget widget-user-2 p-1">
			<div class="widget-user-header bg-blue">
			  <h3 class="widget-user-username">Show Analytics (${
					shows[show].week.name ||
					shows[show].semester.name ||
					shows[show].year.name
				})</h3>
			</div>
			<div class="card-footer p-1">
				<table id="section-analytics-table-event-${show}" class="table table-striped display responsive" style="width: 100%;"></table>
			</div>
		  </div>`;

				makeTable(`#section-analytics-table-event-${show}`, shows[show]);
			}

			this.tableModal.body = html;
		};

		if (type === "member") {
			this.manager.get("WWSUdjs").getDJ(
				`#modal-${this.tableModal.id}`,
				{
					ID: ID,
				},
				(analytics) => {
					callback(analytics);
				}
			);
		} else if (type === "event") {
			this.manager.get("WWSUcalendar").getEvent(
				`#modal-${this.tableModal.id}`,
				{
					ID: ID,
				},
				(analytics) => {
					callback(analytics);
				}
			);
		}
	}
}
