/**
 * Add an error event on the window to show Toasts when uncaught errors occur.
 */
class WWSUonerror {
	constructor(manager, options) {
		this.manager = manager;

		window.addEventListener("error", () => {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();

			// Display error toast
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Uncaught Error!",
				body: "An uncaught error occurred. Please check your developer tools / console and report this to the engineer at wwsu4@wright.edu if it is a bug.",
				subtitle: `Uncaught error occurred. See the console.`,
				autohide: true,
				delay: 15000,
				icon: "fas fa-skull-crossbones",
			});
		});

		window.addEventListener("unhandledrejection", (event) => {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();

			// Display error toast
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Unhandled rejection!",
				body: "An unhandled rejection occurred. Please check your developer tools / console and report this to the engineer at wwsu4@wright.edu if it is a bug.",
				subtitle: `Unhandled rejection occurred. See the console.`,
				autohide: true,
				delay: 15000,
				icon: "fas fa-skull-crossbones",
			});
		});
	}
}
