"use strict";

/* global moment, TAFFY */

/**
 * This class is a custom WWSU event emitter.
 */
class WWSUevents {
  constructor() {
    // Structure: this.events[key] is an object where key = event name; this.events[key][key2] is a function where key2 is the scope.
    this.events = {};
  }

  /**
   * Emit an event across all scopes.
   *
   * @param {string} event name of the event to emit.
   * @param {Array} args array of arguments to pass to the event.
   */
  emitEvent(event, args) {
    if (typeof this.events[event] === "object") {
      for (let scope in this.events[event]) {
        if (
          Object.prototype.hasOwnProperty.call(this.events[event], scope) &&
          typeof this.events[event][scope].fn === "function"
        ) {
          this.events[event][scope].fn(...args);
          if (this.events[event][scope].once) {
            delete this.events[event][scope];
          }
        }
      }
    }
  }

  /**
   * Create an event listener.
   *
   * @param {string} event name of the event
   * @param {string} scope a scope ID allowing for multiple listeners for the same event; providing an ID that already exists will overwrite it.
   * @param {function} fn the function to execute when the event is emitted. Arguments are passed as spreaded parameters.
   */
  on(event, scope, fn) {
    if (typeof this.events[event] === "undefined") {
      this.events[event] = {};
    }
    this.events[event][scope] = { once: false, fn };
  }

  /**
   * Create an event listener that destroys itself after one emission (only on the provided scope).
   *
   * @param {string} event name of the event
   * @param {string} scope a scope ID allowing for multiple listeners for the same event; providing an ID that already exists will overwrite it.
   * @param {function} fn the function to execute when the event is emitted. Arguments are passed as spreaded parameters.
   */
  once(event, scope, fn) {
    if (typeof this.events[event] === "undefined") {
      this.events[event] = {};
    }
    this.events[event][scope] = { once: true, fn };
  }

  /**
   * Destroy an event listener on the provided scope.
   *
   * @param {string} event name of the event
   * @param {string} scope scope ID to destroy the listener from
   */
  off(event, scope) {
    if (this.events[event] && this.events[event][scope]) {
      delete this.events[event][scope];
    }
  }
}

/**
 * WWSUdb manages data from the WWSU websockets
 *
 * @class WWSUdb
 */
// eslint-disable-next-line no-unused-lets
class WWSUdb extends WWSUevents {
  /**
   *Creates an instance of WWSUdb.
   * @param {string} lsName Name of the localStorage database to use; omit to not use any
   * @param {TAFFY} taffy If TAFFY is not available as a global (eg. running fron Node.js), pass TAFFY() in as a parameter.
   * @memberof WWSUdb
   */
  constructor(lsName, taffy) {
    super();
    this._db = taffy || TAFFY();

    // Load localStorage if available
    if (lsName) {
      let lsActive = this._db.store(lsName);
    }
  }

  /**
   * Return the TAFFYDB associated with this member.
   *
   * @readonly
   * @memberof WWSUdb
   */
  get db() {
    return this._db;
  }

  /**
   * Execute a query on the database.
   *
   * @param {Array || Object} _query An array of records to replace in the database (if replace = true), or a query object {insert || update: {record object}} or {remove: record ID}.
   * @param {boolean} [replace=false] If true, this query will replace everything in the TAFFY database.
   * @memberof WWSUdb
   */
  query(_query, replace = false) {
    let query = _.cloneDeep(_query);
    if (replace) {
      if (query.constructor === Array) {
        this.emitEvent("preReplace", [this._db()]);
        this.emitEvent("preChange", [this._db(), query]);
        this._db().remove();
        this._db.insert(query);
        this.emitEvent("replace", [this._db()]);
        this.emitEvent("change", [this._db(), query]);
      }
      return null;
    } else {
      for (let key in query) {
        if (Object.prototype.hasOwnProperty.call(query, key)) {
          this.emitEvent("preChange", [this._db(), query]);
          switch (key) {
            case "insert":
              this.emitEvent("preInsert", [query[key], this._db()]);
              this._db.insert(query[key]);
              this.emitEvent("insert", [query[key], this._db()]);
              break;
            case "update":
              this.emitEvent("preUpdate", [query[key], this._db()]);
              this._db({ ID: query[key].ID }).update(query[key]);
              this.emitEvent("update", [query[key], this._db()]);
              break;
            case "remove":
              this.emitEvent("preRemove", [query[key], this._db()]);
              this._db({ ID: query[key] }).remove();
              this.emitEvent("remove", [query[key], this._db()]);
              break;
          }
          this.emitEvent("change", [this._db(), query]);
        }
      }
    }
  }

  /**
   * Safely find and return array of matching documents in the db using cloneDeep.
   *
   * @param {object|function} _query The search criteria
   * @returns {array|?object} Array of matching records if first = false, or single object (null: none found) of the first found record if true
   */
  find(_query, first = false) {
    let query;
    let records;
    if (typeof _query === "object") {
      query = _.cloneDeep(_query);
      if (!first) {
        records = this._db(query).get();
      } else {
        records = this._db(query).first();
      }
    } else if (typeof _query === "function") {
      query = _query;
      if (!first) {
        records = this._db().get().filter(query);
      } else {
        records = this._db().get().find(query);
      }
    } else {
      records = this._db().get();
    }
    return _.cloneDeep(records);
  }

  /**
   * Call WWSU's API and replace all data in memory with what WWSU returns. Also establishes socket event.
   *
   * @param {WWSUreq} WWSUreq The request to use
   * @param {string} url REST format URL path (eg. "GET /path/to/endpoint/:param")
   * @param {object} data Data to pass in the request
   */
  replaceData(WWSUreq, url, data = {}) {
    WWSUreq.request(url, { data: data }, {}, (body, res) => {
      if (res.statusCode < 400 && body.constructor === Array)
        this.query(body, true);
    });
  }

  /**
   * Assign a socket event to this database which follows WWSU's websocket standards for data.
   *
   * @param {string} event Socket event name to attach
   * @param {sails.io} socket WWSU socket to use
   */
  assignSocketEvent(event, socket) {
    socket.on(event, (data) => {
      this.query(data, false);
    });
  }
}

/**
 * Class for managing requests and authorization to WWSU's API
 *
 * @requires WWSUutil (loaded in WWSUmodules via manager)
 */
class WWSUreq extends WWSUevents {
  /**
   * Construct the class
   *
   * @param {WWSUmodules} manager The WWSU modules that initiated this module
   * @param {object} options Object of options passed in the manager.add method
   * @param {string} options.host Host name of this client
   * @param {string} options.db Name of the WWSU module containing the WWSUdb containing records of those that can authorize with this request, if applicable
   * @param {?object} options.filter Filter applicable records that can authorize in the WWSUdb by this TAFFY query object, if applicable
   * @param {?string} options.usernameField Name of the database column containing names for authorization, if applicable
   * @param {?string} options.authPath URL path in WWSU's API for authorization and getting a token, if applicable
   * @param {?string} options.authName Human friendly name of the type of person (eg. "Director") that must authorize themselves for this request
   * @param {boolean} options.debug Whether to output to the console each request
   */
  constructor(manager, options) {
    super();

    this.manager = manager;
    this.socket = this.manager.socket;
    this.host = options.host || null;
    this.db = options.db || null;
    this.filter = options.filter || null;
    this.authPath = options.authPath || null;
    this.authName = options.authName || null;
    this.usernameField = options.usernameField || null;
    this.loginID = null;
    this.debug = options.debug || false;

    // Storing authorization tokens in memory (instead of cookies or localStorage, which is insecure)
    this._token = null;
    this._time = null;
    this._expiration = null;

    this.errorCodes = new Map([
      [400, "Bad Request"],
      [401, "Authorization Required"],
      [403, "Forbidden / Rejected"],
      [404, "Not Found"],
      [408, "Request Timed Out (Check Network)"],
      [409, "Conflict"],
      [410, "Gone"],
      [411, "Length Required in Header"],
      [412, "Precondition Failed"],
      [413, "Payload Too Large / Too Much Data Sent"],
      [414, "URI Too Long"],
      [415, "Unsupported Media Type"],
      [417, "Expectation Failed"],
      [418, "Client Error"], // Actually "I'm a Teapot", but we are using this for client / JS errors.
      [425, "Too Early to Process"],
      [426, "Protocol Upgrade Required"],
      [429, "Rate Limited / Too Many Requests"],
      [431, "Request Headers Too Large"],
      [451, "Unavailable for Legal Reasons"],
      [500, "Internal Server Error (Contact wwsu4@wright.edu)"],
      [501, "Not Implemented Yet"],
      [502, "Bad Gateway / Upstream Server Offline"],
      [503, "Service Unavailable"],
      [504, "Gateway / Upstream Server Timeout"],
      [505, "HTTP Version Not Supported"],
      [506, "Circular Reference in Negotiation"],
    ]);
  }

  get token() {
    return this._token;
  }

  set token(value) {
    this._token = value;
  }

  get time() {
    return this._time;
  }

  set time(value) {
    this._time = value;
  }

  get expiration() {
    return this._expiration;
  }

  set expiration(value) {
    this._expiration = value;
  }

  // Is the current token expected to be expired?
  expired() {
    return (
      this._token === null ||
      moment().isAfter(
        moment(this.time).add(this.expiration - 1000, "milliseconds")
      )
    );
  }

  /**
   * Check authorization, and then make a request to WWSU's API
   *
   * @param {string} url REST-style URL to use (eg. "VERB /path/to/endpoint/:param/:optional?")
   * @param {object} reqOpts Options to pass to the sails.io socket request (method and url properties will overwrite url parameter!)
   * @param {object} reqOpts.data Data or query parameters to pass along with the request
   * @param {object} opts Options specific for WWSUreq.
   * @param {string} opts.dom DOM query string of the <non-modal> element to block / show login form when login is necessary vis JQuery blockui.
   * @param {string} opts.domModal DOM query string of the <modal> to block / show login form when login is necessary vis JQuery blockui.
   * @param {boolean} opts.hideErrorToast Do not show a toast with error message if returned status code is >=400.
   * @param {function} cb Callback executed after the request is made. Contains body, res as parameter.
   */
  request(url, reqOpts = {}, opts = {}, cb) {
    if (this.debug) {
      console.log(`REQUEST`);
      console.dir({ url, reqOpts, opts });
    }

    // Handle displaying HTTP status code errors if they occur
    const handleStatusCodes = (url2, body, res) => {
      if (res.statusCode < 400) return; // Good status; do not do anything.

      // Emit Event
      this.emitEvent("reqError", [url2, body, res]);

      if (
        opts.hideErrorToast ||
        typeof document === "undefined" ||
        typeof $ !== "function"
      ) {
        // No toasts; log the bad status as a warning to the console instead.
        console.warn(
          `(silent error) ${
            this.errorCodes.get(res.statusCode) || "Unknown Request Error"
          } on ${url2}. ${body}`
        );
        return;
      }

      // Display error toast
      if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
      $(document).Toasts("create", {
        class: res.statusCode >= 500 ? "bg-danger" : "bg-warning",
        title: this.errorCodes.get(res.statusCode) || "Unknown Request Error",
        body:
          body && typeof body === "object" && body.problems
            ? body.problems.join(", ")
            : body,
        subtitle: url2,
        autohide: true,
        delay: 15000,
        icon: "fas fa-skull-crossbones",
      });
    };

    const step2 = (username, password) => {
      // Authorize the provided username and password
      this._authorize(username, password, (body, res, url2) => {
        handleStatusCodes(url2, body, res);

        // If we had a good status, then proceed with the actual request.
        if (res.statusCode < 400) {
          this._tryRequest(url, reqOpts, opts, (body, res, url2) => {
            handleStatusCodes(url2, body, res);
            cb(body, res);
          });
        }
      });
    };

    // Token expected to be expired?
    if (this.expired()) {
      // If auth path is not defined, this request doesn't need authentication; proceed with the request immediately
      if (!this.authPath) {
        this._tryRequest(url, reqOpts, opts, (body, res, url2) => {
          handleStatusCodes(url2, body, res);
          cb(body, res);
        });
        // If no db specified, we don't need to prompt for login; try authorizing immediately as host
      } else if (!this.db) {
        step2(this.host, null);
        // Otherwise, prompt for a login
      } else {
        this._promptLogin((username, password) => step2(username, password));
      }

      // Otherwise, try the request, and for safe measures, prompt for login if we end up getting an auth error
    } else {
      this._tryRequest(url, reqOpts, opts, (body, res, url2) => {
        if (res.statusCode === 401) {
          // Prompt for login if db was specified
          if (this.db) {
            this.token = null;
            this._promptLogin((username, password) =>
              step2(username, password)
            );
          } else {
            step2(this.host, null);
          }
        } else {
          // eslint-disable-next-line callback-return
          handleStatusCodes(url2, body, res);
          cb(body, res);
        }
      });
    }
  }

  /**
   * Helper: Attempt the API request with the current token.
   *
   * @param {string} url REST-style URL to use (eg. "VERB /path/to/endpoint/:param/:optional?")
   * @param {object} reqOpts Options to pass to the sails.io socket request (method and url properties will overwrite url parameter!)
   * @param {object} reqOpts.data Data or query parameters to pass along with the request
   * @param {object} reqOpts.params (GET) Data or query parameters to pass along with the request
   * @param {object} opts Options specific for WWSUreq.
   * @param {string} opts.dom DOM query string of the <non-modal> element to block / show login form when login is necessary vis JQuery blockui.
   * @param {string} opts.domModal DOM query string of the <modal> to block / show login form when login is necessary vis JQuery blockui.
   * @param {boolean} opts.showErrorToast Show a toast with error message if returned status code is >=400. Defaults to false.
   * @param {function} cb Function called after the request is made. Parameters are body, response, url.
   */
  _tryRequest(url, reqOpts, opts, cb) {
    try {
      const doRequest = (cb2) => {
        const actuallyDoRequest = () => {
          this.socket.request(reqOpts, (body, res) => {
            cb2();
            cb(body, res, `${reqOpts.method} ${reqOpts.url}`);
            if (this.debug) {
              console.log(`RESPONSE for ${reqOpts.method} ${reqOpts.url}`);
              console.dir(res);
            }
          });
        };

        // Add CSRF token if applicable via jQuery ajax
        if (
          ["get", "head", "options"].indexOf(reqOpts.method.toLowerCase()) ===
          -1
        ) {
          $.ajax({
            url: `${this.socket.url}/csrfToken`,
            cache: false,
          })
            .done(function (data) {
              if (typeof reqOpts.headers === `undefined`) {
                reqOpts.headers = {
                  "x-csrf-token": data._csrf,
                };
              } else {
                reqOpts.headers["x-csrf-token"] = data._csrf;
              }
              actuallyDoRequest();
            })
            .fail(function (jqXHR, textStatus) {
              cb(
                null,
                {
                  statusCode: 418,
                  body: textStatus,
                  headers: null,
                },
                `${reqOpts.method} ${reqOpts.url}`
              );
            });
        } else {
          actuallyDoRequest();
        }
      };

      // Parse REST url
      let parsedUrl = this.manager
        .get("WWSUutil")
        .restToObject(url, reqOpts.data || reqOpts.params);
      reqOpts.method = reqOpts.method || parsedUrl.method;
      reqOpts.url = reqOpts.url || parsedUrl.url;

      // Add authorization header if applicable (no longer applicable; authorization is stored on the session)
      /*
			if (this.authPath !== null) {
				if (typeof reqOpts.headers === `undefined`) {
					reqOpts.headers = {
						Authorization: "Bearer " + this.token,
					};
				} else {
					reqOpts.headers["Authorization"] = "Bearer " + this.token;
				}
			}
			*/

      if (opts.dom && document && document.querySelector(opts.dom)) {
        $(opts.dom).block({
          message: "<h1>Processing...</h1>",
          css: { border: "3px solid #a00" },
          timeout: 120000,
          onBlock: () => {
            doRequest(() => {
              $(opts.dom).unblock();
              if (opts.domModal) $(`#modal-${opts.domModal} .overlay`).remove();
            });
          },
        });
      } else if (
        opts.domModal &&
        document.querySelector(`#modal-${opts.domModal}`)
      ) {
        $(`#modal-${opts.domModal} .modal-content`)
          .prepend(`<div class="overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
              <h1 class="text-white">Processing...</h1>
        </div>`);
        doRequest(() => {
          if (opts.domModal) $(`#modal-${opts.domModal} .overlay`).remove();
        });
      } else {
        doRequest(() => {});
      }
    } catch (unusedE) {
      // eslint-disable-next-line standard/no-callback-literal
      console.error(unusedE);
      cb(
        null,
        {
          statusCode: 418,
          body: "Please see your console for more error information. Report to wwsu4@wright.edu.",
          headers: null,
        },
        `${reqOpts.method} ${reqOpts.url}`
      );
    }
  }

  /**
   * Make an authorization request to WWSU for a token, and store the token in memory if one is received.
   *
   * @param {string} username Username of the person authorizing
   * @param {string} password Password provided
   * @param {function} cb Function called after request. Parameters are body, res, url.
   */
  _authorize(username, password, cb) {
    let req = this.manager.get("WWSUutil").restToObject(this.authPath, {
      username: username,
    });
    this.socket.request(
      Object.assign(req, { data: { username, password } }),
      (body, res) => {
        if (this.debug) {
          console.log(`AUTHORIZE`);
          console.dir({ req, res });
        }
        if (res.statusCode < 400 && typeof body.token !== `undefined`) {
          this.token = body.token;
          this.expiration = body.expires || 60000 * 5;
          this.time = moment();
        }
        cb(body, res, `${req.method} ${req.url}`);
      }
    );
  }

  /**
   * Create an authorization prompt with iziModal.
   *
   * @param {function} cb Function called after user completes the prompt. Contains (username, password) as parameters.
   */
  _promptLogin(cb) {
    let fdb;
    if (typeof this.filter === "object") {
      fdb = this.manager.get(this.db).db(this.filter);
    } else {
      fdb = this.manager.get(this.db).db();
    }
    if (!fdb || fdb.length < 1) {
      if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "Authorization error",
        body: `There is no ${this.authName} available to authorize. Please report this to wwsu4@wright.edu.`,
        autohide: true,
        delay: 10000,
        icon: "fas fa-skull-crossbones fa-lg",
      });
      return null;
    }

    if (!this.usernameField) {
      if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "Authorization error",
        body: `A username field was not specified for ${this.authName} authorization. Please report this to wwsu4@wright.edu.`,
        autohide: true,
        delay: 10000,
        icon: "fas fa-skull-crossbones fa-lg",
      });
      return null;
    }

    let tempModal = new WWSUmodal(
      `${this.authName} Authorization Required`,
      `bg-danger`,
      ``,
      true,
      {
        overlayClose: false,
        timeout: false,
        closeOnEscape: true,
        closeButton: true,
        onClosed: () => {
          tempModal = undefined;
        },
      }
    );

    tempModal.iziModal("open");
    tempModal.body = `<p>To perform this action, you must authorize with ${this.authName} credentials. Please choose a user, and then type in your password.</p><div id="modal-${tempModal.id}-form"></div>`;

    let util = new WWSUutil();
    util.waitForElement(`#modal-${tempModal.id}-form`, () => {
      $(`#modal-${tempModal.id}-form`).alpaca({
        schema: {
          title: `Authorize Action`,
          type: "object",
          properties: {
            username: {
              type: "string",
              title: `User`,
              required: true,
              enum: fdb.get().map((user) => user.name),
            },
            password: {
              title: "Password",
              format: "password",
              required: true,
            },
          },
        },
        options: {
          form: {
            buttons: {
              submit: {
                title: `Authorize`,
                click: (form, e) => {
                  form.refreshValidationState(true);
                  if (!form.isValid(true)) {
                    form.focus();
                    return;
                  }
                  let value = form.getValue();
                  tempModal.iziModal("close");
                  cb(value.username, value.password);
                },
              },
            },
          },
        },
      });
    });
  }
}

// Class for loading scripts in web pages dynamically
class WWSUScriptLoader {
  constructor() {
    this.loadedScripts = [];
  }

  /**
   * Load a script.
   *
   * @param {string} filename Relative path to the script to load
   * @param {string} filetype js or css
   */
  loadScript(filename, filetype) {
    if (this.loadedScripts.indexOf(filename) === -1) {
      this._loadScript(filename, filetype);
      this.loadedScripts.push(filename);
    }
  }

  /**
   * Helper: Load a script. Should not be called directly; loadScript prevents duplicate script loading.
   *
   * @param {string} filename Relative path to the script to load
   * @param {string} filetype js or css
   */
  _loadScript(filename, filetype) {
    if (filetype === "js") {
      //if filename is a external JavaScript file
      let fileref = document.createElement("script");
      fileref.setAttribute("type", "text/javascript");
      fileref.setAttribute("src", filename);
    } else if (filetype === "css") {
      //if filename is an external CSS file
      let fileref = document.createElement("link");
      fileref.setAttribute("rel", "stylesheet");
      fileref.setAttribute("type", "text/css");
      fileref.setAttribute("href", filename);
    }
    if (typeof fileref !== "undefined")
      document.getElementsByTagName("head")[0].appendChild(fileref);
  }
}

class WWSUutil {
  /**
   * Get the value of the specified URL parameter
   *
   * @param {string} name Name of URL parameter to fetch
   * @returns {?string} Value of the URL parameter being fetched, or null if not set.
   */
  getUrlParameter(name) {
    try {
      name = name.replace(/[[]/, "\\[").replace(/[\]]/, "\\]");
      let regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
      let results = regex.exec(window.location.search);
      return results === null
        ? null
        : decodeURIComponent(results[1].replace(/\+/g, " "));
    } catch (e) {
      console.error(e);
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "Error in getUrlParameter function",
        body: "There was an error in the getUrlParameter function. Please report this to the engineer.",
        autohide: true,
        delay: 10000,
        icon: "fas fa-skull-crossbones fa-lg",
      });
    }
  }

  /**
   * Returns a new URL string filling in URL parameters/paths with the data provided.
   *
   * @param {string} url Original REST style URL ("VERB /path/to/api/endpoint/:param"). (NOTES: Should contain exactly one space [between the verb and URL path], should never contain an optional parameter in the middle of the URL [optional parameter should always be at the end], should never use ? except to indicate an optional parameter [put all query parameters into the data property], and should never use /: except to indicate a parameter)
   * @param {?object} data Query string / body data to be sent in the request, which should also include values for parameters defined in the url.
   * @returns {object} {method: "VERB", url: "/new/api/path/with/parameter/values/included"}
   */
  restToObject(url, data = {}) {
    let returnObject = { method: "get", url: "" };

    // Get the verb.
    let parts1 = url.split(" ");
    if (parts1.length !== 2)
      throw new Error(
        "Expected url to contain exactly one space between the verb and the path."
      );
    returnObject.method = parts1[0];

    // Fill in defined rest parameters into url from data.
    let parts2 = parts1[1].split("/");
    for (let part of parts2) {
      if (part.startsWith(":")) {
        part = part.substring(1);

        // Required parameter did not exist! Throw an error.
        if (!part.endsWith("?") && typeof data[part] === "undefined")
          throw new Error(
            `Expected required parameter ${part} in REST request ${url}, but property did not exist in passed data object.`
          );

        // Remove end question mark indicating optional parameter
        if (part.endsWith("?")) part = part.slice(0, -1);

        // Exclude optional parameters not defined in the data object.
        if (typeof data[part] !== "undefined")
          returnObject.url += `${data[part]}/`;
      } else {
        returnObject.url += `${part}/`;
      }
    }

    return returnObject;
  }

  /**
   * Convert a hexadecimal color into its RGBA values.
   *
   * @param {string} hex A hexadecimal color
   * @param {object} options options.format: specify "array" to return as [red, green, blue, alpha] instead of object
   * @returns {object || array} {red, green, blue, alpha} or [red, green, blue, alpha] values
   */
  hexRgb(hex, options = {}) {
    // function-specific values
    let hexChars = "a-f\\d";
    let match3or4Hex = `#?[${hexChars}]{3}[${hexChars}]?`;
    let match6or8Hex = `#?[${hexChars}]{6}([${hexChars}]{2})?`;
    let nonHexChars = new RegExp(`[^#${hexChars}]`, "gi");
    let validHexSize = new RegExp(`^${match3or4Hex}$|^${match6or8Hex}$`, "i");

    try {
      if (
        typeof hex !== "string" ||
        nonHexChars.test(hex) ||
        !validHexSize.test(hex)
      ) {
        throw new TypeError("Expected a valid hex string");
      }

      hex = hex.replace(/^#/, "");
      let alpha = 255;

      if (hex.length === 8) {
        alpha = parseInt(hex.slice(6, 8), 16) / 255;
        hex = hex.substring(0, 6);
      }

      if (hex.length === 4) {
        alpha = parseInt(hex.slice(3, 4).repeat(2), 16) / 255;
        hex = hex.substring(0, 3);
      }

      if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
      }

      const num = parseInt(hex, 16);
      const red = num >> 16;
      const green = (num >> 8) & 255;
      const blue = num & 255;

      return options.format === "array"
        ? [red, green, blue, alpha]
        : { red, green, blue, alpha };
    } catch (e) {
      console.error(e);
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "hexrgb error",
        body: "There was an error in the hexrgb function. Please report this to the engineer.",
        autohide: true,
        delay: 10000,
        icon: "fas fa-skull-crossbones fa-lg",
      });
    }
  }

  /**
   * Determine if the text should be black or white depending on the provided background color.
   *
   * @param {string} hex The hex color of the background
   * @returns {boolean} True if text should be black, false if it should be white.
   */
  getContrastYIQ(hex) {
    let r = parseInt(hex.substr(1, 2), 16),
      g = parseInt(hex.substr(3, 2), 16),
      b = parseInt(hex.substr(5, 2), 16),
      yiq = (r * 299 + g * 587 + b * 114) / 1000;
    return yiq >= 128;
  }

  /**
   * Escape HTML for use in the web page.
   *
   * @param {string} str The HTML to escape
   */
  escapeHTML(str) {
    let div = document.createElement("div");
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
  }

  /**
   * Call a function when an element exists on the document.
   *
   * @param {string} theelement DOM query string of the element to wait for until it exists
   * @param {function} cb Function to call when the element exists
   * @param {number} termination Terminate checking if element not found within this many request animation frames.
   */
  waitForElement(theelement, cb, termination = 250) {
    termination--;
    if (!document.querySelector(theelement)) {
      if (termination > 0) {
        window.requestAnimationFrame(() =>
          this.waitForElement(theelement, cb, termination)
        );
      } else {
        throw new Error(
          `WWSUutil.waitForElement for element ${theelement} timed out.`
        );
      }
    } else {
      // eslint-disable-next-line callback-return
      cb(document.querySelector(theelement));
    }
  }

  /**
   * Create a UUID
   */
  createUUID() {
    let dt = new Date().getTime();
    let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (c) {
        let r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
      }
    );
    return uuid;
  }

  /**
   * Create a confirmation dialog with Alpaca forms and iziModal
   *
   * @param {string} description Further information to provide in the confirmation
   * @param {?string} confirmText Require user to type this text to confirm their action (null: use simple yes/no confirmation)
   * @param {function} cb Callback executed when and only if action is confirmed.
   */
  confirmDialog(description, confirmText, cb) {
    let tempModal = new WWSUmodal(`Confirm Action`, `bg-warning`, ``, false, {
      overlayClose: false,
      zindex: 5000,
      timeout: false,
      closeOnEscape: false,
      closeButton: false,
      onClosed: () => {
        tempModal = undefined;
      },
    });

    tempModal.iziModal("open");

    tempModal.body = `<p>${description}</p><div id="modal-${tempModal.id}-form"></div>`;

    let util = new WWSUutil();
    util.waitForElement(`#modal-${tempModal.id}-form`, () => {
      $(`#modal-${tempModal.id}-form`).alpaca({
        schema: {
          title: `Confirm Action`,
          type: "object",
          properties: {
            confirmText: {
              type: "string",
              title: `Confirmation`,
              required: confirmText ? true : false,
            },
          },
        },
        options: {
          fields: {
            confirmText: {
              helper: `Please type <strong>${confirmText}</strong> to confirm your action (case sensitive).`,
              hidden: confirmText ? false : true,
              validator: function (callback) {
                let value = this.getValue();
                if (confirmText && value !== `${confirmText}`) {
                  callback({
                    status: false,
                    message: `You must type <strong>${confirmText}</strong> to confirm your action.`,
                  });
                  return;
                }
                callback({
                  status: true,
                });
              },
            },
          },
          form: {
            buttons: {
              submit: {
                title: `Yes`,
                click: (form, e) => {
                  form.refreshValidationState(true);
                  if (!form.isValid(true)) {
                    form.focus();
                    return;
                  }
                  tempModal.iziModal("close");
                  cb();
                },
              },
              dismiss: {
                title: `No`,
                click: (form, e) => {
                  $(document).Toasts("create", {
                    class: "bg-warning",
                    title: "Action canceled",
                    autohide: true,
                    delay: 10000,
                    body: `You clicked No.`,
                  });
                  tempModal.iziModal("close");
                },
              },
            },
          },
        },
      });
    });
  }

  /**
   * Truncate a string to the specified length.
   *
   * @param {string} str String to truncate
   * @param {number} strLength Number of characters the returned string should contain at maximum (including ending string)
   * @param {string} ending What should be appended to the end when the string is truncated
   */
  truncateText(str, strLength = 256, ending = "...") {
    if (str === null) return "";

    if (str.length > strLength) {
      return `${str.substring(0, strLength - ending.length)}${ending}`;
    } else {
      return str;
    }
  }
}

class WWSUqueue {
  constructor() {
    this.timer = null;
    this.taskList = [];
  }

  idleCallback(callback, options) {
    var options = options || {};
    var relaxation = 1;
    var timeout = options.timeout || relaxation;
    var start = Date.now();
    return setTimeout(function () {
      callback({
        get didTimeout() {
          return options.timeout
            ? false
            : Date.now() - start - relaxation > timeout;
        },
        timeRemaining: function () {
          return Math.max(0, relaxation - (Date.now() - start));
        },
      });
    }, relaxation);
  }

  handleTaskQueue(deadline) {
    if (
      this.taskList.length &&
      (deadline.timeRemaining() > 0 || deadline.didTimeout)
    ) {
      let task = this.taskList.shift();
      task();
    }

    if (this.taskList.length) {
      this.timer = this.idleCallback((deadline) => {
        this.handleTaskQueue(deadline);
      });
    } else {
      this.timer = undefined;
    }
  }

  add(fn, time) {
    if (fn) {
      this.taskList.push(fn);
      if (this.taskList.length === 1 || !this.timer) {
        this.timer = this.idleCallback((deadline) => {
          this.handleTaskQueue(deadline);
        });
      }
    }
  }
}

// Build an izi Modal with some additional properties. If iziModal is not loaded, will use bootstrap modals.
// (iziModal is deprecated and will be removed in a future version)
class WWSUmodal {
  /**
   * Construct the modal.
   *
   * @param {string} title Set the initial title
   * @param {?string} bgClass Set the initial color class for the modal background
   * @param {?string} body Set the initial body of the modal
   * @param {boolean} closeButton Should a close button be made in the top right corner?
   * @param {object} modalOptions Options to pass to iziModal or bootstrap modals
   */
  constructor(
    title = `Untitled Window`,
    bgClass,
    body = ``,
    closeButton = true,
    modalOptions = {}
  ) {
    if (!$) throw new Error("WWSUmodal requires jQuery");

    // Make a WWSUutil instance.
    this.util = new WWSUutil();

    // Create a UUID for the modal.
    this.id = this.util.createUUID();

    // Set internal settings
    this._title = title;
    this.bgClass = bgClass;
    this._body = body;
    this.events = {};
    this.modalOptions = modalOptions;
    this.modalOptions.closeButton = closeButton;
    this.modalOptions.backdrop =
      this.modalOptions.overlayClose === false ? "static" : true;

    // Defaults
    this.modalOptions = Object.assign(
      $.fn.iziModal
        ? {
            headerColor: "#6c757d",
            focusInput: false,
            timeoutProgressbar: true,
            timeoutProgressbarColor: "#E32283",
            bodyOverflow: true,
            overlayClose: true,
          }
        : {
            show: true,
            focus: true,
          },
      this.modalOptions
    );
  }

  get title() {
    return this._title;
  }

  set title(value) {
    if (!$) throw new Error("WWSUmodal requires jQuery");

    this._title = value;
    if (this.izi) this.iziModal("setTitle", value);
  }

  // This returns the inner body DOM, not the actual content. The Modal needs to be open first before this can be used!
  get body() {
    return $(`#modal-${this.id}-body`);
  }

  set body(value) {
    if (!$) throw new Error("WWSUmodal requires jQuery");

    this._body = value;
    if (this.izi) this.iziModal("setContent", value);
  }

  /**
   * Add an event listener for the iziModal div container.
   *
   * @param {string} event The event
   * @param {function} fn The callback when the event is fired
   */
  addEvent(event, fn) {
    this.events[event] = fn;
    $(document).off(event, `#modal-${this.id}`);
    $(document).on(event, `#modal-${this.id}`, fn);
  }

  /**
   * Call iziModal. USE THIS opposed to calling this.izi.iziModal directly as this handles some internal things.
   *
   * @param {string} query The method to execute
   * @param {string|object} options A string or object of parameters to pass
   * @returns {?iziModal} the iziModal element (except when opening)
   */
  iziModal(query, options) {
    if (!$) throw new Error("WWSUmodal requires jQuery");

    if (query === "open" || query === "show") {
      // When opening the modal, we must first create it on the DOM

      if (!document.getElementById(`modal-${this.id}`)) {
        if ($.fn.iziModal) {
          $("body").append(
            `<div id="modal-${this.id}">
            <div id="modal-${this.id}-body" class="p-1 ${this.bgClass}">
            </div>
          </div>`
          );
        } else {
          $("body").append(
            `<div class="modal fade" id="modal-${
              this.id
            }" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content ${this.bgClass}">
					<div class="modal-header">
					  <h4 class="modal-title" id="modal-${this.id}-title">${this._title}</h4>
					  ${
              this.modalOptions.closeButton
                ? `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span>
					</button>`
                : ``
            }
					</div>
					<div class="modal-body" id="modal-${this.id}-body">
					${this._body}
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>`
          );
        }

        // Initialize the model once loaded in the DOM
        this.util.waitForElement(`#modal-${this.id}`, () => {
          if ($.fn.iziModal) {
            this.izi = $(`#modal-${this.id}`).iziModal(
              Object.assign({}, this.modalOptions, {
                title: this._title, // Always use this._title for the modal title
                afterRender: (modal) => {
                  // Show the modal after it is ready
                  modal.open(options);

                  // Just in case, reset the body content in the event this._body changed.
                  $(`#modal-${this.id}-body`).html(this._body);

                  // Perform any other afterRender stuff
                  if (typeof this.modalOptions.afterRender === "function")
                    this.modalOptions.afterRender(modal);
                },
                onClosed: (modal) => {
                  // Perform any other onClosed stuff first
                  if (typeof this.modalOptions.onClosed === "function")
                    this.modalOptions.onClosed(modal);

                  // After the modal is closed, we want to destroy the modal
                  try {
                    if (typeof modal.destroy === "function") modal.destroy();
                    this.izi = undefined;
                  } catch (e) {
                    // Ignore errors
                  }

                  // Now, we want to remove the DOM completely
                  $(`#modal-${this.id}`).remove();
                },
              })
            );

            // Re-set event listeners
            for (let event in this.events) {
              $(document).off(event, `#modal-${this.id}`);
              $(document).on(event, `#modal-${this.id}`, this.events[event]);
            }
          } else {
            this.izi = $(`#modal-${this.id}`).modal(
              Object.assign({}, this.modalOptions, { show: true, focus: true })
            );

            // Set important event listeners
            $(document).off(`show.bs.modal`, `#modal-${this.id}`);
            $(document).on(`show.bs.modal`, `#modal-${this.id}`, () => {
              // Reset title and body just in case
              $(`#modal-${this.id}-title`).html(this._title);
              $(`#modal-${this.id}-body`).html(this._body);

              // Execute event function
              if (this.events["show.bs.modal"]) this.events["show.bs.modal"]();
              if (typeof this.modalOptions.afterRender === "function")
                this.modalOptions.afterRender($(`#modal-${this.id}`));
            });
            $(document).off(`hidden.bs.modal`, `#modal-${this.id}`);
            $(document).on(`hidden.bs.modal`, `#modal-${this.id}`, () => {
              // Execute event function
              if (this.events["hidden.bs.modal"])
                this.events["hidden.bs.modal"]();
              if (typeof this.modalOptions.onClosed === "function")
                this.modalOptions.onClosed($(`#modal-${this.id}`));

              // After the modal is closed, we want to destroy the modal
              try {
                $(`#modal-${this.id}`).modal("dispose");
                this.izi = undefined;
              } catch (e) {
                // Ignore errors
              }

              // Now, we want to remove the DOM completely
              $(`#modal-${this.id}`).remove();

              return (
                $(".modal:visible").length &&
                $(document.body).addClass("modal-open")
              ); // Fix scrollbars
            });

            // Re-set other event listeners
            for (let event in this.events) {
              if (event === "show.bs.modal" || event === "hidden.bs.modal")
                continue; // We already defined these
              $(document).off(event, `#modal-${this.id}`);
              $(document).on(event, `#modal-${this.id}`, this.events[event]);
            }
          }
        });
      }

      return; // Do not continue
    }

    // Close is hide for bootstrap modals
    if (query === "close" && !$.fn.iziModal) query = "hide";

    if (query === "setTitle") {
      this._title = options; // If izi is used to set the title, make sure we update internally.
      if (!$.fn.iziModal) {
        $(`#modal-${this.id}-title`).html(this._title);
        $(`#modal-${this.id}`).modal("handleUpdate");
      }

      return this.izi;
    }
    if (query === "setContent") {
      // Set internal body content
      $(`#modal-${this.id}-body`).html(
        typeof options === "object" ? options.content : options
      );
      // If izi is used to set the body, make sure we update internally if default is true.
      if (typeof options === "object" && options.default)
        this._body = options.content;

      if (!$.fn.iziModal) {
        $(`#modal-${this.id}`).modal("handleUpdate");
      }

      return this.izi; // Do not continue
    }

    if (this.izi && this.izi.iziModal) {
      return this.izi.iziModal(query, options);
    } else if (this.izi && this.izi.modal) {
      return this.izi.modal(query);
    }
  }
}

/**
 *  Class for managing DOM animations
 *  Note: We use this instead of requestAnimationFrame because requestAnimationFrame calls not run due to
 *  background throttling are all called once no longer throttled. This is not ideal because not only does
 *  it freeze the UI momentarily, but it is unnecessary; we only need to process the most recently queued
 *  frame of each animation, which is what this class does.
 *
 * @param {WWSUmodules} manager The WWSUmodules which initialized this class
 * @param {string} options.loadingDOM The DOM query string of the div showing we are refreshing the page (undefined if not applicable). Should have d-none class by default.
 */
class WWSUanimations extends WWSUevents {
  constructor(manager, options = {}) {
    super();

    this.loadingDOM = options.loadingDOM;

    // Hidden window detection
    this.hidden;
    if (typeof document.hidden !== "undefined") {
      // Opera 12.10 and Firefox 18 and later support
      this.hidden = "hidden";
    } else if (typeof document.msHidden !== "undefined") {
      this.hidden = "msHidden";
    } else if (typeof document.webkitHidden !== "undefined") {
      this.hidden = "webkitHidden";
    }

    // Animation queue object: key is animation id, value is function to process the animation.
    this.animations = {};

    this.processed = true;
    this.processing = false;
  }

  processAnimations() {
    let processedAnimation = false;
    for (let key in this.animations) {
      if (Object.prototype.hasOwnProperty.call(this.animations, key)) {
        if (this.animations[key]) this.animations[key]();
        delete this.animations[key];
        if (!this.processing) this.emitEvent("updateStatus", [true]);
        this.processing = true;
        processedAnimation = true;
        if (this.loadingDOM) $(this.loadingDOM).removeClass("d-none");
        break;
      }
    }
    if (!processedAnimation) {
      if (this.processing) this.emitEvent("updateStatus", [false]);
      this.processing = false;
      this.processed = true;
      this.animations = {};
      if (this.loadingDOM) $(this.loadingDOM).addClass("d-none");
    } else {
      window.requestAnimationFrame(() => {
        this.processAnimations();
      });
    }
  }

  /**
   * Add an animation to the queue, which either processes immediately if window is active or is queued if not active.
   *
   * @param {string} name Animation name; if an animation with the same name is already queued, it is replaced.
   * @param {function} fn Function called when it is time to process the animation.
   */
  add(name, fn) {
    if (!document[this.hidden]) {
      delete this.animations[name]; // In case we are running the animation queue; don't accidentally process an older frame
      fn();
    } else {
      // If a function for the same name is already queued, it is overwritten; we only need to process the most recent frame.
      this.animations[name] = fn;
      if (this.processed) {
        this.processed = false;
        window.requestAnimationFrame(() => {
          this.processAnimations();
        });
      }
    }
  }
}

if (typeof require !== "undefined") {
  exports.WWSUdb = WWSUdb;
  exports.WWSUreq = WWSUreq;
  exports.WWSUScriptLoader = WWSUScriptLoader;
  exports.WWSUutil = WWSUutil;
  exports.WWSUqueue = WWSUqueue;
  exports.WWSUmodal = WWSUmodal;
  exports.WWSUanimations = WWSUanimations;
  exports.WWSUevents = WWSUevents;
}
