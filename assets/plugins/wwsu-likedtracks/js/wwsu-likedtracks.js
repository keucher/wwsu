"use strict";

// This class manages the track liking system and recent tracks played
class WWSUlikedtracks extends WWSUevents {
	/**
	 * Construct the directors.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/songs/liked",
			like: "POST /api/songs/liked/:ID?",
		};

		this._likedTracks = [];

		this.table;
	}

	// Start the connection. Call this in socket connect event.
	init() {
    this.initialized = false;
    this.manager.checkInitialized();

		this.manager
			.get("noReq")
			.request(this.endpoints.get, { data: {} }, {}, (body, resp) => {
				if (resp.statusCode <= 400) {
					this._likedTracks = body;
					this.emitEvent("init", [body]);

          this.initialized = true;
          this.manager.checkInitialized();
				}
			});

		// Set on event for new meta
		this.manager.get("WWSUMeta").on("newMeta", "WWSUlikedtracks", (newMeta) => {
			if (typeof newMeta.history !== "undefined") this.updateTable();
		});
	}

	/**
	 * Initialize the table for track history.
	 *
	 * @param {string} table The DOM query string for the div container to place the table.
	 */
	initTable(table) {
		this.manager.get("WWSUanimations").add("history-init-table", () => {
			// Init html
			$(table).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><table id="section-nowplaying-history-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-nowplaying-history-table`, () => {
					// Generate table
					this.table = $(`#section-nowplaying-history-table`).DataTable({
						paging: true,
						data: [],
						columns: [
							{ title: "Date/Time Played" },
							{ title: "Artist - Title" },
							{ title: "Actions" },
						],
						columnDefs: [{ responsivePriority: 1, targets: 2 }],
						pageLength: 100,
						drawCallback: () => {
							// Action button click events
							$(".button-track-like").unbind("click");

							$(".button-track-like").click((e) => {
								this.likeTrack(
									$(e.currentTarget).data("id")
										? parseInt($(e.currentTarget).data("id"))
										: $(e.currentTarget).data("track")
								);
							});
						},
					});

					// Update with information
					this.updateTable();
				});
		});
	}

	/**
	 * Update the recent tracks table if it exists
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("history-update-table", () => {
			if (this.table) {
				this.table.clear();
				this.manager.get("WWSUMeta").meta.history.forEach((track) => {
					this.table.row.add([
						moment
							.tz(
								track.time,
								this.manager.has("WWSUMeta")
									? this.manager.get("WWSUMeta").meta.timezone
									: moment.tz.guess()
							)
							.format("LLL"),
						track.track,
						`${
							(track.ID &&
								!this.likedTracks.find((record) => record.ID === track.ID)) ||
							(track.track &&
								!this.likedTracks.find(
									(record) => record.track === track.track
								))
								? `<button type="button" ${
										track.ID
											? `data-id="${track.ID}"`
											: `data-track="${track.track}"`
								  } class="btn btn-success btn-small button-track-like" tabindex="0" title="Like this track; liked tracks play more often on WWSU."><i class="fas fa-thumbs-up p-1"></i> Like</button>`
								: `<button type="button" class="btn btn-outline-primary btn-small disabled" tabindex="0" title="You already liked this track."><i class="far fa-thumbs-up p-1"></i> Liked</button>`
						}`,
					]);
				});
				this.table.draw(false);
			}
		});
	}

	/**
	 * Get the tracks liked by the current user.
	 *
	 * @returns {array} Array of track IDs or 'artist - title' strings liked
	 */
	get likedTracks() {
		return this._likedTracks;
	}

	/**
	 * Like a track.
	 *
	 * @param {number|string} trackID Track ID liked, or string of 'artist - title'
	 */
	likeTrack(trackID) {
		this.manager.get("noReq").request(
			this.endpoints.like,
			{
				data: isNaN(trackID) ? { track: trackID } : { ID: trackID },
			},
			{},
			(body, resp) => {
				if (resp.statusCode < 400) {
					this._likedTracks.push(body);
					this.updateTable();
					this.emitEvent("likedTrack", [body]);

					$(document).Toasts("create", {
						class: "bg-success",
						title: "Track Liked",
						subtitle: trackID,
						autohide: true,
						delay: 10000,
						body: `<p>You successfully liked a track!</p><p>Liked tracks will play more often on WWSU.</p>`,
						icon: "fas fa-music fa-lg",
					});
				}
			}
		);
	}
}
