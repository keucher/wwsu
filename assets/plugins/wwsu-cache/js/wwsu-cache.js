"use strict";

/**
 * Manager for the cache system when we need to use server-processed info to save on CPU
 * REQUIRES: WWSUdb, noReq
 */
class WWSUcache {
  /**
   * Construct the class.
   *
   * @param {WWSUmodules} manager The modules class which initiated this module
   * @param {object} options Options to be passed to this module
   */
  constructor(manager, options) {
    this.manager = manager;

    // Initialize a map of WWSUdbs we will use to manage cache.
    this.cache = new Map();
  }

  /**
   * Initialize a cache.
   *
   * @param {string} model Name of the model to initialize (should be accessible via /cache/(model) in the WWSU API)
   * @param {?WWSUdb} db The WWSUdb to use. If not provided, will create a new one.
   * @param {Object} data Data to be passed to the API when fetching cached data.
   * @returns {WWSUdb} The cache DB
   *
   */
  initCache(model, db, data = {}) {
    let cache;
    // If the cache does not exist, create it with WWSUdb and make socket handler
    if (!this.cache.has(model)) {
      cache = db || new WWSUdb();
      cache.assignSocketEvent(`cache-${model}`, this.manager.socket);
      this.cache.set(model, cache);
    }

    // Replace data in the cache
    cache = this.cache.get(model);
    if (cache) {
      cache.replaceData(this.manager.get("noReq"), `/cache/${model}`, data);
    }

    return cache;
  }

  /**
   * Quick fetch a WWSUdb
   */
  model(model) {
      return this.cache.get(model);
  }
}
