"use strict";

/**
 * Manager for all WWSU modules / classes.
 * @requires JQuery if loadingDOM was provided in the constructor
 */
class WWSUmodules {
  /**
   * Class constructor
   *
   * @param {sails.io} socket Socket connection to WWSU
   * @param {string} loadingDOM DOM query string of the div container containing the "Loading WWSU Data" message, to be displayed until all modules have initialized. Can be undefined if does not exist.
   */
  constructor(socket, loadingDOM) {
    this.socket = socket;
    this.loadingDOM = loadingDOM;

    this.modules = new Map();

    this.initChecker = setInterval(() => {
      let notInitialized = [];

      this.modules.forEach((module, name) => {
        if (typeof module.initialized !== "undefined" && !module.initialized) {
          notInitialized.push(name);
        }
      });

      if (notInitialized.length > 0) {
        console.log(
          `Still waiting for data from WWSU for these modules: ${notInitialized.join(
            "; "
          )}`
        );
      } else {
        this.checkInitialized(); // Possible we missed something (or everything is loaded)
      }
    }, 15000);
  }

  /**
   * Initialize / add a module to the manager. You should add all applicable modules to the manager before calling init on any of them.
   *
   * @param {string} name Name of module
   * @param {class} module The un-initialized module class (its constructor should pass manager and options as parameters)
   * @param {object} options Options to pass to the module when initialized
   */
  add(name, module, options) {
    // Do not re-initialize a module already initialized
    if (!this.modules.has(name)) {
      this.modules.set(name, new module(this, options));
    }

    // allow chaining
    return this;
  }

  /**
   * Get / return a module
   *
   * @param {string} name name of module to return
   */
  get(name) {
    if (!this.modules.has(name))
      throw new Error(`The specified module ${name} was not added yet.`);
    return this.modules.get(name);
  }

  /**
   * See if a module exists
   *
   * @param {string} name name of module to return
   */
  has(name) {
    return this.modules.has(name);
  }

  /**
   * Check if all modules have initialized. If so, remove the "Loading" DOM. Else, show it with progress.
   */
  checkInitialized() {
    // Check if any modules have not yet loaded.
    let totalModules = 0;
    let initializedModules = 0;
    this.modules.forEach((module) => {
      if (typeof module.initialized !== "undefined") {
        totalModules++;
        if (module.initialized) initializedModules++;
      }
    });

    // If all modules have loaded, then hide the loading DOM.
    if (initializedModules >= totalModules) {
      if (this.loadingDOM) $(this.loadingDOM).addClass("d-none");
    } else if (this.loadingDOM) {
      $(this.loadingDOM).removeClass("d-none");
      $(this.loadingDOM).html(
        `Fetching data from WWSU... (${initializedModules} / ${totalModules})`
      );
    }
  }
}
