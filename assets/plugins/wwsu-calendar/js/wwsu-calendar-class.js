"use strict";

/**
 * This class extends CalendarDb to use WWSU's calendar API and build the calendar interface.
 * @requires WWSUthis.manager.get("WWSUutil") WWSU this.manager.get("WWSUutil")ity class
 * @requires CalendarDb Base WWSU calendar class for processing data into calendar events
 * @requires WWSUmodal iziModal wrapper
 * @requires WWSUevents WWSU event emitter
 * @requires $ jQuery
 * @requires $.block jQuery blockUI
 * @requires $.DataTable DataTables.js
 * @requires $.alpaca Alpaca forms custom WWSU build
 */

// REQUIRES the following WWSUmodules: noReq (WWSUreq), directorReq (WWSUreq) (only if managing calendar), djReq (WWSUreq) (Only on DJ web panel), WWSUMeta, WWSUutil, WWSUdjs (only if editing/adding calendar events), WWSUhosts (only if editing/adding calendar events), WWSUclocks (if creating a clockwheel), WWSUconfig, WWSUlogs
// WWSUMeta MUST be loaded before WWSUcalendar is loaded!
class WWSUcalendar extends CalendarDb {
	/**
	 * Create the calendar class.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options = {}) {
		super(undefined, undefined, undefined, manager.get("WWSUMeta"), options); // Create the db

		this.calendarInitialized = false;
		this.scheduleInitialized = false;
		this.clockwheelsInitialized = false;

		this.options = options;

		this.manager = manager;

		this.endpoints = {
			add: "POST /api/calendar/event",
			addSchedule: "POST /api/calendar/schedule",
			edit: "PUT /api/calendar/event/:ID",
			editSchedule: "PUT /api/calendar/schedule/:ID",
			get: "GET /api/calendar/event/:ID?",
			getClockwheels: "GET /api/calendar/clockwheels/host",
			getEventsPlaylists: "GET /api/calendar/events-playlists",
			getSchedule: "GET /api/calendar/schedule/:ID?",
			remove: "DELETE /api/calendar/event/:ID",
			removeSchedule: "DELETE /api/calendar/schedule/:ID",
			inactive: "PUT /api/calendar/event/:ID/deactivate",
			active: "PUT /api/calendar/event/:ID/activate",
		};
		this.data = {
			add: {},
			addSchedule: {},
			edit: {},
			editSchedule: {},
			get: {},
			getClockwheels: {},
			getEventsPlaylists: {},
			getSchedule: {},
			remove: {},
			removeSchedule: {},
			inactive: {},
			active: {},
		};

		this.events = new WWSUevents();

		this.clock;

		this.calendar.assignSocketEvent("calendar", this.manager.socket);
		this.schedule.assignSocketEvent("schedule", this.manager.socket);
		this.clockwheels.assignSocketEvent("clockwheels", this.manager.socket);

		// Emit calendarUpdated whenever a change is made to the calendar.
		this.calendar.on("change", "WWSUcalendar", () => {
			this.calendarUpdated();
		});
		this.schedule.on("change", "WWSUcalendar", () => {
			this.calendarUpdated();
		});
		this.clockwheels.on("change", "WWSUcalendar", () => {
			this.calendarUpdated();
		});

		// WWSUmodules loading check
		this.calendar.on("replace", "WWSUcalendar", () => {
			this.calendarInitialized = true;
			if (
				this.calendarInitialized &&
				this.scheduleInitialized &&
				this.clockwheelsInitialized
			) {
				this.initialized = true;
				this.manager.checkInitialized();
			}
		});
		this.schedule.on("replace", "WWSUcalendar", () => {
			this.scheduleInitialized = true;
			if (
				this.calendarInitialized &&
				this.scheduleInitialized &&
				this.clockwheelsInitialized
			) {
				this.initialized = true;
				this.manager.checkInitialized();
			}
		});
		this.clockwheels.on("replace", "WWSUcalendar", () => {
			this.clockwheelsInitialized = true;
			if (
				this.calendarInitialized &&
				this.scheduleInitialized &&
				this.clockwheelsInitialized
			) {
				this.initialized = true;
				this.manager.checkInitialized();
			}
		});

		// Generate a modal for displaying event conflicts
		this.conflictModal = new WWSUmodal(
			`Event Conflicts`,
			`bg-info`,
			``,
			false,
			{
				overlayClose: false,
				zindex: 2000,
			}
		);

		// Generate other modals
		this.occurrenceModal = new WWSUmodal(`Occurrence`, null, ``, true, {
			zindex: 1100,
			// openFullscreen: true,
		});
		this.newOccurrenceModal = new WWSUmodal(`New Occurrence`, null, ``, true, {
			zindex: 1100,
			// openFullscreen: true,
		});
		this.occurrenceActionModal = new WWSUmodal(
			`Occurrence Action`,
			null,
			``,
			true,
			{
				zindex: 1110,
				overlayClose: false,
				// openFullscreen: true,
			}
		);
		this.eventsModal = new WWSUmodal(`Events`, null, ``, true, {
			zindex: 1100,
			// openFullscreen: true,
		});
		this.schedulesModal = new WWSUmodal(`Schedules`, null, ``, true, {
			zindex: 1110,
			// openFullscreen: true,
		});
		this.scheduleModal = new WWSUmodal(`Schedules`, null, ``, true, {
			zindex: 1120,
			overlayClose: false,
			// openFullscreen: true,
		});
		this.eventModal = new WWSUmodal(`New Event`, null, ``, true, {
			zindex: 1120,
			overlayClose: false,
			// openFullscreen: true,
		});
		this.definitionsModal = new WWSUmodal(
			`Calendar Definitions`,
			null,
			`<p><strong>Event</strong>: Something that can be scheduled on the calendar.
        (Example: "DJ test - The test show")</p>
      <p><strong>Schedule</strong>: Collection of dates/times and/or recurrence
        rules defining when the event takes place on the calendar. (Example:
        "Tuesdays and Thursdays 9 - 10PM")</p>
      <p><strong>Occurrence</strong>: A specific/single date/time the event takes
        place. (Example: "February 30, 2000, 9 - 10PM")</p>`,
			true,
			{
				zindex: 1100,
				// openFullscreen: true,
			}
		);

		this.prerequisitesModal = new WWSUmodal(
			`Calendar Prerequisites`,
			null,
			`<p><span class="badge bg-pink">Prerecord</span> and <span
        class="badge badge-info">Playlist</span>:
      Ensure audio files are
      split in such a way the top-of-hour ID break will air on time. When using RadioDJ... Import
      tracks into RadioDJ, and then create and save a playlist in the playlist
      builder. Note the playlist name you save as. For prerecords, make sure the tracks which will be overwritten each episode are of Track Type "Variable Duration File", "Internet Stream", "Podcast", "File By Date", or "Newest From Folder".</p>
    <p><span class="badge bg-blue">Genre</span>: When using RadioDJ... In RadioDJ, make a track
      rotation and create a Manual Event that triggers the rotation. Note the
      event name you save as.</p>
    <p><span class="badge badge-danger">Show</span>, <span class="badge bg-pink">Prerecord</span>, or
      <span class="badge bg-indigo">Remote</span>: Be sure to add all members hosting the broadcast via
      the
      "Org Members" administration menu before adding the event. For live shows, they will need permission to start live broadcasts. And for remote shows, they will need permission to start remote broadcasts (and there must be at least one authorized host belonging to them in the DJ Controls Hosts page).
    </p>
    <p><span class="badge badge-warning">Office Hours</span> and <span
        class="badge bg-orange">Tasks</span>: Add the director in the system
      via "Directors" administration menu if they have not already been added.</p>`,
			true,
			{
				zindex: 1100,
				// openFullscreen: true,
			}
		);
	}

	/**
	 * Get the timezone we should use.
	 */
	get timezone() {
		return this.manager.has("WWSUMeta")
			? this.manager.get("WWSUMeta").timezone
			: moment.tz.guess();
	}

	// Initialize the calendar. Call this on socket connect event.
	init() {
		this.initialized = false;
		this.manager.checkInitialized();

		this.calendar.replaceData(
			this.manager.get("noReq"),
			this.endpoints.get,
			this.data.get
		);
		this.schedule.replaceData(
			this.manager.get("noReq"),
			this.endpoints.getSchedule,
			this.data.getSchedule
		);
		this.clockwheels.replaceData(
			this.manager.get("noReq"),
			this.endpoints.getClockwheels,
			this.data.getClockwheels
		);
	}

	/**
	 * Initialize a clockwheel.
	 *
	 * @param {string} name Name to use for the clock
	 * @param {string} dom DOM container (must be position relative) to place the clock.
	 */
	initClock(name, dom) {
		if (this.clock) return;
		this.clock = name;
		this.manager.get("WWSUclocks").new(
			name,
			dom,
			{
				labels: ["Not Yet Loaded"],
				datasets: [
					{
						data: [60],
						backgroundColor: ["#000000"],
					},
					{
						data: [720],
						backgroundColor: ["#000000"],
					},
				],
			},
			{
				maintainAspectRatio: false,
				responsive: true,
				cutout: "75%",
				animation: {
					animateRotate: false,
					animateScale: false,
				},
				elements: {
					arc: {
						borderColor: "rgba(0, 0, 0, 0)",
					},
				},
				plugins: {
					legend: {
						display: false,
					},
					tooltip: {
						callbacks: {
							label: function (context) {
								let label = context.dataset.label || "";

								if (label) {
									label += ": ";
								}

								label += moment
									.duration(context.dataset.data[context.dataIndex], "minutes")
									.format("h [hours], m [minutes]");

								return label;
							},
						},
					},
				},
			}
		);
	}

	updateClock(data) {
		if (!this.clock) return;
		// Update the donut
		this.manager.get("WWSUclocks").updateChart(this.clock, data);
	}

	// Shortcuts for WWSUevents
	on(event, scope, fn) {
		this.events.on(event, scope, fn);
	}
	once(event, scope, fn) {
		this.events.once(event, scope, fn);
	}
	off(event, scope, fn) {
		this.events.off(event, scope);
	}
	emitEvent(event, args) {
		this.events.emitEvent(event, args);
	}

	// Emit calendarUpdated event when called.
	calendarUpdated() {
		this.emitEvent("calendarUpdated", []);
	}

	/**
	 * Get a specific calendar event (and their stats) from the system.
	 *
	 * @param {string} dom the DOM query string of the element to block while retrieving data
	 * @param {object} data Data to be passed to the WWSU API
	 * @param {function} cb Function to be called with the API info returned as a parameter.
	 */
	getEvent(dom, data, cb) {
		this.manager.get("hostReq").request(
			this.endpoints.get,
			{
				data: data,
			},
			{ dom },
			(body, resp) => {
				if (resp.statusCode < 400 && typeof body === "object" && body.event) {
					cb(body);
				} else {
					cb(false);
				}
			}
		);
	}

	/**
	 * Generate a simple DataTables.js table of the events in the system and have edit/delete links
	 */
	showSimpleEvents() {
		// Initialize the table class
		this.eventsModal.iziModal("open");
		this.eventsModal.body = `<table id="modal-${this.eventsModal.id}-table" class="table table-striped" style="min-width: 100%;"></table>
								<h5>Actions Key:</h5>
								<div class="container-fluid">
									<div class="row">
										<div class="col">
											<span class="badge badge-info"
												><i class="fas fa-calendar"></i></span
											>Manage Schedules
										</div>
										<div class="col">
											<span class="badge badge-warning"
												><i class="fas fa-edit"></i></span
											>Edit Event / Defaults
										</div>
										<div class="col">
											<span class="badge bg-orange"
												><i class="fas fa-times-circle"></i></span
											>Mark Event Inactive and Delete Schedules
										</div>
										<div class="col">
											<span class="badge badge-success"
												><i class="fas fa-check-circle"></i></span
											>Mark Event Active
										</div>
										<div class="col">
											<span class="badge badge-danger"
												><i class="fas fa-trash"></i></span
											>Delete Event and Schedules
										</div>
									</div>
								</div>
								<button type="button" class="btn btn-outline-success" id="modal-${this.eventsModal.id}-new">New Event</button>
								`;

		// Block the modal while we generate the table
		$(`#modal-${this.eventsModal.id} .modal-content`)
			.prepend(`<div class="overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
              <h1 class="text-white">Making table...</h1>
        </div>`);
		// Generate the data table
		let table = $(`#modal-${this.eventsModal.id}-table`).DataTable({
			paging: true,
			data: [],
			columns: [
				{ title: "Type" },
				{ title: "Event Name" },
				{ title: "Active?" },
				{ title: "Actions" },
			],
			columnDefs: [{ responsivePriority: 1, targets: 3 }],
			order: [
				[0, "asc"],
				[1, "asc"],
			],
			pageLength: 25,
			drawCallback: () => {
				// Action button click events
				$(".btn-event-analytics").unbind("click");
				$(".btn-event-logs").unbind("click");
				$(".btn-event-edit").unbind("click");
				$(".btn-event-editschedule").unbind("click");
				$(".btn-event-delete").unbind("click");
				$(".btn-event-inactive").unbind("click");
				$(".btn-event-active").unbind("click");

				// Analytics
				$(".btn-event-analytics").click((e) => {
					this.manager
						.get("WWSUanalytics")
						.showAnalytics("event", parseInt($(e.currentTarget).data("calendarid")));
				});

				// Logs
				$(".btn-event-logs").click((e) => {
					let event = this.calendar
						.find()
						.find(
							(event) =>
								event.ID === parseInt($(e.currentTarget).data("calendarid"))
						);
					this.manager
						.get("WWSUlogs")
						.viewLogs(`${event.type}: ${event.name}`, { calendarID: event.ID });
				});

				// Edit event
				$(".btn-event-edit").click((e) => {
					let event = this.calendar
						.find()
						.find(
							(event) =>
								event.ID === parseInt($(e.currentTarget).data("calendarid"))
						);
					this.showEventForm(event);
				});

				// Edit event's schedules
				$(".btn-event-editschedule").click((e) => {
					this.showSchedules(parseInt($(e.currentTarget).data("calendarid")));
				});

				// Confirm before deleting when someone wants to delete an event
				$(".btn-event-delete").click((e) => {
					let event = this.calendar
						.find()
						.find(
							(event) =>
								event.ID === parseInt($(e.currentTarget).data("calendarid"))
						);
					this.manager.get("WWSUutil").confirmDialog(
						`<p>Are you sure you want to <b>permanently</b> remove ${
							event.name
						}?</p>
                            <ul>
							<li><strong>It is NOT recommended permanently removing an event unless the event was added out of error</strong>. Instead, mark or leave the event as inactive. Broadcast-related events are automatically deleted when they go a year without airing.</li>
                                <li>Removes the event</li>
                                <li>Removes all schedules of the event from the calendar</li>
                                ${
																	[
																		"show",
																		"sports",
																		"remote",
																		"prerecord",
																		"genre",
																		"playlist",
																	].indexOf(event.type) !== -1
																		? `<li>If one or more schedules were active, notifies all notification subscribers the event has been discontinued</li><li>Removes all notification subscriptions</li>`
																		: ``
																}
                                ${
																	[
																		"show",
																		"sports",
																		"remote",
																		"prerecord",
																		"playlist",
																	].indexOf(event.type) !== -1
																		? `<li>Notifies host members via email that the event has been discontinued.</li>`
																		: ``
																}
                                ${
																	[
																		"show",
																		"sports",
																		"remote",
																		"prerecord",
																		"genre",
																		"playlist",
																	].indexOf(event.type) !== -1
																		? `<li>Does not remove logs; they can still be accessed from the "logs" page of DJ Controls.</li><li>Does not remove analytics, but cannot be accessed anymore from the "Analytics" nor "Org Members" pages of DJ Controls.</li>`
																		: ``
																}
                            </ul>`,
						event.name,
						() => {
							this.removeCalendar(
								this.eventsModal,
								{ ID: parseInt($(e.currentTarget).data("calendarid")) },
								(success) => {
									this.eventsModal.body = `<div class="alert alert-warning">
                                Event changes take several seconds to reflect in the system. Please close and re-open this window.
                                </div>`;
								}
							);
						}
					);
				});

				$(".btn-event-inactive").click((e) => {
					let event = this.calendar
						.find()
						.find(
							(event) =>
								event.ID === parseInt($(e.currentTarget).data("calendarid"))
						);
					this.manager.get("WWSUutil").confirmDialog(
						`<p>Are you sure you want to mark ${event.name} as inactive?</p>
							<ul>
							<li><strong>Do not mark an event as inactive unless it will [probably] no longer air on WWSU now or in the future.</strong></li>
							<li><strong>Marking an event as inactive will remove all its schedules</strong>. This cannot be reversed!</li>
							${
								[
									"show",
									"sports",
									"remote",
									"prerecord",
									"genre",
									"playlist",
								].indexOf(event.type) !== -1
									? `<li>If at least one active schedule was present, all notification subscribers will be notified the event has been discontinued</li><li>Removes all notification subscriptions</li>`
									: ``
							}
							${
								["show", "sports", "remote", "prerecord", "playlist"].indexOf(
									event.type
								) !== -1
									? `<li>Will also email the hosts to inform them the event was marked inactive / discontinued.</li>`
									: ``
							}
							${
								[
									"show",
									"sports",
									"remote",
									"prerecord",
									"genre",
									"playlist",
								].indexOf(event.type) !== -1
									? `<li>Show logs and analytics for this broadcast will remain viewable in the system and in DJ Controls.</li>`
									: ``
							}
							<li>Events are automatically and permanently removed from the system after going one year without airing (broadcast type events only). This means its analytics can no longer be viewed once permanently removed (but logs will still be available).</li>
							</ul>`,
						event.name,
						() => {
							this.inactiveCalendar(
								this.eventsModal,
								{ ID: event.calendarID || event.ID },
								(success) => {
									this.eventsModal.body = `<div class="alert alert-warning">
                                Event changes take several seconds to reflect in the system. Please close and re-open this window.
                                </div>`;
								}
							);
						}
					);
				});

				$(".btn-event-active").click((e) => {
					let event = this.calendar
						.find()
						.find(
							(_event) =>
								_event.ID === parseInt($(e.currentTarget).data("calendarid"))
						);
					this.manager.get("WWSUutil").confirmDialog(
						`<p>Are you sure you want to mark ${event.name} as active?</p>
							<ul>
							<li>Re-activates the event and allows you to create schedules and occurrences.</li>
							<li><strong>Does NOT recover old schedules from before the event was marked inactive</strong>; you will need to add new schedules.</li>
							<li>Does NOT inform hosts nor subscribers that the event has been re-activated; you will need to do this yourself.</li>
							</ul>`,
						null,
						() => {
							console.dir(event);
							let _event = this.verify(
								event,
								this.manager.get("WWSUconfig").usingRadioDJ
							); // Verify the event is valid first just to be extra sure
							console.dir(_event);

							if (!_event.event) {
								if (this.manager.has("WWSUehhh"))
									this.manager.get("WWSUehhh").play();
								$(document).Toasts("create", {
									class: "bg-warning",
									title: "Event verification failed",
									autohide: true,
									delay: 20000,
									body: _event,
								});
								form.focus();
								return;
							}

							this.activeCalendar(
								this.eventsModal,
								{ ID: event.ID },
								(success) => {
									this.eventsModal.body = `<div class="alert alert-warning">
                                Event changes take several seconds to reflect in the system. Please close and re-open this window.
                                </div>`;
								}
							);
						}
					);
				});
			},
		});

		// Populate the data table with data.
		let drawRows = () => {
			this.calendar.find().forEach((calendar) => {
				table.rows.add([
					[
						`<span class="badge bg-${this.getColorClass(calendar)}">${
							calendar.type
						}</span>`,
						calendar.name,
						calendar.active
							? `<span class="badge badge-success" title="This event is active."><i class="fas fa-check-circle p-1"></i>Yes</span>`
							: `<span class="badge badge-danger" title="This event is inactive."><i class="far fa-times-circle p-1"></i>No</span>`,
						`<div class="btn-group">${
							[
								"office-hours",
								"event",
								"prod-booking",
								"onair-booking",
								"task",
							].indexOf(calendar.type) === -1
								? `<button class="btn btn-sm bg-blue btn-event-analytics" data-calendarid="${calendar.ID}" title="View show analytics"><i class="fas fa-chart-line"></i></button><button class="btn btn-sm btn-secondary btn-event-logs" data-calendarid="${calendar.ID}" title="View Broadcast Logs"><i class="fas fa-clipboard-list"></i></button>`
								: ``
						}
								${
									calendar.active || calendar.ID === 1
										? `<button class="btn btn-sm btn-info btn-event-editschedule" data-calendarid="${calendar.ID}" title="Edit Schedule"><i class="fas fa-calendar"></i></button>`
										: `<div class="btn-group">
								<button class="btn btn-sm btn-success btn-event-active" data-calendarid="${calendar.ID}" title="Mark event as active."><i class="fas fa-check-circle"></i></button><button class="btn btn-sm btn-danger btn-event-delete" data-calendarid="${calendar.ID}" title="Permanently delete event and all its schedules."><i class="fas fa-trash"></i></button>`
								}${
							[
								"office-hours",
								"sports",
								"prod-booking",
								"onair-booking",
							].indexOf(calendar.type) === -1
								? `<button class="btn btn-sm btn-warning btn-event-edit" data-calendarid="${
										calendar.ID
								  }" title="Edit Event"><i class="fas fa-edit"></i></button>${
										calendar.active && calendar.ID !== 1
											? `<button class="btn btn-sm bg-orange btn-event-inactive" data-calendarid="${calendar.ID}" title="Mark event as inactive and delete all its schedules."><i class="fas fa-times-circle"></i></button>`
											: ``
								  }`
								: ``
						}</div>`,
					],
				]);
			});
			table.draw(false);

			$(`#modal-${this.eventsModal.id} .overlay`).remove();
		};

		drawRows();

		$(`#modal-${this.eventsModal.id}-new`).unbind("click");
		$(`#modal-${this.eventsModal.id}-new`).click(() => {
			this.showEventForm(null);
		});
	}

	/**
	 * Open a dataTables.js modal with the schedules for the provided calendar event.
	 *
	 * @param {number} calendarID Calendar event ID
	 */
	showSchedules(calendarID) {
		// Initialize the table
		this.schedulesModal.iziModal("open");
		this.schedulesModal.body = `<table id="modal-${this.schedulesModal.id}-table" class="table table-striped" style="min-width: 100%;"></table>
		<h5>Actions Key:</h5>
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<span class="badge badge-warning"
						><i class="fas fa-edit"></i></span
					>Edit
				</div>
				<div class="col">
					<span class="badge badge-danger"
						><i class="fas fa-trash"></i></span
					>Delete
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-outline-success" id="modal-${this.schedulesModal.id}-new">New Schedule</button>`;

		// Block the modal while we populate
		$(`#modal-${this.schedulesModal.id} .modal-content`)
			.prepend(`<div class="overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
              <h1 class="text-white">Loading...</h1>
        </div>`);
		// Generate the table
		let table = $(`#modal-${this.schedulesModal.id}-table`).DataTable({
			scrollCollapse: true,
			paging: true,
			data: [],
			columns: [{ title: "Schedule" }, { title: "Actions" }],
			pageLength: 25,
			columnDefs: [{ responsivePriority: 1, targets: 1 }],
			drawCallback: () => {
				// Action button click events
				$(".btn-schedule-edit").unbind("click");
				$(".btn-schedule-delete").unbind("click");

				// Edit a schedule
				$(".btn-schedule-edit").click((e) => {
					let schedule = this.schedule.find(
						{ ID: parseInt($(e.currentTarget).data("scheduleid")) },
						true
					);
					let calendarID = parseInt($(e.currentTarget).data("calendarid"));
					this.showScheduleForm(schedule, calendarID);
				});

				// Prompt before deleting a schedule
				$(".btn-schedule-delete").click((e) => {
					let schedule = this.schedule.find(
						{ ID: parseInt($(e.currentTarget).data("scheduleid")) },
						true
					);
					let calendarID = parseInt($(e.currentTarget).data("calendarid"));
					let calendar = this.calendar.find({ ID: calendarID }, true);
					this.manager.get("WWSUutil").confirmDialog(
						`<p>Are you sure you want to delete that schedule?</p>
                        <ul>
                            <li>Please <strong>do not</strong> delete schedules to cancel a specific date/time; click the occurrence on the calendar and elect to cancel it.</li>
                            ${
															[
																"show",
																"sports",
																"remote",
																"prerecord",
																"genre",
																"playlist",
															].indexOf(
																schedule ? schedule.type : calendar.type
															) !== -1
																? `<li>Does NOT notify subscribers.</li>`
																: ``
														}
                            ${
															[
																"show",
																"sports",
																"remote",
																"prerecord",
																"playlist",
															].indexOf(
																schedule ? schedule.type : calendar.type
															) !== -1
																? `<li>Does NOT email hosts; you will need to let them know of the change.</li>`
																: ``
														}
                            <li>A conflict check will run, and you will be notified of occurrence changes that will be made to avoid conflicts</li>
                        </ul>`,
						null,
						() => {
							let scheduleID = parseInt($(e.currentTarget).data("scheduleid"));
							this.removeSchedule(
								this.schedulesModal,
								{ ID: scheduleID },
								(success) => {
									if (success) {
										this.schedulesModal.body = `<div class="alert alert-warning">
                                    Schedule changes take several seconds to reflect in the system. Please close and re-open this window.
                                    </div>`;
									}
								}
							);
						}
					);
				});
			},
		});

		// Populate the table with data
		let drawRows = () => {
			this.schedule.find({ calendarID: calendarID }).forEach((schedule) => {
				// Skip all schedule entries that are unauthorized or specify an update/cancellation; these should be managed via the calendar.
				if (schedule.scheduleType !== null) {
					return;
				}

				table.rows.add([
					[
						this.generateScheduleText(schedule),
						`${
							["canceled-system", "updated-system"].indexOf(
								schedule.scheduleType
							) === -1
								? `<div class="btn-group"><button class="btn btn-sm btn-warning btn-schedule-edit" data-scheduleid="${schedule.ID}" data-calendarid="${schedule.calendarID}" title="Edit Schedule"><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-schedule-delete" data-scheduleid="${schedule.ID}" data-calendarid="${schedule.calendarID}" title="Delete Schedule"><i class="fas fa-trash"></i></button></div>`
								: ``
						}`,
					],
				]);
			});
			table.draw(false);
			$(`#modal-${this.schedulesModal.id} .overlay`).remove();
		};

		drawRows();

		$(`#modal-${this.schedulesModal.id}-new`).unbind("click");
		$(`#modal-${this.schedulesModal.id}-new`).click(() => {
			this.showScheduleForm(null, calendarID);
		});
	}

	/**
	 * Get events and playlists from WWSU's API that can be selected in calendar and event forms.
	 *
	 * @param {function} cb Callback containing array of events first parameter, array of playlists second parameter (only holds ID and name properties)
	 */
	getEventsPlaylists(cb) {
		this.manager
			.get("noReq")
			.request(
				this.endpoints.getEventsPlaylists,
				{ data: {} },
				{},
				(body, resp) => {
					if (
						resp.statusCode >= 400 ||
						typeof body !== "object" ||
						!body.playlists ||
						!body.events
					) {
						cb([], []);
					} else {
						cb(body.events, body.playlists);
					}
				}
			);
	}

	/**
	 * Tell WWSU API to add a schedule.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	addSchedule(modal, data, cb) {
		this.doConflictCheck(modal, data, "insert", () => {
			this.manager.get("directorReq").request(
				this.endpoints.addSchedule,
				{
					data: data,
				},
				{ domModal: modal.id },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Schedule added!",
							autohide: true,
							delay: 15000,
							body: `Schedule was added! However, it may take several seconds to register in the WWSU system.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
		});
	}

	/**
	 * Tell WWSU API to edit a schedule.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	editSchedule(modal, data, cb) {
		this.doConflictCheck(modal, data, "update", () => {
			this.manager.get("directorReq").request(
				this.endpoints.editSchedule,
				{
					data: data,
				},
				{ domModal: modal.id },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Schedule edited!",
							autohide: true,
							delay: 15000,
							body: `Schedule was edited! However, it may take several seconds to register in the WWSU system.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
		});
	}

	/**
	 * Tell WWSU API to remove a schedule.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	removeSchedule(modal, data, cb) {
		let schedule;
		try {
			schedule = this.schedule.find({ ID: data.ID }, true);
		} catch (e) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Error removing/reversing schedule",
				body: "There was an error removing/reversing the schedule. Please report this to the engineer.",
				autohide: true,
				delay: 10000,
				icon: "fas fa-skull-crossbones fa-lg",
			});
			cb(false);
			return;
		}
		this.doConflictCheck(modal, schedule, "remove", () => {
			this.manager.get("directorReq").request(
				this.endpoints.removeSchedule,
				{
					data: data,
				},
				{ domModal: modal.id },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Schedule removed!",
							autohide: true,
							delay: 15000,
							body: `Schedule was removed! However, it may take several seconds to register in the WWSU system.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
		});
	}

	/**
	 * Tell WWSU API to add a calendar event.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	addCalendar(modal, data, cb) {
		// No conflict check necessary because new calendar events will never have a schedule immediately on creation.
		this.manager.get("directorReq").request(
			this.endpoints.add,
			{
				data: data,
			},
			{ domModal: modal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Event added",
						autohide: true,
						delay: 10000,
						body: `Event was added!`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Tell WWSU API to edit a calendar event.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	editCalendar(modal, data, cb) {
		// Editing a calendar event requires conflict checking because it may affect the priority of its schedules using default values.
		this.doConflictCheck(modal, data, "updateCalendar", () => {
			this.manager.get("directorReq").request(
				this.endpoints.edit,
				{
					data: data,
				},
				{ domModal: modal.id },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Event edited",
							autohide: true,
							delay: 15000,
							body: `Event was edited! It may take several seconds for it to reflect in the system.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
		});
	}

	/**
	 * Tell WWSU API to remove an entire calendar event.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	removeCalendar(modal, data, cb) {
		// We need to determine if the removal of this calendar (and thus all its schedules) will affect other events
		let calendar = this.calendar.find({ ID: data.ID }, true);
		this.doConflictCheck(modal, calendar, "removeCalendar", () => {
			this.manager.get("directorReq").request(
				this.endpoints.remove,
				{
					data: data,
				},
				{ domModal: modal.id },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Event remove",
							autohide: true,
							delay: 15000,
							body: `Event was permanently removed! However, it may take several seconds to register in the WWSU system.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
		});
	}

	/**
	 * Tell WWSU API to mark a calendar event as inactive.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	inactiveCalendar(modal, data, cb) {
		// We need to determine if the removal of this calendar (and thus all its schedules) will affect other events
		console.dir(data);
		let calendar = this.calendar.find({ ID: data.ID }, true);
		this.doConflictCheck(modal, calendar, "removeCalendar", () => {
			this.manager.get("directorReq").request(
				this.endpoints.inactive,
				{
					data: data,
				},
				{ domModal: modal.id },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Event marked inactive",
							autohide: true,
							delay: 15000,
							body: `Event was marked inactive and all schedules removed! However, it may take several seconds for the changes to reflect in the system.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
		});
	}

	/**
	 * Tell WWSU API to mark a calendar event as active.
	 *
	 * @param {WWSUmodal} modal Modal to get blocked by JQuery UI when prompting for authorization.
	 * @param {object} data Data to pass to the endpoint.
	 * @param {function} cb Function called after the request. True = success, false = failure.
	 */
	activeCalendar(modal, data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.active,
			{
				data: data,
			},
			{ domModal: modal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Event marked active",
						autohide: true,
						delay: 15000,
						body: `Event was marked active! However, it may take several seconds for the changes to reflect in the system.`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Display a modal of a clicked event from the calendar.
	 *
	 * @param {object} event Calendardb event
	 */
	showClickedEvent(event) {
		console.dir(event);
		this.occurrenceModal.iziModal("open");
		this.occurrenceModal.title = `${event.type}: ${event.hosts} - ${event.name}`;
		this.occurrenceModal.body =
			this.generateFullEventCard(event) +
			`<div id="modal-calendar-occurrence-footer"></div>`;

		// Initialize this.manager.get("WWSUutil")ities

		// Initialize choices / actions that can be performed on the event.
		let choices = ["Do Nothing"];

		// Determine what else can be done
		if (event.scheduleType === "updated") {
			choices.push(`Occurrence: Reverse this Update or Reschedule`);
		}
		if (event.scheduleType === "canceled") {
			choices.push(`Occurrence: Reverse this Cancellation`);
		}
		if (
			[
				"canceled",
				"canceled-system",
				"canceled-changed",
				"unscheduled",
			].indexOf(event.scheduleType) === -1
		) {
			choices.push(`Occurrence: Cancel This Date/Time`);
		}
		if (
			[
				"canceled",
				"canceled-system",
				"canceled-changed",
				"unscheduled",
			].indexOf(event.scheduleType) === -1
		) {
			choices.push(
				`Occurrence: Reschedule or Change Details of This Date/Time`
			);
		}
		if (["canceled", "canceled-system"].indexOf(event.scheduleType) !== -1) {
			choices.push(`Occurrence: Un-cancel and Change or Reschedule`);
		}
		choices.push(`Schedules: Add / Edit / Delete`);
		if (
			["sports", "office-hours", "prod-booking", "onair-booking"].indexOf(
				event.type
			) === -1
		) {
			choices.push(`Event: Edit Default Details`);
			if (event.calendarID !== 1)
				choices.push(`Event: Mark Inactive and Delete all Schedules`);
		}

		// generate form
		$(`#modal-calendar-occurrence-footer`).alpaca({
			schema: {
				type: "object",
				properties: {
					action: {
						type: "string",
						required: true,
						title: "Choose an action",
						enum: choices,
					},
				},
			},
			options: {
				fields: {
					action: {
						select: true,
					},
				},
				form: {
					buttons: {
						submit: {
							title: "Perform Action",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Determine what to do based on selected action
								switch (value.action) {
									// Confirm whether or not to reverse schedule changes
									case `Occurrence: Reverse this Update or Reschedule`:
										this.manager.get("WWSUutil").confirmDialog(
											`<p>Are you sure you want to reverse updates made for ${
												event.type
											}: ${event.name} on ${moment(event.start).format(
												"LLLL"
											)}?</p>
                                        <ul>
                                            <li>Discards updates applied to this date/time</li>
                                            <li>Reverts this back to event's regular scheduled time and options if changed</li>
                                            ${
																							value.newTime &&
																							value.newTime !== "" &&
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"genre",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>Notifies subscribers that the broadcast will air at its original date/time.</li>`
																								: ``
																						}
                                            ${
																							value.newTime &&
																							value.newTime !== "" &&
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>Emails hosts to let them know the broadcast is to air at its original date/time.</li>`
																								: ``
																						}
                                            <li>A conflict check will run, and you will be notified of occurrence changes that will be made to avoid conflicts</li>
                                        </ul>`,
											null,
											() => {
												this.removeSchedule(
													this.occurrenceModal,
													{ ID: event.scheduleID },
													(success) => {
														if (success) {
															this.occurrenceModal.iziModal("close");
														}
													}
												);
											}
										);
										break;

									// Confirm whether or not to reverse a cancellation
									case `Occurrence: Reverse this Cancellation`:
										this.manager.get("WWSUutil").confirmDialog(
											`<p>Are you sure you want to reverse the cancellation of ${
												event.type
											}: ${event.name} on ${moment(event.start).format(
												"LLLL"
											)}?</p>
                                        <ul>
                                            <li>Occurrence will be on the schedule again</li>
                                            ${
																							value.newTime &&
																							value.newTime !== "" &&
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"genre",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>Notifies subscribers that the broadcast will air at its original date/time.</li>`
																								: ``
																						}
                                            ${
																							value.newTime &&
																							value.newTime !== "" &&
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>Emails hosts to let them know the broadcast is to air at its original date/time.</li>`
																								: ``
																						}
                                            <li>A conflict check will run, and you will be notified of occurrence changes that will be made to avoid conflicts</li>
                                        </ul>`,
											null,
											() => {
												this.removeSchedule(
													this.occurrenceModal,
													{ ID: event.scheduleID },
													(success) => {
														if (success) {
															this.occurrenceModal.iziModal("close");
														}
													}
												);
											}
										);
										break;

									// Confirm whether or not to mark an event inactive
									case `Event: Mark Inactive and Delete all Schedules`:
										this.manager.get("WWSUutil").confirmDialog(
											`<p>Are you sure you want to mark ${
												event.name
											} as inactive?</p>
                                        <ul>
                                        <li><strong>Do not mark an event as inactive unless it will no longer air on WWSU for the forseeable future.</strong></li>
                                        <li><strong>Marking an event as inactive will remove all its schedules</strong>. This cannot be reversed!</li>
										<li>An inactive event will be permanently removed from the system after going one year without airing a broadcast (or at midnight if not applicable) if not re-activated. This means its analytics can no longer be viewed once permanently removed (but logs will still be available).</li>
                                        ${
																					[
																						"show",
																						"sports",
																						"remote",
																						"prerecord",
																						"genre",
																						"playlist",
																					].indexOf(event.type) !== -1
																						? `<li>If at least one active schedule was present, all notification subscribers will be notified the event has been discontinued</li><li>Removes all notification subscriptions</li>`
																						: ``
																				}
                                        ${
																					[
																						"show",
																						"sports",
																						"remote",
																						"prerecord",
																						"playlist",
																					].indexOf(event.type) !== -1
																						? `<li>Will also email the hosts to inform them the event was marked inactive / discontinued.</li>`
																						: ``
																				}
                                        ${
																					[
																						"show",
																						"sports",
																						"remote",
																						"prerecord",
																						"genre",
																						"playlist",
																					].indexOf(event.type) !== -1
																						? `<li>Show logs and analytics for this broadcast will remain viewable in the system and in DJ Controls.</li>`
																						: ``
																				}
                                        </ul>`,
											event.name,
											() => {
												this.inactiveCalendar(
													this.occurrenceModal,
													{ ID: event.calendarID || event.ID },
													(success) => {
														if (success) {
															this.occurrenceModal.iziModal("close");
														}
													}
												);
											}
										);
										break;

									case `Occurrence: Cancel This Date/Time`:
										this.showCancelForm(event);
										break;

									case `Event: Edit Default Details`:
										let _calendar = this.calendar
											.find()
											.find((_event) => _event.ID === event.calendarID);
										this.showEventForm(_calendar);
										break;

									case `Occurrence: Reschedule or Change Details of This Date/Time`:
									case `Occurrence: Un-cancel and Change or Reschedule`:
										this.showOccurrenceForm(event);
										break;

									case `Schedules: Add / Edit / Delete`:
										this.showSchedules(event.calendarID);
										break;
								}
							},
						},
					},
				},
			},
		});
	}

	/**
	 * Generate HTML card to view a full event
	 *
	 * @param {object} event Event as generated from calendardb.processRecord
	 * @returns {string} HTML of the card
	 */
	generateFullEventCard(event) {
		// Get color and icon for the event
		let colorClass = this.getColorClass(event);
		let iconClass = this.getIconClass(event);

		// Use dark color instead if this event was canceled
		if (
			["canceled", "canceled-system", "canceled-changed"].indexOf(
				event.scheduleType
			) !== -1
		) {
			colorClass = "dark";
		}

		// Determine if we should show a ribbon indicating a changed status of the event
		let badgeInfo;
		if (["canceled-changed"].indexOf(event.scheduleType) !== -1) {
			badgeInfo = `<div class="ribbon-wrapper ribbon-xl">
			<div class="ribbon bg-orange" title="This event was re-scheduled to a different date/time for this instance.">
			  RE-SCHEDULED
			</div>
		  </div>`;
		}
		if (
			["updated", "updated-system"].indexOf(event.scheduleType) !== -1 &&
			event.timeChanged
		) {
			badgeInfo = `<div class="ribbon-wrapper ribbon-xl">
			<div class="ribbon bg-warning" title="This event was re-scheduled from its original date/time; this is the new temporary date/time.">
			  TEMP TIME
			</div>
		  </div>`;
		}
		if (["canceled", "canceled-system"].indexOf(event.scheduleType) !== -1) {
			badgeInfo = `<div class="ribbon-wrapper ribbon-xl">
			<div class="ribbon bg-danger" title="This event was canceled for this date/time.">
			  CANCELED
			</div>
		  </div>`;
		}

		return `<div class="p-2 card card-${colorClass} card-outline position-relative">
		<div class="ribbon-wrapper ribbon-lg">
		<div class="ribbon bg-${colorClass}">
			${event.type}
		</div>
	</div>
	${badgeInfo || ``}
      <div class="card-body box-profile">
        <div class="text-center">
        ${
					event.logo !== null
						? `<img class="profile-user-img img-fluid img-circle" src="${this.manager.socket.url}/api/uploads/${event.logo}" alt="Show Logo">`
						: `<i class="profile-user-img img-fluid img-circle ${iconClass} bg-${colorClass}" style="font-size: 5rem;"></i>`
				}
        </div>

        <h3 class="profile-username text-center">${event.name}</h3>

        <p class="text-muted text-center">${event.hosts}</p>

        <ul class="list-group list-group-unbordered mb-3">
        ${
					event.scheduleReason !== null
						? `<li class="list-group-item text-center"><strong>${event.scheduleReason}</strong></li>`
						: ``
				}
        <li class="list-group-item text-center">
            <b>${
							["canceled", "canceled-system", "canceled-changed"].indexOf(
								event.scheduleType
							) !== -1
								? `Original Time: `
								: `Scheduled Time: `
						}${moment(event.start).format("lll")} - ${moment(event.end).format(
			"hh:mm A"
		)}</b>
        </li>
        <li class="list-group-item">
        ${
					event.banner !== null
						? `<img class="img-fluid" src="${this.manager.socket.url}/api/uploads/${event.banner}" alt="Show Banner">`
						: ``
				}
        </li>
        <li class="list-group-item">
            ${event.description !== null ? event.description : ``}
        </li>
        </ul>
      </div>
    </div>`;
	}

	/**
	 * Show a form to cancel an occurrence.
	 *
	 * @param {object} event CalendarDb event
	 */
	showCancelForm(event) {
		this.occurrenceActionModal.iziModal("open");
		this.occurrenceActionModal.title = `Cancel ${event.type}: ${
			event.hosts
		} - ${event.name} on ${moment(event.start).format("LLLL")}`;
		this.occurrenceActionModal.body = ``;

		// Generate form
		$(this.occurrenceActionModal.body).alpaca({
			schema: {
				type: "object",
				properties: {
					scheduleReason: {
						type: "string",
						title: "Reason for cancellation",
						maxLength: 255,
					},
				},
			},
			options: {
				fields: {
					scheduleReason: {
						type: "textarea",
						helper:
							"The reason will be displayed publicly on the website and will be saved in logs",
					},
				},
				form: {
					buttons: {
						submit: {
							title: "Cancel Event",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Confirm if we want to really cancel
								this.manager.get("WWSUutil").confirmDialog(
									`<p>Are you sure you want to cancel ${event.type}: ${
										event.hosts
									} - ${event.name} on ${moment(event.start).format(
										"LLLL"
									)}?</p>
                                        <ul>
                                            <li>Please <strong>do not</strong> cancel occurrences to make room to schedule other events; scheduling the other event will automatically make adjustments as necessary and reverse the changes should the other event get canceled.</li>
                                            <li>Marks this occurrence as canceled on calendar. <strong>After cancelling, go to an admin DJ Controls, and under "To-do", mark the cancellation as either excused (WWSU-prompted) or unexcused (host-prompted).</strong></li>
                                            ${
																							[
																								"show",
																								"sports",
																								"remote",
																							].indexOf(event.type) !== -1
																								? `<li>If the host tries to broadcast on this date/time, it will be flagged as an unauthorized / unscheduled broadcast.</li>`
																								: ``
																						}
                                            ${
																							["remote"].indexOf(event.type) !==
																							-1
																								? `<li>DJ Controls will deny the host's ability to start a remote broadcast on this date/time if their DJ Controls is member-locked.</li>`
																								: ``
																						}
                                            ${
																							["prerecord", "playlist"].indexOf(
																								event.type
																							) !== -1
																								? `<li>This prerecord or playlist will not be aired by the system on this date/time.</li>`
																								: ``
																						}
                                            ${
																							["genre"].indexOf(event.type) !==
																							-1
																								? `<li>This genre rotation will not start on this date/time; if no other genres are scheduled, the system will go to default rotation.</li>`
																								: ``
																						}
                                            ${
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"genre",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>Subscribers will be notified the event was canceled on this date/time.</li>`
																								: ``
																						}
                                            ${
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>hosts will be emailed informing them their broadcast was canceled on this date/time.</li>`
																								: ``
																						}
                                            <li>Any event occurrences canceled or changed via priorities because of this occurrence will have their cancellations / updates reversed; they will be back on the schedule with their original dates/times. Subscribers will be notified of this as well.</li>
                                        </ul>`,
									null,
									() => {
										this.addSchedule(
											this.occurrenceActionModal,
											{
												calendarID: event.calendarID,
												scheduleID: event.scheduleID,
												scheduleType: "canceled",
												scheduleReason: value.scheduleReason,
												originalTime: event.start,
											},
											(success) => {
												if (success) {
													this.occurrenceActionModal.iziModal("close");
													this.occurrenceModal.iziModal("close");
												}
											}
										);
									}
								);
							},
						},
					},
				},
			},
		});
	}

	/**
	 * Show form to add a new event or edit an existing one.
	 *
	 * @param {?object} event Original event data if editing an event, or null if making a new one
	 */
	showEventForm(event) {
		let _djs = this.manager.get("WWSUdjs").find({ active: true });
		let _hosts = this.manager.get("WWSUhosts").find({ authorized: true });
		let calendarEvents = this.calendar.find().map((_event) => _event.name);

		this.eventModal.title = `${
			event
				? `Edit event ${event.type}: ${event.hosts} - ${event.name}`
				: `New event`
		}`;
		this.eventModal.body = ``;

		if (event) {
			// Weed out host if inactive or not found
			if (!_djs.find((dj) => dj.ID === event.hostDJ)) event.hostDJ = null;

			// Combine cohosts into an array (also weed out cohosts that are not found or inactive)
			event.cohosts = [];
			[event.cohostDJ1, event.cohostDJ2, event.cohostDJ3]
				.filter((cohost) => cohost && _djs.find((dj) => dj.ID === cohost))
				.map((cohost) => {
					event.cohosts.push(cohost);
				});

			// Format logo and banner for file upload system
			if (event.logo) {
				event.logo = {
					id: event.logo,
					name: "Logo",
					size: "N/A",
					url: `${this.manager.socket.url}/api/uploads/${event.logo}`,
					thumbnailUrl: `${this.manager.socket.url}/api/uploads/${event.logo}`,
					deleteUrl: `${this.manager.socket.url}/api/uploads/${
						event.logo
					}?host=${
						this.manager.has("WWSUhosts")
							? this.manager.get("WWSUhosts").client.host
							: "null"
					}`,
					deleteType: "DELETE",
				};
			}
			if (event.banner) {
				event.banner = {
					id: event.banner,
					name: "Banner",
					size: "N/A",
					url: `${this.manager.socket.url}/api/uploads/${event.banner}`,
					thumbnailUrl: `${this.manager.socket.url}/api/uploads/${event.banner}`,
					deleteUrl: `${this.manager.socket.url}/api/uploads/${
						event.banner
					}?host=${
						this.manager.has("WWSUhosts")
							? this.manager.get("WWSUhosts").client.host
							: "null"
					}`,
					deleteType: "DELETE",
				};
			}
		}

		// We need to get the events and playlists we can choose from
		this.getEventsPlaylists((events, playlists) => {
			// If the set playlist ID no longer exists, set to null.
			if (
				event &&
				event.playlistID &&
				!playlists.find((pl) => pl.ID === event.playlistID)
			) {
				event.playlistID = null;
			}

			// If the set event ID no longer exists, set to null.
			if (
				event &&
				event.eventID &&
				!events.find((ev) => ev.ID === event.eventID)
			) {
				event.eventID = null;
			}

			let usingRadioDJ = this.manager.get("WWSUconfig").usingRadioDJ;

			// Generate the form
			this.eventModal.iziModal("open");
			$(this.eventModal.body).alpaca({
				schema: {
					title: "Default Event Properties",
					type: "object",
					properties: {
						type: {
							type: "string",
							required: true,
							title: "Event Type",
							enum: [
								"show",
								"remote",
								"prerecord",
								"genre",
								"playlist",
								"event",
							],
						},
						name: {
							type: "string",
							required: true,
							title: "Event Name",
							maxLength: 255,
						},
						description: {
							type: "string",
							title: "Event Description",
						},
						logo: {
							type: "array",
							title: "Event Logo",
						},
						banner: {
							type: "array",
							title: "Event Banner / Flyer",
						},
						priority: {
							type: "number",
							title: "Event Priority",
							minimum: -1,
							maximum: 10,
						},
						hostDJ: {
							type: "number",
							title: "Host Member",
							enum: _djs.map((dj) => dj.ID),
						},
						cohosts: {
							title: "Co-host Members",
							type: "array",
							items: {
								type: "number",
							},
							maxItems: 3,
							enum: _djs.map((dj) => dj.ID),
						},
						playlistID: {
							type: "number",
							title: "RadioDJ Playlist",
							enum: playlists.map((playlist) => playlist.ID),
						},
						eventID: {
							type: "number",
							title: "RadioDJ Rotation-triggering Manual Event",
							enum: events.map((eventb) => eventb.ID),
						},
					},
				},

				options: {
					fields: {
						type: {
							type: "select",
							optionLabels: [
								"Live Show (In-Studio)",
								"Remote Broadcast (via DJ Controls)",
								"Prerecorded Show (RadioDJ playlist w/ hosts)",
								"Genre Rotation (RadioDJ Rotation)",
								"Playlist (RadioDJ Playlist w/o hosts)",
								"Non-radio Event",
							],
						},
						name: {
							helper: "Event may not share the name of another event",
							validator: function (callback) {
								let value = this.getValue();
								if (value.includes(" -")) {
									callback({
										status: false,
										message: `Invalid; event names may not contain " - " as this is a separation used by the system.`,
									});
									return;
								}
								if (
									calendarEvents.indexOf(value) !== -1 &&
									(!event || !event.name || event.name !== value)
								) {
									callback({
										status: false,
										message:
											"Value in this field matches the name of another event. This is not allowed.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						description: {
							type: "textarea",
						},
						logo: {
							type: "upload",
							maxNumberOfFiles: 1,
							fileTypes: "image/*",
							maxFileSize: 1024 * 1024, // 1 MB file size limit
							upload: {
								autoUpload: false,
								formData: {
									type: "calendar/logo",
									host: this.manager.get("WWSUhosts").client.host,
								},
								url: this.manager.socket.url + "/api/uploads",
							},
							helper:
								"Maximum file size is 1 MB. Will only use the first image. Logo should be square or round if possible. Text should be kept minimal in amount and should be large in size.",
						},
						banner: {
							type: "upload",
							maxNumberOfFiles: 1,
							fileTypes: "image/*",
							maxFileSize: 1024 * 1024, // 1 MB file size limit
							upload: {
								autoUpload: false,
								formData: {
									type: "calendar/banner",
									host: this.manager.get("WWSUhosts").client.host,
								},
								url: this.manager.socket.url + "/api/uploads",
							},
							helper:
								"Maximum file size is 1 MB. Will only use first image. Banners / flyers should be promotional material for the broadcast and are usually large.",
						},
						priority: {
							type: "integer",
							helpers: [
								`Specify a priority. -1 means event should never conflict with other events. 0 means event will only conflict with other events of the same type. 1 - 10 means event will be overridden by other events with a higher priority, and will override events with a lower priority.`,
								`Defaults if left blank: 0 for genres and bookings, 1 for playlists, 3 for prerecords, 5 for live shows, 7 for remotes, 9 for sports broadcasts, and -1 for everything else.`,
								`A conflict check will run to ensure events do not conflict with each other. If any conflicts are detected, you will be informed on the actions that will be taken before you proceed.`,
							],
						},
						hostDJ: {
							type: "select",
							optionLabels: _djs.map((dj) => `${dj.name} (${dj.realName})`),
							helpers: [
								"The org member who signed this broadcast up, or the org member producing this broadcast. This field is required for show, remote, and prerecord events.",
								"<strong>Warning!</strong> If the specified org member is marked inactive or deleted at any time, this event will also be automatically marked inactive and its schedules removed.",
							],
							validator: function (callback) {
								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									["show", "remote", "prerecord"].indexOf(type) !== -1 &&
									(!value || value === "")
								) {
									callback({
										status: false,
										message:
											"Field is required for show, remote, and prerecord events.",
									});
									return;
								}

								// Integrity checks
								let dj = _djs.find((dj) => dj.ID === value);
								if (!dj) {
									callback({
										status: false,
										message:
											"The selected org member could not be found or is marked inactive in the system.",
									});
									return;
								}
								if (type === "show") {
									if (dj.permissions.indexOf("live") === -1) {
										callback({
											status: true,
											message:
												"This org member does not currently have permission to start live broadcasts in the studio. After continuing, don't forget to assign it in Org Members -> edit the member.",
										});
										return;
									}
								}
								if (type === "remote") {
									if (dj.permissions.indexOf("remote") === -1) {
										callback({
											status: true,
											message:
												"This org member does not currently have permission to start remote broadcasts from their computer. After continuing, don't forget to assign it in Org Members -> edit the member. And make sure they have at least one authorized host assigned to them in Hosts.",
										});
										return;
									}

									let host = _hosts.find((_host) => _host.belongsTo === value);
									if (!host) {
										callback({
											status: true,
											message:
												"This org member does not currently have an authorized DJ Controls host assigned to them. After continuing, make sure they have DJ Controls installed, they ran it once, and you edit their host under Hosts, making sure it is authorized and that it is marked as belonging to them.",
										});
										return;
									}
								}

								callback({
									status: true,
								});
							},
						},
						cohosts: {
							type: "select",
							multiple: true,
							optionLabels: _djs.map(
								(dj) => `${dj.name} (${dj.realName}) [${dj.ID}]`
							),
							helpers: [
								"Choose up to 3 other members who co-host this show with the host.",
								"<strong>Definition:</strong> A co-host is NOT just a regular guest; a co-host puts together and helps run the show as well every broadcast. By specifying co-hosts, show analytics will also apply to them.",
							],
							validator: function (callback) {
								let values = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();

								let status = true;
								let messages = [];
								if (values) {
									values.forEach((_value) => {
										let value = _value.value;

										// Integrity checks
										let dj = _djs.find((dj) => dj.ID === value);
										if (!dj) {
											status = false;
											messages.push(
												`Org member ID ${value} could not be found or is marked inactive in the system.`
											);
											return;
										}
										if (type === "show") {
											if (dj.permissions.indexOf("live") === -1) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have permission to start live broadcasts in the studio. After continuing, don't forget to assign it in Org Members -> edit the member.`
												);
												return;
											}
										}
										if (type === "remote") {
											if (dj.permissions.indexOf("remote") === -1) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have permission to start remote broadcasts from their computer. After continuing, don't forget to assign it in Org Members -> edit the member. And make sure they have at least one authorized host assigned to them in Hosts.`
												);
												return;
											}

											let host = _hosts.find(
												(_host) => _host.belongsTo === value
											);
											if (!host) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have an authorized DJ Controls host assigned to them. After continuing, make sure they have DJ Controls installed, they ran it once, and you edit their host under Hosts, making sure it is authorized and that it is marked as belonging to them.`
												);
												return;
											}
										}
									});
								}

								callback({
									status: status,
									message:
										messages.length > 0 ? messages.join("<br />\n") : undefined,
								});
							},
						},
						playlistID: {
							type: "select",
							optionLabels: playlists.map((playlist) => playlist.name),
							helper: "Required for prerecords and playlists only.",
							hidden: !usingRadioDJ,
							validator: function (callback) {
								if (!usingRadioDJ) {
									callback({
										status: true,
									});
									return;
								}

								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									["prerecord", "playlist"].indexOf(type) !== -1 &&
									(!value || value === "")
								) {
									callback({
										status: false,
										message:
											"Field is required for prerecord and playlist events.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						eventID: {
							type: "select",
							optionLabels: events.map((eventb) => eventb.name),
							helper: "Required for genres only.",
							hidden: !usingRadioDJ,
							validator: function (callback) {
								if (!usingRadioDJ) {
									callback({
										status: true,
									});
									return;
								}

								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									["genre"].indexOf(type) !== -1 &&
									(!value || value === "")
								) {
									callback({
										status: false,
										message: "Field is required for genre events.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
					},

					form: {
						buttons: {
							submit: {
								title: `${event ? `Edit` : `Add`} Event`,
								click: (form, e) => {
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();

									// Convert value.cohosts into cohostDJ1 2 and 3
									if (value.cohosts) {
										value.cohosts = value.cohosts.map((item) => item.value);
										if (value.cohosts[0]) value.cohostDJ1 = value.cohosts[0];
										if (value.cohosts[1]) value.cohostDJ2 = value.cohosts[1];
										if (value.cohosts[2]) value.cohostDJ3 = value.cohosts[2];
										delete value.cohosts;
									}

									// Event logo and banner should only contain IDs and should only reflect the first file in the list.
									value.logo =
										value.logo && value.logo[0] ? value.logo[0].id : null;
									value.banner =
										value.banner && value.banner[0] ? value.banner[0].id : null;

									let _event = this.verify(
										value,
										this.manager.get("WWSUconfig").usingRadioDJ
									); // Verify the event is valid first just to be extra sure

									if (!_event.event) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										$(document).Toasts("create", {
											class: "bg-warning",
											title: "Event verification failed",
											autohide: true,
											delay: 20000,
											body: _event,
										});
										form.focus();
										return;
									}

									// Add/edit the event
									if (event) {
										value.ID = event.ID;
										this.editCalendar(this.eventModal, value, (success) => {
											if (success) {
												this.eventModal.iziModal("close");
												this.occurrenceModal.iziModal("close");
											}
										});
									} else {
										this.addCalendar(this.eventModal, value, (success) => {
											if (success) {
												this.eventModal.iziModal("close");
												this.occurrenceModal.iziModal("close");
											}
										});
									}
								},
							},
						},
					},
				},

				data: event ? event : [],
			});
		});
	}

	/**
	 * Show form to edit an occurrence.
	 *
	 * @param {object} event Original event data
	 * @param {?string} newStart Pre-fill a new start date/time for the reschedule
	 * @param {?number} newDuration Pre-fill a new duration for the reschedule
	 */
	showOccurrenceForm(event, newStart, newDuration) {
		let _djs = this.manager.get("WWSUdjs").find({ active: true });
		_djs.push({ ID: 0, name: "(Clear / remove all co-hosts)", realName: null });

		let _hosts = this.manager.get("WWSUhosts").find({ authorized: true });
		let calendarEvents = this.calendar.find().map((event) => event.name);

		let sportsEvents = this.calendar
			.find()
			.filter((event) => event.type === "sports")
			.map((event) => event.name);

		this.occurrenceActionModal.title = `Edit occurrence ${event.type}: ${
			event.hosts
		} - ${event.name} on ${moment(event.start).format("LLLL")}`;
		this.occurrenceActionModal.body = "";

		let validTypes = [];
		let validTypesLabels = [];

		// Limit the types we can switch to for this occurrence depending on the type of the original event.
		switch (event.type) {
			case "show":
			case "remote":
			case "prerecord":
				validTypes = ["show", "remote", "prerecord"];
				validTypesLabels = [
					"Live Show (In-Studio)",
					"Remote Broadcast (via DJ Controls)",
					"Prerecorded Show (RadioDJ playlist w/ hosts)",
				];
				break;
			case "genre":
			case "playlist":
				validTypes = ["genre", "playlist"];
				validTypesLabels = [
					"Genre Rotation (RadioDJ Rotation)",
					"Playlist (RadioDJ Playlist w/o hosts)",
				];
				break;
			case "sports":
				validTypes = ["sports"];
				validTypesLabels = [
					"Sports Broadcast (In-Studio or Remote via DJ Controls)",
				];
				break;
			case "office-hours":
				validTypes = ["office-hours"];
				validTypesLabels = ["Director Office Hours"];
				break;
			case "prod-booking":
			case "onair-booking":
				validTypes = ["prod-booking", "onair-booking"];
				validTypesLabels = [
					"Production Studio Booking",
					"On-Air Studio Booking",
				];
				break;
			case "task":
				validTypes = ["task"];
				validTypesLabels = ["Task / To-Do"];
				break;
		}

		// Get events and playlist we can select
		this.getEventsPlaylists((events, playlists) => {
			// Generate form
			this.occurrenceActionModal.iziModal("open");

			let usingRadioDJ = this.manager.get("WWSUconfig").usingRadioDJ;
			$(this.occurrenceActionModal.body).alpaca({
				schema: {
					title: "Properties for this occurrence only",
					type: "object",
					properties: {
						calendarID: {
							type: "number",
						},
						scheduleID: {
							type: "number",
						},
						scheduleType: {
							type: "string",
						},
						originalTime: {
							type: "string",
						},
						scheduleReason: {
							type: "string",
							title: "Reason for update/change",
							maxLength: 255,
						},
						newTime: {
							format: "date",
							title: "Change date of occurrence",
						},
						startTime: {
							format: "time",
							title: "Change start time of occurrence",
						},
						duration: {
							format: "time",
							title: "Change end time of occurrence",
						},
						type: {
							type: "string",
							title: "Change occurrence type",
							enum: validTypes,
						},
						name: {
							type: "string",
							title: "Change occurrence name",
							maxLength: 255,
						},
						description: {
							type: "string",
							title: "Change occurrence description",
						},
						logo: {
							type: "array",
							title: "Change Occurrence Logo",
						},
						banner: {
							type: "array",
							title: "Change Occurrence Banner / Flyer",
						},
						priority: {
							type: "number",
							title: "Change occurrence priority",
							minimum: -1,
							maximum: 10,
						},
						hostDJ: {
							type: "number",
							title: "Change occurrence Host DJ",
							enum: _djs.filter((dj) => dj.ID > 0).map((dj) => dj.ID),
						},
						cohosts: {
							title: "Change Co-host Members",
							type: "array",
							items: {
								type: "number",
							},
							maxItems: 3,
							enum: _djs.map((dj) => dj.ID),
						},
						playlistID: {
							type: "number",
							title: "Change occurrence RadioDJ Playlist",
							enum: playlists.map((playlist) => playlist.ID),
						},
						eventID: {
							type: "number",
							title:
								"Change occurrence RadioDJ Rotation-triggering Manual Event",
							enum: events.map((eventb) => eventb.ID),
						},
					},
				},
				view: {
					wizard: {
						hideSubmitButton: true,
						bindings: {
							calendarID: 1,
							scheduleID: 1,
							scheduleType: 1,
							originalTime: 1,
							scheduleReason: 1,
							newTime: 1,
							startTime: 1,
							duration: 1,
							type: 2,
							name: 2,
							description: 2,
							logo: 2,
							banner: 2,
							priority: 2,
							hostDJ: 2,
							cohosts: 2,
							playlistID: 2,
							eventID: 2,
						},
						steps: [
							{
								title: "Change Date/Time",
							},
							{
								title: "Change Details",
							},
						],
					},
				},
				options: {
					fields: {
						calendarID: {
							type: "hidden",
						},
						scheduleID: {
							type: "hidden",
						},
						scheduleType: {
							type: "hidden",
						},
						originalTime: {
							type: "hidden",
						},
						scheduleReason: {
							type: "textarea",
							helper:
								"The reason will be displayed publicly on the website and will be saved in logs",
						},
						newTime: {
							dateFormat: `YYYY-MM-DD`,
							helper: `If this occurrence should happen on a different date, specify it here in the station timezone of ${
								this.timezone
							}. The current start date is <strong>${moment
								.parseZone(event.start)
								.format("YYYY-MM-DD")}</strong>.`,
							picker: {
								inline: true,
								sideBySide: true,
							},
						},
						startTime: {
							helper: `If this occurrence should start at a different time, specify it here in the station timezone of ${
								this.timezone
							}. The current start time is <strong>${moment
								.parseZone(event.start)
								.format("hh:mm A")}</strong>.`,
							dateFormat: "hh:mm A",
							picker: {
								inline: true,
								sideBySide: true,
							},
							validator: function (callback) {
								let value = this.getValue();

								if (
									value &&
									value !== "" &&
									!moment(value, "hh:mm A", true).isValid()
								) {
									callback({
										status: false,
										message:
											"You must specify a start time in the format hh:mm A.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						duration: {
							helper: `If this occurrence should end at a different time, specify the end time in the station's timezone of ${
								this.timezone
							}. If the end time is before the start time, we assume the event will run past midnight the next day. The current end time is <strong>${moment
								.parseZone(event.end)
								.format("hh:mm A")}</strong>.`,
							dateFormat: "hh:mm A",
							picker: {
								inline: true,
								sideBySide: true,
							},
							validator: function (callback) {
								let value = this.getValue();

								if (
									value &&
									value !== "" &&
									!moment(value, "hh:mm A", true).isValid()
								) {
									callback({
										status: false,
										message:
											"You must specify an end time in the format hh:mm A.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						logo: {
							type: "upload",
							maxNumberOfFiles: 1,
							fileTypes: "image/*",
							maxFileSize: 1024 * 1024, // 1 MB file size limit
							upload: {
								autoUpload: false,
								formData: {
									type: "calendar/logo",
									host: this.manager.get("WWSUhosts").client.host,
								},
								url: this.manager.socket.url + "/api/uploads",
							},
							helper:
								"Maximum file size is 1 MB. Will only use the first image. Logo should be square or round if possible. Text should be kept minimal in amount and should be large in size.",
						},
						banner: {
							type: "upload",
							maxNumberOfFiles: 1,
							fileTypes: "image/*",
							maxFileSize: 1024 * 1024, // 1 MB file size limit
							upload: {
								autoUpload: false,
								formData: {
									type: "calendar/banner",
									host: this.manager.get("WWSUhosts").client.host,
								},
								url: this.manager.socket.url + "/api/uploads",
							},
							helper:
								"Maximum file size is 1 MB. Will only use first image. Banners / flyers should be promotional material for the broadcast and are usually large.",
						},
						type: {
							type: "select",
							optionLabels: validTypesLabels,
							helper: `If changing the type for this occurrence, specify the new type here. The types you may change to are limited and depend on the original type. The current type is <strong>${event.type}</strong>`,
						},
						name: {
							helper: `If changing the name of this occurrence, specify it here. The current name is <strong>${event.name}</strong>. This field is ignored for bookings and office-hours.`,
							validator: function (callback) {
								let value = this.getValue();
								if (value.includes(" -")) {
									callback({
										status: false,
										message: `Invalid; event names may not contain " - " as this is a separation used by the system.`,
									});
									return;
								}

								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (event.type === "sports") value = value.split(" vs.")[0];
								if (
									value &&
									value !== "" &&
									(((!type || type === "") && event.type === "sports") ||
										type === "sports") &&
									sportsEvents.indexOf(value) === -1
								) {
									callback({
										status: false,
										message: `For sports, name must begin with a valid sport and optionally proceed with " vs. name of opponent team". Valid sports: ${sportsEvents.join(
											", "
										)}`,
									});
									return;
								} else if (
									value &&
									value !== "" &&
									calendarEvents.indexOf(value) !== -1 &&
									(!event || !event.name || event.name !== value)
								) {
									callback({
										status: false,
										message:
											"Value in this field matches the name of another event. This is not allowed.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						description: {
							type: "textarea",
							helper: `Type a new description if you want to change it. The current description: ${
								event.description ? event.description : `--NONE--`
							}`,
						},
						priority: {
							type: "integer",
							helpers: [
								`Change the occurrence priority. -1 means event should never conflict with other events. 0 means event will only conflict with other events of the same type. 1 - 10 means event will be overridden by other events with a higher priority, and will override events with a lower priority. The current priority is <strong>${event.priority}</strong>`,
								`Defaults if left blank: 0 for genres and bookings, 1 for playlists, 3 for prerecords, 5 for live shows, 7 for remotes, 9 for sports broadcasts, and -1 for everything else.`,
								`A conflict check will run to ensure events do not conflict with each other. If any conflicts are detected, you will be informed on the actions that will be taken before you proceed.`,
							],
						},
						hostDJ: {
							type: "select",
							optionLabels: _djs
								.filter((dj) => dj.ID > 0)
								.map((dj) => `${dj.name} (${dj.realName})`),
							helper: `Change the DJ who signed up for this show, or the official WWSU producer for shows run by non-WWSU people. The current hostDJ is set to <strong>${
								event.hostDJ && _djs.find((dj) => dj.ID === event.hostDJ)
									? _djs.find((dj) => dj.ID === event.hostDJ).name
									: `--NONE--`
							}</strong>`,
							validator: function (callback) {
								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();

								if (type === "") type = null;

								if (
									((type &&
										type !== "" &&
										["show", "remote", "prerecord"].indexOf(type) !== -1) ||
										["show", "remote", "prerecord"].indexOf(event.type) !==
											-1) &&
									(!value || value === "") &&
									!event.hostDJ
								) {
									callback({
										status: false,
										message:
											"Field is required for show, remote, and prerecord events.",
									});
									return;
								}

								// Integrity checks
								let dj = _djs.find((dj) => dj.ID === (value || event.hostDJ));
								if (!dj) {
									callback({
										status: false,
										message:
											"The org member for this event could not be found or is marked inactive in the system.",
									});
									return;
								}
								if ((type || event.type) === "show") {
									if (dj.permissions.indexOf("live") === -1) {
										callback({
											status: true,
											message: `${dj.name} (${dj.realName}) does not currently have permission to start live broadcasts in the studio. After continuing, don't forget to assign it in Org Members -> edit the member.`,
										});
										return;
									}
								}
								if ((type || event.type) === "remote") {
									if (dj.permissions.indexOf("remote") === -1) {
										callback({
											status: true,
											message: `${dj.name} (${dj.realName}) does not currently have permission to start remote broadcasts from their computer. After continuing, don't forget to assign it in Org Members -> edit the member. And make sure they have at least one authorized host assigned to them in Hosts.`,
										});
										return;
									}

									let host = _hosts.find(
										(_host) => _host.belongsTo === (value || event.hostDJ)
									);
									if (!host) {
										callback({
											status: true,
											message: `${dj.name} (${dj.realName}) does not currently have an authorized DJ Controls host assigned to them. After continuing, make sure they have DJ Controls installed, they ran it once, and you edit their host under Hosts, making sure it is authorized and that it is marked as belonging to them.`,
										});
										return;
									}
								}

								callback({
									status: true,
								});
							},
						},
						cohosts: {
							type: "select",
							multiple: true,
							optionLabels: _djs.map(
								(dj) => `${dj.name} (${dj.realName}) [${dj.ID}]`
							),
							helpers: [
								`To change the current listed co-hosts (<strong>${
									event.cohostDJ1 &&
									_djs.find((dj) => dj.ID === event.cohostDJ1)
										? _djs.find((dj) => dj.ID === event.cohostDJ1).name
										: ``
								}${
									event.cohostDJ2 &&
									_djs.find((dj) => dj.ID === event.cohostDJ2)
										? ", " + _djs.find((dj) => dj.ID === event.cohostDJ2).name
										: ``
								}${
									event.cohostDJ3 &&
									_djs.find((dj) => dj.ID === event.cohostDJ3)
										? ", " + _djs.find((dj) => dj.ID === event.cohostDJ3).name
										: ``
								}</strong>), please specify the updated list of co-hosts (maximum 3)`,
								"<strong>Definition:</strong> A co-host is NOT just a regular guest; a co-host puts together and helps run the show as well every broadcast. By specifying co-hosts, show analytics will also apply to them.",
							],
							validator: function (callback) {
								let values = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();

								if (type === "") type = null;

								let status = true;
								let messages = [];

								if (values && values.indexOf(0) === -1) {
									values.forEach((_value) => {
										let value = _value.value;

										// Integrity checks
										let dj = _djs.find((dj) => dj.ID === value);
										if (!dj) {
											status = false;
											messages.push(
												`Org member ID ${value} could not be found or is marked inactive in the system.`
											);
											return;
										}
										if ((type || event.type) === "show") {
											if (dj.permissions.indexOf("live") === -1) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have permission to start live broadcasts in the studio. After continuing, don't forget to assign it in Org Members -> edit the member.`
												);
												return;
											}
										}
										if ((type || event.type) === "remote") {
											if (dj.permissions.indexOf("remote") === -1) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have permission to start remote broadcasts from their computer. After continuing, don't forget to assign it in Org Members -> edit the member. And make sure they have at least one authorized host assigned to them in Hosts.`
												);
												return;
											}

											let host = _hosts.find(
												(_host) => _host.belongsTo === value
											);
											if (!host) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have an authorized DJ Controls host assigned to them. After continuing, make sure they have DJ Controls installed, they ran it once, and you edit their host under Hosts, making sure it is authorized and that it is marked as belonging to them.`
												);
												return;
											}
										}
									});
								}

								callback({
									status: status,
									message:
										messages.length > 0 ? messages.join("<br />\n") : undefined,
								});
							},
						},
						playlistID: {
							type: "select",
							optionLabels: playlists.map((playlist) => playlist.name),
							helper: `Change or set the RadioDJ playlist (prerecord and playlist events). The current playlist is set to <strong>${
								event.playlistID &&
								playlists.find((playlist) => playlist.ID === event.playlistID)
									? playlists.find(
											(playlist) => playlist.ID === event.playlistID
									  ).name
									: `--NONE--`
							}</strong>`,
							hidden: !usingRadioDJ,
							validator: function (callback) {
								if (!usingRadioDJ) {
									callback({
										status: true,
									});
									return;
								}
								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									((type &&
										type !== "" &&
										["prerecord", "playlist"].indexOf(type) !== -1) ||
										["prerecord", "playlist"].indexOf(event.type) !== -1) &&
									(!value || value === "") &&
									!event.playlistID
								) {
									callback({
										status: false,
										message:
											"Field is required for prerecord and playlist events.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						eventID: {
							type: "select",
							optionLabels: events.map((eventb) => eventb.name),
							helper: `Change or set the rotation-triggering RadioDJ event (genre events). The current RadioDJ event is set to <strong>${
								event.eventID &&
								events.find((eventb) => eventb.ID === event.eventID)
									? events.find((eventb) => eventb.ID === event.eventID).name
									: `--NONE--`
							}</strong>`,
							hidden: !usingRadioDJ,
							validator: function (callback) {
								if (!usingRadioDJ) {
									callback({
										status: true,
									});
									return;
								}
								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									((type && type !== "" && ["genre"].indexOf(type) !== -1) ||
										["genre"].indexOf(event.type) !== -1) &&
									(!value || value === "") &&
									!event.eventID
								) {
									callback({
										status: false,
										message: "Field is required for genre events.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
					},

					form: {
						buttons: {
							submit: {
								title: `Edit Occurrence`,
								click: (form, e) => {
									console.dir(form);
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();

									// Convert value.cohosts into cohostDJ1 2 and 3
									if (value.cohosts && value.cohosts.length > 0) {
										value.cohostDJ1 = 0;
										value.cohostDJ2 = 0;
										value.cohostDJ3 = 0;

										// Only populate new co-hosts if we did not specify to clear all of them
										if (value.cohosts.indexOf(0) === -1) {
											value.cohosts = value.cohosts.map((item) => item.value);
											if (value.cohosts[0]) value.cohostDJ1 = value.cohosts[0];
											if (value.cohosts[1]) value.cohostDJ2 = value.cohosts[1];
											if (value.cohosts[2]) value.cohostDJ3 = value.cohosts[2];
										}

										delete value.cohosts;
									}

									// Change startTime to 24-hour format if specified
									if (value.startTime) {
										value.startTime = moment
											.utc(value.startTime, "hh:mm A", true)
											.format("HH:mm");
									}

									// LEGACY
									value.newTime = moment(
										`${value.newTime} ${
											value.startTime ||
											moment(event.start)
												.tz(this.stationTimezone)
												.format("HH:mm")
										}`,
										`YYYY-MM-DD HH:mm`
									)
										.tz(this.stationTimezone)
										.toISOString(true);

									// Change duration to number of minutes from startTime to duration time if specified
									if (value.startTime && !value.duration) {
										let originalEndTime = moment(event.end).format("HH:mm");
										value.duration = moment
											.utc(originalEndTime, "HH:mm", true)
											.diff(
												moment.utc(value.startTime, "HH:mm", true),
												"minutes"
											);
										if (value.duration <= 0)
											value.duration = 60 * 24 + value.duration;
									} else if (value.duration) {
										value.duration = moment
											.utc(value.duration, "hh:mm A", true)
											.diff(
												moment.utc(
													value.startTime ||
														event.startTime ||
														moment(event.start).format("HH:mm"),
													"HH:mm",
													true
												),
												"minutes"
											);
										if (value.duration <= 0)
											value.duration = 60 * 24 + value.duration;
									}

									// Event logo and banner should only contain IDs and should only reflect the first file in the list.
									value.logo =
										value.logo && value.logo[0] ? value.logo[0].id : null;
									value.banner =
										value.banner && value.banner[0] ? value.banner[0].id : null;

									let _event = this.verify(
										value,
										this.manager.get("WWSUconfig").usingRadioDJ
									); // Verify the event just to be safe

									if (!_event.event) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										$(document).Toasts("create", {
											class: "bg-warning",
											title: "Event verification failed",
											autohide: true,
											delay: 20000,
											body: _event,
										});
										form.focus();
										return;
									}

									// Confirm if we really want to edit
									this.manager.get("WWSUutil").confirmDialog(
										`<p>Are you sure you want to edit occurrence ${
											event.type
										}: ${event.hosts} - ${event.name} on ${moment(
											event.start
										).format("LLLL")}?</p>
                                        <ul>
                                            <li>Changes will only apply to the event's original occurrence of ${moment(
																							event.start
																						).format("LLLL")}.</li>
                                            <li>A conflict check will run, and you will be notified of occurrence changes that will be made to avoid conflicts</li>
                                            ${
																							value.newTime &&
																							value.newTime !== "" &&
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"genre",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>Subscribers will be notified of the change in date/time.</li>`
																								: ``
																						}
                                            ${
																							value.newTime &&
																							value.newTime !== "" &&
																							[
																								"show",
																								"sports",
																								"remote",
																								"prerecord",
																								"playlist",
																							].indexOf(event.type) !== -1
																								? `<li>DJs will be emailed informing them of the change in date/time.</li>`
																								: ``
																						}
                                            <li>Properties which you did not set a value via the edit form will use the default value from the event. Properties which you had set on the form will use the value you set, even if the default value for the event is edited later.</li>
                                        </ul>`,
										null,
										() => {
											this.addSchedule(
												this.occurrenceActionModal,
												value,
												(success) => {
													if (success) {
														this.occurrenceActionModal.iziModal("close");
														this.occurrenceModal.iziModal("close");
													}
												}
											);
										}
									);
								},
							},
						},
					},
				},

				data: {
					calendarID: event.calendarID,
					scheduleID: event.scheduleID,
					scheduleType: "updated",
					originalTime: event.start,
					newTime: newStart,
					startTime: newStart
						? moment(newStart).tz(this.stationTimezone).format("hh:mm A")
						: undefined,
					duration: newDuration
						? moment(newStart || event.start)
								.tz(this.stationTimezone)
								.add(newDuration, "minutes")
								.format("hh:mm A")
						: undefined,
				},
			});
		});
	}

	/**
	 * Show form to add a new schedule or edit an existing one.
	 *
	 * @param {?object} schedule Original schedule data if editing a schedule, or null if making a new one
	 * @param {number} calendarID The ID of the calendar record pertaining to this schedule
	 */
	showScheduleForm(schedule, calendarID) {
		let event = this.calendar.db({ ID: calendarID }, true).first();

		let _djs = this.manager.get("WWSUdjs").find({ active: true });
		_djs.push({ ID: 0, name: "(Clear / remove all co-hosts)", realName: null });

		let _hosts = this.manager.get("WWSUhosts").find({ authorized: true });

		let calendarEvents = this.calendar.find().map((event) => event.name);
		let sportsEvents = this.calendar
			.find()
			.filter((event) => event.type === "sports")
			.map((event) => event.name);

		if (schedule) {
			// Weed out host if inactive or not found
			if (!_djs.find((dj) => dj.ID === schedule.hostDJ)) schedule.hostDJ = null;

			// Combine cohosts into an array (also weed out cohosts that are not found or inactive)
			schedule.cohosts = [];
			[schedule.cohostDJ1, schedule.cohostDJ2, schedule.cohostDJ3]
				.filter((cohost) => cohost && _djs.find((dj) => dj.ID === cohost))
				.map((cohost) => {
					schedule.cohosts.push(cohost);
				});
		}

		this.scheduleModal.title = `${
			schedule && schedule.ID
				? `Edit schedule ${event.type}: ${event.hosts} - ${event.name}`
				: `New schedule for ${event.type}: ${event.hosts} - ${event.name}`
		}`;
		this.scheduleModal.body = ``;

		let validTypes = [];
		let validTypesLabels = [];

		// Limit the types we can switch to for this occurrence depending on the type of the original event.
		switch (event.type) {
			case "show":
			case "remote":
			case "prerecord":
				validTypes = ["show", "remote", "prerecord"];
				validTypesLabels = [
					"Live Show (In-Studio)",
					"Remote Broadcast (via DJ Controls)",
					"Prerecorded Show (RadioDJ playlist w/ hosts)",
				];
				break;
			case "genre":
			case "playlist":
				validTypes = ["genre", "playlist"];
				validTypesLabels = [
					"Genre Rotation (RadioDJ Rotation)",
					"Playlist (RadioDJ Playlist w/o hosts)",
				];
				break;
			case "sports":
				validTypes = ["sports"];
				validTypesLabels = [
					"Sports Broadcast (In-Studio or Remote via DJ Controls)",
				];
				break;
			case "office-hours":
				validTypes = ["office-hours"];
				validTypesLabels = ["Director Office Hours"];
				break;
			case "prod-booking":
			case "onair-booking":
				validTypes = ["prod-booking", "onair-booking"];
				validTypesLabels = [
					"Production Studio Booking",
					"On-Air Studio Booking",
				];
				break;
			case "task":
				validTypes = ["task"];
				validTypesLabels = ["Task / To-Do"];
				break;
		}

		// Get the events and playlists we can select from
		this.getEventsPlaylists((events, playlists) => {
			// Make some corrections with the original schedule parameter, such as timezone correction.
			schedule =
				schedule && schedule.ID
					? Object.assign(schedule, this.recurrenceRulesToFields(schedule))
					: Object.assign(schedule || {}, { calendarID: calendarID });
			schedule.startDate = schedule.startDate
				? moment
						.tz(
							schedule.startDate,
							this.manager.has("WWSUMeta")
								? this.manager.get("WWSUMeta").meta.timezone
								: moment.tz.guess()
						)
						.toISOString(true)
				: undefined;
			schedule.endDate = schedule.endDate
				? moment
						.tz(
							schedule.endDate,
							this.manager.has("WWSUMeta")
								? this.manager.get("WWSUMeta").meta.timezone
								: moment.tz.guess()
						)
						.toISOString(true)
				: undefined;

			// Convert duration from amount of time to an end time
			if (schedule && schedule.duration) {
				schedule.duration = moment
					.utc(schedule.startTime, "HH:mm", true)
					.add(schedule.duration, "minutes")
					.format("hh:mm A");
			}

			// Format logo and banner for file upload system
			if (schedule.logo) {
				schedule.logo = {
					id: schedule.logo,
					name: "Logo",
					size: "N/A",
					url: `${this.manager.socket.url}/api/uploads/${schedule.logo}`,
					thumbnailUrl: `${this.manager.socket.url}/api/uploads/${schedule.logo}`,
					deleteUrl: `${this.manager.socket.url}/api/uploads/${
						schedule.logo
					}?host=${
						this.manager.has("WWSUhosts")
							? this.manager.get("WWSUhosts").client.host
							: "null"
					}`,
					deleteType: "DELETE",
				};
			}
			if (schedule.banner) {
				schedule.banner = {
					id: schedule.banner,
					name: "Banner",
					size: "N/A",
					url: `${this.manager.socket.url}/api/uploads/${schedule.banner}`,
					thumbnailUrl: `${this.manager.socket.url}/api/uploads/${schedule.banner}`,
					deleteUrl: `${this.manager.socket.url}/api/uploads/${
						schedule.banner
					}?host=${
						this.manager.has("WWSUhosts")
							? this.manager.get("WWSUhosts").client.host
							: "null"
					}`,
					deleteType: "DELETE",
				};
			}

			// If the set playlist ID no longer exists, set to null.
			if (
				schedule &&
				schedule.playlistID &&
				!playlists.find((pl) => pl.ID === schedule.playlistID)
			) {
				schedule.playlistID = null;
			}

			// If the set event ID no longer exists, set to null.
			if (
				schedule &&
				schedule.eventID &&
				!events.find((ev) => ev.ID === schedule.eventID)
			) {
				schedule.eventID = null;
			}

			let usingRadioDJ = this.manager.get("WWSUconfig").usingRadioDJ;

			// Generate form
			this.scheduleModal.iziModal("open");
			$(this.scheduleModal.body).alpaca({
				schema: {
					title: "Schedule",
					type: "object",
					properties: {
						ID: {
							type: "number",
						},
						calendarID: {
							type: "number",
						},
						scheduleID: {
							type: "number",
						},
						oneTime: {
							title: "One-time Dates",
							type: "array",
							items: {
								title: "One-time Date",
								format: "date",
							},
						},
						startTime: {
							title: "Event start time",
							format: "time",
							required: true,
						},
						duration: {
							title: "Event end time",
							format: "time",
							required: true,
						},
						recurDW: {
							title: "Recurrence: Days of Week",
							type: "array",
							items: {
								type: "number",
							},
							enum: [0, 1, 2, 3, 4, 5, 6],
						},
						recurWM: {
							title: "Recurrence: Weeks of Month",
							type: "array",
							items: {
								type: "number",
							},
							enum: [0, 1, 2, 3, 4],
						},
						recurDM: {
							title: "Recurrence: Days of Month",
							type: "array",
							items: {
								type: "number",
							},
							enum: [
								1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
								19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
							],
						},
						recurEveryWeeks: {
							title: "Recurrence: Every X Weeks",
							type: "number",
							required: true,
							default: 0,
							min: 0,
							max: 52,
						},
						startDate: {
							title: "Recurrence: Start Date",
							format: "date",
						},
						endDate: {
							title: "Recurrence: End Date",
							format: "date",
						},
						type: {
							type: "string",
							title: "Change Type for this schedule",
							enum: validTypes,
						},
						name: {
							type: "string",
							title: "Change Name for this schedule",
							maxLength: 255,
						},
						description: {
							type: "string",
							title: "Change description for this schedule",
						},
						logo: {
							type: "array",
							title: "Change logo for this schedule",
						},
						banner: {
							type: "array",
							title: "Change Banner / Flyer for this schedule",
						},
						priority: {
							type: "number",
							title: "Change priority for this schedule",
							minimum: -1,
							maximum: 10,
						},
						hostDJ: {
							type: "number",
							title: "Change Host Member for this schedule",
							enum: _djs.filter((dj) => dj.ID > 0).map((dj) => dj.ID),
						},
						cohosts: {
							title: "Change Co-host Members for this schedule",
							type: "array",
							items: {
								type: "number",
							},
							maxItems: 3,
							enum: _djs.map((dj) => dj.ID),
						},
						playlistID: {
							type: "number",
							title: "Change RadioDJ Playlist for this schedule",
							enum: playlists.map((playlist) => playlist.ID),
						},
						eventID: {
							type: "number",
							title:
								"Change RadioDJ Rotation-triggering Manual Event for this schedule",
							enum: events.map((eventb) => eventb.ID),
						},
					},
				},
				view: {
					wizard: {
						hideSubmitButton: true,
						bindings: {
							ID: 1,
							calendarID: 1,
							scheduleID: 1,
							oneTime: 1,
							startTime: 1,
							duration: 1,
							recurDW: 2,
							recurWM: 2,
							recurDM: 2,
							recurEveryWeeks: 2,
							startDate: 2,
							endDate: 2,
							type: 3,
							name: 3,
							description: 3,
							logo: 3,
							banner: 3,
							priority: 3,
							hostDJ: 3,
							cohosts: 3,
							playlistID: 3,
							eventID: 3,
						},
						steps: [
							{
								title: "Dates/Times",
							},
							{
								title: "Recurrence",
							},
							{
								title: "Change Details",
							},
						],
					},
				},
				options: {
					fields: {
						ID: {
							type: "hidden",
						},
						calendarID: {
							type: "hidden",
						},
						scheduleID: {
							type: "hidden",
						},
						oneTime: {
							helper: `Specify specific non-recurring one-time dates you would like the event to occur. Note: one-time dates ignore all recurrence rules including start and end date.`,
							fields: {
								item: {
									dateFormat: `YYYY-MM-DD`,
									picker: {
										inline: true,
										sideBySide: true,
									},
								},
							},
							actionbar: {
								showLabels: true,
								actions: [
									{
										label: "Add",
										action: "add",
									},
									{
										label: "Remove",
										action: "remove",
									},
									{
										label: "Move Up",
										action: "up",
										enabled: false,
									},
									{
										label: "Move Down",
										action: "down",
										enabled: false,
									},
								],
							},
						},
						startTime: {
							helper: `Specify start time in the station's timezone of ${this.timezone}. This start time will be used for all recurring occurrences and one-time dates. For different start times, create multiple schedules.`,
							dateFormat: "hh:mm A",
							picker: {
								inline: true,
								sideBySide: true,
							},
							validator: function (callback) {
								let value = this.getValue();

								if (
									value &&
									value !== "" &&
									!moment(value, "hh:mm A", true).isValid()
								) {
									callback({
										status: false,
										message:
											"You must specify a start time in the format hh:mm A.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						duration: {
							helper: `Specify the end time in the station's timezone of ${this.timezone}. If the end time is before the start time, we assume the event will run past midnight the next day. This end time will be used for all recurring occurrences and one-time dates. For different end times, create multiple schedules.`,
							dateFormat: "hh:mm A",
							picker: {
								inline: true,
								sideBySide: true,
							},
							validator: function (callback) {
								let value = this.getValue();

								if (
									value &&
									value !== "" &&
									!moment(value, "hh:mm A", true).isValid()
								) {
									callback({
										status: false,
										message:
											"You must specify an end time in the format hh:mm A.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						recurDW: {
							helper:
								"If you want this event to recur on specific days of the week, choose them here. This recurring filter will be combined with others you specify.",
							type: "select",
							multiple: true,
							optionLabels: [
								"Sunday",
								"Monday",
								"Tuesday",
								"Wednesday",
								"Thursday",
								"Friday",
								"Saturday",
							],
						},
						recurWM: {
							helper:
								"If you want this event to recur on specific weeks of the month, choose them here. This recurring filter will be combined with others you specify.",
							type: "select",
							multiple: true,
							optionLabels: [
								"First",
								"Second",
								"Third",
								"Fourth",
								"Fifth (only if applicable)",
							],
						},
						recurDM: {
							helper:
								"If you want this event to recur on specific days of the month, choose them here.",
							type: "select",
							multiple: true,
						},
						recurEveryWeeks: {
							helpers: [
								"This event will only recur every specified number of weeks starting from the Start Date. For example, if you specify 2, the event will occur bi-weekly.",
								"You must set this to 1 or more to have recurrence.",
							],
							validator: function (callback) {
								let value = this.getValue();

								let recurDW =
									this.getParent().childrenByPropertyId["recurDW"].getValue();
								let recurWM =
									this.getParent().childrenByPropertyId["recurWM"].getValue();
								let recurDM =
									this.getParent().childrenByPropertyId["recurDM"].getValue();

								if (
									value === 0 &&
									((recurDW && recurDW.length > 0) ||
										(recurWM && recurWM.length > 0) ||
										(recurDM && recurDM.length > 0))
								) {
									callback({
										status: false,
										message:
											"You have one or more recurrence rules set! Did you mean to have recurrence? If so, set this to 1 or more. If not, remove all recurrence rules.",
									});
									return;
								}

								if (
									value > 0 &&
									(!recurDW || recurDW.length === 0) &&
									(!recurWM || recurWM.length === 0) &&
									(!recurDM || recurDM.length === 0)
								) {
									callback({
										status: false,
										message:
											"You must set at least 1 recurrence rule (such as selecting all days of the week). Otherwise, set this to 0 for no recurrence.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						startDate: {
							dateFormat: `YYYY-MM-DD`,
							helper: `If a date is specified, recurring occurrences will not happen before this date.`,
							picker: {
								inline: true,
								sideBySide: true,
							},
						},
						endDate: {
							dateFormat: `YYYY-MM-DD`,
							helper: `Recurring occurrences will not happen after this date. It is recommended to set this as the end of the show scheduling period (such as the semester).`,
							picker: {
								inline: true,
								sideBySide: true,
							},
							validator: function (callback) {
								let value = this.getValue();

								let recurDW =
									this.getParent().childrenByPropertyId["recurDW"].getValue();
								let recurWM =
									this.getParent().childrenByPropertyId["recurWM"].getValue();
								let recurDM =
									this.getParent().childrenByPropertyId["recurDM"].getValue();

								if (
									(!value || value === "") &&
									((recurDW && recurDW.length > 0) ||
										(recurWM && recurWM.length > 0) ||
										(recurDM && recurDM.length > 0))
								) {
									callback({
										status: false,
										message: "endDate is required for recurring schedules.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						type: {
							type: "select",
							optionsLabels: validTypesLabels,
							helper: `Specify the event type for this schedule if different from the event default of <strong>${event.type}</strong>`,
						},
						name: {
							helper: `Specify an event name that should be used for this schedule if different from the event default of <strong>${event.name}</strong>. This field is ignored for bookings and office-hours.`,
							validator: function (callback) {
								let value = this.getValue();
								if (value.includes(" -")) {
									callback({
										status: false,
										message: `Invalid; event names may not contain " - " as this is a separation used by the system.`,
									});
									return;
								}
								if (event.type === "sports") value = value.split(" vs.")[0];
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									value &&
									value !== "" &&
									(((!type || type === "") && event.type === "sports") ||
										type === "sports") &&
									sportsEvents.indexOf(value) === -1
								) {
									callback({
										status: false,
										message: `For sports, name must begin with a valid sport and optionally proceed with " vs. name of opponent team". Valid sports: ${sportsEvents.join(
											", "
										)}`,
									});
									return;
								} else if (
									value &&
									value !== "" &&
									calendarEvents.indexOf(value) !== -1 &&
									(!event || !event.name || event.name !== value)
								) {
									callback({
										status: false,
										message:
											"Value in this field matches the name of another event. This is not allowed.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						description: {
							type: "textarea",
							helper: `Specify a description that should be used for this schedule if different from the event default: ${
								event.description ? event.description : `--NONE--`
							}`,
						},
						logo: {
							type: "upload",
							maxNumberOfFiles: 1,
							fileTypes: "image/*",
							maxFileSize: 1024 * 1024, // 1 MB file size limit
							upload: {
								autoUpload: false,
								formData: {
									type: "calendar/logo",
									host: this.manager.get("WWSUhosts").client.host,
								},
								url: this.manager.socket.url + "/api/uploads",
							},
							helper:
								"Maximum file size is 1 MB. Will only use the first image. Logo should be square or round if possible. Text should be kept minimal in amount and should be large in size.",
						},
						banner: {
							type: "upload",
							maxNumberOfFiles: 1,
							fileTypes: "image/*",
							maxFileSize: 1024 * 1024, // 1 MB file size limit
							upload: {
								autoUpload: false,
								formData: {
									type: "calendar/banner",
									host: this.manager.get("WWSUhosts").client.host,
								},
								url: this.manager.socket.url + "/api/uploads",
							},
							helper:
								"Maximum file size is 1 MB. Will only use the first image. Banners / flyers should be promotional material for the broadcast and are usually large.",
						},
						priority: {
							type: "integer",
							helpers: [
								`Change the schedule priority. -1 means event should never conflict with other events. 0 means event will only conflict with other events of the same type. 1 - 10 means event will be overridden by other events with a higher priority, and will override events with a lower priority. The current priority is <strong>${event.priority}</strong>`,
								`Defaults if left blank: 0 for genres and bookings, 1 for playlists, 3 for prerecords, 5 for live shows, 7 for remotes, 9 for sports broadcasts, and -1 for everything else.`,
								`A conflict check will run to ensure events do not conflict with each other. If any conflicts are detected, you will be informed on the actions that will be taken before you proceed.`,
							],
						},
						hostDJ: {
							type: "select",
							optionLabels: _djs
								.filter((dj) => dj.ID > 0)
								.map((dj) => `${dj.name} (${dj.realName})`),
							helper: `Specify the host DJ running this show for this schedule if different from the event default of <strong>${
								event.hostDJ && _djs.find((dj) => dj.ID === event.hostDJ)
									? _djs.find((dj) => dj.ID === event.hostDJ).name
									: `--NONE--`
							}</strong>`,
							validator: function (callback) {
								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();

								if (type === "") type = null;

								if (
									((type &&
										type !== "" &&
										["show", "remote", "prerecord"].indexOf(type) !== -1) ||
										["show", "remote", "prerecord"].indexOf(event.type) !==
											-1) &&
									(!value || value === "") &&
									!event.hostDJ
								) {
									callback({
										status: false,
										message:
											"Field is required for show, remote, and prerecord events.",
									});
									return;
								}

								// Integrity checks
								let dj = _djs.find((dj) => dj.ID === (value || event.hostDJ));
								if (!dj) {
									callback({
										status: false,
										message:
											"The org member for this event could not be found or is marked inactive in the system.",
									});
									return;
								}
								if ((type || event.type) === "show") {
									if (dj.permissions.indexOf("live") === -1) {
										callback({
											status: true,
											message: `${dj.name} (${dj.realName}) does not currently have permission to start live broadcasts in the studio. After continuing, don't forget to assign it in Org Members -> edit the member.`,
										});
										return;
									}
								}
								if ((type || event.type) === "remote") {
									if (dj.permissions.indexOf("remote") === -1) {
										callback({
											status: true,
											message: `${dj.name} (${dj.realName}) does not currently have permission to start remote broadcasts from their computer. After continuing, don't forget to assign it in Org Members -> edit the member. And make sure they have at least one authorized host assigned to them in Hosts.`,
										});
										return;
									}

									let host = _hosts.find(
										(_host) => _host.belongsTo === (value || event.hostDJ)
									);
									if (!host) {
										callback({
											status: true,
											message: `${dj.name} (${dj.realName}) does not currently have an authorized DJ Controls host assigned to them. After continuing, make sure they have DJ Controls installed, they ran it once, and you edit their host under Hosts, making sure it is authorized and that it is marked as belonging to them.`,
										});
										return;
									}
								}

								callback({
									status: true,
								});
							},
						},
						cohosts: {
							type: "select",
							multiple: true,
							optionLabels: _djs.map(
								(dj) => `${dj.name} (${dj.realName}) [${dj.ID}]`
							),
							helpers: [
								`To change the current listed co-hosts (<strong>${
									event.cohostDJ1 &&
									_djs.find((dj) => dj.ID === event.cohostDJ1)
										? _djs.find((dj) => dj.ID === event.cohostDJ1).name
										: ``
								}${
									event.cohostDJ2 &&
									_djs.find((dj) => dj.ID === event.cohostDJ2)
										? ", " + _djs.find((dj) => dj.ID === event.cohostDJ2).name
										: ``
								}${
									event.cohostDJ3 &&
									_djs.find((dj) => dj.ID === event.cohostDJ3)
										? ", " + _djs.find((dj) => dj.ID === event.cohostDJ3).name
										: ``
								}</strong>), please specify the updated list of co-hosts (maximum 3)`,
								"<strong>Definition:</strong> A co-host is NOT just a regular guest; a co-host puts together and helps run the show as well every broadcast. By specifying co-hosts, show analytics will also apply to them.",
							],
							validator: function (callback) {
								let values = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();

								if (type === "") type = null;

								let status = true;
								let messages = [];

								if (values && values.indexOf(0) === -1) {
									values.forEach((_value) => {
										let value = _value.value;
										
										// Integrity checks
										let dj = _djs.find((dj) => dj.ID === value);
										if (!dj) {
											status = false;
											messages.push(
												`Org member ID ${value} could not be found or is marked inactive in the system.`
											);
											return;
										}
										if ((type || event.type) === "show") {
											if (dj.permissions.indexOf("live") === -1) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have permission to start live broadcasts in the studio. After continuing, don't forget to assign it in Org Members -> edit the member.`
												);
												return;
											}
										}
										if ((type || event.type) === "remote") {
											if (dj.permissions.indexOf("remote") === -1) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have permission to start remote broadcasts from their computer. After continuing, don't forget to assign it in Org Members -> edit the member. And make sure they have at least one authorized host assigned to them in Hosts.`
												);
												return;
											}

											let host = _hosts.find(
												(_host) => _host.belongsTo === value
											);
											if (!host) {
												messages.push(
													`${dj.name} (${dj.realName}) does not currently have an authorized DJ Controls host assigned to them. After continuing, make sure they have DJ Controls installed, they ran it once, and you edit their host under Hosts, making sure it is authorized and that it is marked as belonging to them.`
												);
												return;
											}
										}
									});
								}

								callback({
									status: status,
									message:
										messages.length > 0 ? messages.join("<br />\n") : undefined,
								});
							},
						},
						playlistID: {
							type: "select",
							optionLabels: playlists.map((playlist) => playlist.name),
							helper: `Set the RadioDJ playlist (prerecord and playlist events) for this schedule if different from the event default of <strong>${
								event.playlistID &&
								playlists.find((playlist) => playlist.ID === event.playlistID)
									? playlists.find(
											(playlist) => playlist.ID === event.playlistID
									  ).name
									: `--NONE--`
							}</strong>`,
							hidden: !usingRadioDJ,
							validator: function (callback) {
								if (!usingRadioDJ) {
									callback({
										status: true,
									});
									return;
								}

								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									((type &&
										type !== "" &&
										["prerecord", "playlist"].indexOf(type) !== -1) ||
										["prerecord", "playlist"].indexOf(event.type) !== -1) &&
									(!value || value === "") &&
									!event.playlistID
								) {
									callback({
										status: false,
										message:
											"Field is required for prerecord and playlist events.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
						eventID: {
							type: "select",
							optionLabels: events.map((eventb) => eventb.name),
							helper: `Set the rotation-triggering RadioDJ event (genre events) if different from the event default of <strong>${
								event.eventID &&
								events.find((eventb) => eventb.ID === event.eventID)
									? events.find((eventb) => eventb.ID === event.eventID).name
									: `--NONE--`
							}</strong>`,
							hidden: !usingRadioDJ,
							validator: function (callback) {
								if (!usingRadioDJ) {
									callback({
										status: true,
									});
									return;
								}
								let value = this.getValue();
								let type =
									this.getParent().childrenByPropertyId["type"].getValue();
								if (
									((type && type !== "" && ["genre"].indexOf(type) !== -1) ||
										["genre"].indexOf(event.type) !== -1) &&
									(!value || value === "") &&
									!event.eventID
								) {
									callback({
										status: false,
										message: "Field is required for genre events.",
									});
									return;
								}
								callback({
									status: true,
								});
							},
						},
					},

					form: {
						buttons: {
							submit: {
								title: `${schedule && schedule.ID ? `Edit` : `Add`} Schedule`,
								click: (form, e) => {
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();

									// Map recurring rules to an array
									value.recurDM = value.recurDM.map((val) => val.value);
									value.recurWM = value.recurWM.map((val) => val.value);
									value.recurDW = value.recurDW.map((val) => val.value);

									// Prepare fields for database
									value = Object.assign(
										value,
										this.fieldsToRecurrenceRules(value)
									);
									delete value.recurDM;
									delete value.recurDW;
									delete value.recurWM;
									delete value.recurEveryWeeks;

									// Change startTime to 24-hour format
									value.startTime = moment
										.utc(value.startTime, "hh:mm A", true)
										.format("HH:mm");

									// Change duration to number of minutes from startTime to duration time
									value.duration = moment
										.utc(value.duration, "hh:mm A", true)
										.diff(
											moment.utc(value.startTime, "HH:mm", true),
											"minutes"
										);
									if (value.duration <= 0)
										value.duration = 60 * 24 + value.duration;

									// Convert value.cohosts into cohostDJ1 2 and 3
									if (value.cohosts && value.cohosts.length > 0) {
										value.cohostDJ1 = 0;
										value.cohostDJ2 = 0;
										value.cohostDJ3 = 0;

										// Only populate new co-hosts if we did not specify to clear all of them
										if (value.cohosts.indexOf(0) === -1) {
											value.cohosts = value.cohosts.map((item) => item.value);
											if (value.cohosts[0]) value.cohostDJ1 = value.cohosts[0];
											if (value.cohosts[1]) value.cohostDJ2 = value.cohosts[1];
											if (value.cohosts[2]) value.cohostDJ3 = value.cohosts[2];
										}

										delete value.cohosts;
									}

									// Event logo and banner should only contain IDs and should only reflect the first file in the list.
									value.logo =
										value.logo && value.logo[0] ? value.logo[0].id : null;
									value.banner =
										value.banner && value.banner[0] ? value.banner[0].id : null;

									let _event = this.verify(
										value,
										this.manager.get("WWSUconfig").usingRadioDJ
									); // Verify the event just to be safe

									if (!_event.event) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										$(document).Toasts("create", {
											class: "bg-warning",
											title: "Event verification failed",
											autohide: true,
											delay: 20000,
											body: _event,
										});
										form.focus();
										return;
									}

									// Add or edit the schedule
									if (!schedule || !schedule.ID) {
										this.addSchedule(this.scheduleModal, value, (success) => {
											if (success) {
												this.scheduleModal.iziModal("close");
												this.occurrenceModal.iziModal("close");
												this.schedulesModal.body = `<div class="alert alert-warning">
                                            Schedule changes take several seconds to reflect in the system. Please close and re-open this window.
                                            </div>`;
											}
										});
									} else {
										this.editSchedule(this.scheduleModal, value, (success) => {
											if (success) {
												this.scheduleModal.iziModal("close");
												this.occurrenceModal.iziModal("close");
												this.schedulesModal.body = `<div class="alert alert-warning">
                                            Schedule changes take several seconds to reflect in the system. Please close and re-open this window.
                                            </div>`;
											}
										});
									}
								},
							},
						},
					},
				},

				data: schedule,
			});
		});
	}

	/**
	 * Do conflict checking on an event and show a modal if conflicts are detected.
	 *
	 * @param {WWSUmodal} modal Modal to block while checking for conflicts
	 * @param {object} event The CalendarDb event being added, edited, or deleted
	 * @param {string} action What we are doing with event: insert, update, or remove
	 * @param {function} cb Callback fired when there are no conflicts detected or the user agreed to proceed with the conflict resolution steps.
	 */
	doConflictCheck(modal, event, action, cb) {
		console.dir(event);
		$(`#modal-${modal.id} .modal-content`).prepend(`<div class="overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
              <h1 class="text-white">Processing...</h1>
        </div>`);
		if (["remove", "removeCalendar"].indexOf(action) === -1) {
			event = this.verify(event, this.manager.get("WWSUconfig").usingRadioDJ);
		} else {
			event = {
				event: _.cloneDeep(event),
			};
		}
		if (!event.event) {
			$(`#modal-${modal.id} .overlay`).remove();
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Error resolving schedule conflicts",
				body: `Event is invalid: ${event}`,
				autohide: true,
				delay: 10000,
				icon: "fas fa-skull-crossbones fa-lg",
			});
			console.dir(event);
			return;
		}
		let query = {};
		query[action] =
			action === "remove" || action === "removeCalendar"
				? event.event.ID
				: event.event;

		this.checkConflicts(
			(conflicts) => {
				console.dir(conflicts);
				$(`#modal-${modal.id} .overlay`).remove();

				// If no conflicts detected, then fire callback immediately

				if (
					conflicts.additions.length === 0 &&
					conflicts.removals.length === 0 &&
					conflicts.errors.length === 0
				) {
					cb();
					return;
				}

				let actions = [];

				conflicts.errors.map((error) => {
					actions.push(`<li><strong>ERROR: </strong>${error}</li>`);
				});

				conflicts.additions.map((conflict) => {
					if (conflict.scheduleType === "canceled-system") {
						actions.push(
							`<li>${conflict.type}: ${conflict.hosts} - ${conflict.name} <ul>
		  <li>Original date/time: ${moment(conflict.originalTime).format(
				"LLLL"
			)} - ${moment(conflict.originalTime)
								.add(conflict.originalDuration, "minutes")
								.format("LLLL")}</li>
		  <li>Action: will be <strong>CANCELED</strong></li>
		  <li>Reason: ${conflict.scheduleReason}</li>
		  </ul></li>`
						);
					}
					if (conflict.scheduleType === "updated-system" && !conflict.newTime) {
						actions.push(
							`<li>${conflict.type}: ${conflict.hosts} - ${conflict.name} <ul>
		  <li>Original date/time: ${moment(conflict.originalTime).format(
				"LLLL"
			)} - ${moment(conflict.originalTime)
								.add(conflict.originalDuration, "minutes")
								.format("LLLL")}</li>
		  <li>Action: Will <strong>END EARLIER</strong></li>
		  <li>New end time: ${moment(conflict.originalTime)
				.add(conflict.duration, "minutes")
				.format("LLLL")}</li>
			<li>Reason: ${conflict.scheduleReason}</li>
				</ul></li>`
						);
					}
					if (conflict.scheduleType === "updated-system" && conflict.newTime) {
						actions.push(
							`<li>${conflict.type}: ${conflict.hosts} - ${conflict.name} <ul>
		  <li>Original date/time: ${moment(conflict.originalTime).format(
				"LLLL"
			)} - ${moment(conflict.originalTime)
								.add(conflict.originalDuration, "minutes")
								.format("LLLL")}</li>
		  <li>Action: Will <strong>START LATER</strong></li>
		  <li>New start date/time: ${moment(conflict.newTime).format("LLLL")}</li>
			<li>Reason: ${conflict.scheduleReason}</li>
			</ul></li>`
						);
					}
				});

				conflicts.removals.map((conflict) => {
					if (conflict.scheduleType === "canceled-system") {
						actions.push(
							`<li>${conflict.type}: ${conflict.hosts} - ${conflict.name} <ul>
		  <li>Original date/time: ${moment(conflict.originalTime).format(
				"LLLL"
			)} - ${moment(conflict.originalTime)
								.add(conflict.originalDuration, "minutes")
								.format("LLLL")}</li>
		  <li>Previously: Canceled</li>
		  <li>Action: will be <strong>UN-CANCELED</strong> (put back on the schedule).</li></ul></li>`
						);
					}
					if (conflict.scheduleType === "updated-system" && !conflict.newTime) {
						actions.push(
							`<li>${conflict.type}: ${conflict.hosts} - ${conflict.name} <ul>
		  <li>Original date/time: ${moment(conflict.originalTime).format(
				"LLLL"
			)} - ${moment(conflict.originalTime)
								.add(conflict.originalDuration, "minutes")
								.format("LLLL")}</li>
		  <li>Previously: Re-scheduled to end at ${moment(conflict.originalTime)
				.add(conflict.duration, "minutes")
				.format("LLLL")}</li>
		  <li>Action: will <strong>END AT ORIGINALLY SCHEDULED TIME</strong></li></ul></li>`
						);
					}
					if (conflict.scheduleType === "updated-system" && conflict.newTime) {
						actions.push(
							`<li>${conflict.type}: ${conflict.hosts} - ${conflict.name} <ul>
		  <li>Original date/time: ${moment(conflict.originalTime).format(
				"LLLL"
			)} - ${moment(conflict.originalTime)
								.add(conflict.originalDuration, "minutes")
								.format("LLLL")}</li>
		  <li>Previously: Re-scheduled to start at ${moment(conflict.newTime).format(
				"LLLL"
			)}</li>
		  <li>Action: will <strong>START AT ORIGINALLY SCHEDULED TIME</strong></li></ul></li>`
						);
					}
				});

				this.conflictModal.iziModal("open");
				this.conflictModal.body = `<p>The following changes will be made to resolve event conflicts if you continue:</p>
			<div id="modal-${this.conflictModal.id}-conflicts"></div>
			${
				conflicts.errors.length === 0
					? `<div class="alert alert-info">
			<p>DJs will be emailed and subscribers notified of cancellations / changes or their reversals.</p>
		  </div>
			<button type="button" class="btn btn-success" id="modal-${this.conflictModal.id}-continue">Continue</button>
			<button type="button" id="modal-${this.conflictModal.id}-close" class="btn btn-danger">Cancel</button>`
					: `<div class="alert alert-danger">
<p>You cannot proceed due to the errors stated above.</p>
</div>
<button type="button" id="modal-${this.conflictModal.id}-close" class="btn btn-danger">Cancel</button>`
			}`;
				$(`#modal-${this.conflictModal.id}-conflicts`).html(
					`<ul>${actions.join("")}</ul>`
				);

				$(`#modal-${this.conflictModal.id}-continue`).unbind("click");
				$(`#modal-${this.conflictModal.id}-continue`).click((e) => {
					cb();
					this.conflictModal.iziModal("close");
				});

				$(`#modal-${this.conflictModal.id}-close`).unbind("click");
				$(`#modal-${this.conflictModal.id}-close`).click((e) => {
					this.conflictModal.iziModal("close");
				});
			},
			[query],
			(string) => {
				$(".conflict-check-progress").html(string);
			}
		);
	}

	/**
	 * this.manager.get("WWSUutil")ity function to convert schedule fields into moment.recur recurrence rules.
	 *
	 * @param {object} record The form record to check
	 * @returns {object} properties recurrenceRules and recurrenceInterval to use in the database.
	 */
	fieldsToRecurrenceRules(record) {
		let criteria = { recurrenceRules: null, recurrenceInterval: null };

		criteria.recurrenceRules = [];

		if (record.recurDM && record.recurDM.length > 0) {
			criteria.recurrenceRules.push({
				measure: "daysOfMonth",
				units: record.recurDM,
			});
		}

		if (record.recurWM && record.recurWM.length > 0) {
			criteria.recurrenceRules.push({
				measure:
					record.recurDW && record.recurDW.length > 0
						? "weeksOfMonthByDay"
						: "weeksOfMonth",
				units: record.recurWM,
			});
		}

		if (record.recurDW && record.recurDW.length > 0) {
			criteria.recurrenceRules.push({
				measure: "daysOfWeek",
				units: record.recurDW,
			});
		}

		if (record.recurEveryWeeks && record.recurEveryWeeks > 0) {
			criteria.recurrenceInterval = {
				measure: "weeks",
				unit: record.recurEveryWeeks,
			};
		}

		return criteria;
	}

	/**
	 * this.manager.get("WWSUutil")ity function to convert recurrenceRules and recurrenceInterval into form fields.
	 *
	 * @param {object} record The event record.
	 * @returns {object} recurDM, recurWM, recurDW, and recurEveryWeeks.
	 */
	recurrenceRulesToFields(record) {
		let criteria = {
			recurDM: null,
			recurWM: null,
			recurDW: null,
			recurEveryWeeks: 0,
		};

		if (record.recurrenceRules && record.recurrenceRules.length > 0) {
			record.recurrenceRules.map((rule) => {
				if (!rule.measure || !rule.units || rule.units.length === 0) return;
				switch (rule.measure) {
					case "daysOfMonth":
						criteria.recurDM = rule.units;
						break;
					case "weeksOfMonth":
					case "weeksOfMonthByDay":
						criteria.recurWM = rule.units;
						break;
					case "daysOfWeek":
						criteria.recurDW = rule.units;
						break;
				}
			});
		}

		if (
			record.recurrenceInterval &&
			record.recurrenceInterval.measure &&
			record.recurrenceInterval.measure === "weeks" &&
			record.recurrenceInterval.unit
		) {
			criteria.recurEveryWeeks = record.recurrenceInterval.unit;
		}

		return criteria;
	}

	/**
	 * Call this function from fullcalendar.io when someone clicks/drags to create a new occurrence via a modal.
	 *
	 * @param {string} start The ISO timestamp of when the user wants the occurrence to start.
	 * @param {string} end The ISO timestamp of when the user wants the occurrence to end.
	 */
	newOccurrence(start, end) {
		let duration = moment(end).diff(start, "minutes");

		// Duration check; do not allow events more than 1 day long
		if (duration > 60 * 24) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "Multi-day Events Not Allowed",
				body: "Occurrences may not last more than 24 hours. Consider setting up a recurring schedule.",
				autohide: true,
				delay: 15000,
			});
			return;
		}

		this.newOccurrenceModal.iziModal("open");
		this.newOccurrenceModal.title = `New Occurrence (${moment(start).format(
			"lll"
		)} - ${moment(end).format("LT")})`;
		this.newOccurrenceModal.body = ``;

		$(this.newOccurrenceModal.body).alpaca({
			schema: {
				type: "object",
				properties: {
					event: {
						title: "Choose Event",
						type: "number",
						required: true,
						enum: this.calendar
							.db({ active: true })
							.get()
							.map((cal) => cal.ID),
					},
					recur: {
						type: "boolean",
						title: "Recurs / Repeats?",
					},
				},
			},
			options: {
				fields: {
					event: {
						type: "select",
						optionLabels: this.calendar
							.db({ active: true })
							.get()
							.map((cal) => `${cal.type}: ${cal.hosts} - ${cal.name}`),
						helper:
							"If the event you want is not listed, you may need to add it first under the Manage Events button on the calendar.",
					},
					recur: {
						rightLabel: "Yes",
						helpers: [
							`If unchecked, the time of ${moment(start).format(
								"lll"
							)} - ${moment(end).format(
								"lll"
							)} will be filled in as a one-time occurrence on the next screen.`,
							`If checked, the time of ${moment(start).format(
								"dddd h:mm A"
							)} - ${moment(end).format(
								"dddd h:mm A"
							)} will be filled in as a weekly recurring schedule on the next screen (but you can change recurrence settings).`,
						],
					},
				},

				form: {
					buttons: {
						submit: {
							title: `Continue`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								if (value.recur) {
									this.showScheduleForm(
										{
											recurDW: parseInt(moment(start).format("e")),
											recurEveryWeeks: 1,
											startTime: moment(start).format("HH:mm"),
											duration: duration,
										},
										value.event
									);
								} else {
									this.showScheduleForm(
										{
											oneTime: [moment(start).format("YYYY-MM-DD")],
											startTime: moment(start).format("HH:mm"),
											recurEveryWeeks: 0,
											duration: duration,
										},
										value.event
									);
								}
							},
						},
					},
				},
			},
		});
	}
}
