/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

// Endpoints that should be allowed even for banned clients.
const noBanCheck = ["logRequest", "loadCheck"];

// Base policies for most everything.
const basicWeb = ["logRequest", "isBanned", "loadCheck"];

// Additional policies for endpoints that must be called via a web socket
const basicSockets = basicWeb.concat(["isSocket"]);

// Policies for endpoints requiring host authorization
const requireHost = basicSockets.concat(["isAuthorizedHost"]);

// Policies for endpoints requiring DJ / org member authorization
const requireDJ = basicSockets.concat(["isAuthorizedDJ"]);

// Policies for endpoints requiring director authorization
const requireDirector = basicSockets.concat(["isAuthorizedDirector"]);

// Policies for endpoints requiring administrator director authorization
const requireAdminDirector = basicSockets.concat(["isAuthorizedAdminDirector"]);

module.exports.policies = {
  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/

  // DEFAULTS
  "*": ["logRequest", "isBannedToPage", "loadCheck"], // Default for everything, including non-API paths
  "api/*": requireHost, // By default, API paths are restricted to unbanned, sockets, and authorized hosts unless otherwise specified in this file

  // Auth
  "api/auth/*": ["logRequest", "isBanned", "isSocket"], // No authorization for authorization; that wouldn't make sense now would it? Also skip load checking on auth.

  // Analytics
  "api/analytics/weekly/get": basicSockets,

  // Announcements
  "api/announcements/*": requireDirector,
  "api/announcements/get": basicSockets,

  // Attendance
  "api/attendance/*": requireDirector,
  "api/attendance/get": requireHost,

  // Blogs
  "api/blogs/host/delete": requireDirector,
  "api/blogs/host/get": requireHost,
  "api/blogs/host/post": requireDirector,
  "api/blogs/host/put": requireDirector,
  "api/blogs/web/get": basicSockets,
  "api/blogs/web/post": requireDJ,
  "api/blogs/web/search": basicSockets,

  // Bookings
  "api/bookings/director/post": requireDirector,
  "api/bookings/dj/post": requireDJ,

  // Calendar
  "api/calendar/*": basicSockets,
  "api/calendar/clockwheels/web/*": requireDJ,
  "api/calendar/event/activate": requireDirector,
  "api/calendar/event/deactivate": requireDirector,
  "api/calendar/event/delete": requireDirector,
  "api/calendar/event/get": basicSockets,
  "api/calendar/event/post": requireDirector,
  "api/calendar/event/put": requireDirector,
  "api/calendar/events-playlists/get": basicSockets,
  "api/calendar/schedule/delete": requireDirector,
  "api/calendar/schedule/get": basicSockets,
  "api/calendar/schedule/post": requireDirector,
  "api/calendar/schedule/put": requireDirector,
  "api/calendar/web/*": requireDJ,
  "api/calendar/web/get": basicWeb, // don't require sockets; The Wright State website needs access

  // Call; use default API policies

  // Config
  "api/config/*": requireDirector,
  "api/config/get": basicSockets,

  // Delay
  "api/delay/*": ["logRequest", "isBanned", "isSocket", "isAuthorizedHost"], // No load check for delay; dumping is very time sensitive and cannot afford throttling

  // Directors
  "api/directors/get": basicWeb, // don't require sockets; The Wright State website needs access
  "api/directors/*": requireAdminDirector,

  // Discipline
  "api/discipline/acknowledge": noBanCheck, // No ban check because banned clients should still be able to acknowledge their discipline message.
  "api/discipline/host/delete": requireDirector,
  "api/discipline/host/put": requireDirector, // While regular hosts can add discipline (eg DJs or directors), only directors should be able to edit.,
  "api/discipline/web/get": noBanCheck, // Banned clients should still be able to call this endpoint to get their discipline messages.

  // DJ Notes
  "api/djnotes/*": requireDirector,
  "api/djnotes/get": requireHost,

  // DJs
  "api/djs/*": requireAdminDirector, // By default, DJ operations should require an admin director due to the door code system
  "api/djs/host/activate": requireDirector, // Any director should be able to activate a member; no door code leaking risk here
  "api/djs/host/post": requireDirector, // Any director should be able to add a new member; no door code leaking risk here
  "api/djs/web/get": requireDJ,
  "api/djs/host/get": basicSockets,

  // EAS
  "api/eas/*": requireDirector,
  "api/eas/get": basicWeb, // don't require sockets; The Guardian Media Group website uses this
  "eas/speak": ["logRequest", "isBanned"], // No load check; this should be immediate always due to the timely nature
  "eas/speak-new": ["logRequest", "isBanned"], // No load check; this should be immediate always due to the timely nature

  // Email
  "api/email/post": requireDirector,

  // Flags
  "api/flags/post": basicSockets,

  // Hosts
  "api/hosts/*": requireDirector,
  "api/hosts/get": requireHost,
  "api/hosts/log-out": requireHost,
  "api/hosts/log-in/director": requireDirector,
  "api/hosts/log-in/dj": requireDJ,

  // Inventory
  "api/inventory/*": requireDirector,
  "api/inventory/items/get": requireHost,

  // Lockdown
  "api/lockdown/get": basicSockets,

  // Logs
  "api/logs/put": requireDirector,
  "api/logs/acknowledge-all": requireDirector,

  // Messages
  "api/messages/web/get": basicSockets,
  "api/messages/web/post": basicSockets,

  // Meta
  "api/meta/get": basicWeb, // don't require sockets; The Wright State website and any other sites listing what we are playing need access
  "api/meta/put": ["logRequest", "isBanned", "hasMetaSecret"],

  // Recipients
  "api/recipients/display/post": basicSockets,
  "api/recipients/web/post": basicSockets,
  "api/recipients/web/put": basicSockets,

  // Requests
  "api/requests/post": basicSockets,

  // RSS
  "api/rss/get": basicSockets,

  // Server
  "api/server/*": [
    "logRequest",
    "isBanned",
    "isSocket",
    "isAuthorizedAdminDirector",
  ], // No load checks; server operations should be immediate.

  // Shootout
  "api/shootout/get": basicSockets,
  "api/shootout/put": requireDirector,

  // Silence
  "api/silence/*": ["logRequest", "isBanned", "isSocket", "isAuthorizedHost"], // No load check for silence as they should always be immediate

  // Songs
  "api/songs/genres/get": basicSockets,
  "api/songs/liked/get": basicSockets,
  "api/songs/liked/post": basicSockets,
  "api/songs/long/put": requireDirector,
  "api/songs/get": basicSockets,

  // Sports
  "api/sports/get": basicSockets,
  "api/sports/put": basicSockets,

  // State
  "api/state/*": ["logRequest", "isBanned", "isSocket", "isAuthorizedHost"], // No load check for states as they should always be immediate
  "api/state/radiodj/*": [
    "logRequest",
    "isBanned",
    "isSocket",
    "isAuthorizedDirector",
  ], // No load check for states as they should always be immediate

  // Status
  "api/status/get": basicSockets,
  "api/status/put": requireHost,
  "api/status/post": basicSockets,

  // Subscribers
  "api/subscribers/directors/*": requireDirector,
  "api/subscribers/web/*": basicSockets,

  // Timesheet
  "api/timesheet/post": requireDirector,
  "api/timesheet/put": requireAdminDirector,
  "api/timesheet/get": basicSockets,
  "api/timesheet/delete": requireAdminDirector,

  // Underwritings
  "api/underwritings/*": requireDirector,
  "api/underwritings/get": basicSockets,

  // Uploads
  "api/uploads/*": basicWeb, // don't require sockets; often used to render images on web pages

  // Version
  "api/version/put": requireAdminDirector,

  // Weather
  "api/weather/get": basicWeb, // don't require sockets; The Guardian Media Group uses this
};
