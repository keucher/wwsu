// Delay shut down if crucial tasks are in progress.

module.exports.beforeShutdown = (cb) => {
  // Do not proceed if sails has not even fully lifted.
  if (!sails.config.custom.lifted) {
    return cb();
  }

  sails.log.debug(
    "Before shutdown called. Waiting for critical tasks to finish..."
  );

  let checkTimer;

  // Force shut down after 3 minutes
  let forceTermination = setTimeout(() => {
    sails.log.warn(
      "beforeShutdown did not finish after 3 minutes! Forcefully proceeding to shut down..."
    );
    clearTimeout(checkTimer);

    // Log the forceful shut down
    let event = `The graceful shut down did not finish after 3 minutes. The shut down was changed to forceful.`;
    for (let task in sails.models.status.tasks) {
      if (
        !Object.prototype.hasOwnProperty.call(sails.models.status.tasks, task)
      )
        continue;

      if (
        (typeof sails.models.status.tasks[task] === "number" &&
          sails.models.status.tasks[task] > 0) ||
        sails.models.status.tasks[task].size > 0
      ) {
        event += `<br />${task} tasks that were still pending: ${
          typeof sails.models.status.tasks[task] === "number"
            ? sails.models.status.tasks[task]
            : sails.models.status.tasks[task].size
        }`;
      }
    }

    sails.models.logs
      .create({
        attendanceID: sails.models.meta.memory.attendanceID,
        logtype: "shutdown",
        loglevel: "orange",
        logsubtype: "",
        logIcon: `fas fa-power-off`,
        title: `Graceful shutdown timed out!`,
        event: event,
      })
      .fetch()
      .exec(() => {
        cb();
      });
  }, 180000);

  const checkTasks = () => {
    let delay = false;

    // Iterate over each task in status and set delay to true if any of them are running.
    for (let task in sails.models.status.tasks) {
      if (
        !Object.prototype.hasOwnProperty.call(sails.models.status.tasks, task)
      )
        continue;

      if (
        (typeof sails.models.status.tasks[task] === "number" &&
          sails.models.status.tasks[task] > 0) ||
        sails.models.status.tasks[task].size > 0
      ) {
        delay = true;
        sails.log.verbose(
          `${task} is still running (${
            typeof sails.models.status.tasks[task] === "number"
              ? sails.models.status.tasks[task]
              : sails.models.status.tasks[task].size
          } tasks)...`
        );
      }
    }

    if (delay) {
      // We should delay? Wait 1 second and call the function again.
      checkTimer = setTimeout(() => {
        checkTasks();
      }, 1000);
    } else {
      // All tasks are done / not running. Proceed shut down by calling the callback.
      sails.log.verbose(`All tasks complete! Proceeding to shut down.`);
      clearTimeout(forceTermination);
      cb();
    }
  };

  // ChangingState to indicate we are shutting down so that no state changes are made and DJ Controls blocks operation buttons.
  sails.helpers.meta.change
    .with({ changingState: "Shutting down..." })
    .exec(() => {
      // Log the shut down request
      sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "shutdown",
          loglevel: "info",
          logsubtype: "",
          logIcon: `fas fa-power-off`,
          title: `Graceful shutdown requested.`,
          event: `The node application received a signal to gracefully shut down. If this was unexpected, please check the server and error logs.`,
        })
        .fetch()
        .exec(() => {
          checkTasks();
        });
    });
};
