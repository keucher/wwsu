/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/

  /*
    RESTful API
    (We use PUT instead of PATCH for wider browser compatibility)
  */

  // Analytics (passed)
  "GET /api/analytics/listeners": { action: "api/analytics/listeners/get" },
  "GET /api/analytics/showtime": { action: "api/analytics/showtime/get" },
  "GET /api/analytics/weekly": { action: "api/analytics/weekly/get" },
  "GET /api/analytics/yearly": { action: "api/analytics/yearly/get" },

  // Announcements (passed)
  "DELETE /api/announcements/:ID": { action: "api/announcements/delete" },
  "GET /api/announcements/:ID?": { action: "api/announcements/get" },
  "POST /api/announcements": { action: "api/announcements/post" },
  "PUT /api/announcements/:ID": { action: "api/announcements/put" },

  // Attendance (passed)
  "PUT /api/attendance/recalculate": { action: "api/attendance/recalculate" }, // Recalculate all records without analytics
  "GET /api/attendance/:ID?": { action: "api/attendance/get" },
  "PUT /api/attendance/:ID": { action: "api/attendance/put" },
  "PUT /api/attendance/:ID/recalculate": { action: "api/attendance/recalculate" }, // Recalculate specific record ID

  // Auth (passed)
  "GET /api/auth/admin-director": { action: "api/auth/admin-director" },
  "GET /api/auth/director": { action: "api/auth/director" },
  "GET /api/auth/dj": { action: "api/auth/dj" },
  "GET /api/auth/host": { action: "api/auth/host" },

  // Blogs (passed host)
  "DELETE /api/blogs/host/:ID": { action: "api/blogs/host/delete" },
  "GET /api/blogs/host/:ID?": { action: "api/blogs/host/get" },
  "GET /api/blogs/web/:ID": { action: "api/blogs/web/get" },
  "GET /api/blogs/web": { action: "api/blogs/web/search" },
  "POST /api/blogs/host": { action: "api/blogs/host/post" },
  "POST /api/blogs/web": { action: "api/blogs/web/post" },
  "PUT /api/blogs/host/:ID": { action: "api/blogs/host/put" },

  // Bookings
  "POST /api/bookings/director": { action: "api/bookings/director/post" },
  "POST /api/bookings/dj": { action: "api/bookings/dj/post" },

  // Calendar Clockwheels
  "GET /api/calendar/clockwheels/host": {
    action: "api/calendar/clockwheels/host/get",
  },
  "GET /api/calendar/clockwheels/web": {
    action: "api/calendar/clockwheels/web/get",
  },
  "POST /api/calendar/clockwheels/web": {
    action: "api/calendar/clockwheels/web/post",
  },
  "PUT /api/calendar/clockwheels/web/:ID": {
    action: "api/calendar/clockwheels/web/put",
  },

  // Calendar events (Passed)
  "DELETE /api/calendar/event/:ID": { action: "api/calendar/event/delete" },
  "GET /api/calendar/event/:ID?": { action: "api/calendar/event/get" },
  "POST /api/calendar/event": { action: "api/calendar/event/post" },
  "PUT /api/calendar/event/:ID/activate": {
    action: "api/calendar/event/activate",
  },
  "PUT /api/calendar/event/:ID/deactivate": {
    action: "api/calendar/event/deactivate",
  },
  "PUT /api/calendar/event/:ID": {
    action: "api/calendar/event/put",
  },

  // Calendar events-playlists (cannot test yet)
  "GET /api/calendar/events-playlists": {
    action: "api/calendar/events-playlists/get",
  },

  // Calendar schedules (cannot test yet)
  "DELETE /api/calendar/schedule/:ID": {
    action: "api/calendar/schedule/delete",
  },
  "GET /api/calendar/schedule/:ID?": { action: "api/calendar/schedule/get" },
  "POST /api/calendar/schedule": { action: "api/calendar/schedule/post" },
  "PUT /api/calendar/schedule/:ID": { action: "api/calendar/schedule/put" },

  // Calendar web / panel (cannot test yet)
  "GET /api/calendar/web": { action: "api/calendar/web/get" },
  "POST /api/calendar/web/cancel": { action: "api/calendar/web/cancel" },
  "PUT /api/calendar/web/description": {
    action: "api/calendar/web/description",
  },

  // Call (cannot test yet)
  "GET /api/call/credential": { action: "api/call/credential" },
  "POST /api/call/bad-call": { action: "api/call/bad-call" },
  "POST /api/call/finalize": { action: "api/call/finalize" },
  "POST /api/call/start/:ID?": { action: "api/call/start" },
  "POST /api/call/very-bad-call": { action: "api/call/very-bad-call" },
  "PUT /api/call/quality/:quality": { action: "api/call/quality" },

  // Categories (passed)
  "GET /api/categories": { action: "api/categories/get" },

  // Config (passed)
  "GET /api/config": { action: "api/config/get" },
  "PUT /api/config/start-of-semester": {
    action: "api/config/start-of-semester/put",
  },
  "PUT /api/config/secrets": {
    action: "api/config/secrets/put",
  },
  "PUT /api/config/skyway": {
    action: "api/config/skyway/put",
  },
  "PUT /api/config/requests": {
    action: "api/config/requests/put",
  },
  "PUT /api/config/maintenance": {
    action: "api/config/maintenance/put",
  },
  "DELETE /api/config/radiodjs/:ID": {
    action: "api/config/radiodjs/delete",
  },
  "POST /api/config/radiodjs": {
    action: "api/config/radiodjs/post",
  },
  "PUT /api/config/radiodjs/:ID": {
    action: "api/config/radiodjs/put",
  },
  "PUT /api/config/songlikes": {
    action: "api/config/songlikes/put",
  },
  "DELETE /api/config/breaks/:ID": {
    action: "api/config/breaks/delete",
  },
  "POST /api/config/breaks": {
    action: "api/config/breaks/post",
  },
  "PUT /api/config/breaks/:ID": {
    action: "api/config/breaks/put",
  },
  "PUT /api/config/max-queue": {
    action: "api/config/max-queue/put",
  },
  "PUT /api/config/filter-profanity": {
    action: "api/config/filter-profanity/put",
  },
  "PUT /api/config/sanitize": {
    action: "api/config/sanitize/put",
  },
  "PUT /api/config/discord": {
    action: "api/config/discord/put",
  },
  "PUT /api/config/websites": {
    action: "api/config/websites/put",
  },
  "PUT /api/config/shoutcast": {
    action: "api/config/shoutcast/put",
  },
  "PUT /api/config/owncast": {
    action: "api/config/owncast/put",
  },
  "PUT /api/config/status": {
    action: "api/config/status/put",
  },
  "DELETE /api/config/nws/:ID": {
    action: "api/config/nws/delete",
  },
  "POST /api/config/nws": {
    action: "api/config/nws/post",
  },
  "PUT /api/config/nws/:ID": {
    action: "api/config/nws/put",
  },
  "DELETE /api/config/sports/:ID": {
    action: "api/config/sports/delete",
  },
  "POST /api/config/sports": {
    action: "api/config/sports/post",
  },
  "PUT /api/config/sports/:ID": {
    action: "api/config/sports/put",
  },
  "PUT /api/config/break-times": {
    action: "api/config/break-times/put",
  },
  "PUT /api/config/tomorrowio": {
    action: "api/config/tomorrowio/put",
  },
  "PUT /api/config/campus-alerts": {
    action: "api/config/campus-alerts/put",
  },
  "PUT /api/config/bookings": {
    action: "api/config/bookings/put",
  },
  "PUT /api/config/scores": {
    action: "api/config/scores/put",
  },
  "PUT /api/config/onesignal": {
    action: "api/config/onesignal/put",
  },
  "DELETE /api/config/categories/:ID": {
    action: "api/config/categories/delete",
  },
  "POST /api/config/categories": {
    action: "api/config/categories/post",
  },
  "PUT /api/config/categories/:ID": {
    action: "api/config/categories/put",
  },
  "PUT /api/config/reputation": {
    action: "api/config/reputation/put",
  },

  // Delay
  "PUT /api/delay": { action: "api/delay/put" },
  "POST /api/delay/dump": { action: "api/delay/dump" },

  // Directors (passed)
  "DELETE /api/directors/:ID": { action: "api/directors/delete" },
  "GET /api/directors/:name?": { action: "api/directors/get" },
  "POST /api/directors": { action: "api/directors/post" },
  "PUT /api/directors/:ID": { action: "api/directors/put" },

  // Discipline (Passed)
  "DELETE /api/discipline/host/:ID": { action: "api/discipline/host/delete" },
  "GET /api/discipline/host": { action: "api/discipline/host/get" },
  "GET /api/discipline/web": { action: "api/discipline/web/get" },
  "POST /api/discipline/host": { action: "api/discipline/host/post" },
  "PUT /api/discipline/acknowledge/:ID": {
    action: "api/discipline/acknowledge",
  },
  "PUT /api/discipline/host/:ID": { action: "api/discipline/host/put" },

  // Display
  "POST /api/display/refresh": { action: "api/display/refresh" },

  // Djnotes (passed)
  "DELETE /api/djnotes/:ID": { action: "api/djnotes/delete" },
  "GET /api/djnotes/:dj?": { action: "api/djnotes/get" },
  "POST /api/djnotes": { action: "api/djnotes/post" },
  "PUT /api/djnotes/:ID": { action: "api/djnotes/put" },

  // DJs (passed)
  "DELETE /api/djs/host/:ID": { action: "api/djs/host/delete" },
  "GET /api/djs/host/:ID?": { action: "api/djs/host/get" },
  "GET /api/djs/host/:ID/door-code": { action: "api/djs/host/get-door-code" },
  "GET /api/djs/web": { action: "api/djs/web/get" },
  "POST /api/djs/host": { action: "api/djs/host/post" },
  "PUT /api/djs/host/:ID": { action: "api/djs/host/put" },
  "PUT /api/djs/host/:ID/activate": { action: "api/djs/host/activate" },
  "PUT /api/djs/host/:ID/deactivate": { action: "api/djs/host/deactivate" },

  // EAS (passed)
  "DELETE /api/eas/:ID": { action: "api/eas/delete" },
  "GET /api/eas": { action: "api/eas/get" },
  "POST /api/eas": { action: "api/eas/post" },
  "POST /api/eas/test": { action: "api/eas/test" },
  "PUT /api/eas/:ID": { action: "api/eas/put" },

  // Email (passed)
  "POST /api/email": { action: "api/email/post" },

  // Flags
  "POST /api/flags": { action: "api/flags/post" },

  // Hosts (passed)
  "DELETE /api/hosts/:ID": { action: "api/hosts/delete" },
  "GET /api/hosts/:host": { action: "api/hosts/get" },
  "PUT /api/hosts/:ID": { action: "api/hosts/put" },

  // Hosts Lockdown (passed)
  "POST /api/hosts/:ID/log-in/director": {
    action: "api/hosts/log-in/director",
  },
  "POST /api/hosts/:ID/log-in/dj": { action: "api/hosts/log-in/dj" },
  "PUT /api/hosts/:ID/log-out": { action: "api/hosts/log-out" },

  // Inventory (passed)
  "DELETE /api/inventory/checkout/:ID": {
    action: "api/inventory/checkout/delete",
  },
  "DELETE /api/inventory/items/:ID": { action: "api/inventory/items/delete" },
  "GET /api/inventory/items/:ID?": { action: "api/inventory/items/get" },
  "POST /api/inventory/items": { action: "api/inventory/items/post" },
  "POST /api/inventory/items/:ID/check-out": {
    action: "api/inventory/items/check-out",
  },
  "PUT /api/inventory/checkout/:ID": { action: "api/inventory/checkout/put" },
  "PUT /api/inventory/checkout/:ID/check-in": {
    action: "api/inventory/checkout/check-in",
  },
  "PUT /api/inventory/items/:ID": { action: "api/inventory/items/put" },

  // Lockdown (passed)
  "GET /api/lockdown": { action: "api/lockdown/get" },

  // Logs (passed)
  "GET /api/logs": { action: "api/logs/get" },
  "POST /api/logs": { action: "api/logs/post" },
  "PUT /api/logs/:ID": { action: "api/logs/put" },
  "PUT /api/logs/acknowledge-all": { action: "api/logs/acknowledge-all" },

  // Messages (host passed)
  "DELETE /api/messages/host/:ID": { action: "api/messages/host/delete" },
  "GET /api/messages/host": { action: "api/messages/host/get" },
  "GET /api/messages/web": { action: "api/messages/web/get" },
  "POST /api/messages/host": { action: "api/messages/host/post" },
  "POST /api/messages/web": { action: "api/messages/web/post" },

  // Meta (passed)
  "GET /api/meta": { action: "api/meta/get" },
  "GET /api/meta/update/:metaSecret": { action: "api/meta/put" },
  "PUT /api/meta/:metaSecret": { action: "api/meta/put" },

  // Recipients
  "GET /api/recipients": { action: "api/recipients/get" },
  "POST /api/recipients/display": { action: "api/recipients/display/post" },
  "POST /api/recipients/host": { action: "api/recipients/host/post" },
  "POST /api/recipients/web": { action: "api/recipients/web/post" },
  "PUT /api/recipients/host": { action: "api/recipients/host/put" },
  "PUT /api/recipients/web": { action: "api/recipients/web/put" },

  // Requests
  "GET /api/requests": { action: "api/requests/get" },
  "POST /api/requests": { action: "api/requests/post" },
  "POST /api/requests/:ID/queue": { action: "api/requests/queue" },

  // RSS
  "GET /api/rss": { action: "api/rss/get" },

  // Server (Passed)
  "POST /api/server/sails/reboot": { action: "api/server/sails/reboot" },

  // Shootout
  "GET /api/shootout": { action: "api/shootout/get" },
  "PUT /api/shootout/:name": { action: "api/shootout/put" },

  // Silence (passed)
  "PUT /api/silence/active": { action: "api/silence/active" },
  "PUT /api/silence/inactive": { action: "api/silence/inactive" },

  // Songs
  "GET /api/songs": { action: "api/songs/get" },
  "GET /api/songs/genres": { action: "api/songs/genres/get" },
  "GET /api/songs/liked": { action: "api/songs/liked/get" },
  "GET /api/songs/long": { action: "api/songs/long/get" },
  "GET /api/songs/reports": { action: "api/songs/reports/get" },
  "GET /api/songs/sorter": { action: "api/songs/sorter/get" },
  "POST /api/songs/liked/:ID?": { action: "api/songs/liked/post" },
  "POST /api/songs/liners/queue": { action: "api/songs/liners/queue" },
  "POST /api/songs/psas/queue": { action: "api/songs/psas/queue" },
  "POST /api/songs/top-adds/queue": { action: "api/songs/top-adds/queue" },
  "PUT /api/songs/long/:ID?": { action: "api/songs/long/put" },
  "PUT /api/songs/sorter": { action: "api/songs/sorter/put" },

  // Sports
  "GET /api/sports": { action: "api/sports/get" },
  "PUT /api/sports/:name": { action: "api/sports/put" },

  // State
  "POST /api/state/automation": { action: "api/state/automation" },
  "POST /api/state/break": { action: "api/state/break" },
  "POST /api/state/live": { action: "api/state/live" },
  "POST /api/state/remote": { action: "api/state/remote" },
  "POST /api/state/return": { action: "api/state/return" },
  "POST /api/state/radiodj/change": { action: "api/state/radiodj/change" },
  "POST /api/state/sports": { action: "api/state/sports" },
  "POST /api/state/remote/sports": { action: "api/state/sports-remote" },

  // Status (passed)
  "GET /api/status": { action: "api/status/get" },
  "POST /api/status": { action: "api/status/post" },
  "PUT /api/status/:name": { action: "api/status/put" },

  // Subscribers
  "DELETE /api/subscribers/directors/:device": {
    action: "api/subscribers/directors/delete",
  },
  "DELETE /api/subscribers/web/:device": {
    action: "api/subscribers/web/delete",
  },
  "GET /api/subscribers/web/:device": { action: "api/subscribers/web/get" },
  "POST /api/subscribers/directors/:device": {
    action: "api/subscribers/directors/post",
  },
  "POST /api/subscribers/web/:device": { action: "api/subscribers/web/post" },

  // Timesheet
  "DELETE /api/timesheet/:ID": { action: "api/timesheet/delete" },
  "GET /api/timesheet": { action: "api/timesheet/get" },
  "POST /api/timesheet": { action: "api/timesheet/post" },
  "PUT /api/timesheet/:ID": { action: "api/timesheet/put" },

  // Underwritings (passed)
  "DELETE /api/underwritings/:ID": { action: "api/underwritings/delete" },
  "GET /api/underwritings": { action: "api/underwritings/get" },
  "POST /api/underwritings": { action: "api/underwritings/post" },
  "PUT /api/underwritings/:ID": { action: "api/underwritings/put" },

  // Uploads (passed)
  "GET /api/uploads/:ID": function (req, res) {
    sails.models.uploads
      .findOne({ ID: req.param("ID") })
      .then((record) => {
        res.sendFile(record.path);
      })
      .catch((e) => {
        res.sendStatus(404);
      });
  },
  "DELETE /api/uploads/:ID": { action: "api/uploads/delete" },
  "POST /api/uploads": { action: "api/uploads/post" },

  // Version
  "GET /api/version/:app": { action: "api/version/get" },
  "PUT /api/version/:app": { action: "api/version/put" },

  // Weather (passed)
  "GET /api/weather": { action: "api/weather/get" },

  /*
    SAILS STUFF
  */

  "GET /csrfToken": { action: "security/grant-csrf-token" },

  /*
    WEB PAGES
  */

  "GET /": { action: "index" },
  "GET /become-member": { action: "become-member/index" },
  "GET /blog/:id/:monikor?": { action: "blog/index" },
  "GET /blogs/:category?": { action: "blogs/index" },
  "GET /bookings": { action: "bookings/index" },
  "GET /calendar": { action: "calendar/index" },
  "GET /call": { action: "call/index" },
  "GET /chat": { action: "chat/index" },
  "GET /directors": { action: "directors/index" },
  "GET /directors/panel": { action: "directors/panel/index" },
  "GET /directors/panel/calendar": { action: "directors/panel/calendar" },
  "GET /directors/panel/shootout": { action: "directors/panel/shootout" },
  "GET /directors/panel/director/:director": function (req, res) {
    return res.view("directors/director", {
      layout: "directors/layout",
      director: req.param("director"),
    });
  },
  "GET /directors/panel/timesheet": { action: "timesheet/index" },
  "GET /display/internal": { action: "display/internal" },
  "GET /display/public": { action: "display/public" },
  "GET /eas/speak": { action: "eas/speak" },
  "GET /eas/speak-new": { action: "eas/speak-new" },
  "GET /listen": { action: "listen/index" },
  "GET /members": { action: "members/index" },
  "GET /notifications": { action: "notifications/index" },
  "GET /nowplaying": { action: "nowplaying/index" },
  "GET /partner": { action: "partner/index" },
  "GET /radioshows": { action: "radioshows/index" },
  "GET /request": { action: "request/index" },
  "GET /sports": { action: "sports/index" },
  "GET /sports/basketball": { action: "sports/basketball/overlay" },
  "GET /sports/basketball/remote": { action: "sports/basketball/remote" },
  "GET /sports/football": { action: "sports/football/overlay" },
  "GET /sports/football/remote": { action: "sports/football/remote" },
  "GET /video": { action: "video/index" },
  "GET /wcrd": { action: "wcrd/index" },

  /*
      REDIRECTS
  */

  "GET /recordings": function (req, res) {
    if (sails.config.custom.basic.recordings) {
      return res.redirect(sails.config.custom.basic.recordings);
    } else {
      return res
        .status(404)
        .send("The recordings website URL has not been configured. Please contact wwsu4@wright.edu.");
    }
  },
  "GET /video/embed": function (req, res) {
    if (sails.config.custom.basic.owncastStream) {
      return res.redirect(sails.config.custom.basic.owncastStream);
    } else {
      return res
        .status(404)
        .send("The video stream server has not been configured. Please contact wwsu4@wright.edu.");
    }
  },
};
