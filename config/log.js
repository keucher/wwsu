// Custom logger via Winston in SailsJS

const { version } = require("../package");

const {
  createLogger,
  format,
  transports,
  loggers,
  exceptions,
} = require("winston");
require("winston-daily-rotate-file");
const Transport = require("winston-transport");

const { combine, timestamp, colorize, label, printf, align, json } = format;
const { SPLAT } = require("triple-beam");
const { isObject } = require("@sailshq/lodash");

// Do logging when an uncaught exception occurs.
class UncaughtExceptionTransport extends Transport {
  constructor(opts) {
    super(opts);

    this.alreadyThrown = false;
  }

  async log(err, callback) {
    setImmediate(() => {
      this.emit("logged", err);
    });

    // Are we already handling an exception? That's a problem! Bail immediately.
    if (this.alreadyThrown) {
      return callback();
    }

    this.alreadyThrown = true;

    try {
      sails.log.warn("Uncaught exception transport triggered.");

      // Log the exception in the logs system, but tolerate errors.
      await sails.models.logs
        .create({
          attendanceID: null,
          logtype: "uncaught-exception",
          loglevel: "danger",
          logsubtype: "",
          logIcon: `fas fa-server`,
          title: `An uncaught exception occurred in the sails.js server!`,
          event: `Please check the sails.js logs for more information. The application was forcefully terminated as a precaution (If running on a process manager, the app will hopefully reboot in a minute).<br />
          Error: ${err.message}`,
        })
        .fetch()
        .tolerate((errs) => {
          sails.log.error(errs);
        });

      // Send an email to directors (make it immediate since we are about to shut down sails).
      await sails.helpers.emails.queueEmergencies(
        `Uncaught exception occurred on WWSU sails.js app`,
        `Directors,<br /><br />

  An uncaught exception occurred on the WWSU sails.js server/app, causing it to terminate / reboot. Please investigate and fix this issue immediately.<br /><br />

  Additional information: ${err.message} (please check the sails.js logs for more information / stack traces).`,
        true
      );

      // Try lowering sails gracefully
      sails.lower(() => {
        callback();
      });
    } catch (e) {
      sails.log.error(e);
      callback();
    }
  }
}

function formatObject(param) {
  if (isObject(param)) {
    return JSON.stringify(param);
  }
  return param;
}

const all = format((info) => {
  const splat = info[SPLAT] || [];
  const message = formatObject(info.message);
  const error = splat.find((obj) => obj instanceof Error);
  const rest = splat.map(formatObject).join(" ");
  info.message = `${message} ${
    error ? "\n" + formatObject(error) : ``
  } ${rest}`;
  return info;
});

// Main Sails log
loggers.add("app", {
  level: "info",
  format: combine(
    all(),
    label({ label: version }),
    timestamp(),
    colorize(),
    align(),
    printf(
      (info) =>
        `${info.timestamp} [${info.label}] ${info.level}: ${formatObject(
          info.message
        )}`
    )
  ),
  transports: [
    new transports.Console(),
    new transports.DailyRotateFile({
      filename: "logs/app-out-%DATE%.log",
      datePattern: "YYYY-MM-DD-HH",
      maxFiles: "14d",
      format: json(),
      json: true,
    }),
  ],
  // Uncaught exceptions
  exceptionHandlers: [
    new transports.File({
      filename: "logs/app-exceptions.log",
      format: json(),
      maxsize: 1024 * 1024 * 5,
      maxFiles: 10,
      json: true,
    }),
    new UncaughtExceptionTransport({
      format: combine(
        all(),
        align(),
        printf((info) => `${formatObject(info.message)}`)
      ),
    }),
  ],
});

// API requests
loggers.add("api", {
  level: "http",
  format: combine(
    all(),
    label({ label: version }),
    timestamp(),
    colorize(),
    align(),
    printf(
      (info) =>
        `${info.timestamp} [${info.label}] ${info.level}: ${formatObject(
          info.message
        )}`
    )
  ),
  transports: [
    new transports.Console(),
    new transports.DailyRotateFile({
      filename: "logs/app-api-%DATE%.log",
      datePattern: "YYYY-MM-DD-HH",
      maxFiles: "14d",
      format: json(),
      json: true,
    }),
  ],
});

module.exports.log = {
  custom: loggers.get("app"), // Sails logging
  inspect: false,
  level: "silly", // WARNING! This property is ignored in Sails; you must configure log levels manually in winston above.
  api: loggers.get("api"), // API logging, separate from Sails logging
};
